import csv
import argparse
from os import listdir
from os.path import join, isdir, isfile
import ntpath
from pprint import pprint
from functools import reduce
import re
import pandas as pd
import os
from timeit import default_timer
import numpy as np
import mmap

def timer_start():
	# pass
	return default_timer()

def timer_end(start, msg):
	pass
	end = default_timer()
	print("%s: %.7f sec" % (msg, end - start))


############## Heuristics #############
def wrgSize_max175(
    l0, l1, l2, inputCacheSizeX, inputCacheSizeY, kernelCacheSize, tileDepth, df, meta_generated_files_dir, job_dirnames):
    maxx = 175
    return l0 * l1 * l2 <= maxx

def wrgSize_max275(
    l0, l1, l2, inputCacheSizeX, inputCacheSizeY, kernelCacheSize, tileDepth, df, meta_generated_files_dir, job_dirnames):
    maxx = 275
    return l0 * l1 * l2 <= maxx

def inputCacheSizeX_max4(
    l0, l1, l2, inputCacheSizeX, inputCacheSizeY, kernelCacheSize, tileDepth, df, meta_generated_files_dir, job_dirnames):
    maxx = 4
    return inputCacheSizeX <= maxx

def inputCacheSizeY_max2(
    l0, l1, l2, inputCacheSizeX, inputCacheSizeY, kernelCacheSize, tileDepth, df, meta_generated_files_dir, job_dirnames):
    maxx = 2
    return inputCacheSizeY <= maxx

def kernelCacheSize_max1(
    l0, l1, l2, inputCacheSizeX, inputCacheSizeY, kernelCacheSize, tileDepth, df, meta_generated_files_dir, job_dirnames):
    maxx = 1
    return kernelCacheSize <= maxx

def tileDepth_max4(
    l0, l1, l2, inputCacheSizeX, inputCacheSizeY, kernelCacheSize, tileDepth, df, meta_generated_files_dir, job_dirnames):
    maxx = 4
    return tileDepth <= maxx

p_kernel_name = re.compile(rb"cl::Kernel (?P<kernel_name>kernel_\d+);")

def no_mapglb_with_barrier(
    l0, l1, l2, inputCacheSizeX, inputCacheSizeY, kernelCacheSize, tileDepth, df, meta_generated_files_dir, job_dirnames):

    preserve_rows = np.ones(len(df.index), dtype=bool)
    # print(df.mapTransform)
    # print(df.mapTransform.str)
    # print(df.mapTransform.str.contains("4"))
    # print(df.mapTransform[df.mapTransform.astype("string").str.contains("4").isna()])
    for i, row in df.reset_index()[df.mapTransform.str.contains("4")].iterrows():
        # print(row)
        tune_point_files_dir = join(meta_generated_files_dir, job_dirnames[row.job_idx], str(row.layer_idx), str(row.tune_point_idx_within_job))
        
        libconv_path = join(tune_point_files_dir, "libconv.cpp")
        try:
            with open(libconv_path, 'rb', 0) as f, \
                mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as libconv_data:
                m = p_kernel_name.search(libconv_data)
                if m is None:
                    print("Could not find kernel name in " + libconv_path)
                    quit()

                kernel_path = join(tune_point_files_dir, m.group("kernel_name").decode("utf-8") + ".cl")
                try:
                    with open(kernel_path, 'rb', 0) as fk, \
                        mmap.mmap(fk.fileno(), 0, access=mmap.ACCESS_READ) as kernel_data:
                        
                        if kernel_data.find(b'barrier') != -1:
                            # Found a barrier
                            preserve_rows[i] = 0

                except EnvironmentError:
                    print("Could not open " + kernel_path)
                    quit()
        except EnvironmentError:
            print("Could not open " + libconv_path)
            quit()

    # for index, x in np.ndenumerate(preserve_rows):
    #     print(index, df.iloc[index].tune_point_idx, x)
    print("Removing " + str(len(df.index) - preserve_rows.sum()) + " out of " + str(df.mapTransform.str.contains("4").sum()) + " points with mapGlb out of " + str(len(df.index)) + " points")

    # quit()

    return preserve_rows

heuristics = {
    "wrgSize_max175": wrgSize_max175,
    "wrgSize_max275": wrgSize_max275,
    "inputCacheSizeX_max4": inputCacheSizeX_max4,
    "inputCacheSizeY_max2": inputCacheSizeY_max2,
    "kernelCacheSize_max1": kernelCacheSize_max1,
    "tileDepth_max4": tileDepth_max4,
    "no_mapglb_with_barrier": no_mapglb_with_barrier,
}

def filter_by_heuristics(df, meta_generated_files_dir, job_dirnames):
    filtered_df = df
    for heur in [heuristics[ah] for ah in args.apply_heuristics]:
        # heur(
        #     filtered_df.l0.to_numpy(), 
        #     filtered_df.l1.to_numpy(), 
        #     filtered_df.l2.to_numpy(), 
        #     filtered_df.inputCacheSizeX.to_numpy(), 
        #     filtered_df.inputCacheSizeY.to_numpy(), 
        #     filtered_df.kernelCacheSize.to_numpy(), 
        #     filtered_df.tileDepth.to_numpy(),
        #     filtered_df, meta_generated_files_dir, job_dirnames)
        filtered_df = filtered_df[ heur(
            filtered_df.l0.to_numpy(), 
            filtered_df.l1.to_numpy(), 
            filtered_df.l2.to_numpy(), 
            filtered_df.inputCacheSizeX.to_numpy(), 
            filtered_df.inputCacheSizeY.to_numpy(), 
            filtered_df.kernelCacheSize.to_numpy(), 
            filtered_df.tileDepth.to_numpy(),
            filtered_df, meta_generated_files_dir, job_dirnames) ]
    return filtered_df

parser = argparse.ArgumentParser()
parser.add_argument('--metaconfig', nargs="+", action="store", required=True)
parser.add_argument('--out_config_dir', action="store", required=True)
parser.add_argument('--timings_csv', action="store")
parser.add_argument('--parameter_csv', action="store", required=True)
parser.add_argument('--apply_heuristics', nargs="+", action="store", choices = heuristics.keys(), default=[])
parser.add_argument('--skip_explored_successful', action="store_true")

args = parser.parse_args()

assert not(args.skip_explored_successful and args.timings_csv is None)

############# Meta configs ##########
metaconfigs_files = []
metaconfig_layers = set()

for metaconfig_fname in args.metaconfig:
    metaconfigs_files.append(pd.read_csv(metaconfig_fname, sep='\s*,\s*', engine='python'))
    for _, metaconfig in metaconfigs_files[-1].iterrows():
        metaconfig_layers = metaconfig_layers.union(set(range(metaconfig.LAYER_FIRST, metaconfig.LAYER_LAST + 1, 1)))

    assert len(metaconfigs_files[-1]["NETWORK_LABEL"].unique()) == 1
assert len(set([m.iloc[0]["NETWORK_LABEL"] for m in metaconfigs_files])) == 1


############# Parameters ##########
start = timer_start()
if isfile(args.parameter_csv):
    parameters = pd.read_csv(args.parameter_csv, dtype={"mapTransform": "string"})
else:
    assert isdir(args.parameter_csv)
    files = [join(args.parameter_csv, l) for l in os.listdir(args.parameter_csv) if l.endswith(".csv") and not l.endswith("_par_mappings.csv")]
    parameters = None
    for filename in files:
        new_parameters = pd.read_csv(filename, dtype={"mapTransform": "string"})
        if parameters is None:
            parameters = new_parameters
        else:
            parameters = pd.concat([parameters, new_parameters], ignore_index=True)

def get_job_idx(row):
    if "_" in row.tune_point_idx:
        return int(row.tune_point_idx.split("_")[0])
    else:
        return -1

def get_tunepoint_idx_within_job(row):
    if "_" in row.tune_point_idx:
        return int(row.tune_point_idx.split("_")[1])
    else:
        return int(row.tune_point_idx)

parameters["job_idx"] = parameters.apply(lambda row: get_job_idx(row), axis="columns")
parameters["tune_point_idx_within_job"] = parameters.apply(lambda row: get_tunepoint_idx_within_job(row), axis="columns")
parameters = parameters.sort_values(by=["job_idx", "tune_point_idx_within_job"], ignore_index = True)
timer_end(start, "parameter_csv")

############# Timings ##########
if args.timings_csv is not None:
    start = timer_start()
    points_to_skip = pd.read_csv(args.timings_csv, usecols = ["layer", "tunepoint", "job_idx", "csvpath", "success_status"], dtype = {
            "tunepoint": 'int64',
            "csvpath": "str",
            "success_status": "bool",
            "job_idx": "int32"
        })

    # points_to_skip.drop(points_to_skip[points_to_skip["success_status"] == 1].index, inplace=True)
    # print(points_to_skip)

    points_to_skip = points_to_skip[points_to_skip["layer"].\
        isin(metaconfig_layers)].\
        drop_duplicates(subset = ["layer", "tunepoint", "csvpath", "job_idx"],  ignore_index = True)
    print(points_to_skip)

    # p_csvpath = re.compile(r".*log_.+?_(?P<generator_idx>\d+)_\d\d\.\d\d\.\d\d\d\d\_\d\d\.\d\d\.\d\d\.log")
    # p_csvpath = re.compile(r".*generated_files_" + metaconfigs_files[0].iloc[0]["NETWORK_LABEL"] + r"_(?P<job_idx>\d+)_.*")
    # if len(points_to_skip.index) > 0:
    #     # points_to_skip["job_idx"] = points_to_skip.apply(lambda row: int(p_csvpath.match(row["csvpath"]).group("job_idx")), axis="columns")
    #     timer_end(start, "points_to_skip")
    #     points_to_skip.drop_duplicates(["layer", "tunepoint", "job_idx"], inplace = True, ignore_index = True)
    #     # pd.set_option('max_colwidth', 200)
    print("Points to skip: " + str(len(points_to_skip)))


for metaconfig_i, metaconfigs in enumerate(metaconfigs_files):

    ############# Output configs ##########
    output_path = join(
        args.out_config_dir, 
        ntpath.basename(args.metaconfig[metaconfig_i]).\
            replace("meta_", "").\
            replace(".csv", "") +
            ("_" + "_".join(args.apply_heuristics) if len(args.apply_heuristics) > 0 else "") + \
            ("_unexplored_only" if args.skip_explored_successful else "") + ".csv")
    output_summary_path = output_path.replace(".csv", "_summary.csv")
    summary = {
        "network_name" : None,
        "network_label" : None, 
        "layer_min" : None,
        "layer_max" : None,
        "tunepoint_min" : None,
        "tunepoint_max" : None
    }
    with open(output_path, "w") as config_file:
        config_writer = csv.writer(config_file)
        config_writer.writerow("NETWORK_NAME,NETWORK_LABEL,LAYER_FIRST,LAYER_LAST,TUNEPOINT_FIRST,TUNEPOINT_LAST,TRIALS,FREQ,C_FILES_PATH".\
            split(","))

        for _, metaconfig in metaconfigs.iterrows():
            print("Compiling:")
            print(metaconfig)
            print("New config path: " + output_path)

            for layer in range(metaconfig.LAYER_FIRST, metaconfig.LAYER_LAST + 1, 1):
                if layer in [6, 9, 11, 12]:
                    continue

                layer_points = parameters[
                    (parameters.net_name == metaconfig.NETWORK_NAME) &
                    (parameters.net_label == metaconfig.NETWORK_LABEL) &
                    (parameters.layer_idx == layer)]
                # print("parameters types:")
                # print(parameters.dtypes)
                # print("layer_points types:")
                # print(layer_points.dtypes)
                # print("points_to_skip types:")
                # print(points_to_skip.dtypes)

                meta_generated_files_dir = join("generated_files", metaconfigs.iloc[0]["C_FILES_PATH"])
                job_dirnames = {}
                for job_idx in layer_points["job_idx"].unique():
                    p_generated_files_dirname = re.compile(r"(?P<dirname>generated_files_" + metaconfig.NETWORK_LABEL + "_" + str(job_idx) + ")")
                    amatch = None
                    for d in listdir(meta_generated_files_dir):
                        amatch = p_generated_files_dirname.match(d)
                        if amatch is not None:
                            break
                    if amatch is None:
                        print("WARNING: Cannot find generated files dir for job " + str(job_idx))
                    else:
                        job_dirnames[job_idx] = amatch.group("dirname")

                print("Total points for layer " + str(layer) + ": " + str(len(layer_points.index)))

                if args.skip_explored_successful:
                    if (args.timings_csv is not None and len(points_to_skip.index) > 0):
                        layer_points_merged = layer_points.merge(points_to_skip, how='outer', indicator=True,
                            left_on=["layer_idx", "tune_point_idx_within_job", "job_idx"], 
                            right_on=["layer", "tunepoint", "job_idx"])
                        unexplored_points = layer_points_merged[layer_points_merged["_merge"] == "left_only"]
                    else:
                        unexplored_points = layer_points

                    print("Total unexplored points for layer " + str(layer) + ": " + str(len(unexplored_points)))
                    df_to_filter = unexplored_points
                else:
                    df_to_filter = layer_points

                # df_to_filter[["tune_point_idx_diff", "continuous_seq_no"]] = np.nan
                df_to_filter.reindex(columns=df_to_filter.columns.tolist() + ["tune_point_idx_diff", "continuous_seq_no"])
                
                # Filter out jobs whose files we don't have
                df_to_filter = df_to_filter[df_to_filter["job_idx"].isin(job_dirnames.keys())]

                matching_points = filter_by_heuristics(df_to_filter, meta_generated_files_dir, job_dirnames).copy()

                print("Points matching the heuristic and having C files: " + str(len(matching_points)))
                matching_points = matching_points.iloc[ metaconfig.TUNEPOINT_GLOBAL_FIRST : metaconfig.TUNEPOINT_GLOBAL_LAST + 1 ]
                print("Selecting " + str(len(matching_points)) + " points.")
                
                matching_points["tune_point_idx_diff"] = matching_points.tune_point_idx_within_job.diff().fillna(1)

                seq_no = 0
                def get_seq_no(row):
                    global seq_no
                    if row.tune_point_idx_diff != 1:
                        seq_no += 1
                    return seq_no

                matching_points["continuous_seq_no"] = matching_points.apply(lambda row: get_seq_no(row), axis="columns")

                continuous_point_sequences = matching_points.\
                    groupby(["job_idx", "continuous_seq_no"]).\
                    agg({"tune_point_idx_within_job": ["min", "max"]}).\
                    reset_index()
                print("Found " + str(len(continuous_point_sequences)) + " continuous sequences")
                for _, config in continuous_point_sequences.iterrows():
                    config_writer.writerow([metaconfig["NETWORK_NAME"], metaconfig["NETWORK_LABEL"], 
                            layer, layer, 
                            config["tune_point_idx_within_job"]["min"], 
                            config["tune_point_idx_within_job"]["max"],
                            metaconfig["TRIALS"], metaconfig["FREQ"], 
                            join(metaconfigs.iloc[0]["C_FILES_PATH"], job_dirnames[config["job_idx"].iloc[0]])])

                point_min = continuous_point_sequences.tune_point_idx_within_job["min"].min()
                point_max = continuous_point_sequences.tune_point_idx_within_job["max"].max()
                if summary["layer_min"] is None or \
                    layer < summary["layer_min"] or \
                    (layer == summary["layer_min"] and point_min < summary["tunepoint_min"]):
                    summary["layer_min"] = layer
                    summary["tunepoint_min"] = point_min

                if summary["layer_max"] is None or \
                    layer > summary["layer_max"] or \
                    (layer == summary["layer_max"] and point_max > summary["tunepoint_max"]):
                    summary["layer_max"] = layer
                    summary["tunepoint_max"] = point_max

                if summary["network_name"] is None:
                    summary["network_name"] = metaconfig["NETWORK_NAME"]
                else:
                    assert summary["network_name"] == metaconfig["NETWORK_NAME"]
                if summary["network_label"] is None:
                    summary["network_label"] = metaconfig["NETWORK_LABEL"]
                else:
                    assert summary["network_label"] == metaconfig["NETWORK_LABEL"]

    
    with open(output_summary_path, "w") as config_summary_file:
        print("New config summary path: " + output_summary_path)
        config_summary_writer = csv.writer(config_summary_file)
        config_summary_writer.writerow("NETWORK_NAME,NETWORK_LABEL,LAYER_MIN,LAYER_MAX,TUNEPOINT_MIN,TUNEPOINT_MAX".\
            split(","))
        config_summary_writer.writerow([summary["network_name"], summary["network_label"], 
            str(summary["layer_min"]), str(summary["layer_max"]), 
            str(summary["tunepoint_min"]), str(summary["tunepoint_max"])])
