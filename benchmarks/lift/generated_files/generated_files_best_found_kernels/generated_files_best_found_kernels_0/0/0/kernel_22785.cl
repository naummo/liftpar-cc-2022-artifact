// Layer 0, tune point 0
// inputChannels -> 3, inputWidth -> 224, inputHeight -> 224,
// padFuncX -> 1, padFuncY -> 1,
// kernelWidth -> 3, kernelHeight -> 3,
// kernelStrideX -> 1, kernelStrideY -> 1,
// numKernels -> 64
// Vectorization
// "mapVectorize.213." -> 1,
// "mapVectorize.246." -> 1
// Unsliding
// 
// Parallelization and fusion
// "mapTransform.28." -> (10*Code.mapWrg() + Code.parDims(0)()),
// "mapTransform.29." -> (10*Code.mapWrg() + Code.parDims(1)()),
// "mapTransform.30." -> (Code.replaceInnerMapWithOuter()),
// "mapTransform.51." -> (10*Code.mapWrg() + Code.parDims(2)()),
// "mapTransform.52." -> (10*Code.mapLcl() + Code.parDims(2)()),
// "mapTransform.68." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.70." -> (10*Code.mapLcl() + Code.parDims(0)()),
// "mapTransform.109." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.110." -> (10*Code.mapLcl() + Code.parDims(0)())
// padOptRight -> 4, padOptBottom -> 16,
// tileWidth -> 4, tileHeight -> 16, tileDepth -> 3,
// seqWindowsPerThreadX -> 4, seqWindowsPerThreadY -> 1,
// seqKernelsPerThread -> 4,
// kernelGroupSize -> 4,
// windowTileWidth -> 1, windowTileHeight -> 1,
// inputCacheSizeX -> 4, inputCacheSizeY -> 1,
// inputCacheDepth -> 3,
// kernelCacheSize -> 4,
// localSizes(0) -> 1,
// localSizes(1) -> 3,
// localSizes(2) -> 16,
// nWrgs(0) -> 16,
// nWrgs(1) -> 855,
// nWrgs(2) -> 1
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__11077, global float* v__11078){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        for (int v_gl_id_11074 = get_global_id(2); (v_gl_id_11074 < 224); v_gl_id_11074 = (208 + v_gl_id_11074)){
            for (int v_gl_id_11075 = get_global_id(1); (v_gl_id_11075 < 224); v_gl_id_11075 = (220 + v_gl_id_11075)){
                // iteration count is exactly 1, no loop emitted
                {
                    int v_gl_id_11076 = get_global_id(0); 
                    v__11078[(v_gl_id_11076 + (64 * v_gl_id_11075) + (14336 * v_gl_id_11074))] = id((((v_gl_id_11075 < 0) || (v_gl_id_11075 >= 228)) ? 0 : (((v_gl_id_11074 < 0) || (v_gl_id_11074 >= 240)) ? 0 : v__11077[(v_gl_id_11076 + (64 * v_gl_id_11075) + (14592 * v_gl_id_11074))]))); 
                }
            }
        }
    }
}