// Layer 0, tuning point 2645
// Lambda generation timestamp: 2019-07-18 02:19:19.055 
// Kernel generation timestamp: 2019-07-23 05:54:11.087
// ND ranges of conv lambda 0: (3,25,1), (24,52900,1)
// Applied rules: 
/* Parameters: 
inputChannels =         3, inputWidthHeight =    224, kernelChannels =       64,    
kernelStride =          1, kernelWidthHeight =     3, nInputs =               1,    
nKernelsPerWrg =        8, nWindowsPerThread =     1, padFunc =               1,    
padOptTotal =           6, seqOpsPerThread =       9, tileWidthHeight =       7,    
vectorLen =             1) */

#include <bits/stdc++.h>

using namespace std;

    

#include <iostream>
#include <CL/cl2.hpp>
#include <fstream>

std::string readFile(const char *filename){

  std::ifstream in(filename, std::ios::in);

  if (in.fail())
  {
  std::cerr << "Error reading file " << filename << std::endl;
  exit(1); }

  std::string contents;
  in.seekg(0, std::ios::end);
  contents.resize(in.tellg());
  in.seekg(0, std::ios::beg);
  in.read(&contents[0], contents.size());
  in.close();
  return contents;
  }

    

int platformId = 0;
int deviceId = 0;

 std::vector<cl::Platform> allPlatforms;
 cl::Platform platform;
 std::vector<cl::Device> allDevices;
 cl::Device device;
 cl::Context context;
 cl::CommandQueue lift_queue;

      ; 
std::string kernel_string_14354;
cl::Program::Sources kernel_source_14354;
cl::Program kernel_program_14354;
cl::Kernel kernel_14354;
std::string kernel_string_14355;
cl::Program::Sources kernel_source_14355;
cl::Program kernel_program_14355;
cl::Kernel kernel_14355;
std::string kernel_string_14356;
cl::Program::Sources kernel_source_14356;
cl::Program kernel_program_14356;
cl::Kernel kernel_14356;
std::chrono::milliseconds cpu_clock_start_14356;
std::chrono::milliseconds cpu_clock_end_14356;
cl::Event event_14356;
std::chrono::milliseconds cpu_clock_start_14355;
std::chrono::milliseconds cpu_clock_end_14355;
cl::Event event_14355;
; 
; 
; 
; 
; 
; 
std::chrono::milliseconds cpu_clock_start_14354;
std::chrono::milliseconds cpu_clock_end_14354;
cl::Event event_14354;
; 
; 
; 
; 
; 
; 
void lift_init(){
    
	cl::Platform::get(&allPlatforms);
 if (allPlatforms.size() == 0) {
 std::cerr << " No platforms found. Check OpenCL installation!" << std::endl;
 exit(1);
 }

 platform = allPlatforms[platformId];

 platform.getDevices(CL_DEVICE_TYPE_ALL, &allDevices);
 if (allDevices.size() == 0) {
 std::cerr << " No devices found. Check OpenCL installation!" << std::endl;
 exit(1);
 }

 device = allDevices[deviceId];

 std::cerr << "Using platform: " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl;
 std::cerr << "Using device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;

 // create context
 cl::Context tmp_context({ device });
 context = std::move(tmp_context);

 // create queue
 cl::CommandQueue tmp_queue(context, device, CL_QUEUE_PROFILING_ENABLE);
 lift_queue = std::move(tmp_queue);
      ; 
    kernel_string_14354 = readFile("kernel_14354.cl"); 
    kernel_source_14354 = cl::Program::Sources(1, {kernel_string_14354.c_str(), kernel_string_14354.length()}); 
    kernel_program_14354 = cl::Program(context, kernel_source_14354); 
    if ((kernel_program_14354.build({ device }) != CL_SUCCESS)){
        std::cerr << "kernel build error" << std::endl; 
        char* log; 
        size_t logsize; 
        assert((clGetProgramBuildInfo(kernel_program_14354.get(), device.get(), CL_PROGRAM_BUILD_LOG, 0, NULL, &logsize) == CL_SUCCESS)); 
        log = (char*)malloc(sizeof(char) * logsize); 
        assert((clGetProgramBuildInfo(kernel_program_14354.get(), device.get(), CL_PROGRAM_BUILD_LOG, logsize, log, NULL) == CL_SUCCESS)); 
        std::cout << log << std::endl; 
        exit(1); 
    }
    kernel_14354 = cl::Kernel(kernel_program_14354, "KERNEL"); 
    kernel_string_14355 = readFile("kernel_14355.cl"); 
    kernel_source_14355 = cl::Program::Sources(1, {kernel_string_14355.c_str(), kernel_string_14355.length()}); 
    kernel_program_14355 = cl::Program(context, kernel_source_14355); 
    if ((kernel_program_14355.build({ device }) != CL_SUCCESS)){
        std::cerr << "kernel build error" << std::endl; 
        char* log; 
        size_t logsize; 
        assert((clGetProgramBuildInfo(kernel_program_14355.get(), device.get(), CL_PROGRAM_BUILD_LOG, 0, NULL, &logsize) == CL_SUCCESS)); 
        log = (char*)malloc(sizeof(char) * logsize); 
        assert((clGetProgramBuildInfo(kernel_program_14355.get(), device.get(), CL_PROGRAM_BUILD_LOG, logsize, log, NULL) == CL_SUCCESS)); 
        std::cout << log << std::endl; 
        exit(1); 
    }
    kernel_14355 = cl::Kernel(kernel_program_14355, "KERNEL"); 
    kernel_string_14356 = readFile("kernel_14356.cl"); 
    kernel_source_14356 = cl::Program::Sources(1, {kernel_string_14356.c_str(), kernel_string_14356.length()}); 
    kernel_program_14356 = cl::Program(context, kernel_source_14356); 
    if ((kernel_program_14356.build({ device }) != CL_SUCCESS)){
        std::cerr << "kernel build error" << std::endl; 
        char* log; 
        size_t logsize; 
        assert((clGetProgramBuildInfo(kernel_program_14356.get(), device.get(), CL_PROGRAM_BUILD_LOG, 0, NULL, &logsize) == CL_SUCCESS)); 
        log = (char*)malloc(sizeof(char) * logsize); 
        assert((clGetProgramBuildInfo(kernel_program_14356.get(), device.get(), CL_PROGRAM_BUILD_LOG, logsize, log, NULL) == CL_SUCCESS)); 
        std::cout << log << std::endl; 
        exit(1); 
    }
    kernel_14356 = cl::Kernel(kernel_program_14356, "KERNEL"); 
}

double cpu_time_in_ms( std::chrono::milliseconds start, std::chrono::milliseconds finish ){
 return (finish - start).count();
}

double gpu_time_in_ms( cl::Event event ){

  cl_ulong start;
  cl_ulong end;
  cl_int err;

  event.wait();

  err = clGetEventProfilingInfo(event(), CL_PROFILING_COMMAND_START,
                                sizeof(start), &start, NULL);
  assert(err == CL_SUCCESS);

  err = clGetEventProfilingInfo(event(), CL_PROFILING_COMMAND_END,
                                sizeof(end), &end, NULL);
  assert(err == CL_SUCCESS);

  return ((double)(end - start)) * 1.0e-6;
}

double diff_percent(double lhs, double rhs) {
  if(std::min(lhs,rhs)==0)
    return -1;
  else
    return std::abs(lhs-rhs)/std::min(lhs,rhs);
}

          ; 
void print_clock(){
    std::cout<<"func_name, cpu_time_ms, gpu_time_ms, diff_percentage"<<std::endl; 
    {
        double cpu_time_ms = cpu_time_in_ms(cpu_clock_start_14356, cpu_clock_end_14356);
        double gpu_time_ms = gpu_time_in_ms(event_14356);
        double diff_pc = diff_percent(cpu_time_ms, gpu_time_ms);
        std::cout<<"OclFunCall_14356"<<", "<<cpu_time_ms<<", "<<gpu_time_ms<<", "<<diff_pc<<std::endl; 
    }
    {
        double cpu_time_ms = cpu_time_in_ms(cpu_clock_start_14355, cpu_clock_end_14355);
        double gpu_time_ms = gpu_time_in_ms(event_14355);
        double diff_pc = diff_percent(cpu_time_ms, gpu_time_ms);
        std::cout<<"OclFunCall_14355"<<", "<<cpu_time_ms<<", "<<gpu_time_ms<<", "<<diff_pc<<std::endl; 
    }
    {
        double cpu_time_ms = cpu_time_in_ms(cpu_clock_start_14354, cpu_clock_end_14354);
        double gpu_time_ms = gpu_time_in_ms(event_14354);
        double diff_pc = diff_percent(cpu_time_ms, gpu_time_ms);
        std::cout<<"OclFunCall_14354"<<", "<<cpu_time_ms<<", "<<gpu_time_ms<<", "<<diff_pc<<std::endl; 
    }
}
void post_execute(){
    print_clock(); 
}

namespace lift {; 

void execute(float * v_initial_param_14292_4025, float * v_initial_param_14293_4026, float * v_initial_param_14294_4027, float * & v_user_func_14357_4034){
    // Allocate memory for output pointers
    cl::Buffer v_user_func_14345_4029(context, CL_MEM_READ_WRITE, (64 * sizeof(float)));
    cl::Buffer v_user_func_14355_4032(context, CL_MEM_READ_WRITE, (3385600 * sizeof(float)));
    cl::Buffer v_user_func_14343_4028(context, CL_MEM_READ_WRITE, (1728 * sizeof(float)));
    cl::Buffer v_user_func_14356_4033(context, CL_MEM_READ_WRITE, (3211264 * sizeof(float)));
    cl::Buffer v_user_func_14354_4031(context, CL_MEM_READ_WRITE, (161472 * sizeof(float)));
    cl::Buffer v_user_func_14353_4030(context, CL_MEM_READ_WRITE, (150528 * sizeof(float)));
    v_user_func_14357_4034 = reinterpret_cast<float *>(malloc((3211264 * sizeof(float)))); 
    ; 
    lift_queue.enqueueWriteBuffer(v_user_func_14343_4028, CL_TRUE, 0, (1728 * sizeof(float)), v_initial_param_14292_4025, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    ; 
    lift_queue.enqueueWriteBuffer(v_user_func_14345_4029, CL_TRUE, 0, (64 * sizeof(float)), v_initial_param_14293_4026, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    ; 
    lift_queue.enqueueWriteBuffer(v_user_func_14353_4030, CL_TRUE, 0, (150528 * sizeof(float)), v_initial_param_14294_4027, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    kernel_14354.setArg(0, v_user_func_14353_4030); 
    kernel_14354.setArg(1, v_user_func_14354_4031); 
    cpu_clock_start_14354 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    lift_queue.enqueueNDRangeKernel(kernel_14354, cl::NullRange, cl::NDRange(3,232,232), cl::NDRange(1,1,1), NULL, (&event_14354)); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    cpu_clock_end_14354 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    kernel_14355.setArg(0, v_user_func_14343_4028); 
    kernel_14355.setArg(1, v_user_func_14345_4029); 
    kernel_14355.setArg(2, v_user_func_14354_4031); 
    kernel_14355.setArg(3, v_user_func_14355_4032); 
    cpu_clock_start_14355 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    lift_queue.enqueueNDRangeKernel(kernel_14355, cl::NullRange, cl::NDRange(24,52900,1), cl::NDRange(3,25,1), NULL, (&event_14355)); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    cpu_clock_end_14355 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    kernel_14356.setArg(0, v_user_func_14355_4032); 
    kernel_14356.setArg(1, v_user_func_14356_4033); 
    cpu_clock_start_14356 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    lift_queue.enqueueNDRangeKernel(kernel_14356, cl::NullRange, cl::NDRange(224,14336,1), cl::NDRange(1,1,1), NULL, (&event_14356)); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    cpu_clock_end_14356 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    ; 
    lift_queue.enqueueReadBuffer(v_user_func_14356_4033, CL_TRUE, 0, (3211264 * sizeof(float)), v_user_func_14357_4034, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    post_execute(); 
}
}; 