struct LayerConfig {
    int layer_id;
    int n_inputs;
    int input_WH;
    int in_channels;

    int input_pad;

    int kernel_WH;
    int kernel_stride;
    int out_channels;
  };
  
  
  const LayerConfig layerConfig = {

  .layer_id=0,
  .n_inputs=1,
  .input_WH=224,
  .in_channels=3,
  .input_pad=1,
  .kernel_WH=3,
  .kernel_stride=1,
  .out_channels=64

  };
