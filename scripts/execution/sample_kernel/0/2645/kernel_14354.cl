float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__4023, global float* v__4024){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        // iteration count is exactly 1, no loop emitted
        {
            int v_gl_id_4020 = get_global_id(2); 
            // iteration count is exactly 1, no loop emitted
            {
                int v_gl_id_4021 = get_global_id(1); 
                // iteration count is exactly 1, no loop emitted
                {
                    int v_gl_id_4022 = get_global_id(0); 
                    v__4024[(v_gl_id_4022 + (3 * v_gl_id_4021) + (696 * v_gl_id_4020))] = id(((((-1 + v_gl_id_4021) < 0) || ((-1 + v_gl_id_4021) >= 224)) ? 0 : ((((-1 + v_gl_id_4020) < 0) || ((-1 + v_gl_id_4020) >= 224)) ? 0 : v__4023[(-675 + v_gl_id_4022 + (3 * v_gl_id_4021) + (672 * v_gl_id_4020))]))); 
                }
            }
        }
    }
}