// Layer 0, tune point 0
// inputChannels -> 3, inputWidth -> 224, inputHeight -> 224,
// padFuncX -> 1, padFuncY -> 1,
// kernelWidth -> 3, kernelHeight -> 3,
// kernelStrideX -> 1, kernelStrideY -> 1,
// numKernels -> 64
// Vectorization
// "mapVectorize.213." -> 1,
// "mapVectorize.246." -> 1
// Unsliding
// 
// Parallelization and fusion
// "mapTransform.28." -> (10*Code.mapWrg() + Code.parDims(0)()),
// "mapTransform.29." -> (10*Code.mapWrg() + Code.parDims(1)()),
// "mapTransform.30." -> (Code.replaceInnerMapWithOuter()),
// "mapTransform.51." -> (10*Code.mapWrg() + Code.parDims(2)()),
// "mapTransform.52." -> (10*Code.mapLcl() + Code.parDims(2)()),
// "mapTransform.68." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.70." -> (10*Code.mapLcl() + Code.parDims(0)()),
// "mapTransform.109." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.110." -> (10*Code.mapLcl() + Code.parDims(0)())
// padOptRight -> 4, padOptBottom -> 16,
// tileWidth -> 4, tileHeight -> 16, tileDepth -> 3,
// seqWindowsPerThreadX -> 4, seqWindowsPerThreadY -> 1,
// seqKernelsPerThread -> 4,
// kernelGroupSize -> 4,
// windowTileWidth -> 1, windowTileHeight -> 1,
// inputCacheSizeX -> 4, inputCacheSizeY -> 1,
// inputCacheDepth -> 3,
// kernelCacheSize -> 4,
// localSizes(0) -> 1,
// localSizes(1) -> 3,
// localSizes(2) -> 16,
// nWrgs(0) -> 16,
// nWrgs(1) -> 855,
// nWrgs(2) -> 1
// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

float id(float x){
    {
        { return x; }; 
    }
}
float4 id4(float4 x){
    {
        { return x; }; 
    }
}
float add(float x, float y){
    {
        { return x+y; }; 
    }
}
float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
kernel __attribute((reqd_work_group_size(1,3,16)))
void KERNEL(const global float* restrict v__11449, const global float* restrict v__11450, global float* v__11477){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        local float v__11471[768];
        // Typed Value memory
        float v__10607; 
        float v__10616; 
        float v__10628; 
        // Private Memory
        float v__11453_0;
        float v__11453_1;
        float v__11453_2;
        float v__11453_3;
        float v__11453_4;
        float v__11453_5;
        float v__11453_6;
        float v__11453_7;
        float v__11453_8;
        float v__11453_9;
        float v__11453_10;
        float v__11453_11;
        float v__11453_12;
        float v__11453_13;
        float v__11453_14;
        float v__11453_15;
        float v__11457_0;
        float v__11457_1;
        float v__11457_2;
        float v__11457_3;
        float v__11457_4;
        float v__11457_5;
        float v__11457_6;
        float v__11457_7;
        float v__11457_8;
        float v__11457_9;
        float v__11457_10;
        float v__11457_11;
        float v__11460_0;
        float v__11460_1;
        float v__11460_2;
        float v__11460_3;
        float v__11460_4;
        float v__11460_5;
        float v__11460_6;
        float v__11460_7;
        float v__11460_8;
        float v__11460_9;
        float v__11460_10;
        float v__11460_11;
        float v__11461_0;
        float v__11461_1;
        float v__11461_2;
        float v__11461_3;
        float v__11461_4;
        float v__11461_5;
        float v__11461_6;
        float v__11461_7;
        float v__11461_8;
        float v__11461_9;
        float v__11461_10;
        float v__11461_11;
        float v__11461_12;
        float v__11461_13;
        float v__11461_14;
        float v__11461_15;
        float v__11465_0;
        float v__11465_1;
        float v__11465_2;
        float v__11465_3;
        float v__11465_4;
        float v__11465_5;
        float v__11465_6;
        float v__11465_7;
        float v__11465_8;
        float v__11465_9;
        float v__11465_10;
        float v__11465_11;
        float v__11465_12;
        float v__11465_13;
        float v__11465_14;
        float v__11465_15;
        float v__11472_0;
        // iteration count is exactly 1, no loop emitted
        {
            int v_wg_id_11388 = get_group_id(0); 
            // iteration count is exactly 1, no loop emitted
            {
                int v_wg_id_11389 = get_group_id(1); 
                // iteration count is exactly 1, no loop emitted
                {
                    int v_wg_id_11390 = get_group_id(2); 
                    // iteration count is exactly 1, no loop emitted
                    {
                        int v_l_id_11391 = get_local_id(2); 
                        // map_seq
                        // iteration count is exactly 1, no loop emitted
                        {
                            int v_i_11392 = 0; 
                            // iteration count is exactly 1, no loop emitted
                            {
                                int v_l_id_11393 = get_local_id(1); 
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_l_id_11394 = get_local_id(0); 
                                    float v_tmp_11478 = 0.0f; 
                                    v__10607 = v_tmp_11478; 
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11453_0 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11453_1 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11453_2 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11453_3 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11453_4 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11453_5 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11453_6 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11453_7 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11453_8 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11453_9 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11453_10 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11453_11 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11453_12 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11453_13 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11453_14 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11453_15 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // reduce_seq
                                    for (int v_i_11399 = 0; (v_i_11399 < 3); v_i_11399 = (1 + v_i_11399)){
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11457_0 = id(v__11450[(3 * (v_i_11399 + (3 * v_l_id_11393) + (9 * v_l_id_11394) + (36 * v_wg_id_11388) + (36 * v_wg_id_11390)))]); 
                                        v__11457_1 = id(v__11450[(1 + (3 * v_i_11399) + (9 * v_l_id_11393) + (27 * v_l_id_11394) + (108 * v_wg_id_11388) + (108 * v_wg_id_11390))]); 
                                        v__11457_2 = id(v__11450[(2 + (3 * v_i_11399) + (9 * v_l_id_11393) + (27 * v_l_id_11394) + (108 * v_wg_id_11388) + (108 * v_wg_id_11390))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11457_3 = id(v__11450[(3 * (9 + v_i_11399 + (3 * v_l_id_11393) + (9 * v_l_id_11394) + (36 * v_wg_id_11388) + (36 * v_wg_id_11390)))]); 
                                        v__11457_4 = id(v__11450[(28 + (3 * v_i_11399) + (9 * v_l_id_11393) + (27 * v_l_id_11394) + (108 * v_wg_id_11388) + (108 * v_wg_id_11390))]); 
                                        v__11457_5 = id(v__11450[(29 + (3 * v_i_11399) + (9 * v_l_id_11393) + (27 * v_l_id_11394) + (108 * v_wg_id_11388) + (108 * v_wg_id_11390))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11457_6 = id(v__11450[(3 * (18 + v_i_11399 + (3 * v_l_id_11393) + (9 * v_l_id_11394) + (36 * v_wg_id_11388) + (36 * v_wg_id_11390)))]); 
                                        v__11457_7 = id(v__11450[(55 + (3 * v_i_11399) + (9 * v_l_id_11393) + (27 * v_l_id_11394) + (108 * v_wg_id_11388) + (108 * v_wg_id_11390))]); 
                                        v__11457_8 = id(v__11450[(56 + (3 * v_i_11399) + (9 * v_l_id_11393) + (27 * v_l_id_11394) + (108 * v_wg_id_11388) + (108 * v_wg_id_11390))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11457_9 = id(v__11450[(3 * (27 + v_i_11399 + (3 * v_l_id_11393) + (9 * v_l_id_11394) + (36 * v_wg_id_11388) + (36 * v_wg_id_11390)))]); 
                                        v__11457_10 = id(v__11450[(82 + (3 * v_i_11399) + (9 * v_l_id_11393) + (27 * v_l_id_11394) + (108 * v_wg_id_11388) + (108 * v_wg_id_11390))]); 
                                        v__11457_11 = id(v__11450[(83 + (3 * v_i_11399) + (9 * v_l_id_11393) + (27 * v_l_id_11394) + (108 * v_wg_id_11388) + (108 * v_wg_id_11390))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11460_0 = id(v__11449[(3 * (v_i_11399 + (4 * (v_wg_id_11389 % 57)) + (230 * v_l_id_11391) + (230 * v_l_id_11393) + (690 * v_l_id_11394) + (3680 * (v_wg_id_11389 / 57))))]); 
                                        v__11460_1 = id(v__11449[(1 + (3 * v_i_11399) + (12 * (v_wg_id_11389 % 57)) + (690 * v_l_id_11391) + (690 * v_l_id_11393) + (2070 * v_l_id_11394) + (11040 * (v_wg_id_11389 / 57)))]); 
                                        v__11460_2 = id(v__11449[(2 + (3 * v_i_11399) + (12 * (v_wg_id_11389 % 57)) + (690 * v_l_id_11391) + (690 * v_l_id_11393) + (2070 * v_l_id_11394) + (11040 * (v_wg_id_11389 / 57)))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11460_3 = id(v__11449[(3 * (1 + v_i_11399 + (4 * (v_wg_id_11389 % 57)) + (230 * v_l_id_11391) + (230 * v_l_id_11393) + (690 * v_l_id_11394) + (3680 * (v_wg_id_11389 / 57))))]); 
                                        v__11460_4 = id(v__11449[(4 + (3 * v_i_11399) + (12 * (v_wg_id_11389 % 57)) + (690 * v_l_id_11391) + (690 * v_l_id_11393) + (2070 * v_l_id_11394) + (11040 * (v_wg_id_11389 / 57)))]); 
                                        v__11460_5 = id(v__11449[(5 + (3 * v_i_11399) + (12 * (v_wg_id_11389 % 57)) + (690 * v_l_id_11391) + (690 * v_l_id_11393) + (2070 * v_l_id_11394) + (11040 * (v_wg_id_11389 / 57)))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11460_6 = id(v__11449[(3 * (2 + v_i_11399 + (4 * (v_wg_id_11389 % 57)) + (230 * v_l_id_11391) + (230 * v_l_id_11393) + (690 * v_l_id_11394) + (3680 * (v_wg_id_11389 / 57))))]); 
                                        v__11460_7 = id(v__11449[(7 + (3 * v_i_11399) + (12 * (v_wg_id_11389 % 57)) + (690 * v_l_id_11391) + (690 * v_l_id_11393) + (2070 * v_l_id_11394) + (11040 * (v_wg_id_11389 / 57)))]); 
                                        v__11460_8 = id(v__11449[(8 + (3 * v_i_11399) + (12 * (v_wg_id_11389 % 57)) + (690 * v_l_id_11391) + (690 * v_l_id_11393) + (2070 * v_l_id_11394) + (11040 * (v_wg_id_11389 / 57)))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11460_9 = id(v__11449[(3 * (3 + v_i_11399 + (4 * (v_wg_id_11389 % 57)) + (230 * v_l_id_11391) + (230 * v_l_id_11393) + (690 * v_l_id_11394) + (3680 * (v_wg_id_11389 / 57))))]); 
                                        v__11460_10 = id(v__11449[(10 + (3 * v_i_11399) + (12 * (v_wg_id_11389 % 57)) + (690 * v_l_id_11391) + (690 * v_l_id_11393) + (2070 * v_l_id_11394) + (11040 * (v_wg_id_11389 / 57)))]); 
                                        v__11460_11 = id(v__11449[(11 + (3 * v_i_11399) + (12 * (v_wg_id_11389 % 57)) + (690 * v_l_id_11391) + (690 * v_l_id_11393) + (2070 * v_l_id_11394) + (11040 * (v_wg_id_11389 / 57)))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11479 = 0.0f; 
                                        v__10616 = v_tmp_11479; 
                                        v__11461_0 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_0 = mult(v__11460_0, v__11457_0); 
                                        v__11461_0 = add(v__11461_0, v__11465_0); 
                                        v__11465_0 = mult(v__11460_1, v__11457_1); 
                                        v__11461_0 = add(v__11461_0, v__11465_0); 
                                        v__11465_0 = mult(v__11460_2, v__11457_2); 
                                        v__11461_0 = add(v__11461_0, v__11465_0); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11480 = 0.0f; 
                                        v__10616 = v_tmp_11480; 
                                        v__11461_1 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_1 = mult(v__11460_3, v__11457_0); 
                                        v__11461_1 = add(v__11461_1, v__11465_1); 
                                        v__11465_1 = mult(v__11460_4, v__11457_1); 
                                        v__11461_1 = add(v__11461_1, v__11465_1); 
                                        v__11465_1 = mult(v__11460_5, v__11457_2); 
                                        v__11461_1 = add(v__11461_1, v__11465_1); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11481 = 0.0f; 
                                        v__10616 = v_tmp_11481; 
                                        v__11461_2 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_2 = mult(v__11460_6, v__11457_0); 
                                        v__11461_2 = add(v__11461_2, v__11465_2); 
                                        v__11465_2 = mult(v__11460_7, v__11457_1); 
                                        v__11461_2 = add(v__11461_2, v__11465_2); 
                                        v__11465_2 = mult(v__11460_8, v__11457_2); 
                                        v__11461_2 = add(v__11461_2, v__11465_2); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11482 = 0.0f; 
                                        v__10616 = v_tmp_11482; 
                                        v__11461_3 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_3 = mult(v__11460_9, v__11457_0); 
                                        v__11461_3 = add(v__11461_3, v__11465_3); 
                                        v__11465_3 = mult(v__11460_10, v__11457_1); 
                                        v__11461_3 = add(v__11461_3, v__11465_3); 
                                        v__11465_3 = mult(v__11460_11, v__11457_2); 
                                        v__11461_3 = add(v__11461_3, v__11465_3); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11483 = 0.0f; 
                                        v__10616 = v_tmp_11483; 
                                        v__11461_4 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_4 = mult(v__11460_0, v__11457_3); 
                                        v__11461_4 = add(v__11461_4, v__11465_4); 
                                        v__11465_4 = mult(v__11460_1, v__11457_4); 
                                        v__11461_4 = add(v__11461_4, v__11465_4); 
                                        v__11465_4 = mult(v__11460_2, v__11457_5); 
                                        v__11461_4 = add(v__11461_4, v__11465_4); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11484 = 0.0f; 
                                        v__10616 = v_tmp_11484; 
                                        v__11461_5 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_5 = mult(v__11460_3, v__11457_3); 
                                        v__11461_5 = add(v__11461_5, v__11465_5); 
                                        v__11465_5 = mult(v__11460_4, v__11457_4); 
                                        v__11461_5 = add(v__11461_5, v__11465_5); 
                                        v__11465_5 = mult(v__11460_5, v__11457_5); 
                                        v__11461_5 = add(v__11461_5, v__11465_5); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11485 = 0.0f; 
                                        v__10616 = v_tmp_11485; 
                                        v__11461_6 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_6 = mult(v__11460_6, v__11457_3); 
                                        v__11461_6 = add(v__11461_6, v__11465_6); 
                                        v__11465_6 = mult(v__11460_7, v__11457_4); 
                                        v__11461_6 = add(v__11461_6, v__11465_6); 
                                        v__11465_6 = mult(v__11460_8, v__11457_5); 
                                        v__11461_6 = add(v__11461_6, v__11465_6); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11486 = 0.0f; 
                                        v__10616 = v_tmp_11486; 
                                        v__11461_7 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_7 = mult(v__11460_9, v__11457_3); 
                                        v__11461_7 = add(v__11461_7, v__11465_7); 
                                        v__11465_7 = mult(v__11460_10, v__11457_4); 
                                        v__11461_7 = add(v__11461_7, v__11465_7); 
                                        v__11465_7 = mult(v__11460_11, v__11457_5); 
                                        v__11461_7 = add(v__11461_7, v__11465_7); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11487 = 0.0f; 
                                        v__10616 = v_tmp_11487; 
                                        v__11461_8 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_8 = mult(v__11460_0, v__11457_6); 
                                        v__11461_8 = add(v__11461_8, v__11465_8); 
                                        v__11465_8 = mult(v__11460_1, v__11457_7); 
                                        v__11461_8 = add(v__11461_8, v__11465_8); 
                                        v__11465_8 = mult(v__11460_2, v__11457_8); 
                                        v__11461_8 = add(v__11461_8, v__11465_8); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11488 = 0.0f; 
                                        v__10616 = v_tmp_11488; 
                                        v__11461_9 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_9 = mult(v__11460_3, v__11457_6); 
                                        v__11461_9 = add(v__11461_9, v__11465_9); 
                                        v__11465_9 = mult(v__11460_4, v__11457_7); 
                                        v__11461_9 = add(v__11461_9, v__11465_9); 
                                        v__11465_9 = mult(v__11460_5, v__11457_8); 
                                        v__11461_9 = add(v__11461_9, v__11465_9); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11489 = 0.0f; 
                                        v__10616 = v_tmp_11489; 
                                        v__11461_10 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_10 = mult(v__11460_6, v__11457_6); 
                                        v__11461_10 = add(v__11461_10, v__11465_10); 
                                        v__11465_10 = mult(v__11460_7, v__11457_7); 
                                        v__11461_10 = add(v__11461_10, v__11465_10); 
                                        v__11465_10 = mult(v__11460_8, v__11457_8); 
                                        v__11461_10 = add(v__11461_10, v__11465_10); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11490 = 0.0f; 
                                        v__10616 = v_tmp_11490; 
                                        v__11461_11 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_11 = mult(v__11460_9, v__11457_6); 
                                        v__11461_11 = add(v__11461_11, v__11465_11); 
                                        v__11465_11 = mult(v__11460_10, v__11457_7); 
                                        v__11461_11 = add(v__11461_11, v__11465_11); 
                                        v__11465_11 = mult(v__11460_11, v__11457_8); 
                                        v__11461_11 = add(v__11461_11, v__11465_11); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11491 = 0.0f; 
                                        v__10616 = v_tmp_11491; 
                                        v__11461_12 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_12 = mult(v__11460_0, v__11457_9); 
                                        v__11461_12 = add(v__11461_12, v__11465_12); 
                                        v__11465_12 = mult(v__11460_1, v__11457_10); 
                                        v__11461_12 = add(v__11461_12, v__11465_12); 
                                        v__11465_12 = mult(v__11460_2, v__11457_11); 
                                        v__11461_12 = add(v__11461_12, v__11465_12); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11492 = 0.0f; 
                                        v__10616 = v_tmp_11492; 
                                        v__11461_13 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_13 = mult(v__11460_3, v__11457_9); 
                                        v__11461_13 = add(v__11461_13, v__11465_13); 
                                        v__11465_13 = mult(v__11460_4, v__11457_10); 
                                        v__11461_13 = add(v__11461_13, v__11465_13); 
                                        v__11465_13 = mult(v__11460_5, v__11457_11); 
                                        v__11461_13 = add(v__11461_13, v__11465_13); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11493 = 0.0f; 
                                        v__10616 = v_tmp_11493; 
                                        v__11461_14 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_14 = mult(v__11460_6, v__11457_9); 
                                        v__11461_14 = add(v__11461_14, v__11465_14); 
                                        v__11465_14 = mult(v__11460_7, v__11457_10); 
                                        v__11461_14 = add(v__11461_14, v__11465_14); 
                                        v__11465_14 = mult(v__11460_8, v__11457_11); 
                                        v__11461_14 = add(v__11461_14, v__11465_14); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11494 = 0.0f; 
                                        v__10616 = v_tmp_11494; 
                                        v__11461_15 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11465_15 = mult(v__11460_9, v__11457_9); 
                                        v__11461_15 = add(v__11461_15, v__11465_15); 
                                        v__11465_15 = mult(v__11460_10, v__11457_10); 
                                        v__11461_15 = add(v__11461_15, v__11465_15); 
                                        v__11465_15 = mult(v__11460_11, v__11457_11); 
                                        v__11461_15 = add(v__11461_15, v__11465_15); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11453_0 = add(v__11453_0, v__11461_0); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11453_1 = add(v__11453_1, v__11461_1); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11453_2 = add(v__11453_2, v__11461_2); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11453_3 = add(v__11453_3, v__11461_3); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11453_4 = add(v__11453_4, v__11461_4); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11453_5 = add(v__11453_5, v__11461_5); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11453_6 = add(v__11453_6, v__11461_6); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11453_7 = add(v__11453_7, v__11461_7); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11453_8 = add(v__11453_8, v__11461_8); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11453_9 = add(v__11453_9, v__11461_9); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11453_10 = add(v__11453_10, v__11461_10); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11453_11 = add(v__11453_11, v__11461_11); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11453_12 = add(v__11453_12, v__11461_12); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11453_13 = add(v__11453_13, v__11461_13); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11453_14 = add(v__11453_14, v__11461_14); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11453_15 = add(v__11453_15, v__11461_15); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                    // end reduce_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11471[(16 * (v_l_id_11393 + v_l_id_11394 + (3 * v_l_id_11391)))] = id(v__11453_0); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11471[(1 + (16 * v_l_id_11393) + (16 * v_l_id_11394) + (48 * v_l_id_11391))] = id(v__11453_1); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11471[(2 * (1 + (8 * v_l_id_11393) + (8 * v_l_id_11394) + (24 * v_l_id_11391)))] = id(v__11453_2); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11471[(3 + (16 * v_l_id_11393) + (16 * v_l_id_11394) + (48 * v_l_id_11391))] = id(v__11453_3); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11471[(4 * (1 + (4 * v_l_id_11393) + (4 * v_l_id_11394) + (12 * v_l_id_11391)))] = id(v__11453_4); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11471[(5 + (16 * v_l_id_11393) + (16 * v_l_id_11394) + (48 * v_l_id_11391))] = id(v__11453_5); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11471[(2 * (3 + (8 * v_l_id_11393) + (8 * v_l_id_11394) + (24 * v_l_id_11391)))] = id(v__11453_6); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11471[(7 + (16 * v_l_id_11393) + (16 * v_l_id_11394) + (48 * v_l_id_11391))] = id(v__11453_7); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11471[(8 * (1 + (2 * v_l_id_11393) + (2 * v_l_id_11394) + (6 * v_l_id_11391)))] = id(v__11453_8); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11471[(9 + (16 * v_l_id_11393) + (16 * v_l_id_11394) + (48 * v_l_id_11391))] = id(v__11453_9); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11471[(2 * (5 + (8 * v_l_id_11393) + (8 * v_l_id_11394) + (24 * v_l_id_11391)))] = id(v__11453_10); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11471[(11 + (16 * v_l_id_11393) + (16 * v_l_id_11394) + (48 * v_l_id_11391))] = id(v__11453_11); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11471[(4 * (3 + (4 * v_l_id_11393) + (4 * v_l_id_11394) + (12 * v_l_id_11391)))] = id(v__11453_12); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11471[(13 + (16 * v_l_id_11393) + (16 * v_l_id_11394) + (48 * v_l_id_11391))] = id(v__11453_13); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11471[(2 * (7 + (8 * v_l_id_11393) + (8 * v_l_id_11394) + (24 * v_l_id_11391)))] = id(v__11453_14); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11471[(15 + (16 * v_l_id_11393) + (16 * v_l_id_11394) + (48 * v_l_id_11391))] = id(v__11453_15); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                }
                            }
                            barrier(CLK_LOCAL_MEM_FENCE);
                            for (int v_l_id_11441 = get_local_id(1); (v_l_id_11441 < 4); v_l_id_11441 = (3 + v_l_id_11441)){
                                // map_seq
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_i_11442 = 0; 
                                    for (int v_l_id_11443 = get_local_id(0); (v_l_id_11443 < 4); v_l_id_11443 = (1 + v_l_id_11443)){
                                        float v_tmp_11495 = 0.0f; 
                                        v__10628 = v_tmp_11495; 
                                        // map_seq
                                        // unroll
                                        v__11472_0 = id(v__10628); 
                                        // end unroll
                                        // end map_seq
                                        // reduce_seq
                                        for (int v_i_11445 = 0; (v_i_11445 < 3); v_i_11445 = (1 + v_i_11445)){
                                            // map_seq
                                            // unroll
                                            v__11472_0 = add(v__11472_0, v__11471[(v_l_id_11443 + (4 * v_l_id_11441) + (16 * v_i_11445) + (48 * v_l_id_11391))]); 
                                            // end unroll
                                            // end map_seq
                                        }
                                        // end reduce_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11477[(v_l_id_11441 + (4 * v_wg_id_11388) + (4 * v_wg_id_11390) + (64 * v_l_id_11443) + (256 * (v_wg_id_11389 % 57)) + (14592 * v_l_id_11391) + (233472 * (v_wg_id_11389 / 57)))] = id(v__11472_0); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                }
                                // end map_seq
                            }
                        }
                        // end map_seq
                    }
                    barrier(CLK_LOCAL_MEM_FENCE);
                }
            }
        }
    }
}