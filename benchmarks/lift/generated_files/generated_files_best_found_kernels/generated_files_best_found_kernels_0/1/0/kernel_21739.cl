// Layer 1, tune point 0
// inputChannels -> 64, inputWidth -> 224, inputHeight -> 224,
// padFuncX -> 1, padFuncY -> 1,
// kernelWidth -> 3, kernelHeight -> 3,
// kernelStrideX -> 1, kernelStrideY -> 1,
// numKernels -> 64
// "mapVectorize.213." -> 1,
// "mapVectorize.246." -> 1
// Unsliding
// 
// Parallelization and fusion
// "mapTransform.28." -> (10*Code.mapWrg() + Code.parDims(0)()),
// "mapTransform.29." -> (10*Code.mapWrg() + Code.parDims(1)()),
// "mapTransform.30." -> (Code.replaceInnerMapWithOuter()),
// "mapTransform.51." -> (10*Code.mapWrg() + Code.parDims(2)()),
// "mapTransform.52." -> (10*Code.mapLcl() + Code.parDims(2)()),
// "mapTransform.68." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.70." -> (10*Code.mapLcl() + Code.parDims(0)()),
// "mapTransform.109." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.110." -> (10*Code.mapLcl() + Code.parDims(0)())
// padOptRight -> 4, padOptBottom -> 16,
// tileWidth -> 4, tileHeight -> 16, tileDepth -> 32,
// seqWindowsPerThreadX -> 4, seqWindowsPerThreadY -> 1,
// seqKernelsPerThread -> 4,
// kernelGroupSize -> 4,
// windowTileWidth -> 1, windowTileHeight -> 1,
// inputCacheSizeX -> 4, inputCacheSizeY -> 1,
// inputCacheDepth -> 4,
// kernelCacheSize -> 4,
// localSizes(0) -> 8,
// localSizes(1) -> 3,
// localSizes(2) -> 16,
// nWrgs(0) -> 16,
// nWrgs(1) -> 855,
// nWrgs(2) -> 1
// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

float id(float x){
    {
        { return x; }; 
    }
}
float4 id4(float4 x){
    {
        { return x; }; 
    }
}
float add(float x, float y){
    {
        { return x+y; }; 
    }
}
float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
kernel __attribute((reqd_work_group_size(8,3,16)))
void KERNEL(const global float* restrict v__11013, const global float* restrict v__11014, global float* v__11041){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        local float v__11035[6144];
        // Typed Value memory
        float v__10611; 
        float v__10620; 
        float v__10632; 
        // Private Memory
        float v__11017_0;
        float v__11017_1;
        float v__11017_2;
        float v__11017_3;
        float v__11017_4;
        float v__11017_5;
        float v__11017_6;
        float v__11017_7;
        float v__11017_8;
        float v__11017_9;
        float v__11017_10;
        float v__11017_11;
        float v__11017_12;
        float v__11017_13;
        float v__11017_14;
        float v__11017_15;
        float4 v__11020_0;
        float v__11021_0;
        float v__11021_1;
        float v__11021_2;
        float v__11021_3;
        float v__11021_4;
        float v__11021_5;
        float v__11021_6;
        float v__11021_7;
        float v__11021_8;
        float v__11021_9;
        float v__11021_10;
        float v__11021_11;
        float v__11021_12;
        float v__11021_13;
        float v__11021_14;
        float v__11021_15;
        float4 v__11023_0;
        float v__11024_0;
        float v__11024_1;
        float v__11024_2;
        float v__11024_3;
        float v__11024_4;
        float v__11024_5;
        float v__11024_6;
        float v__11024_7;
        float v__11024_8;
        float v__11024_9;
        float v__11024_10;
        float v__11024_11;
        float v__11024_12;
        float v__11024_13;
        float v__11024_14;
        float v__11024_15;
        float v__11025_0;
        float v__11025_1;
        float v__11025_2;
        float v__11025_3;
        float v__11025_4;
        float v__11025_5;
        float v__11025_6;
        float v__11025_7;
        float v__11025_8;
        float v__11025_9;
        float v__11025_10;
        float v__11025_11;
        float v__11025_12;
        float v__11025_13;
        float v__11025_14;
        float v__11025_15;
        float v__11029_0;
        float v__11029_1;
        float v__11029_2;
        float v__11029_3;
        float v__11029_4;
        float v__11029_5;
        float v__11029_6;
        float v__11029_7;
        float v__11029_8;
        float v__11029_9;
        float v__11029_10;
        float v__11029_11;
        float v__11029_12;
        float v__11029_13;
        float v__11029_14;
        float v__11029_15;
        float v__11036_0;
        // iteration count is exactly 1, no loop emitted
        {
            int v_wg_id_10948 = get_group_id(0); 
            // iteration count is exactly 1, no loop emitted
            {
                int v_wg_id_10949 = get_group_id(1); 
                // iteration count is exactly 1, no loop emitted
                {
                    int v_wg_id_10950 = get_group_id(2); 
                    // iteration count is exactly 1, no loop emitted
                    {
                        int v_l_id_10951 = get_local_id(2); 
                        // map_seq
                        // iteration count is exactly 1, no loop emitted
                        {
                            int v_i_10952 = 0; 
                            // iteration count is exactly 1, no loop emitted
                            {
                                int v_l_id_10953 = get_local_id(1); 
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_l_id_10954 = get_local_id(0); 
                                    float v_tmp_11043 = 0.0f; 
                                    v__10611 = v_tmp_11043; 
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11017_0 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11017_1 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11017_2 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11017_3 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11017_4 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11017_5 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11017_6 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11017_7 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11017_8 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11017_9 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11017_10 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11017_11 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11017_12 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11017_13 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11017_14 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11017_15 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // reduce_seq
                                    for (int v_i_10959 = 0; (v_i_10959 < 6); v_i_10959 = (1 + v_i_10959)){
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11020_0 = id4(vload4((v_l_id_10954 + (8 * (v_i_10959 / 3)) + (16 * (v_i_10959 % 3)) + (48 * v_l_id_10953) + (576 * v_wg_id_10948) + (576 * v_wg_id_10950)),v__11014 + 0)); 
                                        v__11021_0 = v__11020_0.s0; 
                                        v__11021_1 = v__11020_0.s1; 
                                        v__11021_2 = v__11020_0.s2; 
                                        v__11021_3 = v__11020_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11020_0 = id4(vload4((144 + v_l_id_10954 + (8 * (v_i_10959 / 3)) + (16 * (v_i_10959 % 3)) + (48 * v_l_id_10953) + (576 * v_wg_id_10948) + (576 * v_wg_id_10950)),v__11014 + 0)); 
                                        v__11021_4 = v__11020_0.s0; 
                                        v__11021_5 = v__11020_0.s1; 
                                        v__11021_6 = v__11020_0.s2; 
                                        v__11021_7 = v__11020_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11020_0 = id4(vload4((288 + v_l_id_10954 + (8 * (v_i_10959 / 3)) + (16 * (v_i_10959 % 3)) + (48 * v_l_id_10953) + (576 * v_wg_id_10948) + (576 * v_wg_id_10950)),v__11014 + 0)); 
                                        v__11021_8 = v__11020_0.s0; 
                                        v__11021_9 = v__11020_0.s1; 
                                        v__11021_10 = v__11020_0.s2; 
                                        v__11021_11 = v__11020_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11020_0 = id4(vload4((432 + v_l_id_10954 + (8 * (v_i_10959 / 3)) + (16 * (v_i_10959 % 3)) + (48 * v_l_id_10953) + (576 * v_wg_id_10948) + (576 * v_wg_id_10950)),v__11014 + 0)); 
                                        v__11021_12 = v__11020_0.s0; 
                                        v__11021_13 = v__11020_0.s1; 
                                        v__11021_14 = v__11020_0.s2; 
                                        v__11021_15 = v__11020_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11023_0 = id4(vload4((v_l_id_10954 + (8 * (v_i_10959 / 3)) + (16 * (v_i_10959 % 3)) + (64 * (v_wg_id_10949 % 57)) + (3680 * v_l_id_10951) + (3680 * v_l_id_10953) + (58880 * (v_wg_id_10949 / 57))),v__11013 + 0)); 
                                        v__11024_0 = v__11023_0.s0; 
                                        v__11024_1 = v__11023_0.s1; 
                                        v__11024_2 = v__11023_0.s2; 
                                        v__11024_3 = v__11023_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11023_0 = id4(vload4((16 + v_l_id_10954 + (8 * (v_i_10959 / 3)) + (16 * (v_i_10959 % 3)) + (64 * (v_wg_id_10949 % 57)) + (3680 * v_l_id_10951) + (3680 * v_l_id_10953) + (58880 * (v_wg_id_10949 / 57))),v__11013 + 0)); 
                                        v__11024_4 = v__11023_0.s0; 
                                        v__11024_5 = v__11023_0.s1; 
                                        v__11024_6 = v__11023_0.s2; 
                                        v__11024_7 = v__11023_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11023_0 = id4(vload4((32 + v_l_id_10954 + (8 * (v_i_10959 / 3)) + (16 * (v_i_10959 % 3)) + (64 * (v_wg_id_10949 % 57)) + (3680 * v_l_id_10951) + (3680 * v_l_id_10953) + (58880 * (v_wg_id_10949 / 57))),v__11013 + 0)); 
                                        v__11024_8 = v__11023_0.s0; 
                                        v__11024_9 = v__11023_0.s1; 
                                        v__11024_10 = v__11023_0.s2; 
                                        v__11024_11 = v__11023_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11023_0 = id4(vload4((48 + v_l_id_10954 + (8 * (v_i_10959 / 3)) + (16 * (v_i_10959 % 3)) + (64 * (v_wg_id_10949 % 57)) + (3680 * v_l_id_10951) + (3680 * v_l_id_10953) + (58880 * (v_wg_id_10949 / 57))),v__11013 + 0)); 
                                        v__11024_12 = v__11023_0.s0; 
                                        v__11024_13 = v__11023_0.s1; 
                                        v__11024_14 = v__11023_0.s2; 
                                        v__11024_15 = v__11023_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11045 = 0.0f; 
                                        v__10620 = v_tmp_11045; 
                                        v__11025_0 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_0 = mult(v__11024_0, v__11021_0); 
                                        v__11025_0 = add(v__11025_0, v__11029_0); 
                                        v__11029_0 = mult(v__11024_1, v__11021_1); 
                                        v__11025_0 = add(v__11025_0, v__11029_0); 
                                        v__11029_0 = mult(v__11024_2, v__11021_2); 
                                        v__11025_0 = add(v__11025_0, v__11029_0); 
                                        v__11029_0 = mult(v__11024_3, v__11021_3); 
                                        v__11025_0 = add(v__11025_0, v__11029_0); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11046 = 0.0f; 
                                        v__10620 = v_tmp_11046; 
                                        v__11025_1 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_1 = mult(v__11024_4, v__11021_0); 
                                        v__11025_1 = add(v__11025_1, v__11029_1); 
                                        v__11029_1 = mult(v__11024_5, v__11021_1); 
                                        v__11025_1 = add(v__11025_1, v__11029_1); 
                                        v__11029_1 = mult(v__11024_6, v__11021_2); 
                                        v__11025_1 = add(v__11025_1, v__11029_1); 
                                        v__11029_1 = mult(v__11024_7, v__11021_3); 
                                        v__11025_1 = add(v__11025_1, v__11029_1); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11047 = 0.0f; 
                                        v__10620 = v_tmp_11047; 
                                        v__11025_2 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_2 = mult(v__11024_8, v__11021_0); 
                                        v__11025_2 = add(v__11025_2, v__11029_2); 
                                        v__11029_2 = mult(v__11024_9, v__11021_1); 
                                        v__11025_2 = add(v__11025_2, v__11029_2); 
                                        v__11029_2 = mult(v__11024_10, v__11021_2); 
                                        v__11025_2 = add(v__11025_2, v__11029_2); 
                                        v__11029_2 = mult(v__11024_11, v__11021_3); 
                                        v__11025_2 = add(v__11025_2, v__11029_2); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11048 = 0.0f; 
                                        v__10620 = v_tmp_11048; 
                                        v__11025_3 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_3 = mult(v__11024_12, v__11021_0); 
                                        v__11025_3 = add(v__11025_3, v__11029_3); 
                                        v__11029_3 = mult(v__11024_13, v__11021_1); 
                                        v__11025_3 = add(v__11025_3, v__11029_3); 
                                        v__11029_3 = mult(v__11024_14, v__11021_2); 
                                        v__11025_3 = add(v__11025_3, v__11029_3); 
                                        v__11029_3 = mult(v__11024_15, v__11021_3); 
                                        v__11025_3 = add(v__11025_3, v__11029_3); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11049 = 0.0f; 
                                        v__10620 = v_tmp_11049; 
                                        v__11025_4 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_4 = mult(v__11024_0, v__11021_4); 
                                        v__11025_4 = add(v__11025_4, v__11029_4); 
                                        v__11029_4 = mult(v__11024_1, v__11021_5); 
                                        v__11025_4 = add(v__11025_4, v__11029_4); 
                                        v__11029_4 = mult(v__11024_2, v__11021_6); 
                                        v__11025_4 = add(v__11025_4, v__11029_4); 
                                        v__11029_4 = mult(v__11024_3, v__11021_7); 
                                        v__11025_4 = add(v__11025_4, v__11029_4); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11050 = 0.0f; 
                                        v__10620 = v_tmp_11050; 
                                        v__11025_5 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_5 = mult(v__11024_4, v__11021_4); 
                                        v__11025_5 = add(v__11025_5, v__11029_5); 
                                        v__11029_5 = mult(v__11024_5, v__11021_5); 
                                        v__11025_5 = add(v__11025_5, v__11029_5); 
                                        v__11029_5 = mult(v__11024_6, v__11021_6); 
                                        v__11025_5 = add(v__11025_5, v__11029_5); 
                                        v__11029_5 = mult(v__11024_7, v__11021_7); 
                                        v__11025_5 = add(v__11025_5, v__11029_5); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11051 = 0.0f; 
                                        v__10620 = v_tmp_11051; 
                                        v__11025_6 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_6 = mult(v__11024_8, v__11021_4); 
                                        v__11025_6 = add(v__11025_6, v__11029_6); 
                                        v__11029_6 = mult(v__11024_9, v__11021_5); 
                                        v__11025_6 = add(v__11025_6, v__11029_6); 
                                        v__11029_6 = mult(v__11024_10, v__11021_6); 
                                        v__11025_6 = add(v__11025_6, v__11029_6); 
                                        v__11029_6 = mult(v__11024_11, v__11021_7); 
                                        v__11025_6 = add(v__11025_6, v__11029_6); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11052 = 0.0f; 
                                        v__10620 = v_tmp_11052; 
                                        v__11025_7 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_7 = mult(v__11024_12, v__11021_4); 
                                        v__11025_7 = add(v__11025_7, v__11029_7); 
                                        v__11029_7 = mult(v__11024_13, v__11021_5); 
                                        v__11025_7 = add(v__11025_7, v__11029_7); 
                                        v__11029_7 = mult(v__11024_14, v__11021_6); 
                                        v__11025_7 = add(v__11025_7, v__11029_7); 
                                        v__11029_7 = mult(v__11024_15, v__11021_7); 
                                        v__11025_7 = add(v__11025_7, v__11029_7); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11053 = 0.0f; 
                                        v__10620 = v_tmp_11053; 
                                        v__11025_8 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_8 = mult(v__11024_0, v__11021_8); 
                                        v__11025_8 = add(v__11025_8, v__11029_8); 
                                        v__11029_8 = mult(v__11024_1, v__11021_9); 
                                        v__11025_8 = add(v__11025_8, v__11029_8); 
                                        v__11029_8 = mult(v__11024_2, v__11021_10); 
                                        v__11025_8 = add(v__11025_8, v__11029_8); 
                                        v__11029_8 = mult(v__11024_3, v__11021_11); 
                                        v__11025_8 = add(v__11025_8, v__11029_8); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11054 = 0.0f; 
                                        v__10620 = v_tmp_11054; 
                                        v__11025_9 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_9 = mult(v__11024_4, v__11021_8); 
                                        v__11025_9 = add(v__11025_9, v__11029_9); 
                                        v__11029_9 = mult(v__11024_5, v__11021_9); 
                                        v__11025_9 = add(v__11025_9, v__11029_9); 
                                        v__11029_9 = mult(v__11024_6, v__11021_10); 
                                        v__11025_9 = add(v__11025_9, v__11029_9); 
                                        v__11029_9 = mult(v__11024_7, v__11021_11); 
                                        v__11025_9 = add(v__11025_9, v__11029_9); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11055 = 0.0f; 
                                        v__10620 = v_tmp_11055; 
                                        v__11025_10 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_10 = mult(v__11024_8, v__11021_8); 
                                        v__11025_10 = add(v__11025_10, v__11029_10); 
                                        v__11029_10 = mult(v__11024_9, v__11021_9); 
                                        v__11025_10 = add(v__11025_10, v__11029_10); 
                                        v__11029_10 = mult(v__11024_10, v__11021_10); 
                                        v__11025_10 = add(v__11025_10, v__11029_10); 
                                        v__11029_10 = mult(v__11024_11, v__11021_11); 
                                        v__11025_10 = add(v__11025_10, v__11029_10); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11056 = 0.0f; 
                                        v__10620 = v_tmp_11056; 
                                        v__11025_11 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_11 = mult(v__11024_12, v__11021_8); 
                                        v__11025_11 = add(v__11025_11, v__11029_11); 
                                        v__11029_11 = mult(v__11024_13, v__11021_9); 
                                        v__11025_11 = add(v__11025_11, v__11029_11); 
                                        v__11029_11 = mult(v__11024_14, v__11021_10); 
                                        v__11025_11 = add(v__11025_11, v__11029_11); 
                                        v__11029_11 = mult(v__11024_15, v__11021_11); 
                                        v__11025_11 = add(v__11025_11, v__11029_11); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11057 = 0.0f; 
                                        v__10620 = v_tmp_11057; 
                                        v__11025_12 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_12 = mult(v__11024_0, v__11021_12); 
                                        v__11025_12 = add(v__11025_12, v__11029_12); 
                                        v__11029_12 = mult(v__11024_1, v__11021_13); 
                                        v__11025_12 = add(v__11025_12, v__11029_12); 
                                        v__11029_12 = mult(v__11024_2, v__11021_14); 
                                        v__11025_12 = add(v__11025_12, v__11029_12); 
                                        v__11029_12 = mult(v__11024_3, v__11021_15); 
                                        v__11025_12 = add(v__11025_12, v__11029_12); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11058 = 0.0f; 
                                        v__10620 = v_tmp_11058; 
                                        v__11025_13 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_13 = mult(v__11024_4, v__11021_12); 
                                        v__11025_13 = add(v__11025_13, v__11029_13); 
                                        v__11029_13 = mult(v__11024_5, v__11021_13); 
                                        v__11025_13 = add(v__11025_13, v__11029_13); 
                                        v__11029_13 = mult(v__11024_6, v__11021_14); 
                                        v__11025_13 = add(v__11025_13, v__11029_13); 
                                        v__11029_13 = mult(v__11024_7, v__11021_15); 
                                        v__11025_13 = add(v__11025_13, v__11029_13); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11059 = 0.0f; 
                                        v__10620 = v_tmp_11059; 
                                        v__11025_14 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_14 = mult(v__11024_8, v__11021_12); 
                                        v__11025_14 = add(v__11025_14, v__11029_14); 
                                        v__11029_14 = mult(v__11024_9, v__11021_13); 
                                        v__11025_14 = add(v__11025_14, v__11029_14); 
                                        v__11029_14 = mult(v__11024_10, v__11021_14); 
                                        v__11025_14 = add(v__11025_14, v__11029_14); 
                                        v__11029_14 = mult(v__11024_11, v__11021_15); 
                                        v__11025_14 = add(v__11025_14, v__11029_14); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11060 = 0.0f; 
                                        v__10620 = v_tmp_11060; 
                                        v__11025_15 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11029_15 = mult(v__11024_12, v__11021_12); 
                                        v__11025_15 = add(v__11025_15, v__11029_15); 
                                        v__11029_15 = mult(v__11024_13, v__11021_13); 
                                        v__11025_15 = add(v__11025_15, v__11029_15); 
                                        v__11029_15 = mult(v__11024_14, v__11021_14); 
                                        v__11025_15 = add(v__11025_15, v__11029_15); 
                                        v__11029_15 = mult(v__11024_15, v__11021_15); 
                                        v__11025_15 = add(v__11025_15, v__11029_15); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11017_0 = add(v__11017_0, v__11025_0); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11017_1 = add(v__11017_1, v__11025_1); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11017_2 = add(v__11017_2, v__11025_2); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11017_3 = add(v__11017_3, v__11025_3); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11017_4 = add(v__11017_4, v__11025_4); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11017_5 = add(v__11017_5, v__11025_5); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11017_6 = add(v__11017_6, v__11025_6); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11017_7 = add(v__11017_7, v__11025_7); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11017_8 = add(v__11017_8, v__11025_8); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11017_9 = add(v__11017_9, v__11025_9); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11017_10 = add(v__11017_10, v__11025_10); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11017_11 = add(v__11017_11, v__11025_11); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11017_12 = add(v__11017_12, v__11025_12); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11017_13 = add(v__11017_13, v__11025_13); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11017_14 = add(v__11017_14, v__11025_14); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11017_15 = add(v__11017_15, v__11025_15); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                    // end reduce_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11035[(16 * (v_l_id_10954 + (8 * v_l_id_10953) + (24 * v_l_id_10951)))] = id(v__11017_0); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11035[(1 + (16 * v_l_id_10954) + (128 * v_l_id_10953) + (384 * v_l_id_10951))] = id(v__11017_1); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11035[(2 * (1 + (8 * v_l_id_10954) + (64 * v_l_id_10953) + (192 * v_l_id_10951)))] = id(v__11017_2); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11035[(3 + (16 * v_l_id_10954) + (128 * v_l_id_10953) + (384 * v_l_id_10951))] = id(v__11017_3); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11035[(4 * (1 + (4 * v_l_id_10954) + (32 * v_l_id_10953) + (96 * v_l_id_10951)))] = id(v__11017_4); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11035[(5 + (16 * v_l_id_10954) + (128 * v_l_id_10953) + (384 * v_l_id_10951))] = id(v__11017_5); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11035[(2 * (3 + (8 * v_l_id_10954) + (64 * v_l_id_10953) + (192 * v_l_id_10951)))] = id(v__11017_6); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11035[(7 + (16 * v_l_id_10954) + (128 * v_l_id_10953) + (384 * v_l_id_10951))] = id(v__11017_7); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11035[(8 * (1 + (2 * v_l_id_10954) + (16 * v_l_id_10953) + (48 * v_l_id_10951)))] = id(v__11017_8); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11035[(9 + (16 * v_l_id_10954) + (128 * v_l_id_10953) + (384 * v_l_id_10951))] = id(v__11017_9); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11035[(2 * (5 + (8 * v_l_id_10954) + (64 * v_l_id_10953) + (192 * v_l_id_10951)))] = id(v__11017_10); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11035[(11 + (16 * v_l_id_10954) + (128 * v_l_id_10953) + (384 * v_l_id_10951))] = id(v__11017_11); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11035[(4 * (3 + (4 * v_l_id_10954) + (32 * v_l_id_10953) + (96 * v_l_id_10951)))] = id(v__11017_12); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11035[(13 + (16 * v_l_id_10954) + (128 * v_l_id_10953) + (384 * v_l_id_10951))] = id(v__11017_13); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11035[(2 * (7 + (8 * v_l_id_10954) + (64 * v_l_id_10953) + (192 * v_l_id_10951)))] = id(v__11017_14); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11035[(15 + (16 * v_l_id_10954) + (128 * v_l_id_10953) + (384 * v_l_id_10951))] = id(v__11017_15); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                }
                            }
                            barrier(CLK_LOCAL_MEM_FENCE);
                            for (int v_l_id_11005 = get_local_id(1); (v_l_id_11005 < 4); v_l_id_11005 = (3 + v_l_id_11005)){
                                // map_seq
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_i_11006 = 0; 
                                    for (int v_l_id_11007 = get_local_id(0); (v_l_id_11007 < 4); v_l_id_11007 = (8 + v_l_id_11007)){
                                        float v_tmp_11061 = 0.0f; 
                                        v__10632 = v_tmp_11061; 
                                        // map_seq
                                        // unroll
                                        v__11036_0 = id(v__10632); 
                                        // end unroll
                                        // end map_seq
                                        // reduce_seq
                                        for (int v_i_11009 = 0; (v_i_11009 < 24); v_i_11009 = (1 + v_i_11009)){
                                            // map_seq
                                            // unroll
                                            v__11036_0 = add(v__11036_0, v__11035[(v_l_id_11007 + (4 * v_l_id_11005) + (16 * v_i_11009) + (384 * v_l_id_10951))]); 
                                            // end unroll
                                            // end map_seq
                                        }
                                        // end reduce_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11041[(v_l_id_11005 + (4 * v_wg_id_10948) + (4 * v_wg_id_10950) + (64 * v_l_id_11007) + (256 * (v_wg_id_10949 % 57)) + (14592 * v_l_id_10951) + (233472 * (v_wg_id_10949 / 57)))] = id(v__11036_0); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                }
                                // end map_seq
                            }
                        }
                        // end map_seq
                    }
                    barrier(CLK_LOCAL_MEM_FENCE);
                }
            }
        }
    }
}