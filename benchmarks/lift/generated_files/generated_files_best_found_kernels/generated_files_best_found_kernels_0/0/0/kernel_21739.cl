// Layer 0, tune point 0
// inputChannels -> 3, inputWidth -> 224, inputHeight -> 224,
// padFuncX -> 1, padFuncY -> 1,
// kernelWidth -> 3, kernelHeight -> 3,
// kernelStrideX -> 1, kernelStrideY -> 1,
// numKernels -> 64
// Vectorization
// "mapVectorize.213." -> 1,
// "mapVectorize.246." -> 1
// Unsliding
// 
// Parallelization and fusion
// "mapTransform.28." -> (10*Code.mapWrg() + Code.parDims(0)()),
// "mapTransform.29." -> (10*Code.mapWrg() + Code.parDims(1)()),
// "mapTransform.30." -> (Code.replaceInnerMapWithOuter()),
// "mapTransform.51." -> (10*Code.mapWrg() + Code.parDims(2)()),
// "mapTransform.52." -> (10*Code.mapLcl() + Code.parDims(2)()),
// "mapTransform.68." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.70." -> (10*Code.mapLcl() + Code.parDims(0)()),
// "mapTransform.109." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.110." -> (10*Code.mapLcl() + Code.parDims(0)())
// padOptRight -> 4, padOptBottom -> 16,
// tileWidth -> 4, tileHeight -> 16, tileDepth -> 3,
// seqWindowsPerThreadX -> 4, seqWindowsPerThreadY -> 1,
// seqKernelsPerThread -> 4,
// kernelGroupSize -> 4,
// windowTileWidth -> 1, windowTileHeight -> 1,
// inputCacheSizeX -> 4, inputCacheSizeY -> 1,
// inputCacheDepth -> 3,
// kernelCacheSize -> 4,
// localSizes(0) -> 1,
// localSizes(1) -> 3,
// localSizes(2) -> 16,
// nWrgs(0) -> 16,
// nWrgs(1) -> 855,
// nWrgs(2) -> 1
// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

float id(float x){
    {
        { return x; }; 
    }
}
float4 id4(float4 x){
    {
        { return x; }; 
    }
}
float add(float x, float y){
    {
        { return x+y; }; 
    }
}
float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
kernel __attribute((reqd_work_group_size(1,3,16)))
void KERNEL(const global float* restrict v__11003, const global float* restrict v__11004, global float* v__11031){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        local float v__11025[768];
        // Typed Value memory
        float v__10607; 
        float v__10616; 
        float v__10628; 
        // Private Memory
        float v__11007_0;
        float v__11007_1;
        float v__11007_2;
        float v__11007_3;
        float v__11007_4;
        float v__11007_5;
        float v__11007_6;
        float v__11007_7;
        float v__11007_8;
        float v__11007_9;
        float v__11007_10;
        float v__11007_11;
        float v__11007_12;
        float v__11007_13;
        float v__11007_14;
        float v__11007_15;
        float v__11011_0;
        float v__11011_1;
        float v__11011_2;
        float v__11011_3;
        float v__11011_4;
        float v__11011_5;
        float v__11011_6;
        float v__11011_7;
        float v__11011_8;
        float v__11011_9;
        float v__11011_10;
        float v__11011_11;
        float v__11014_0;
        float v__11014_1;
        float v__11014_2;
        float v__11014_3;
        float v__11014_4;
        float v__11014_5;
        float v__11014_6;
        float v__11014_7;
        float v__11014_8;
        float v__11014_9;
        float v__11014_10;
        float v__11014_11;
        float v__11015_0;
        float v__11015_1;
        float v__11015_2;
        float v__11015_3;
        float v__11015_4;
        float v__11015_5;
        float v__11015_6;
        float v__11015_7;
        float v__11015_8;
        float v__11015_9;
        float v__11015_10;
        float v__11015_11;
        float v__11015_12;
        float v__11015_13;
        float v__11015_14;
        float v__11015_15;
        float v__11019_0;
        float v__11019_1;
        float v__11019_2;
        float v__11019_3;
        float v__11019_4;
        float v__11019_5;
        float v__11019_6;
        float v__11019_7;
        float v__11019_8;
        float v__11019_9;
        float v__11019_10;
        float v__11019_11;
        float v__11019_12;
        float v__11019_13;
        float v__11019_14;
        float v__11019_15;
        float v__11026_0;
        // iteration count is exactly 1, no loop emitted
        {
            int v_wg_id_10942 = get_group_id(0); 
            // iteration count is exactly 1, no loop emitted
            {
                int v_wg_id_10943 = get_group_id(1); 
                // iteration count is exactly 1, no loop emitted
                {
                    int v_wg_id_10944 = get_group_id(2); 
                    // iteration count is exactly 1, no loop emitted
                    {
                        int v_l_id_10945 = get_local_id(2); 
                        // map_seq
                        // iteration count is exactly 1, no loop emitted
                        {
                            int v_i_10946 = 0; 
                            // iteration count is exactly 1, no loop emitted
                            {
                                int v_l_id_10947 = get_local_id(1); 
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_l_id_10948 = get_local_id(0); 
                                    float v_tmp_11032 = 0.0f; 
                                    v__10607 = v_tmp_11032; 
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11007_0 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11007_1 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11007_2 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11007_3 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11007_4 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11007_5 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11007_6 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11007_7 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11007_8 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11007_9 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11007_10 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11007_11 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11007_12 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11007_13 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11007_14 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11007_15 = id(v__10607); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // reduce_seq
                                    for (int v_i_10953 = 0; (v_i_10953 < 3); v_i_10953 = (1 + v_i_10953)){
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11011_0 = id(v__11004[(3 * (v_i_10953 + (3 * v_l_id_10947) + (9 * v_l_id_10948) + (36 * v_wg_id_10942) + (36 * v_wg_id_10944)))]); 
                                        v__11011_1 = id(v__11004[(1 + (3 * v_i_10953) + (9 * v_l_id_10947) + (27 * v_l_id_10948) + (108 * v_wg_id_10942) + (108 * v_wg_id_10944))]); 
                                        v__11011_2 = id(v__11004[(2 + (3 * v_i_10953) + (9 * v_l_id_10947) + (27 * v_l_id_10948) + (108 * v_wg_id_10942) + (108 * v_wg_id_10944))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11011_3 = id(v__11004[(3 * (9 + v_i_10953 + (3 * v_l_id_10947) + (9 * v_l_id_10948) + (36 * v_wg_id_10942) + (36 * v_wg_id_10944)))]); 
                                        v__11011_4 = id(v__11004[(28 + (3 * v_i_10953) + (9 * v_l_id_10947) + (27 * v_l_id_10948) + (108 * v_wg_id_10942) + (108 * v_wg_id_10944))]); 
                                        v__11011_5 = id(v__11004[(29 + (3 * v_i_10953) + (9 * v_l_id_10947) + (27 * v_l_id_10948) + (108 * v_wg_id_10942) + (108 * v_wg_id_10944))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11011_6 = id(v__11004[(3 * (18 + v_i_10953 + (3 * v_l_id_10947) + (9 * v_l_id_10948) + (36 * v_wg_id_10942) + (36 * v_wg_id_10944)))]); 
                                        v__11011_7 = id(v__11004[(55 + (3 * v_i_10953) + (9 * v_l_id_10947) + (27 * v_l_id_10948) + (108 * v_wg_id_10942) + (108 * v_wg_id_10944))]); 
                                        v__11011_8 = id(v__11004[(56 + (3 * v_i_10953) + (9 * v_l_id_10947) + (27 * v_l_id_10948) + (108 * v_wg_id_10942) + (108 * v_wg_id_10944))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11011_9 = id(v__11004[(3 * (27 + v_i_10953 + (3 * v_l_id_10947) + (9 * v_l_id_10948) + (36 * v_wg_id_10942) + (36 * v_wg_id_10944)))]); 
                                        v__11011_10 = id(v__11004[(82 + (3 * v_i_10953) + (9 * v_l_id_10947) + (27 * v_l_id_10948) + (108 * v_wg_id_10942) + (108 * v_wg_id_10944))]); 
                                        v__11011_11 = id(v__11004[(83 + (3 * v_i_10953) + (9 * v_l_id_10947) + (27 * v_l_id_10948) + (108 * v_wg_id_10942) + (108 * v_wg_id_10944))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11014_0 = id(v__11003[(3 * (v_i_10953 + (4 * (v_wg_id_10943 % 57)) + (230 * v_l_id_10945) + (230 * v_l_id_10947) + (690 * v_l_id_10948) + (3680 * (v_wg_id_10943 / 57))))]); 
                                        v__11014_1 = id(v__11003[(1 + (3 * v_i_10953) + (12 * (v_wg_id_10943 % 57)) + (690 * v_l_id_10945) + (690 * v_l_id_10947) + (2070 * v_l_id_10948) + (11040 * (v_wg_id_10943 / 57)))]); 
                                        v__11014_2 = id(v__11003[(2 + (3 * v_i_10953) + (12 * (v_wg_id_10943 % 57)) + (690 * v_l_id_10945) + (690 * v_l_id_10947) + (2070 * v_l_id_10948) + (11040 * (v_wg_id_10943 / 57)))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11014_3 = id(v__11003[(3 * (1 + v_i_10953 + (4 * (v_wg_id_10943 % 57)) + (230 * v_l_id_10945) + (230 * v_l_id_10947) + (690 * v_l_id_10948) + (3680 * (v_wg_id_10943 / 57))))]); 
                                        v__11014_4 = id(v__11003[(4 + (3 * v_i_10953) + (12 * (v_wg_id_10943 % 57)) + (690 * v_l_id_10945) + (690 * v_l_id_10947) + (2070 * v_l_id_10948) + (11040 * (v_wg_id_10943 / 57)))]); 
                                        v__11014_5 = id(v__11003[(5 + (3 * v_i_10953) + (12 * (v_wg_id_10943 % 57)) + (690 * v_l_id_10945) + (690 * v_l_id_10947) + (2070 * v_l_id_10948) + (11040 * (v_wg_id_10943 / 57)))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11014_6 = id(v__11003[(3 * (2 + v_i_10953 + (4 * (v_wg_id_10943 % 57)) + (230 * v_l_id_10945) + (230 * v_l_id_10947) + (690 * v_l_id_10948) + (3680 * (v_wg_id_10943 / 57))))]); 
                                        v__11014_7 = id(v__11003[(7 + (3 * v_i_10953) + (12 * (v_wg_id_10943 % 57)) + (690 * v_l_id_10945) + (690 * v_l_id_10947) + (2070 * v_l_id_10948) + (11040 * (v_wg_id_10943 / 57)))]); 
                                        v__11014_8 = id(v__11003[(8 + (3 * v_i_10953) + (12 * (v_wg_id_10943 % 57)) + (690 * v_l_id_10945) + (690 * v_l_id_10947) + (2070 * v_l_id_10948) + (11040 * (v_wg_id_10943 / 57)))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector scalar loop
                                        // unroll
                                        v__11014_9 = id(v__11003[(3 * (3 + v_i_10953 + (4 * (v_wg_id_10943 % 57)) + (230 * v_l_id_10945) + (230 * v_l_id_10947) + (690 * v_l_id_10948) + (3680 * (v_wg_id_10943 / 57))))]); 
                                        v__11014_10 = id(v__11003[(10 + (3 * v_i_10953) + (12 * (v_wg_id_10943 % 57)) + (690 * v_l_id_10945) + (690 * v_l_id_10947) + (2070 * v_l_id_10948) + (11040 * (v_wg_id_10943 / 57)))]); 
                                        v__11014_11 = id(v__11003[(11 + (3 * v_i_10953) + (12 * (v_wg_id_10943 % 57)) + (690 * v_l_id_10945) + (690 * v_l_id_10947) + (2070 * v_l_id_10948) + (11040 * (v_wg_id_10943 / 57)))]); 
                                        // end unroll
                                        // end mapSeqVector scalar loop
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11034 = 0.0f; 
                                        v__10616 = v_tmp_11034; 
                                        v__11015_0 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_0 = mult(v__11014_0, v__11011_0); 
                                        v__11015_0 = add(v__11015_0, v__11019_0); 
                                        v__11019_0 = mult(v__11014_1, v__11011_1); 
                                        v__11015_0 = add(v__11015_0, v__11019_0); 
                                        v__11019_0 = mult(v__11014_2, v__11011_2); 
                                        v__11015_0 = add(v__11015_0, v__11019_0); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11035 = 0.0f; 
                                        v__10616 = v_tmp_11035; 
                                        v__11015_1 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_1 = mult(v__11014_3, v__11011_0); 
                                        v__11015_1 = add(v__11015_1, v__11019_1); 
                                        v__11019_1 = mult(v__11014_4, v__11011_1); 
                                        v__11015_1 = add(v__11015_1, v__11019_1); 
                                        v__11019_1 = mult(v__11014_5, v__11011_2); 
                                        v__11015_1 = add(v__11015_1, v__11019_1); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11036 = 0.0f; 
                                        v__10616 = v_tmp_11036; 
                                        v__11015_2 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_2 = mult(v__11014_6, v__11011_0); 
                                        v__11015_2 = add(v__11015_2, v__11019_2); 
                                        v__11019_2 = mult(v__11014_7, v__11011_1); 
                                        v__11015_2 = add(v__11015_2, v__11019_2); 
                                        v__11019_2 = mult(v__11014_8, v__11011_2); 
                                        v__11015_2 = add(v__11015_2, v__11019_2); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11037 = 0.0f; 
                                        v__10616 = v_tmp_11037; 
                                        v__11015_3 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_3 = mult(v__11014_9, v__11011_0); 
                                        v__11015_3 = add(v__11015_3, v__11019_3); 
                                        v__11019_3 = mult(v__11014_10, v__11011_1); 
                                        v__11015_3 = add(v__11015_3, v__11019_3); 
                                        v__11019_3 = mult(v__11014_11, v__11011_2); 
                                        v__11015_3 = add(v__11015_3, v__11019_3); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11038 = 0.0f; 
                                        v__10616 = v_tmp_11038; 
                                        v__11015_4 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_4 = mult(v__11014_0, v__11011_3); 
                                        v__11015_4 = add(v__11015_4, v__11019_4); 
                                        v__11019_4 = mult(v__11014_1, v__11011_4); 
                                        v__11015_4 = add(v__11015_4, v__11019_4); 
                                        v__11019_4 = mult(v__11014_2, v__11011_5); 
                                        v__11015_4 = add(v__11015_4, v__11019_4); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11039 = 0.0f; 
                                        v__10616 = v_tmp_11039; 
                                        v__11015_5 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_5 = mult(v__11014_3, v__11011_3); 
                                        v__11015_5 = add(v__11015_5, v__11019_5); 
                                        v__11019_5 = mult(v__11014_4, v__11011_4); 
                                        v__11015_5 = add(v__11015_5, v__11019_5); 
                                        v__11019_5 = mult(v__11014_5, v__11011_5); 
                                        v__11015_5 = add(v__11015_5, v__11019_5); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11040 = 0.0f; 
                                        v__10616 = v_tmp_11040; 
                                        v__11015_6 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_6 = mult(v__11014_6, v__11011_3); 
                                        v__11015_6 = add(v__11015_6, v__11019_6); 
                                        v__11019_6 = mult(v__11014_7, v__11011_4); 
                                        v__11015_6 = add(v__11015_6, v__11019_6); 
                                        v__11019_6 = mult(v__11014_8, v__11011_5); 
                                        v__11015_6 = add(v__11015_6, v__11019_6); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11041 = 0.0f; 
                                        v__10616 = v_tmp_11041; 
                                        v__11015_7 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_7 = mult(v__11014_9, v__11011_3); 
                                        v__11015_7 = add(v__11015_7, v__11019_7); 
                                        v__11019_7 = mult(v__11014_10, v__11011_4); 
                                        v__11015_7 = add(v__11015_7, v__11019_7); 
                                        v__11019_7 = mult(v__11014_11, v__11011_5); 
                                        v__11015_7 = add(v__11015_7, v__11019_7); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11042 = 0.0f; 
                                        v__10616 = v_tmp_11042; 
                                        v__11015_8 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_8 = mult(v__11014_0, v__11011_6); 
                                        v__11015_8 = add(v__11015_8, v__11019_8); 
                                        v__11019_8 = mult(v__11014_1, v__11011_7); 
                                        v__11015_8 = add(v__11015_8, v__11019_8); 
                                        v__11019_8 = mult(v__11014_2, v__11011_8); 
                                        v__11015_8 = add(v__11015_8, v__11019_8); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11043 = 0.0f; 
                                        v__10616 = v_tmp_11043; 
                                        v__11015_9 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_9 = mult(v__11014_3, v__11011_6); 
                                        v__11015_9 = add(v__11015_9, v__11019_9); 
                                        v__11019_9 = mult(v__11014_4, v__11011_7); 
                                        v__11015_9 = add(v__11015_9, v__11019_9); 
                                        v__11019_9 = mult(v__11014_5, v__11011_8); 
                                        v__11015_9 = add(v__11015_9, v__11019_9); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11044 = 0.0f; 
                                        v__10616 = v_tmp_11044; 
                                        v__11015_10 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_10 = mult(v__11014_6, v__11011_6); 
                                        v__11015_10 = add(v__11015_10, v__11019_10); 
                                        v__11019_10 = mult(v__11014_7, v__11011_7); 
                                        v__11015_10 = add(v__11015_10, v__11019_10); 
                                        v__11019_10 = mult(v__11014_8, v__11011_8); 
                                        v__11015_10 = add(v__11015_10, v__11019_10); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11045 = 0.0f; 
                                        v__10616 = v_tmp_11045; 
                                        v__11015_11 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_11 = mult(v__11014_9, v__11011_6); 
                                        v__11015_11 = add(v__11015_11, v__11019_11); 
                                        v__11019_11 = mult(v__11014_10, v__11011_7); 
                                        v__11015_11 = add(v__11015_11, v__11019_11); 
                                        v__11019_11 = mult(v__11014_11, v__11011_8); 
                                        v__11015_11 = add(v__11015_11, v__11019_11); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11046 = 0.0f; 
                                        v__10616 = v_tmp_11046; 
                                        v__11015_12 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_12 = mult(v__11014_0, v__11011_9); 
                                        v__11015_12 = add(v__11015_12, v__11019_12); 
                                        v__11019_12 = mult(v__11014_1, v__11011_10); 
                                        v__11015_12 = add(v__11015_12, v__11019_12); 
                                        v__11019_12 = mult(v__11014_2, v__11011_11); 
                                        v__11015_12 = add(v__11015_12, v__11019_12); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11047 = 0.0f; 
                                        v__10616 = v_tmp_11047; 
                                        v__11015_13 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_13 = mult(v__11014_3, v__11011_9); 
                                        v__11015_13 = add(v__11015_13, v__11019_13); 
                                        v__11019_13 = mult(v__11014_4, v__11011_10); 
                                        v__11015_13 = add(v__11015_13, v__11019_13); 
                                        v__11019_13 = mult(v__11014_5, v__11011_11); 
                                        v__11015_13 = add(v__11015_13, v__11019_13); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11048 = 0.0f; 
                                        v__10616 = v_tmp_11048; 
                                        v__11015_14 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_14 = mult(v__11014_6, v__11011_9); 
                                        v__11015_14 = add(v__11015_14, v__11019_14); 
                                        v__11019_14 = mult(v__11014_7, v__11011_10); 
                                        v__11015_14 = add(v__11015_14, v__11019_14); 
                                        v__11019_14 = mult(v__11014_8, v__11011_11); 
                                        v__11015_14 = add(v__11015_14, v__11019_14); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11049 = 0.0f; 
                                        v__10616 = v_tmp_11049; 
                                        v__11015_15 = id(v__10616); 
                                        // reduce_seq
                                        // unroll
                                        v__11019_15 = mult(v__11014_9, v__11011_9); 
                                        v__11015_15 = add(v__11015_15, v__11019_15); 
                                        v__11019_15 = mult(v__11014_10, v__11011_10); 
                                        v__11015_15 = add(v__11015_15, v__11019_15); 
                                        v__11019_15 = mult(v__11014_11, v__11011_11); 
                                        v__11015_15 = add(v__11015_15, v__11019_15); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11007_0 = add(v__11007_0, v__11015_0); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11007_1 = add(v__11007_1, v__11015_1); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11007_2 = add(v__11007_2, v__11015_2); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11007_3 = add(v__11007_3, v__11015_3); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11007_4 = add(v__11007_4, v__11015_4); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11007_5 = add(v__11007_5, v__11015_5); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11007_6 = add(v__11007_6, v__11015_6); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11007_7 = add(v__11007_7, v__11015_7); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11007_8 = add(v__11007_8, v__11015_8); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11007_9 = add(v__11007_9, v__11015_9); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11007_10 = add(v__11007_10, v__11015_10); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11007_11 = add(v__11007_11, v__11015_11); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11007_12 = add(v__11007_12, v__11015_12); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11007_13 = add(v__11007_13, v__11015_13); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11007_14 = add(v__11007_14, v__11015_14); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11007_15 = add(v__11007_15, v__11015_15); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                    // end reduce_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11025[(16 * (v_l_id_10947 + v_l_id_10948 + (3 * v_l_id_10945)))] = id(v__11007_0); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11025[(1 + (16 * v_l_id_10947) + (16 * v_l_id_10948) + (48 * v_l_id_10945))] = id(v__11007_1); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11025[(2 * (1 + (8 * v_l_id_10947) + (8 * v_l_id_10948) + (24 * v_l_id_10945)))] = id(v__11007_2); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11025[(3 + (16 * v_l_id_10947) + (16 * v_l_id_10948) + (48 * v_l_id_10945))] = id(v__11007_3); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11025[(4 * (1 + (4 * v_l_id_10947) + (4 * v_l_id_10948) + (12 * v_l_id_10945)))] = id(v__11007_4); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11025[(5 + (16 * v_l_id_10947) + (16 * v_l_id_10948) + (48 * v_l_id_10945))] = id(v__11007_5); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11025[(2 * (3 + (8 * v_l_id_10947) + (8 * v_l_id_10948) + (24 * v_l_id_10945)))] = id(v__11007_6); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11025[(7 + (16 * v_l_id_10947) + (16 * v_l_id_10948) + (48 * v_l_id_10945))] = id(v__11007_7); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11025[(8 * (1 + (2 * v_l_id_10947) + (2 * v_l_id_10948) + (6 * v_l_id_10945)))] = id(v__11007_8); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11025[(9 + (16 * v_l_id_10947) + (16 * v_l_id_10948) + (48 * v_l_id_10945))] = id(v__11007_9); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11025[(2 * (5 + (8 * v_l_id_10947) + (8 * v_l_id_10948) + (24 * v_l_id_10945)))] = id(v__11007_10); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11025[(11 + (16 * v_l_id_10947) + (16 * v_l_id_10948) + (48 * v_l_id_10945))] = id(v__11007_11); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11025[(4 * (3 + (4 * v_l_id_10947) + (4 * v_l_id_10948) + (12 * v_l_id_10945)))] = id(v__11007_12); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11025[(13 + (16 * v_l_id_10947) + (16 * v_l_id_10948) + (48 * v_l_id_10945))] = id(v__11007_13); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11025[(2 * (7 + (8 * v_l_id_10947) + (8 * v_l_id_10948) + (24 * v_l_id_10945)))] = id(v__11007_14); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11025[(15 + (16 * v_l_id_10947) + (16 * v_l_id_10948) + (48 * v_l_id_10945))] = id(v__11007_15); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                }
                            }
                            barrier(CLK_LOCAL_MEM_FENCE);
                            for (int v_l_id_10995 = get_local_id(1); (v_l_id_10995 < 4); v_l_id_10995 = (3 + v_l_id_10995)){
                                // map_seq
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_i_10996 = 0; 
                                    for (int v_l_id_10997 = get_local_id(0); (v_l_id_10997 < 4); v_l_id_10997 = (1 + v_l_id_10997)){
                                        float v_tmp_11050 = 0.0f; 
                                        v__10628 = v_tmp_11050; 
                                        // map_seq
                                        // unroll
                                        v__11026_0 = id(v__10628); 
                                        // end unroll
                                        // end map_seq
                                        // reduce_seq
                                        for (int v_i_10999 = 0; (v_i_10999 < 3); v_i_10999 = (1 + v_i_10999)){
                                            // map_seq
                                            // unroll
                                            v__11026_0 = add(v__11026_0, v__11025[(v_l_id_10997 + (4 * v_l_id_10995) + (16 * v_i_10999) + (48 * v_l_id_10945))]); 
                                            // end unroll
                                            // end map_seq
                                        }
                                        // end reduce_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11031[(v_l_id_10995 + (4 * v_wg_id_10942) + (4 * v_wg_id_10944) + (64 * v_l_id_10997) + (256 * (v_wg_id_10943 % 57)) + (14592 * v_l_id_10945) + (233472 * (v_wg_id_10943 / 57)))] = id(v__11026_0); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                }
                                // end map_seq
                            }
                        }
                        // end map_seq
                    }
                    barrier(CLK_LOCAL_MEM_FENCE);
                }
            }
        }
    }
}