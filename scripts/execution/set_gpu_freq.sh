#!/usr/bin/env bash

sudo echo "userspace" > /sys/class/misc/mali0/device/devfreq/e82c0000.mali/governor
sudo echo 103750000 > /sys/class/misc/mali0/device/devfreq/e82c0000.mali/min_freq
sudo echo 767000000 > /sys/class/misc/mali0/device/devfreq/e82c0000.mali/max_freq
sudo echo $1 > /sys/class/misc/mali0/device/devfreq/e82c0000.mali/userspace/set_freq