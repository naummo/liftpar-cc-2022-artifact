import numpy as np
import argparse
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rc
from os.path import join
import matplotlib
import seaborn as sns
from timeit import default_timer

def timer_start():
	# pass
	return default_timer()

def timer_end(start, msg):
	pass
	end = default_timer()
	print("%s: %.7f sec" % (msg, end - start))

parser = argparse.ArgumentParser()
parser.add_argument('--netname', action="store", dest="netname")
parser.add_argument('--netlabel', action="store", dest="netlabel")

parser.add_argument('--constrained_results_and_params_csv', action="store", dest="constrained_results_and_params_csv")
parser.add_argument('--random_results_and_params_csv', action="store", dest="random_results_and_params_csv")
parser.add_argument('--plot_path', action="store", dest="plot_path")

parser.add_argument('--limit_to_layer', action='store', dest="limit_to_layer")

args = parser.parse_args()

##################### Settings ######################
scatter_marker_size = 2.5
sns.set() 

plt.rc('text', usetex=True)
rc('font',**{'family':'serif','serif':['Palatino']})

rot = 0
fontsize = 13

rc("legend", facecolor="white")

colors = ["#56ae6c",
"#8960b3",
"#b0923b",
"#ba495b"]

##################### Helpers #####################
true_id_to_continuous_collated = lambda true_id : \
    0 if true_id == 0 else \
    1 if true_id == 2 else \
    2 if true_id == 5 else \
    3 if true_id == 7 else \
    4 if true_id == 10 else \
    5 if true_id == 12 else \
    7 if true_id == 17 else \
    8 if true_id == 19 else \
    10 # if true_id == 24

def get_n_layer_duplicates(row):
    return (
    2 if row["layer"] == 5 else
    2 if row["layer"] == 8 else
    3 if row["layer"] == 10 else
    1)
        

##################### Load data #####################
def load_lift_data(results_and_params_csv: str):
    start = timer_start()
    if "temp" in results_and_params_csv:
        lift_data = pd.read_csv(results_and_params_csv, sep='\s*,\s*', engine='python', dtype = {
            "temp_before": 'Int32',
            "temp_after": 'Int32',
            "mapUnslide": "str",
            "mapTransform": "str",
            "mapVectorize": "str"
        })
    else:
        lift_data = pd.read_csv(results_and_params_csv, dtype = {
            "temp_before": 'Int32',
            "temp_after": 'Int32',
            "mapUnslide": "str",
            "mapTransform": "str",
            "mapVectorize": "str"
        })
    lift_data.columns = lift_data.columns.str.strip()
    if "collated_layer_id" not in lift_data.columns:
        lift_data["collated_layer_id"] = lift_data["layer"]

    ##################### Limits #######################


    failed_tunepoints = lift_data[
        (lift_data["fail_reported"] == 1) |
        ( 
            (lift_data["success_status"] == 0) &
            pd.isnull(lift_data["reached_runtime_limit"])
        )
        ]["tunepoint"].unique()
    print("Dropping " + str(len(failed_tunepoints)) + " failed experiments")
    # failed_tunepoints_layer = lift_data[(lift_data["fail_reported"] != 0) & (lift_data["layer"] == args.limit_to_layer)]["tunepoint"].unique()
    failed_tunepoints_layer = lift_data[(lift_data["fail_reported"] != 0)]["tunepoint"].unique()
    # print("L" + str(limit_to_layer) + " failed unique points: " + str(len(failed_tunepoints_layer)))
    print("failed unique points: " + str(len(failed_tunepoints_layer)))
    lift_data_successful = lift_data.drop( lift_data[lift_data["tunepoint"].isin(failed_tunepoints)].index, inplace=False)

    if args.limit_to_layer is not None:
        layers_to_retain = (lift_data_successful["layer"] == int(args.limit_to_layer)) & (lift_data_successful["layer"].notna())
        if int(args.limit_to_layer) not in lift_data_successful["layer"].unique():
            print("Could not find any successful experiments on layer " + str(args.limit_to_layer) + "! Aborting.")
            quit()
    else:
        layers_to_retain = lift_data_successful["layer"].notna()
    jobs_to_retain = True

    lift_data_filtered_layer_jobs = lift_data_successful[layers_to_retain & jobs_to_retain]

    lift_data_filtered = lift_data_filtered_layer_jobs

    lift_data_clean = lift_data.drop( 
        lift_data.index.difference(lift_data_filtered.index))#, inplace=True)

    print(lift_data_clean)

    lift_data = lift_data_clean.reset_index()

    ##################### Formatting #######################
    lift_data["temp_before"] /= 1000
    lift_data["temp_after"] /= 1000

    lift_data["n_layer_duplicates"] = lift_data.apply(get_n_layer_duplicates, axis="columns")
    print(lift_data)
    return lift_data

primary_lift_data = load_lift_data(results_and_params_csv = args.random_results_and_params_csv)


def get_throughput_vector(inputWidth, inputHeight, inputChannels, padFuncX, padFuncY, kernelWidth, kernelHeight, kernelStrideX, kernelStrideY, numKernels, total_runtime_median):
    n_windows_x = np.floor((inputWidth + 2*padFuncX - (kernelWidth - kernelStrideX) / kernelStrideX))
    n_windows_y = np.floor((inputHeight + 2*padFuncY - (kernelHeight - kernelStrideY) / kernelStrideY))

    macs_per_out_el = kernelWidth * kernelHeight
    macs_per_feat_map = macs_per_out_el * inputChannels * numKernels * n_windows_x * n_windows_y
    return ((1000 * macs_per_feat_map / total_runtime_median) / 1000000000)

def compute_throughput(lift_data):

    start = timer_start()
    lift_data["layer_Gflops_required"] = (get_throughput_vector(
        lift_data.inputWidth.to_numpy(), lift_data.inputHeight.to_numpy(), 
        lift_data.inputChannels.to_numpy(),
        lift_data.padFuncX.to_numpy(), lift_data.padFuncY.to_numpy(), 
        lift_data.kernelWidth.to_numpy(), lift_data.kernelHeight.to_numpy(), 
        lift_data.kernelStrideX.to_numpy(), lift_data.kernelStrideY.to_numpy(), 
        lift_data.numKernels.to_numpy(), 1) / 1000).round(decimals=2)

    lift_data["throughputGflops"] = get_throughput_vector(
        lift_data.inputWidth.to_numpy(), lift_data.inputHeight.to_numpy(), 
        lift_data.inputChannels.to_numpy(),
        lift_data.padFuncX.to_numpy(), lift_data.padFuncY.to_numpy(), 
        lift_data.kernelWidth.to_numpy(), lift_data.kernelHeight.to_numpy(), 
        lift_data.kernelStrideX.to_numpy(), lift_data.kernelStrideY.to_numpy(), 
        lift_data.numKernels.to_numpy(), lift_data.total_runtime_median.to_numpy()).round(decimals=2)
    timer_end(start, "get_throughput_vector")


def compute_total_exploration_time(data):
    # data["total_exec_time"] = 
    data["total_explore_duration_sec"] = data.parFuse_search_duration_sec + data.compile_duration_sec + data.total_exec_duration_sec

compute_total_exploration_time(primary_lift_data)
compute_throughput(primary_lift_data)

secondary_lift_data = load_lift_data(results_and_params_csv = args.constrained_results_and_params_csv)
secondary_lift_data["parMapExploreStrategy"] = "constrained"

primary_lift_data_nonans = primary_lift_data.copy()
secondary_lift_data_nonans = secondary_lift_data.copy()

primary_lift_data_nonans["total_runtime_median"].fillna(value = 
    np.max([
        primary_lift_data["total_runtime_median"].max(),
        secondary_lift_data["total_runtime_median"].max()]), inplace=True) # Dangerous!

secondary_lift_data_nonans["total_runtime_median"].fillna(value = 
    np.max([
        primary_lift_data["total_runtime_median"].max(),
        secondary_lift_data["total_runtime_median"].max()]), inplace=True) # Dangerous!

compute_throughput(secondary_lift_data_nonans)
compute_total_exploration_time(secondary_lift_data_nonans)

primary_lift_data_with_strategy_nonans = primary_lift_data_nonans.copy()
primary_lift_data_with_strategy_nonans["parMapExploreStrategy"] = "random"

max_len = 450

# n_total_points = min(max_len,
#     min(len(primary_lift_data_with_strategy_nonans.index),
#         len(secondary_lift_data_nonans.index)))
total_constrained_points = min(max_len, len(secondary_lift_data_nonans.index))
total_random_points = min(max_len, len(primary_lift_data_with_strategy_nonans.index))

print("primary_lift_data_with_strategy_nonans.len = " + (str(len(primary_lift_data_with_strategy_nonans.index))))
print("secondary_lift_data_nonans.len = " + (str(len(secondary_lift_data_nonans.index))))

two_liftdatas_concatenated = pd.concat([primary_lift_data_with_strategy_nonans.head(total_random_points), secondary_lift_data_nonans.head(total_constrained_points)], ignore_index = True)

n_samples_for_expectancy = 100

randState = np.random.RandomState(seed=10)

def get_expectancies(n_candidates_evaluated: int, strategy: str, n_points: int):
    
    df = two_liftdatas_concatenated[
        two_liftdatas_concatenated.parMapExploreStrategy == strategy]

    if n_candidates_evaluated == n_points:
        return (df["throughputGflops"].max(), 
            df["parFuse_search_duration_sec"].sum(), 
            df["compile_duration_sec"].sum(), 
            df["total_exec_duration_sec"].sum(), 
            df["total_explore_duration_sec"].sum())
    else:
        throughput_maxes = []
        parFuse_duration_sums = []
        compile_duration_sums = []
        total_exec_duration_sums = []
        explore_duration_sums = []

        for i in range(n_samples_for_expectancy):
            sample = df.sample(n = n_candidates_evaluated, replace = False, random_state = randState, ignore_index = True)
            
            throughput_maxes.append(sample["throughputGflops"].max())
            parFuse_duration_sums.append(sample["parFuse_search_duration_sec"].sum())
            compile_duration_sums.append(sample["compile_duration_sec"].sum())
            total_exec_duration_sums.append(sample["total_exec_duration_sec"].sum())
            explore_duration_sums.append(sample["total_explore_duration_sec"].sum())

        return (
            np.median(throughput_maxes), 
            np.median(parFuse_duration_sums), 
            np.median(compile_duration_sums), 
            np.median(total_exec_duration_sums), 
            np.median(explore_duration_sums))


throughput_expectancy_per_n_candidates_evaluated = pd.DataFrame(columns = 
    ["nCandidatesEvaluated", "parMapExploreStrategy", "throughputExpectancy",
    "parFuseDurationExpectancySec", "compileDurationExpectancySec", "totalExecDurationExpectancySec", "explorationDurationExpectancySec"])
print("Calculating exploration duration expectancy, please wait...")
for strategy, n_points in [
    ("constrained", total_constrained_points), 
    ("random", total_random_points)]:
    for n_candidates_evaluated in range(1, n_points + 1): # 1, 2, .., 250

        throughputExpectancy, \
            parFuse_duration_expectancy, \
            compile_duration_expectancy, \
            total_exec_duration_expectancy, \
            exploration_duration_expectancy =\
            get_expectancies(n_candidates_evaluated, strategy, n_points)

        throughput_expectancy_per_n_candidates_evaluated = throughput_expectancy_per_n_candidates_evaluated.append({
            "nCandidatesEvaluated": n_candidates_evaluated,
            "parMapExploreStrategy": strategy,
            "throughputExpectancy": throughputExpectancy,
            
            "parFuseDurationExpectancySec": parFuse_duration_expectancy,
            "compileDurationExpectancySec": compile_duration_expectancy,
            "totalExecDurationExpectancySec": total_exec_duration_expectancy,
            "explorationDurationExpectancySec": exploration_duration_expectancy
        }, ignore_index = True)

throughput_expectancy_per_n_candidates_evaluated["throughputExpectancySmoothened"] = np.nan

from scipy.ndimage.filters import gaussian_filter1d

for strategy in ["constrained", "random"]:

    min_per_strategy = throughput_expectancy_per_n_candidates_evaluated[
        throughput_expectancy_per_n_candidates_evaluated["parMapExploreStrategy"] == strategy
        ]["explorationDurationExpectancySec"].min()
    max_per_strategy = throughput_expectancy_per_n_candidates_evaluated[
        throughput_expectancy_per_n_candidates_evaluated["parMapExploreStrategy"] == strategy
        ]["explorationDurationExpectancySec"].max()
    range_per_strategy = max_per_strategy - min_per_strategy

    strategy_selector = (throughput_expectancy_per_n_candidates_evaluated["parMapExploreStrategy"] == strategy)
    smoothening_cutoff = max(1, range_per_strategy * 0.0001)

    most_points_selector = (
        throughput_expectancy_per_n_candidates_evaluated["explorationDurationExpectancySec"] >= (min_per_strategy + smoothening_cutoff)
        ) & (
        throughput_expectancy_per_n_candidates_evaluated["explorationDurationExpectancySec"] <= (max_per_strategy - smoothening_cutoff)
        )
    last_points_selector = (
        throughput_expectancy_per_n_candidates_evaluated["explorationDurationExpectancySec"] < (min_per_strategy + smoothening_cutoff)
        ) | (
        throughput_expectancy_per_n_candidates_evaluated["explorationDurationExpectancySec"] > (max_per_strategy - smoothening_cutoff)
        )
        

    throughput_expectancy_per_n_candidates_evaluated.loc[strategy_selector & most_points_selector, "throughputExpectancySmoothened"] =\
        gaussian_filter1d(
            pd.to_numeric(
                throughput_expectancy_per_n_candidates_evaluated.loc[strategy_selector & most_points_selector, "throughputExpectancy"], 
                downcast="float"),
            sigma = 2,
            mode = "nearest"
            )

    throughput_expectancy_per_n_candidates_evaluated.loc[strategy_selector & last_points_selector, "throughputExpectancySmoothened"] =\
        throughput_expectancy_per_n_candidates_evaluated.loc[strategy_selector & last_points_selector, "throughputExpectancy"]

throughput_expectancy_per_n_candidates_evaluated

# L(9) #4636, kernel_sharing_coalesce_fix_repr_for_cc2_shepherd
gpgpu_lambda_gen_duration_sec = 6.157
gpgpu_compile_duration_sec = 2.648
# From 2019-08-12 09:05:03.618000  to 2019-08-12 09:05:03.758000 in vgg_kernel_sharing_caolesc_fix_timings_and_mem_best.csv
gpgpu_total_exec_duration_sec = 0.140 * 3
gpgpu_exploration_time = gpgpu_lambda_gen_duration_sec + gpgpu_compile_duration_sec + gpgpu_total_exec_duration_sec

gpgpu_best_runtime_ms = 94.83
l8 = primary_lift_data[primary_lift_data["layer"] == 8].iloc[0]
gpgpu_best_throughputGflops = get_throughput_vector(
    l8.inputWidth, l8.inputHeight, 
    l8.inputChannels,
    l8.padFuncX, l8.padFuncY, 
    l8.kernelWidth, l8.kernelHeight, 
    l8.kernelStrideX, l8.kernelStrideY, 
    l8.numKernels, gpgpu_best_runtime_ms).round(decimals=2)

gpgpu_data = pd.DataFrame(data={
    "throughputGflops": [gpgpu_best_throughputGflops], 
    "total_explore_duration_sec": [gpgpu_exploration_time],
    "parMapExploreStrategy": ["manual"]})
gpgpu_data

constrained_method = secondary_lift_data_nonans.head(total_constrained_points)

random_method = primary_lift_data.head(total_random_points)

duration_distributions = pd.DataFrame(data={
    "parMapExploreStrategy": ["manual", "constrained", "random"], 
    "explorationMeanPerc": [
        100,
        100,
        100],

    "execMeanPerc": [
        100 * gpgpu_total_exec_duration_sec / gpgpu_exploration_time,
        (100 * constrained_method.total_exec_duration_sec / constrained_method.total_explore_duration_sec).mean(),
        (100 * random_method.total_exec_duration_sec / random_method.total_explore_duration_sec).mean()
    ], "compileMeanPerc": [
        100 *(gpgpu_lambda_gen_duration_sec + gpgpu_compile_duration_sec) / gpgpu_exploration_time,
        (100 * constrained_method.compile_duration_sec / constrained_method.total_explore_duration_sec).mean(),
        (100 * random_method.compile_duration_sec / random_method.total_explore_duration_sec).mean()
    ], "parFuseMeanPerc": [
        0,
        (100 * constrained_method.parFuse_search_duration_sec / constrained_method.total_explore_duration_sec).mean(),
        (100 * random_method.parFuse_search_duration_sec / random_method.total_explore_duration_sec).mean()
    ]})

duration_distributions["pfAndCompMeanPerc"] = \
    duration_distributions["parFuseMeanPerc"] + duration_distributions["compileMeanPerc"]

fig, axes = plt.subplot_mosaic([['left', 'upper right'],
                               ['left', 'lower right']],
                              gridspec_kw={'width_ratios': [4, 1], 'height_ratios': [1, 4]}, 
                              figsize=(6.5, 3)) #3.6))
ax = axes["left"]

color = {
    "manual": "#ba495b",
    "constrained": "#56ae6c",
    "random": "#3B74B0"}

y_metric = "throughputExpectancySmoothened"

sns.scatterplot(
    data = gpgpu_data,
    x = "total_explore_duration_sec",
    y = "throughputGflops",
    color = [color["manual"]],
    s = 96,
    label = "emptylabel", zorder=2,
    ax = ax) #, legend=0)

sns.lineplot(
    data = throughput_expectancy_per_n_candidates_evaluated,
    x = "explorationDurationExpectancySec",
    y = y_metric,
    hue = "parMapExploreStrategy",
    palette = [color["constrained"]],#, "#ba495b"],
    hue_order = ["constrained"],
    ax = ax, zorder=2)

sns.lineplot(
    data = throughput_expectancy_per_n_candidates_evaluated,
    x = "explorationDurationExpectancySec",
    y = y_metric,
    hue = "parMapExploreStrategy",
    palette = [color["random"]],#, "#ba495b"],
    hue_order = ["random"],
    ax = ax, zorder=2) #, legend=0)

e = throughput_expectancy_per_n_candidates_evaluated

# dotted line
y = e[e.parMapExploreStrategy == "constrained"].throughputExpectancy.max()
ax.plot(
    [e[e.parMapExploreStrategy == "constrained"].explorationDurationExpectancySec.max(), 
     e.explorationDurationExpectancySec.max()], 
    [y, y], 
    linestyle='--', linewidth=1.0, color=color["constrained"],
    zorder=1)

ax.set_xlim(
    left = throughput_expectancy_per_n_candidates_evaluated["explorationDurationExpectancySec"].min() * 0.4, 
    right = throughput_expectancy_per_n_candidates_evaluated["explorationDurationExpectancySec"].max() * 2)

ax.set(xscale='log')
min_s = 60
hour_s = min_s*60
day_s = hour_s*24
week_s = day_s*7
ax.set_xticks(ticks=[3, 10, min_s, 10*min_s, 98*min_s, day_s, week_s, 5*week_s])
ax.set_yticks(ticks=[5, 10, 15, 20, 25, 30])

ax.set_xticklabels(
    labels=["3sec", "10sec", "1min", "10min", "98min", "1d", "1w", "5w"], 
    rotation=30, ha="center", va="top", rotation_mode="anchor")
ax.set_ylabel(r'\begin{center}\textbf{Throughput \lbrack GFLOPs per sec\rbrack}\end{center}')
ax.set_xlabel(r'\begin{center}\textbf{Exploration duration}\end{center}')

handles, _ = ax.get_legend_handles_labels()

ax.legend(
    [handles[2], handles[0], handles[1]],
    ["Manual", "Solver", "Random"],
    prop={'size': 10},
    handlelength = 1.0, handleheight = 1.0,
    loc="upper center", 
    bbox_to_anchor=(0.5, 1.2),
    ncol=3)


vert_offset = 5
custom_fontsize = 12
weight="black"

def annotate_n_candidates(strategy, n_candidates, extra_y_stop_offset = 0, force_flip=False):

    if strategy == "manual":
        x = gpgpu_data.iloc[0]["total_explore_duration_sec"]
        y = gpgpu_data.iloc[0]["throughputGflops"]
        extra_y_start_offset = 0.75
    else:
        all_points = e[(e["parMapExploreStrategy"] == strategy) & (e["nCandidatesEvaluated"] == n_candidates)]

        if len(all_points) == 0:
            return

        datapoint = e[(e["parMapExploreStrategy"] == strategy) & (e["nCandidatesEvaluated"] == n_candidates)].iloc[0]

        x = datapoint["explorationDurationExpectancySec"]
        y = datapoint[y_metric]
        extra_y_start_offset = 0

    flip = -1 if (force_flip or y < 10) else 1
    y_offset = y - (extra_y_stop_offset + vert_offset) * flip

    ax.plot(
        [x, x], 
        [y + extra_y_start_offset, y_offset + (0.8 if flip == 1 else -0.25)], 
        linestyle='--', linewidth=1.0, color="black", alpha=0.4, zorder=1)

    ax.text(
        x = x, 
        y = y_offset,
        s = '{:d}'.format(n_candidates),
        size = custom_fontsize, fontweight=weight, rotation="horizontal",
        horizontalalignment='center',
        verticalalignment='top' if flip == 1 else 'bottom')

annotate_n_candidates("manual", 1, force_flip=True)

# for n in [1, 10, 50, 150, 450]:
annotate_n_candidates("constrained", 1, extra_y_stop_offset = -1.5)
annotate_n_candidates("constrained", 10)
annotate_n_candidates("constrained", 50, extra_y_stop_offset = 4)
annotate_n_candidates("constrained", 150, extra_y_stop_offset = 2.5)
annotate_n_candidates("constrained", 450)

annotate_n_candidates("random", 1)
annotate_n_candidates("random", 10)
annotate_n_candidates("random", 50, extra_y_stop_offset = 3)
annotate_n_candidates("random", 150, extra_y_stop_offset = 2.5)
annotate_n_candidates("random", 450)

ax = axes["lower right"]

color = {
    "exec": "#FFD700",
    "compile": "#A4730C",
    "parFuse": "#412300"}

sns.barplot(
    x = "parMapExploreStrategy",
    y = "explorationMeanPerc",
    data = duration_distributions, color=color["exec"], edgecolor="black", linewidth=.5,
    order = ["manual", "constrained", "random"],
    ax = ax)
sns.barplot(
    x = "parMapExploreStrategy",
    y = "pfAndCompMeanPerc",
    data = duration_distributions, color=color["compile"], edgecolor="black", linewidth=.5,
    order = ["manual", "constrained", "random"],
    ax = ax)
sns.barplot(
    x = "parMapExploreStrategy",
    y = "parFuseMeanPerc",
    data = duration_distributions, color=color["parFuse"], edgecolor="black", linewidth=.5,
    order = ["manual", "constrained", "random"],
    ax = ax)

def change_width(ax, new_value) :
    for patch in ax.patches :
        current_width = patch.get_width()
        diff = current_width - new_value

        # we change the bar width
        patch.set_width(new_value)

        # we recenter the bar
        patch.set_x(patch.get_x() + diff * .5)

change_width(ax, .65)

ax.set_yticks(ticks=[0, 25, 50, 75, 100])
ax.set_xticklabels(["Manual", "Solver", "Random"])

ax.set_ylabel(r'\begin{center}\textbf{Duration \lbrack\%\rbrack}\end{center}')
ax.set_xlabel(r'\begin{center}\textbf{Parallelization\\method}\end{center}')
plt.xticks(rotation=30, ha="center", va="top", rotation_mode="anchor")


ax = axes["upper right"]
ax.axis('off')
l = ax.legend([
    matplotlib.patches.Rectangle(xy=(0,0), width=1.0, height=1.0, facecolor=color["parFuse"], edgecolor="black", linewidth=0.5),
    matplotlib.patches.Rectangle(xy=(0,0), width=1.0, height=1.0, facecolor=color["compile"], edgecolor="black", linewidth=0.5),
    matplotlib.patches.Rectangle(xy=(0,0), width=1.0, height=1.0, facecolor=color["exec"], edgecolor="black", linewidth=0.5),
    ],
    ["Parallel mapping search", r"Lift compilation", r"\begin{flushleft}OpenCL compilation\\and execution\end{flushleft}"],
    handlelength = 0.75,
    prop={'size': 10},
    loc="upper right", 
    bbox_to_anchor=(1.25, 2.165)
    )
# for txt in l.get_texts():
#     txt.set_ha("right")

plt.tight_layout(rect=(None,None,None,None))
fig.subplots_adjust(top=0.9, bottom=0.3)
plt.savefig(join(args.plot_path))
plt.show()