// Layer 10, tune point 0
// inputChannels -> 512, inputWidth -> 14, inputHeight -> 14,
// padFuncX -> 1, padFuncY -> 1,
// kernelWidth -> 3, kernelHeight -> 3,
// kernelStrideX -> 1, kernelStrideY -> 1,
// numKernels -> 512
// Vectorization
// "mapVectorize.213." -> 1,
// "mapVectorize.246." -> 1
// Unsliding
// 
// Parallelization and fusion
// "mapTransform.28." -> (10*Code.mapWrg() + Code.parDims(0)()),
// "mapTransform.29." -> (10*Code.mapWrg() + Code.parDims(1)()),
// "mapTransform.30." -> (Code.replaceInnerMapWithOuter()),
// "mapTransform.51." -> (10*Code.mapWrg() + Code.parDims(2)()),
// "mapTransform.52." -> (10*Code.mapLcl() + Code.parDims(2)()),
// "mapTransform.68." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.70." -> (10*Code.mapLcl() + Code.parDims(0)()),
// "mapTransform.109." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.110." -> (10*Code.mapLcl() + Code.parDims(0)())
// padOptRight -> 1, padOptBottom -> 1,
// tileWidth -> 5, tileHeight -> 5, tileDepth -> 16,
// seqWindowsPerThreadX -> 5, seqWindowsPerThreadY -> 1,
// seqKernelsPerThread -> 4,
// kernelGroupSize -> 4,
// windowTileWidth -> 1, windowTileHeight -> 1,
// inputCacheSizeX -> 5, inputCacheSizeY -> 1,
// inputCacheDepth -> 4,
// kernelCacheSize -> 4,
// localSizes(0) -> 4,
// localSizes(1) -> 3,
// localSizes(2) -> 5,
// nWrgs(0) -> 128,
// nWrgs(1) -> 9,
// nWrgs(2) -> 1
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__11537, global float* v__11538){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        // iteration count is exactly 1, no loop emitted
        {
            int v_gl_id_11534 = get_global_id(2); 
            // iteration count is exactly 1, no loop emitted
            {
                int v_gl_id_11535 = get_global_id(1); 
                // iteration count is exactly 1, no loop emitted
                {
                    int v_gl_id_11536 = get_global_id(0); 
                    v__11538[(v_gl_id_11536 + (512 * v_gl_id_11535) + (8704 * v_gl_id_11534))] = id(((((-1 + v_gl_id_11535) < 0) || ((-1 + v_gl_id_11535) >= 14)) ? 0 : ((((-1 + v_gl_id_11534) < 0) || ((-1 + v_gl_id_11534) >= 14)) ? 0 : v__11537[(-7680 + v_gl_id_11536 + (512 * v_gl_id_11535) + (7168 * v_gl_id_11534))]))); 
                }
            }
        }
    }
}