// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

float id(float x){
    {
        { return x; }; 
    }
}
float dotAndSumUp(float acc, float l, float r){
    {
        { return acc + dot(l, r); }; 
    }
}
float add(float x, float y){
    {
        { return x+y; }; 
    }
}
kernel __attribute((reqd_work_group_size(3,25,1)))
void KERNEL(const global float* restrict v__3930, const global float* restrict v__3931, const global float* restrict v__3932, global float* v__3951){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        local float v__3945[600];
        // Typed Value memory
        float v__3935; 
        // Private Memory
        float v__3936_0;
        float v__3936_1;
        float v__3936_2;
        float v__3936_3;
        float v__3936_4;
        float v__3936_5;
        float v__3936_6;
        float v__3936_7;
        float v__3939_0;
        float v__3940_0;
        float v__3940_1;
        float v__3940_2;
        float v__3940_3;
        float v__3940_4;
        float v__3940_5;
        float v__3940_6;
        float v__3940_7;
        float v__3947_0;
        // iteration count is exactly 1, no loop emitted
        {
            int v_wg_id_3902 = get_group_id(1); 
            // iteration count is exactly 1, no loop emitted
            {
                int v_wg_id_3903 = get_group_id(0); 
                // iteration count is exactly 1, no loop emitted
                {
                    int v_l_id_3915 = get_local_id(1); 
                    // iteration count is exactly 1, no loop emitted
                    {
                        int v_l_id_3916 = get_local_id(0); 
                        float v_tmp_4019 = 0.0f; 
                        v__3935 = v_tmp_4019; 
                        // map_seq
                        // unroll
                        // map_seq
                        // unroll
                        v__3936_0 = id(v__3935); 
                        v__3936_1 = id(v__3935); 
                        v__3936_2 = id(v__3935); 
                        v__3936_3 = id(v__3935); 
                        v__3936_4 = id(v__3935); 
                        v__3936_5 = id(v__3935); 
                        v__3936_6 = id(v__3935); 
                        v__3936_7 = id(v__3935); 
                        // end unroll
                        // end map_seq
                        // end unroll
                        // end map_seq
                        // reduce_seq
                        for (int v_i_3919 = 0; (v_i_3919 < 9); v_i_3919 = (1 + v_i_3919)){
                            // map_seq
                            // unroll
                            v__3939_0 = id(v__3932[(v_l_id_3916 + (15 * (v_wg_id_3902 % 46)) + (696 * (v_l_id_3915 / 5)) + (696 * (v_i_3919 / 3)) + (3480 * (v_wg_id_3902 / 46)) + (3 * (v_i_3919 % 3)) + (3 * (v_l_id_3915 % 5)))]); 
                            // end unroll
                            // end map_seq
                            // map_seq
                            // unroll
                            v__3940_0 = id(v__3930[(v_l_id_3916 + (3 * v_i_3919) + (216 * v_wg_id_3903))]); 
                            v__3940_1 = id(v__3930[(27 + v_l_id_3916 + (3 * v_i_3919) + (216 * v_wg_id_3903))]); 
                            v__3940_2 = id(v__3930[(54 + v_l_id_3916 + (3 * v_i_3919) + (216 * v_wg_id_3903))]); 
                            v__3940_3 = id(v__3930[(81 + v_l_id_3916 + (3 * v_i_3919) + (216 * v_wg_id_3903))]); 
                            v__3940_4 = id(v__3930[(108 + v_l_id_3916 + (3 * v_i_3919) + (216 * v_wg_id_3903))]); 
                            v__3940_5 = id(v__3930[(135 + v_l_id_3916 + (3 * v_i_3919) + (216 * v_wg_id_3903))]); 
                            v__3940_6 = id(v__3930[(162 + v_l_id_3916 + (3 * v_i_3919) + (216 * v_wg_id_3903))]); 
                            v__3940_7 = id(v__3930[(189 + v_l_id_3916 + (3 * v_i_3919) + (216 * v_wg_id_3903))]); 
                            // end unroll
                            // end map_seq
                            // map_seq
                            // unroll
                            // map_seq
                            // unroll
                            v__3936_0 = dotAndSumUp(v__3936_0, v__3939_0, v__3940_0); 
                            v__3936_1 = dotAndSumUp(v__3936_1, v__3939_0, v__3940_1); 
                            v__3936_2 = dotAndSumUp(v__3936_2, v__3939_0, v__3940_2); 
                            v__3936_3 = dotAndSumUp(v__3936_3, v__3939_0, v__3940_3); 
                            v__3936_4 = dotAndSumUp(v__3936_4, v__3939_0, v__3940_4); 
                            v__3936_5 = dotAndSumUp(v__3936_5, v__3939_0, v__3940_5); 
                            v__3936_6 = dotAndSumUp(v__3936_6, v__3939_0, v__3940_6); 
                            v__3936_7 = dotAndSumUp(v__3936_7, v__3939_0, v__3940_7); 
                            // end unroll
                            // end map_seq
                            // end unroll
                            // end map_seq
                        }
                        // end reduce_seq
                        // map_seq
                        // unroll
                        // map_seq
                        // unroll
                        v__3945[(v_l_id_3916 + (3 * v_l_id_3915))] = id(v__3936_0); 
                        v__3945[(75 + v_l_id_3916 + (3 * v_l_id_3915))] = id(v__3936_1); 
                        v__3945[(150 + v_l_id_3916 + (3 * v_l_id_3915))] = id(v__3936_2); 
                        v__3945[(225 + v_l_id_3916 + (3 * v_l_id_3915))] = id(v__3936_3); 
                        v__3945[(300 + v_l_id_3916 + (3 * v_l_id_3915))] = id(v__3936_4); 
                        v__3945[(375 + v_l_id_3916 + (3 * v_l_id_3915))] = id(v__3936_5); 
                        v__3945[(450 + v_l_id_3916 + (3 * v_l_id_3915))] = id(v__3936_6); 
                        v__3945[(525 + v_l_id_3916 + (3 * v_l_id_3915))] = id(v__3936_7); 
                        // end unroll
                        // end map_seq
                        // end unroll
                        // end map_seq
                    }
                }
                barrier(CLK_LOCAL_MEM_FENCE);
                // iteration count is exactly 1 or less, no loop emitted
                if ((get_local_id(1) < 8)){
                    int v_l_id_3926 = get_local_id(1); 
                    for (int v_l_id_3927 = get_local_id(0); (v_l_id_3927 < 25); v_l_id_3927 = (3 + v_l_id_3927)){
                        v__3947_0 = id(v__3931[(v_l_id_3926 + (8 * v_wg_id_3903))]); 
                        // reduce_seq
                        for (int v_i_3928 = 0; (v_i_3928 < 3); v_i_3928 = (1 + v_i_3928)){
                            v__3947_0 = add(v__3947_0, v__3945[(v_i_3928 + (75 * v_l_id_3926) + (3 * v_l_id_3927))]); 
                        }
                        // end reduce_seq
                        // map_seq
                        // unroll
                        v__3951[((v_l_id_3927 % 5) + (423200 * v_wg_id_3903) + (230 * (v_l_id_3927 / 5)) + (1150 * (v_wg_id_3902 / 46)) + (52900 * v_l_id_3926) + (5 * (v_wg_id_3902 % 46)))] = id(v__3947_0); 
                        // end unroll
                        // end map_seq
                    }
                }
            }
        }
    }
}
