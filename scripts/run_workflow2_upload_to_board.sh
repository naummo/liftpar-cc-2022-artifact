#!/bin/bash
set -e
set -x

if [ $# -eq 2 ]
then 

	N_POINTS_TO_EXPLORE_PER_LAYER=$1
	EXPERIMENT_LABEL=$2

	BUILD_DIR=$ROOTDIR/benchmarks/lift
	SCALA_LOG_PATH=$BUILD_DIR/scala_logs
	GENERATED_FILES_DIR=generated_files_$EXPERIMENT_LABEL

	FIRST_LAYER=0
	LAST_LAYER=10

	FIRST_POINT=0
	UNTIL_POINT=$N_POINTS_TO_EXPLORE_PER_LAYER

	RUNCONFIG_NAME=${EXPERIMENT_LABEL}_l${FIRST_LAYER}-${LAST_LAYER}_k${FIRST_POINT}-${N_POINTS_TO_EXPLORE_PER_LAYER}.csv

	mkdir -p $BUILD_DIR
	mkdir -p $BUILD_DIR/runConfigs
	mkdir -p $BUILD_DIR/metaRunConfigs
	mkdir -p $BUILD_DIR/parsed_timings
	mkdir -p $BUILD_DIR/parsed_parameters

	cd $BUILD_DIR

	# Parse the parameters of the generated kernels into a csv file
	python $ROOTDIR/scripts/log_processing/parameterLogParser.py --netname vgg --netlabel ${EXPERIMENT_LABEL} --parameter_logs_searchspace_1 $SCALA_LOG_PATH/$EXPERIMENT_LABEL/ --format1 15 --timingsdir parsed_timings --parametersdir parsed_parameters --n_threads 1

	# Generate an execution script template
	python $ROOTDIR/scripts/execution/generate_runConfigs.py --label ${EXPERIMENT_LABEL} --first_layer ${FIRST_LAYER} --last_layer ${LAST_LAYER} --first_point ${FIRST_POINT} --until_point ${UNTIL_POINT} --trials $ITERATIONS --n_boards 1 --meta --files_path ${GENERATED_FILES_DIR}

	# Generate an execution script based on the template and the kernels that were actually generated
	python $ROOTDIR/scripts/execution/compile_meta_config.py --out_config_dir runConfigs --parameter_csv parsed_parameters/${EXPERIMENT_LABEL} --metaconfig metaRunConfigs/${RUNCONFIG_NAME}

	$ROOTDIR/scripts/execution/setup_benchmark.sh --board_number 5 --monitor_top true --monitor_temp true --monitor_hanging true --bulk_upload true --use_old_data_layout true --monitor_perf_and_cool_down true --skip_prepad true --meta_run true --config_file runConfigs/${RUNCONFIG_NAME}
else
	echo "Wrong arguments supplied"
		echo "Usage: scripts/run_workflow2_upload_to_board.sh [n_points_to_execute_per_layer] [experiment_label]"
		echo ""
		echo "How many points to execute per layer?"
		echo "What is the experiment label?"
	exit -1
fi
