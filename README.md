# Mapping Parallelism in a Functional IR Artifact for CC 2022
This repository presents the artifact and workflows to supplement the paper "Mapping Parallelism in a Functional IR through Constraint Satisfaction" to be presented at the [ACM SIGPLAN International Conference on Compiler Construction](https://conf.researchr.org/home/CC-2022) in 2022.

The setup, installation and running of the workflows involved with this artifact are outlined below and it is assumed that the user is using a **Debian**-flavoured distribution of Linux.
This artifact has been tested using Ubuntu 21.04 x86_64.

**If you have any trouble installing the artifact or performing the evaluation, please create a [new issue](https://gitlab.com/naummo/liftpar-cc-2022-artifact/-/issues/new) on this repository or contact the [authors](#authors) directly via email.**

## Table of Contents
* [1. Hardware Requirements](#1-hardware-requirements)
* [2. Software requirements](#2-software-requirements)
* [3. Installation](#3-Installation)
* [4. Workflows to evaluate the artifact](#4-workflows) 
* [5. Reuse and Repurposing](#5-reuse-and-repurposing)

## 1. Hardware Requirements
To evaluate this artifact you will need a host machine to install this artifact on, and access to a board with Mali G72 running Linux. 
This artifact was evaluated using Hikey 970 with HiSilicon Kirin 970 SoC, but it can also be reconfigured to generate parallel kernels for other platforms with OpenCL GPUs. Section 5 discusses ways to optimize the search for other hardware specifications; the setup instructions should work on other Linux-based platforms.

## 2. Software Requirements

This artifact can be installed using Docker or directly on a Linux machine.
There are instructions on how to install these requirements in later sections. 

* Host machine
  * git
  * [git-lfs](https://git-lfs.github.com/)
  * wget
  * curl
  * For the Docker installation:
    * Docker engine
  * For the alternative direct installation:
    * Python 3+ (including: matplotlib, seaborn, scipy, pandas)
    * Texlive LaTeX (including: dvipng, texlive-latex-extra, texlive-fonts-recommended, cm-super)
    * Rsync
    * [Lift](https://github.com/lift-project/lift), which requires:
        * Java 8 SDK
        * SBT
        * CMake (3.8+)
* OpenCL GPU device
  * Debian OS 
  * OpenCL library
  * [gcc-linaro-7.2.1-2017.11-x86_64_aarch64-linux-gnu](https://releases.linaro.org/components/toolchain/binaries/7.2-2017.11/aarch64-linux-gnu/gcc-linaro-7.2.1-2017.11-x86_64_aarch64-linux-gnu.tar.xz)


## 3. Installation

Install `git`, `git-lfs`, `curl` and `wget`:
```
sudo apt-get update
sudo apt-get install git curl wget
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
sudo apt-get install git-lfs
git lfs install
```
Now that both `git` and `git-lfs` are installed, clone and enter the artifact repository:
```
git clone git@gitlab.com:naummo/liftpar-cc-2022-artifact.git
cd liftpar-cc-2022-artifact
```

### 3.1. Host: Docker

Install the Docker engine from [https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/).

Start the Docker daemon and build our artifact image:
```
sudo systemctl start docker.service
docker build . -t liftpar
```
Enter the docker container and change the directory:
```
docker run -it liftpar
cd $ROOTDIR
```
From here on out `$ROOTDIR` will refer to the path where the workspace is set up inside the container, i.e. `/home`.

The host is now set up; proceed to setting up the OpenCL GPU board as per Section 3.3.

### 3.2. Host: Direct Installation

Initialize the environment variables:
```
source environment.env
```
From here on out `$ROOTDIR` will refer to the path where this repository resides.

### 3.2.1. Install Rsync
```
sudo apt-get install rsync
``` 

### 3.2.2. Install LaTeX 
Any version of LaTeX supported by Python's Matplotlib would do. A good choice is following:
```
  sudo apt-get install dvipng texlive-latex-extra texlive-fonts-recommended cm-super
```

### 3.2.3. Install Python and Packages
Any 3+ Python installation should support this artifact. You can proceed using Anaconda and encapsulating all dependencies in an environment. Anaconda can be downloaded from 
[https://www.anaconda.com/products/individual](https://www.anaconda.com/products/individual). 
We used Python 3.9 64-Bit (x86) Installer (581 MB) for Linux. Anaconda can be installed as follows:
  ```
  wget https://repo.anaconda.com/archive/Anaconda3-2021.11-Linux-x86_64.sh
  chmod +x Anaconda3-2021.11-Linux-x86_64.sh
  ./Anaconda3-2021.11-Linux-x86_64.sh
  ```

Create and activate a new `conda` environment:
  ```
  conda create -n liftpar python=3.9
  conda activate liftpar
  ```

Install the dependencies:
  ```
  pip install matplotlib seaborn scipy pandas
  ```

### 3.2.4. Install Lift
Lift requires the Java 8 SDK, the Scala Build Tool (SBT), and CMake (3.8+).

1. To install the Java 8 SDK:
  ```
  sudo apt-get install openjdk-8-jdk wget
  ```
  Alternatively, follow these instructions to install the Oracle Java 8 SDK: https://docs.datastax.com/en/jdk-install/doc/jdk-install/installOracleJdkDeb.html

2. To install SBT:
  ```
  $ROOTDIR/scripts/installation/build_sbt.sh
  ```

3. To install CMake:
  ```
  $ROOTDIR/scripts/installation/build_cmake.sh
  ```
  
4. To install _Lift_ itself:
  ```
  $ROOTDIR/scripts/installation/build_lift.sh
  ```

### 3.3. Set up the OpenCL GPU Board
The benchmarks should be runnable with any Linux-based OpenCL GPU board. Here we provide instructions for the Hikey 970 boaard.

The Hikey board must be running Lebian 9 since it includes OpenCL library. Since the deprecation of the official mirror, the OS can be downloaded from the following two sources:

  [https://www.bwbot.org/s/sgRMKZ](https://www.bwbot.org/s/sgRMKZ)

  [https://mega.nz/file/9RwiGbbK#e-_XpNeEb0N_VinNacO9Xxj8RvFDsWRz5NqkAd08pQM](https://mega.nz/file/9RwiGbbK#e-_XpNeEb0N_VinNacO9Xxj8RvFDsWRz5NqkAd08pQM)

The following tutorials on setting up Hikey 970 are heplful:

[https://github.com/wincle626/Hikey970_Linux_Installation](https://github.com/wincle626/Hikey970_Linux_Installation)

[https://doc.bwbot.org/zh-cn/books-online/hikey970-doc/topic/547.html](https://doc.bwbot.org/zh-cn/books-online/hikey970-doc/topic/547.html) (in Chinese)

You can install `clinfo` using `sudo apt-get install clinfo` to confirm OpenCL installation.

Other Linux-based boards with a GPU should work with this artifact as long as OpenCL and gcc is installed.

#### 3.7. ARM Compute Library and TVM
We don't expect the valuators of this artifact to reproduce ARM Compute Library and TVM results, and we include their results in CSV files in `$ROOTDIR/benchmarks/arm_compute_tvm/`.
However, they can be reproduced as described in sections 5.1.2 and 5.1.3 of our paper.

The following documentation is helpful for setting up ARM Compute:
https://arm-software.github.io/ComputeLibrary/latest/how_to_build.xhtml#S1_2_linux

We used the following version of gcc with ARM Compute: [gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu](https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-a/10.3-2021.07/binrel/gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu.tar.xz)

This is a good tutorial on setting up TVM on Hikey 960 and 970:
https://www.96boards.org/blog/nnvm-tvm-on-hikey960/



## 4. Workflows

There are two workflows to validate this artifact:
1. Reproduce the _Lift_ VGG-16 results reported in Figure 4 of our paper by rerunning the best implementations we found through exploration of the design space of parallel mappings.
2. Reproduce the results reported in Figures 4 and 5 of our paper by rediscovering the best implementations through time-intensive design space exploration. The search can be reconfigured for other Mali boards that are not Hikey 970 and have different limitations.

We expect the evaluators of this artifact to perform the first workflow fully.
We included the scripts to perform the second workflow and invite evaluators to perform this workflow to see how it works, but we do not expect evaluators to perform the same experiment as we did for the paper as this takes around **30 hours** to complete.

### 4.1. Workflow One

#### 4.1.1. Reproduce Figure 4

The experimental setup we used is explained in detail in Section 5 of the paper.
Running all scripts **should not take more than 10 minutes**.

* Preparation: To reproduce our best-found results:
  * You can change the number of `ITERATIONS` performed for each _Lift_ candidate implementation. We report median numbers and the default `ITERATIONS` value is 3. To change the number of iterations:
      ```
      export ITERATIONS=<number_of_iterations>
      ```
  * Authenticate with the Mali board to avoid having to enter your password on each file transfer:
    ```
    ssh-copy-id -i ~/.ssh/id_rsa.pub -p <ssh_port> <board_user>@<board_address>
    ```
  * Record board's address and port in the respective fields in `$ROOTDIR/scripts/execution/get_board_ip.sh`
  * Save the default board username in your `~/.ssh/config`:
    ```
    Host <board_address>
          HostName <board_address>
          User <board_user>
    ```

* To reproduce the results reported in Figure 4:
  * Run:
    ```
    $ROOTDIR/scripts/run_workflow1_upload_to_board.sh
    ```
  * Once the files are transferred to the board and the script returns, ssh into the board and start the execution:
    ```
    ssh <board_address>
    cd ~/cc22-artifact/generated_files_best_found_kernels
    screen
    sudo rm -f last_run_point.txt && sudo ./runAll.sh
    ```
  * `screen` is required so that the execution doesn't stop if the SSH connection drops. You can detach and reattach to the `screen` with `Ctrl+A,D` and `screen -r` respectively.
  * The connection might drop if the board detects performance drop due to high temperature and reboots itself to cool down. The execution will continue after the reboot automatically.
  * Once the execution is finished, disconnect from the board and fetch the execution logs:
    ```
    $ROOTDIR/scripts/execution/fetch_board_logs.sh --board_number 5 --network_name vgg --network_label best_found_kernels
    ```
  * Finally, generate Figure 4 as follows:
    ```
    $ROOTDIR/scripts/run_workflow1_figure4.sh
    ```
  The generated plot can be found in `$ROOTDIR/plots/workflow1_figure4.pdf`.
  The experiment executes one implementation per each unique convolution layer of VGG-16 `$ITERATIONS` (an environment variable) number of times.


#### 4.1.2. Verification

##### Implementations

The best-performing implementations can be found in the following directory:
  ```
  $ROOTDIR/benchmarks/lift/generated_files/generated_files_best_found_kernels/generated_files_best_found_kernels_0/<layer_id>/0/
  ```
Where layer ID is the order in which the layer appears in VGG-16 among the convolutional layers only. Note that the duplicate layer configurations are skipped.

Each solution includes two implementations:
  * A full solution with data pre-padding and de-padding consisting of the host code (`libconv_all_kernels.cpp`) and 3 OpenCL kernels for preprocessing, convolution and postprocessing.
  * A partial solution with only convolution consisting of the host code (`libconv.cpp`) and 1 OpenCL kernel.
For example, the convolution kernel for Layer 8 can be accessed here:

    ```
    $ROOTDIR/benchmarks/lift/generated_files/generated_files_best_found_kernels/generated_files_best_found_kernels_0/8/0/kernel_22792.cl
    ```

##### Lift Convolution Expressions
The high-level lambda described in Listing 1 in our paper, that was rewritten and parallelized to generate the best-found kernels can be examined on line 45 here:
```
$ROOTDIR/tools/lift/src/main/benchmarks/conv/conv_lambdas/DirectConvLambda.scala
```

The rewritten derivative lambda that was parallelized can be examined here:
```
$ROOTDIR/tools/lift/src/main/benchmarks/conv/conv_lambdas/DirectConvRewritten.scala
```
Note that at his stage all instances of `Map` are abstract, and the parallelization decision (i.e. replacing with `MapGlb`, `MapWrg`, `MapLcl` and `MapSeq`) is yet to be made.

##### Parallelization Constraints
The implementation of the production rules for constraint generation described in Section 4 can be found here:
```
$ROOTDIR/tools/lift/src/main/benchmarks/conv/passes/ParallelizationAndFusion.scala
```

Rule 1 is implemented on line 540, Rule 2: line 569, Rule 3: line 223, Rules 4 and 5: line 255, Rule 6: line 506, Rule 7: line 329.

##### Functional Correctness
Functional correctness is confirmed by comparing the produced outputs with pre-generated gold results produced using PyTorch stored in `$ROOTDIR/golden_values`. They can also be checked against the manually written C++ convolution implementation provided in `$ROOTDIR/scripts/execution/test_harness.cpp:91`. To rerun the results with the handwritten comparison implementation, enable the `USE_GOLD_FUN` on line 89 in `test_harness.cpp` and rerun the kernels.

Both inputs and weights were generated randomly and provided as binary files.

##### Memory Consumption
The memory consumption is calculated based on the amount of padding chosen during the tuning stage, and based on the extra local memory buffers allocated by _Lift_. These numbers are hardcoded in `$ROOTDIR/scripts/log_processing/plot_figure4.py` on line 211. They can be verified by checking static local memory allocation in the generated kernel. For example `kernel_22792.cl` above allocates 6144 floats locally on line 75.

##### ARM Compute Library and TVM
The comparison timings and memory consumption of the ARM Compute Library and TVM can be examined in `$ROOTDIR/benchmarks/arm_compute_tvm/`. They were collected using the setup described in sections 5.1.2 and 5.1.3 of our paper.

This workflow was fairly quickly executable since we only reran the best versions of the kernels we generated. 
In the following, we explain how to completely redo the experiments, which requires significantly more time.

### 4.2. Workflow Two

#### 4.2.1. Design Space Exploration

In the previous workflow, only the best-found _Lift_-generated OpenCL kernels for each convolutional layer have been executed.
In this workflow, we reexplore the parallelization space by generating and executing many OpenCL kernels using _Lift_.
Reproducing both figures should take around **30 hours**. Reproducing only Figure 5 should take around **3.5 hours** since only one VGG-16 layer exploration needs to be timed, while Figure 4 requires exploring all the 9 layer configurations.

1. Before generating the kernels, we need to decide how many candidates (points) to try per each of the 9 layer configurations. The best results reported in Figure 4 were found by exploring 500 points per layer which took **4.5 hours** to find parallel mappings and generate the kernels and **25.5 hours** to execute. Each implementation is executed `$ITERATIONS` times (3 by default) (an environment variable).

2. The parallel mapping search is random, but we can set the seed used to initialize the randomizer. This seed can be changed in the `randomSeed` field of the following configuration file:
    ```
    $ROOTDIR/tools/lift/src/main/benchmarks/conv/settings/envSettings.json
    ```

3. If you are using a Mali board other than Hikey 970, you can retarget the space exploration for your device characteristics. To achieve this, change the respective fields in `envSettings.json` above. The reconfigurable settings include:
    * Maximum allowed number of OpenCL threads per each dimension of the workgroup size.
    * Maximum OpenCL workgroup size.
    * Maximum number of registers that can be used before spilling occurs.
    * Maximum global and local memory allocation per buffer and in total.

4. If you intend to reproduce Figure 4, and have time to perform the search for all layers, proceed to the next step. If you want to perform the search only for the layer required for Figure 5 -- which will take you **30 minutes** to search and compile the candidates and **3.5 hours** to execute -- set `FIRST_LAYER` and `LAST_LAYER` both to `8` on lines 15 and 16 in the following files: 
    ```
    $ROOTDIR/scripts/run_workflow2_kernel_generation.sh
    $ROOTDIR/scripts/run_workflow2_upload_to_board.sh
    ```
    Layer configuration 8 corresponds to VGG-16 layers 19 and 21.

    When you want to perform the full search, reset `FIRST_LAYER` and `LAST_LAYER` to 0 and 10 respectively.

5. To perform the search and generate the implementations, execute the following script:
    ```
    export LABEL=<experiment_label>
    $ROOTDIR/scripts/run_workflow2_kernel_generation.sh <candidates_per_layer> $LABEL
    ```
    Where `experiment_label` is the name you can choose for this experiment. To rerun the experiment, choose another name to preserve the previous results.
    
    For example:
    ```
    export LABEL=parmapping_search0
    $ROOTDIR/scripts/run_workflow2_kernel_generation.sh 500 $LABEL
    ```

    * Please make sure that the machine running the search does not go into sleep mode during the process since this will skew search timings.

    * There is a code generation timeout in _Lift_, that aborts constraint satisfaction and code generation if it takes too long, and proceeds to the next candidate. By default the timeout is 10 seconds, which is sufficient on the platforms similar to Intel Core i7-2620M with 8 GB RAM, Intel Core i7-4770K with 16 GB RAM and Intel Xeon E5-2630v3 with 8 GB RAM. However, if you observe that too many candidates are skipped due to timeout, you can increase the compilation timeout in `$ROOTDIR/scripts/generation/generate_lift_kernels_in_par_search_space.sh` on line 30, using the parameter `compilation_timeout` (in seconds).

6. The script will take time to generate all kernels. The script will alternate between all convolutional layers, generating 200 kernels for each layer at a time. You can interrupt the search early by killing the script and still be able to use the implementations generated so far. The script will report how many kernels it has generated so far, but you can also check the generated files in a separate terminal yourself:
    ```
    $ROOTDIR/benchmarks/lift/generated_files/generated_files_$LABEL/generated_files_$LABEL_0/<layer_number>/<candidate_number>/
    ```
    You can proceed to the next stage of uploading the generated kernels in a separate terminal before generation completes, if enough kernels have been generated.

7. Generated kernels can now be uploaded to the Mali board by executing the following:
    ```
    $ROOTDIR/scripts/run_workflow2_upload_to_board.sh <candidates_per_layer> $LABEL
    ```
    Where `candidates_per_layer` can be lower than in step 4 if you killed the search prematurely. 

    This will parse the exploration log, extract all the parallelization and tuning parameter values, generate execution configuration script and transfer all files (implementations, inputs, weights, gold) to the board. Once the script returns, ssh into the board and start the execution:
    ```
    ssh <board_address> -p <ssh_port>
    export LABEL=<experiment_label>
    cd ~/cc22-artifact/generated_files_$LABEL
    screen
    sudo rm -f last_run_point.txt && sudo ./runAll.sh
    ```
    `experiment_label` must match the one used in step 4.
  
    The connection might drop if the board detects performance drop due to high temperature and reboots itself to cool down. The execution will continue after the reboot automatically.
  
    The screen is required so that the execution doesn't stop if the SSH connection drops. You can detach and reattach to the screen with `Ctrl+A,D` and `screen -r` respectively.
  
  
    Once the execution is finished, disconnect from the board and fetch the execution logs:
    ```
    $ROOTDIR/scripts/execution/fetch_board_logs.sh --board_number 5 --network_name vgg --network_label $LABEL
    ```

#### 4.2.2. Reproduce Figure 4
    
  Figure 4 can be generated based on the results of exploration of all nine layer configurations collected in the previous stage as follows:
  ```
  $ROOTDIR/scripts/run_workflow2_figure4.sh $LABEL
  ```
  The generated plot can be found in `$ROOTDIR/plots/workflow2_figure4.pdf`.

#### 4.2.3. Reproduce Figure 5
    
  Figure 5 can be generated based on the timings collected both from the generation and execution of layer configuration 8 (referred to as layers 19 and 21 in the paper) as follows:
  ```
  $ROOTDIR/scripts/run_workflow2_figure5.sh $LABEL
  ```
  The generated plot can be found in `$ROOTDIR/plots/workflow2_figure4.pdf`.

#### 4.2.4. Verification
  Best reported execution timings can be verified using the execution logs in the following directory:
  ```
  $ROOTDIR/benchmarks/lift/board_logs/vgg/$LABEL/generated_files_$LABEL/
  ```
  Generation timings can be verified using the generation logs in the following directory:
  ```
  $ROOTDIR/benchmarks/lift/scala_logs/$LABEL/
  ```
  Memory consumption can be verified using the generated files:
  ```
  $ROOTDIR/benchmarks/lift/generated_files/generated_files_$LABEL/generated_files_$LABEL_0/<layer_number>/<candidate_number>/
  ```

  The manually parallelized _Lift_ expression for convolution that was used to produce the "Manual" data point on Figure 5 can be examined on line 475 here:
  ```
  $ROOTDIR/tools/lift/src/main/benchmarks/conv/gpgpu20/ConvStencil3D.scala
  ```
  
## 5. Reuse and Repurposing

### 5.1. Reuse
We created this artifact in a modular way to facilitate using our results as a comparison point for future research.
Here we describe the organization of our scripts and where the performance results are stored.

Our workflow scripts is split into separate stages which can be executed on their own:
1. Script to run _Lift_ exploration: `$ROOTDIR/scripts/generation/generate_lift_kernels_in_par_search_space.sh`
   
    Example usage: `$ROOTDIR/scripts/run_workflow2_kernel_generation.sh`

2. Script for parsing the generation logs: `$ROOTDIR/scripts/log_processing/parameterLogParser.py`

    Example usage: `$ROOTDIR/scripts/run_workflow2_upload_to_board.sh`

3. Scripts to create the execution scripts for a given experiment: `$ROOTDIR/scripts/execution/{generate_runConfigs.py, compile_meta_config.py}`
    
    Example usage: `$ROOTDIR/scripts/run_workflow2_upload_to_board.sh`

4. Script to set up execution environment on the board and upload the files: `$ROOTDIR/scripts/execution/setup_benchmark.sh`
    
    Example usage: `$ROOTDIR/scripts/run_workflow2_upload_to_board.sh`

5. Script to fetch the execution logs from the board: `$ROOTDIR/scripts/execution/fetch_board_logs.sh`

6. Scripts to parse and combine results from execution and generation logs:
    ```
    $ROOTDIR/scripts/log_processing/logsToCsv.py
    $ROOTDIR/scripts/log_processing/timeCsvCollator.py
    $ROOTDIR/scripts/log_processing/aggregatorOfTimingsAndParameters.py
    ```

    Example usage: `$ROOTDIR/scripts/run_workflow2_figure5.sh`

7. Plotting scripts:
    ```
    $ROOTDIR/scripts/log_processing/plot_figure4.py
    $ROOTDIR/scripts/log_processing/plot_figure5.py
    ```

    Example Usage: `$ROOTDIR/scripts/run_workflow2_figure{4, 5}.sh`

### 5.2. Repurposing

#### 5.2.1. Applications

This artifact uses direct convolution as an example of complex parallelization problem. 
However, our methodology is not specific to convolution: the generic IR of _Lift_ can be used to express other applications to be parallelized through constraint satisfaction.

Example convolution expressions can be accessed here:
```
$ROOTDIR/tools/lift/src/main/benchmarks/conv/conv_lambdas/DirectConvLambda.scala
$ROOTDIR/tools/lift/src/main/benchmarks/conv/conv_lambdas/DirectConvRewritten.scala
```

Examples of other applications expressed in _Lift_ in prior work include GEMM, Acoustic stencils, K-Means, N-Body and Jacobi (Hagedorn et al, 2018). These and other examples can be accessed here:
```
$ROOTDIR/tools/lift/src/main/benchmarks/
```
These examples are expressed using the same language primitives as convolution, so parallelizing them using this artifact shouldn't require much engineering.

#### 5.2.2. Hardware & Programming Models
  The automatic search of parallel mappings can be reconfigured to target hardware other than Mali G72 GPU. We provide two ways of customizing the search:
  - Setting the specifications of the target hardware such as work group size and memory allocation limits. Section 4.2.1 of this document describes how to adjust these.
  - Adding new parallelization rules and heuristics for other devices (e.g. desktop GPUs) and programming models. Section 4.1.2 (Parallelization Constraints) points to the implementation of the current constraint production rules, which can be adapted to other platforms.
    - For message-passing programming models such as OpenMP, synchronization presents harder challenge. Our method could be applied to this problem by adding extra synchronization constraints defining valid message-passing parallelization.

## Authors
* Naums Mogers, University of Edinburgh ([naums.mogers@ed.ac.uk](mailto:naums.mogers@ed.ac.uk))
* Lu Li, University of Edinburgh ([lu.li@ed.ac.uk](mailto:lu.li@ed.ac.uk))
* Valentin Radu, University of Sheffield ([v.radu@sheffield.ac.uk](mailto:v.radu@sheffield.ac.uk))
* Christoph Dubach, McGill University ([christophe.dubach@mcgill.ca](mailto:christophe.dubach@mcgill.ca))

## Acknowledgements
We thank the entire [Lift team](http://www.lift-project.org/index.html#team) for their development efforts on the Lift compiler.

We thank the creators of the CGO 2018 artifact ([https://gitlab.com/larisa.stoltzfus/liftstencil-cgo2018-artifact](https://gitlab.com/larisa.stoltzfus/liftstencil-cgo2018-artifact)) which was used as a basis of this artifact: Bastian Hagedorn, Larisa Stoltzfus, Michel Steuwer, Sergei Gorlatch, Christophe Dubach.

This work was supported by the Engineering and Physical Sciences Research Council (grant EP/L01503X/1), EPSRC Centre for Doctoral Training in Pervasive Parallelism at the University of Edinburgh, School of Informatics.
This work has made use of the resources provided by the Edinburgh Compute and Data Facility (ECDF) ([http://www.ecdf.ed.ac.uk/](http://www.ecdf.ed.ac.uk/)).

We also acknowledge the support of the Natural Sciences and Engineering Research Council of Canada (NSERC) Discovery Grants Program [grant RGPIN-2020-05889], and the Canada CIFAR AI Chairs Program.