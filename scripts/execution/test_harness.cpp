#define CL_HPP_TARGET_OPENCL_VERSION 200
#define CL_TARGET_OPENCL_VERSION 200
#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <stdlib.h>
#include <assert.h>

#include <numeric>
#include <iostream>
#include <vector>
#include <functional>

#define IM2COL IM2COL_VAL
#define PART_CONV PARTCONV

#if SKIP_VERIFICATION_VAL == false
#define PERFORM_VERIFICATION
#endif

#if USE_OLD_DATA_LAYOUT_VAL == 1
    #define USE_OLD_DATA_LAYOUT
#endif

#if IM2COL_VAL == true
    #define USE_IM2COL
#endif

// This comment is outdated:
// If we don't want func verification, only one kernel is run (part or full conv)
// If we do want func verification:
// If the convolution is done in one kernel and there is no opt padding, only one kernel is run (we use prepadded inputs and there is no depadding to do)
// If the convolution is done in one kernel, but there is opt padding, three kernels are run: pad + conv + depad
// if the convolution is split in partial and full convolution, everything is done in 4 kernels (pad, part conv, final conv, depad)
#ifdef PERFORM_VERIFICATION
    #ifndef USE_IM2COL
        // #if PART_CONV == 1 || PAD_OPT_RIGHT != 0 || PAD_OPT_BOTTOM != 0
        #define RUN_MULTI_KERNEL_FUNC_VERIFICATION
        // #endif
    #endif
#endif


#ifdef RUN_MULTI_KERNEL_FUNC_VERIFICATION
    #include "./libconv_all_kernels.cpp"
#else
    #include "./libconv.cpp"
#endif

#include "BOARD_WD/layer_configs/NETWORK_NAME/LAYER_ID/layerconfig.cpp"

float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
float add(float x, float y){
    {
        { return x+y; }; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}

void print_time() {
    using std::chrono::system_clock;
    auto currentTime = std::chrono::system_clock::now();
    char buffer[80];

    auto transformed = currentTime.time_since_epoch().count() / 1000000;

    auto millis = transformed % 1000;

    std::time_t tt;
    tt = system_clock::to_time_t ( currentTime );
    auto timeinfo = localtime (&tt);
    strftime (buffer,80,"%F %H:%M:%S",timeinfo);
    sprintf(buffer, "%s:%03d",buffer,(int)millis);
    std::cout << std::string(buffer) << std::endl;
}

// int width = 224+2+4; //230
// int height = 224+2+16; // 242

// #define USE_GOLD_FUN
#ifdef USE_GOLD_FUN
float conv(float* inputs, float* weights, int oy, int ox, int oo) {
    float result = 0;
    float inp = 0;
    for (int y=-1; y <= 1; y++) {
        for (int x=-1; x <= 1; x++) {
            for (int iCh=0; iCh < layerConfig.in_channels; iCh++) {
                if (ox+x < 0 || ox+x >= layerConfig.input_WH || 
                    oy+y < 0 || oy+y >= layerConfig.input_WH)
                    inp = 0;
                else 
                    inp = inputs[iCh + (ox+x) * layerConfig.in_channels + (oy+y) * layerConfig.in_channels * layerConfig.input_WH];
                result += inp * 
                    weights[iCh + (x+1) * layerConfig.in_channels + (y+1) * layerConfig.in_channels * 3 + oo * layerConfig.in_channels * 3 * 3];
            }
        }
    }

    return result;
}

float conv_vect_old_layout(float* inputs, float* weights, int oy, int ox, int oo) {

    if (layerConfig.in_channels < 4 || layerConfig.in_channels % 4 != 0) {
        fprintf(stderr, "\nlayerConfig.in_channels < 4 || layerConfig.in_channels % 4 != 0\n");
        exit(1);
    }
    
    float result = 0;
    float dot = 0;
    for (int y=-1; y <= 1; y++) {
        for (int x=-1; x <= 1; x++) {
            for (int iCh=0; iCh < layerConfig.in_channels; iCh += 4) {
                if (ox+x < 0 || ox+x >= layerConfig.input_WH || 
                    oy+y < 0 || oy+y >= layerConfig.input_WH)
                    dot = 0;
                else {
                    dot = 0;
                    // for (int v = 0; v < 4; v++) {
                    //     dot += inputs[v + iCh + (ox+x) * layerConfig.in_channels + (oy+y) * layerConfig.in_channels * layerConfig.input_WH] *
                    //              weights[v + iCh + (x+1) * layerConfig.in_channels + (y+1) * layerConfig.in_channels * 3 + oo * layerConfig.in_channels * 3 * 3];
                    // }
                    std::vector<float> a{
                        inputs[0 + iCh + (ox+x) * layerConfig.in_channels + (oy+y) * layerConfig.in_channels * layerConfig.input_WH],
                        inputs[1 + iCh + (ox+x) * layerConfig.in_channels + (oy+y) * layerConfig.in_channels * layerConfig.input_WH],
                        inputs[2 + iCh + (ox+x) * layerConfig.in_channels + (oy+y) * layerConfig.in_channels * layerConfig.input_WH],
                        inputs[3 + iCh + (ox+x) * layerConfig.in_channels + (oy+y) * layerConfig.in_channels * layerConfig.input_WH]};

                    std::vector<float> b{
                        weights[0 + iCh + (x+1) * layerConfig.in_channels + (y+1) * layerConfig.in_channels * 3 + oo * layerConfig.in_channels * 3 * 3],
                        weights[1 + iCh + (x+1) * layerConfig.in_channels + (y+1) * layerConfig.in_channels * 3 + oo * layerConfig.in_channels * 3 * 3],
                        weights[2 + iCh + (x+1) * layerConfig.in_channels + (y+1) * layerConfig.in_channels * 3 + oo * layerConfig.in_channels * 3 * 3],
                        weights[3 + iCh + (x+1) * layerConfig.in_channels + (y+1) * layerConfig.in_channels * 3 + oo * layerConfig.in_channels * 3 * 3]};

                    dot = std::inner_product(a.begin(), a.end(), b.begin(), 0.0f);
                }
                result += dot;
            }
        }
    }

    return result;
}

float conv_new_layout(float* inputs, float* weights, int oy, int ox, int oo) {
    float result = 0;
    float inp = 0;
    for (int iCh=0; iCh < layerConfig.in_channels; iCh++) {
        for (int y=-1; y <= 1; y++) {
            for (int x=-1; x <= 1; x++) {
                if (ox+x < 0 || ox+x >= layerConfig.input_WH || 
                    oy+y < 0 || oy+y >= layerConfig.input_WH)
                    inp = 0;
                else 
                    // inp = inputs[iCh + (ox+x) * layerConfig.in_channels + (oy+y) * layerConfig.in_channels * layerConfig.input_WH];
                    inp = inputs[(ox+x) + (oy+y) * layerConfig.input_WH + iCh * layerConfig.input_WH * layerConfig.input_WH];
                result += inp * 
                    // weights[iCh + (x+1) * layerConfig.in_channels + (y+1) * layerConfig.in_channels * 3 + oo * layerConfig.in_channels * 3 * 3];
                    weights[(x+1) + (y+1) * 3 + iCh * 3 * 3 + oo * layerConfig.in_channels * 3 * 3];
            }
        }
    }

    return result;
}
#endif


void read_file(string file_path, 
               string label, 
               float** buffer,
               int buffer_size) {
    ifstream ifs(file_path, std::ios::binary);

    *buffer = (float*)malloc(buffer_size * sizeof(float));

    fprintf(stderr, "Reading %s... ", file_path.c_str());
    if (!ifs.is_open()) {
        std::cerr << "\nCouldn't open the " << label << " file. Exiting.\n";
        exit(1);
    }
    // istream_iterator<float> start_x(random_x_file), end_x;
    // copy(start_x, end_x, input_X.begin());
    ifs.read(reinterpret_cast<char*>(*buffer), (buffer_size) * sizeof(float));
    if (!ifs) {
        fprintf(stderr, "\nSomething went wrong when reading the file. Bytes read: %d\n", ifs.gcount());
        exit(1);
    }
    fprintf(stderr, "Done.\n");
}


int main( int argc, char** argv ) {
    fprintf(stderr, "Using CORRECT test_harness.cpp linked to by test_harness_gemm_conv.cpp\n");

	assert(argc == 2);
	const std::string kernel_path(argv[1]);

	lift_init(kernel_path);

    const int input_W_padded = layerConfig.input_WH + layerConfig.input_pad*2;
    const int input_H_padded = layerConfig.input_WH + layerConfig.input_pad*2;
    const int input_W_optpadded = input_W_padded + PAD_OPT_RIGHT;
    const int input_H_optpadded = input_H_padded + PAD_OPT_BOTTOM;
    const int output_W = (input_W_padded - (layerConfig.kernel_WH - layerConfig.kernel_stride)) / layerConfig.kernel_stride;
    const int output_H = (input_H_padded - (layerConfig.kernel_WH - layerConfig.kernel_stride)) / layerConfig.kernel_stride;
    const int output_W_optpadded = (input_W_optpadded - (layerConfig.kernel_WH - layerConfig.kernel_stride)) / layerConfig.kernel_stride;
    const int output_H_optpadded = (input_H_optpadded - (layerConfig.kernel_WH - layerConfig.kernel_stride)) / layerConfig.kernel_stride;
    const unsigned int platform_id = 0, device_id = 0;

    const int input_size_nopad_no_im2col = layerConfig.n_inputs * layerConfig.input_WH * layerConfig.input_WH * layerConfig.in_channels;
    const int input_size_padded_no_im2col = layerConfig.n_inputs * input_W_optpadded * input_H_optpadded * layerConfig.in_channels;
    const int input_size_padded_im2col = layerConfig.n_inputs * output_H_optpadded * output_W_optpadded * layerConfig.in_channels * layerConfig.kernel_WH * layerConfig.kernel_WH;

    string input_nopad_no_im2col_new_layout_path = "BOARD_WD/randomeX/NETWORK_NAME/LAYER_ID/random_x.bin";
    string input_nopad_no_im2col_old_layout_path = "BOARD_WD/randomeX/NETWORK_NAME/LAYER_ID/random_x_old_data_layout.bin";
    string input_padded_no_im2col_new_layout_path = "BOARD_WD/randomeX/NETWORK_NAME/LAYER_ID/random_x_padded_padopt_right_PAD_OPT_RIGHT_bottom_PAD_OPT_BOTTOM.bin";
    string input_padded_no_im2col_old_layout_path = "BOARD_WD/randomeX/NETWORK_NAME/LAYER_ID/random_x_padded_padopt_right_PAD_OPT_RIGHT_bottom_PAD_OPT_BOTTOM_old_data_layout.bin";
    
    string input_nopad_im2col_new_layout_path = "not_implemented";
    string input_nopad_im2col_old_layout_path = "not_implemented";
    string input_padded_im2col_new_layout_path = "not_implemented";
    string input_padded_im2col_old_layout_path = "BOARD_WD/randomeX/NETWORK_NAME/LAYER_ID/random_x_padded_padopt_right_1_bottom_1_old_data_layout_im2col.bin";

    float* input_X;
    float* input_X_noim2col;

#ifdef RUN_MULTI_KERNEL_FUNC_VERIFICATION
    // no prepadding
    #ifdef USE_OLD_DATA_LAYOUT
        // Old data layout
        #ifdef USE_IM2COL
            fprintf(stderr, "Error, fail: not implemented: RUN_MULTI_KERNEL_FUNC_VERIFICATION and USE_OLD_DATA_LAYOUT and USE_IM2COL");
            exit(1);
        #else
            read_file(input_nopad_no_im2col_old_layout_path, "inputs", &input_X, input_size_nopad_no_im2col);
        #endif
    #else
        // New data layout
        #ifdef USE_IM2COL
            fprintf(stderr, "Error, fail: not implemented: RUN_MULTI_KERNEL_FUNC_VERIFICATION and (NOT USE_OLD_DATA_LAYOUT) and USE_IM2COL");
        #else
            read_file(input_nopad_no_im2col_new_layout_path, "inputs", &input_X, input_size_nopad_no_im2col);
        #endif
    #endif

#else
    // yes to prepadding
    #ifdef USE_OLD_DATA_LAYOUT
        // Old data layout
        #ifdef USE_IM2COL
            read_file(input_padded_im2col_old_layout_path, "inputs", &input_X, input_size_padded_im2col + 73728);
        #else
            read_file(input_padded_no_im2col_old_layout_path, "inputs", &input_X, input_size_padded_no_im2col);
        #endif
    #else
        // New data layout
        #ifdef USE_IM2COL
            read_file(input_padded_im2col_new_layout_path, "inputs", &input_X, input_size_padded_im2col);
        #else
            read_file(input_padded_no_im2col_new_layout_path, "inputs", &input_X, input_size_padded_no_im2col);
        #endif
    #endif

#endif

#ifdef USE_IM2COL
    #ifdef PERFORM_VERIFICATION
        #ifdef USE_GOLD_FUN
            #ifdef USE_OLD_DATA_LAYOUT
                read_file(input_nopad_no_im2col_old_layout_path, "input_X_noim2col", &input_X_noim2col, input_size_nopad_no_im2col);
            #else
                read_file(input_nopad_no_im2col_new_layout_path, "input_X_noim2col", &input_X_noim2col, input_size_nopad_no_im2col);
            #endif
        #endif
    #endif
#else
    input_X_noim2col = input_X;
#endif

    // fprintf(stderr, "input_X:\n");
    // for (int y=0; y < 5000; y++) {
    //     // if (abs(input_X[y]) >= 0.00000001f) {
    //     //     fprintf(stderr, "input_X[%d] = %f\n", y, input_X[y]);
    //     // }
    //     fprintf(stderr, "%f ", input_X[y]);
    // }
    // return( 0);

     /* float input_B[out_channels_SV] = {0.0, 1.0, 2.0}; */
    // vector<float> input_B(layerConfig.out_channels, 1.58f);

#ifdef USE_OLD_DATA_LAYOUT
    string weights_path = "BOARD_WD/randomeW/NETWORK_NAME/LAYER_ID/random_w_old_data_layout.bin";
#else
    string weights_path = "BOARD_WD/randomeW/NETWORK_NAME/LAYER_ID/random_w.bin";
#endif

    const int k_size = layerConfig.out_channels * layerConfig.kernel_WH * layerConfig.kernel_WH * layerConfig.in_channels;
    float* input_K;
    read_file(weights_path, "weights", &input_K, k_size);

    float *out_flat = nullptr;

    print_time();
    lift::execute(input_X, input_K, out_flat);
    print_time();

#ifdef USE_OLD_DATA_LAYOUT
    // Old data layout
    
    #ifdef RUN_MULTI_KERNEL_FUNC_VERIFICATION
        float (&out)[layerConfig.n_inputs][output_H][output_W][layerConfig.out_channels] = 
            *reinterpret_cast<float (*)[layerConfig.n_inputs][output_H][output_W][layerConfig.out_channels]>(out_flat);
    #else
        float (&out)[layerConfig.n_inputs][output_H_optpadded][output_W_optpadded][layerConfig.out_channels] = 
            *reinterpret_cast<float (*)[layerConfig.n_inputs][output_H_optpadded][output_W_optpadded][layerConfig.out_channels]>(out_flat);
    #endif

    #ifdef PERFORM_VERIFICATION
        #ifndef USE_GOLD_FUN
            string gold_file_path = "BOARD_WD/golden_data/NETWORK_NAME_old_data_layout/LAYER_ID/data.bin";
        #endif
    #endif
#else
    // New data layout
    
    #ifdef RUN_MULTI_KERNEL_FUNC_VERIFICATION
        float (&out)[layerConfig.n_inputs][layerConfig.out_channels][output_H][output_W] = 
            *reinterpret_cast<float (*)[layerConfig.n_inputs][layerConfig.out_channels][output_H][output_W]>(out_flat); 
    #else
        float (&out)[layerConfig.n_inputs][layerConfig.out_channels][output_H_optpadded][output_W_optpadded] = 
            *reinterpret_cast<float (*)[layerConfig.n_inputs][layerConfig.out_channels][output_H_optpadded][output_W_optpadded]>(out_flat); 
    #endif

    #ifdef PERFORM_VERIFICATION
        #ifndef USE_GOLD_FUN
            string gold_file_path = "BOARD_WD/golden_data/NETWORK_NAME_old_data_layout/LAYER_ID/data.bin";
        #endif
    #endif
#endif

#ifdef PERFORM_VERIFICATION
    #ifndef USE_GOLD_FUN
        const int golden_data_size = layerConfig.n_inputs * layerConfig.out_channels * output_W * output_H;

        float* gold_flat;

        read_file(gold_file_path, "gold", &gold_flat, golden_data_size);

        #ifdef USE_OLD_DATA_LAYOUT
            // Old data layout
            float (&gold)[layerConfig.n_inputs][output_H][output_W][layerConfig.out_channels] = 
                *reinterpret_cast<float (*)[layerConfig.n_inputs][output_H][output_W][layerConfig.out_channels]>(gold_flat);
        #else
            // New data layout
            float (&gold)[layerConfig.n_inputs][layerConfig.out_channels][output_H][output_W] = 
                *reinterpret_cast<float (*)[layerConfig.n_inputs][layerConfig.out_channels][output_H][output_W]>(gold_flat);
        #endif
    #endif
#endif


/* #include "./golden.c" */

    // Verify the result
    bool result=true;

    int counter = 0; 
    float one_gold = 0;
    float one_gold_new_layout = 0;
    float precision = 0.00001; 

#ifdef PERFORM_VERIFICATION
        for (int i=0; i < layerConfig.n_inputs; i++) {
#ifdef USE_OLD_DATA_LAYOUT
            // Old data layout
            for (int j=0; j < output_H; j++) {
                for (int k=0; k < output_W; k++) {
                    for (int l=0; l < layerConfig.out_channels; l++) {
#else
            // New data layout
            for (int j=0; j < layerConfig.out_channels; j++) {
                for (int k=0; k < output_H; k++) {
                    for (int l=0; l < output_W; l++) {
#endif
                        #ifdef USE_GOLD_FUN
                            #ifdef USE_OLD_DATA_LAYOUT
                                one_gold = conv(input_X_noim2col, input_K, j, k, l); 
                            #endif
                        #else
                            one_gold = gold[i][j][k][l];
                        #endif
                        if ( (out[i][j][k][l] == 0 && one_gold != 0) ||
                             (out[i][j][k][l] != 0 && one_gold == 0) ||
                             (one_gold != 0 && out[i][j][k][l] != 0 && abs((out[i][j][k][l] - one_gold) / one_gold) > precision ) ) { //  0.00001 
                            result=false;

                            fprintf(stderr, "out[%d][%d][%d][%d] = ", i, j, k, l);
                            fprintf(stderr, "%f != %f\n", out[i][j][k][l], one_gold);
                            fprintf(stderr, "out:\n");
                            
                            for (int y=0; y < 100; y++) {
                                fprintf(stderr, "%f ", out[i][j][k][l + y]);
                            }
#ifdef USE_GOLD_FUN
                            fprintf(stderr, "\ngold:\n");
                            for (int y=0; y < 100; y++) {
#ifdef USE_OLD_DATA_LAYOUT
                                fprintf(stderr, "%f ", conv(input_X_noim2col, input_K, j, k, l+y));
#endif
                            }                    
                            fprintf(stderr, "\n\nstarting from zero:\n");
                            fprintf(stderr, "out:\n");
                            for (int y=0; y < 100; y++) {
                                fprintf(stderr, "%f ", out[0][0][0][y]);
                            }
                            fprintf(stderr, "\ngold:\n");
                            for (int y=0; y < 100; y++) {
#ifdef USE_OLD_DATA_LAYOUT
                                fprintf(stderr, "%f ", conv(input_X_noim2col, input_K, 0, 0, y));
#endif
                            }
#else
                            fprintf(stderr, "\ngold:\n");
                            for (int y=0; y < 100; y++) {
                                fprintf(stderr, "%f ", gold[i][j][k][l + y]);
                            }                    
                            fprintf(stderr, "\n\nstarting from zero:\n");
                            fprintf(stderr, "out:\n");
                            for (int y=0; y < 100; y++) {
                                fprintf(stderr, "%f ", out[0][0][0][y]);
                            }
                            fprintf(stderr, "\ngold:\n");
                            for (int y=0; y < 100; y++) {
                                fprintf(stderr, "%f ", gold[0][0][0][y]);
                            }
#endif
                            fprintf(stderr, "\ninput_X starting from el 0:\n");
                            for (int y=0; y < 100; y++) {
                                fprintf(stderr, "%f ", input_X[y]);
                            }
                            fprintf(stderr, "\ninput_X_noim2col starting from el 0:\n");
                            for (int y=0; y < 100; y++) {
                                fprintf(stderr, "%f ", input_X_noim2col[y]);
                            }

                            fprintf(stderr, "\nFail.\n");
                            fprintf(stderr, "Precision: %f\n", precision);
                            return( 1 );
                        }
                    }
                }
            }
        }
#else
        cout <<"SKIPPING VERIFICATION" << endl;
#endif

    std::cerr<< "Success!\n";
    fprintf(stderr, "Precision: %f\n", precision);

    std::cerr << "Done.\n";
    return( EXIT_SUCCESS );
}
