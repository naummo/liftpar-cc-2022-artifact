# import cProfile
from datetime import datetime
import numpy as np
import argparse
import pandas as pd
import re
import os
from os.path import join, isdir
import ntpath
from timeit import default_timer
import hashlib
import multiprocessing
from typing import List, Tuple

def timer_start():
	# pass
	return default_timer()

def timer_end(start, msg):
	pass
	end = default_timer()
	print("%s: %.7f sec" % (msg, end - start))


parser = argparse.ArgumentParser()
parser.add_argument('--netname', action="store", dest="netname", required=True)
parser.add_argument('--netlabel', action="store", dest="netlabel", required=True)
parser.add_argument('--verbose', action="store_true")
parser.add_argument('--logdir', action="store", dest="logdir", required=True)
parser.add_argument('--allow_missing_file_errors', action="store_true")
parser.add_argument('--n_threads', action="store", dest="n_threads", required=True, type=int)

args = parser.parse_args()

logdirs = [join(args.logdir, args.netname, args.netlabel)]

p_fname = re.compile(r".+_board_(?P<board>\d+)_.+")
p_config_announce = re.compile(r"Running the following config:")
p_c_files_path = re.compile(r"  c_files_path = (?P<c_files_path>.*)\n")
p_set_freq = re.compile(r"Setting frequency (?P<set_freq>\d+).*")
p_report_freq = re.compile(r"Now frequency is\n")
p_checksum = re.compile(r"(?P<checksum>.+?)\s.*")
p_timings = re.compile(r"OclFunCall.*, (-*\d+), (-*\d+(?:\.\d+)*), (-*\d+(?:\.\d+)*)")
p_label = re.compile(r"Benchmarking layer config (\d+) with tuning combination (\d+): trial (\d+)")
p_temp_before = re.compile(r"Temperature before execution:\n")
p_temp_after = re.compile(r"Temperature after execution:\n")
p_number = re.compile(r"(?P<number>\d+)\n")
p_success = re.compile(r"Success!")
p_fail = re.compile(r"Fail.")
p_clock = re.compile(r"\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d:\d\d\d")
p_killed = re.compile(r"Killed the process for running for longer than (?P<limit>\d+?) seconds.")

buffer_size = 10

column_dtypes = {
    "layer" : pd.UInt32Dtype(),
    "tunepoint" : pd.UInt32Dtype(),
    "total_runtime" : np.float32, 
    "rt0" : np.float32,
    "rt1" : np.float32,
    "rt2" : np.float32,
    "rt3" : np.float32,
    "success_status" : pd.UInt8Dtype(),
    "fail_reported" : pd.UInt8Dtype(),
    "reached_runtime_limit" : pd.UInt32Dtype(),
    "temp_before" : pd.UInt32Dtype(),
    "temp_after" : pd.UInt32Dtype(),
    "clock_start" : np.dtype('datetime64[ns]'),
    "clock_stop" : np.dtype('datetime64[ns]'),
    "board" : pd.UInt32Dtype(),
    "set_freq" : pd.UInt64Dtype(),
    "report_freq": pd.UInt64Dtype(),
    "c_files_path": str
}

empty_row = {
    "layer" : pd.NA,
    "tunepoint" : pd.NA,
    "total_runtime" : np.nan, 
    "rt0" : np.nan,
    "rt1" : np.nan,
    "rt2" : np.nan,
    "rt3" : np.nan,
    "success_status" : 0,
    "fail_reported" : 0,
    "reached_runtime_limit" : pd.NA,
    "temp_before" : pd.NA,
    "temp_after" : pd.NA,
    "clock_start" : np.nan,
    "clock_stop" : np.nan,
    "board" : pd.NA,
    "set_freq" : pd.NA,
    "report_freq": pd.NA,
    "c_files_path": ""
}
    # "mem_consum_before_comp_started" : pd.UInt32Dtype(),
    # ("max_mem_consum_during_comp", pd.UInt32Dtype())]
# timings = pd.concat([pd.Series(name=col, dtype=dt) for col, dt in column_dtypes], axis=1)

# def init(buf, idx):
#     for column in column_dtypes:
#         buf.at[idx, column[0]] = np.nan
#     buf.at[idx, "success_status"] = 0


# def allocate_buffer():
#     buf = empty(
#         types = ",".join(list(zip(*column_dtypes))[1]),
#         size = buffer_size,
#         cols = list(zip(*column_dtypes))[0])[0]
    # buf = empty(
    #     types = ",".join([str(v) for v in column_dtypes.values()]),
    #     size = buffer_size,
    #     cols = column_dtypes.keys())[0]

#     for column in column_dtypes:
#         buf.at[:, column[0]] = np.nan
#     buf.at[:, "success_status"] = 0
    
    
#     buf["layer"] = buf["layer"].astype('Int32')
#     buf["tunepoint"] = buf["tunepoint"].astype('Int32')
#     buf["success_status"] = buf["success_status"].astype('Int16')
#     buf["fail_reported"] = buf["fail_reported"].astype('Int16')
#     buf["reached_runtime_limit"] = buf["reached_runtime_limit"].astype('Int32')
#     buf["temp_before"] = buf["temp_before"].astype('Int32')
#     buf["temp_after"] = buf["temp_after"].astype('Int32')
#     buf["board"] = buf["board"].astype('Int32')
#     buf["set_freq"] = buf["set_freq"].astype('Int64')
#     buf["report_freq"] = buf["report_freq"].astype('Int64')

#     return buf

def allocate_buffer():
    # df = pd.DataFrame(np.empty(0, dtype=column_dtypes))
    series = {}
    for c in column_dtypes:
        series[c] = pd.Series([], dtype=column_dtypes[c])
    df = pd.DataFrame(series)
    return df

def fastforward_due_to_errors(line: str, skip_to_next_point_due_to_error):
    if (args.allow_missing_file_errors and 
        ("No such file or directory" in line or 
            "command not found" in line)):
        skip_to_next_point_due_to_error = True
        return (True, skip_to_next_point_due_to_error)
    else:
        return (False, skip_to_next_point_due_to_error)

def fastforward_until_match(line, p, line_i, logfile, skip_to_next_point_due_to_error):
    m = p.match(line)
    fastforward, skip_to_next_point_due_to_error = fastforward_due_to_errors(line, skip_to_next_point_due_to_error)

    while (m is None and 
        not fastforward):
        line = next(logfile, None)
        line_i += 1
        m = p.match(line)
        fastforward, skip_to_next_point_due_to_error = fastforward_due_to_errors(line, skip_to_next_point_due_to_error)
    return (m, line_i, logfile, skip_to_next_point_due_to_error)

def parse_log(input: Tuple[str, List[str], str, int]):
    (logpath, old_cvs, logdir, search_space_idx) = input

    logname = ntpath.basename(logpath)

    # start = timer_start()
    # md5sum_output = subprocess.run(["md5sum", logpath], stdout=subprocess.PIPE)
    # match_checksum = p_checksum.match(md5sum_output.stdout.decode('utf-8'))
    # logchecksum = match_checksum.group("checksum")
    # timer_end(start, "checksum unix")
    # if not match_checksum:
    #     print("Could not get checksum of the following logfile using 'md5sum': " + logpath)
    #     quit()
    # start = timer_start()
    with open(logpath, 'rb') as f:
        logchecksum = hashlib.md5(f.read()).hexdigest()
    # timer_end(start, "checksum python")

    # start = timer_start()
    if (logname + "." + logchecksum + ".csv") in old_cvs:
        # timer_end(start, "os.listdir")
        # print("Skipping the following log since it was already parsed: " + logpath)
        print(".", end = '', flush=True)
        return
    else:
        outdated_files = filter(lambda f : f.startswith(logname) and f.endswith(".csv"), \
            os.listdir(join(logdir, "csv")))
        for filename in outdated_files:
            print("\nMarking the following CSV as outdated: " + filename)
            os.rename(
                join(logdir, "csv", filename), 
                join(logdir, "csv", filename + ".outdated"))

    print("\nParsing the log " + logpath)
    # print("!", end = '', flush=True)

    # start = timer_start()
    timings = allocate_buffer()
    # timer_end(start, "allocate_buffer")
    # last_record_idx = None
    current_layer = None
    current_tunepoint = None
    current_idx = None
    current_trial = None
    skip_to_next_point_due_to_error = False
    look_for_c_files_path_inside_file = False
    c_files_path = ""

    if logname.startswith("logof_"):
        look_for_c_files_path_inside_file = True

    m_fname = p_fname.match(logname)
    if m_fname is None:
        print("ERROR -- could not extract the board number from logpath " + logpath)
        quit()
    else:
        board = int(m_fname.group("board"))

    logfile = open(logpath)

    line_i = -1
    for line in logfile:
        line_i += 1

        # Config
        if look_for_c_files_path_inside_file:
            match_config = p_config_announce.match(line)
            if match_config:
                if args.verbose: print("match_config  on: " + line)
                match_c_files_path = None
                while match_c_files_path is None:
                    line = next(logfile, None)
                    line_i += 1
                    if not line.startswith("  "):
                        print("Wrong format of the log -- while parsing config announcement, found a line that doesn't start with \"  \" before encountering c_files_path")
                        print("Line " + str(line_i) + ": " + line)
                        quit()
                    match_c_files_path = p_c_files_path.match(line)
                c_files_path = match_c_files_path.group("c_files_path")


        # Layer and tunepoint
        match_label = p_label.match(line)
        if match_label:
            if args.verbose: print("match_label  on: " + line)
            current_layer = int(match_label.group(1))
            current_tunepoint = int(match_label.group(2)) + 1000 * search_space_idx
            new_trial = int(match_label.group(3))

            if look_for_c_files_path_inside_file and c_files_path == "":
                print("Found trial announcement before the config info!")
                quit()

            # init row
            if current_idx is None:
                current_idx = 0
                current_trial = new_trial
                timings.at[current_idx, :] = empty_row.values()
            else:
                if not (timings.at[current_idx, "layer"] == current_layer and 
                    timings.at[current_idx, "tunepoint"] == current_tunepoint and 
                    current_trial == new_trial):
                    current_idx += 1
                    current_trial = new_trial
                    timings.at[current_idx, :] = empty_row.values()


            timings.at[current_idx, "layer"] = current_layer
            timings.at[current_idx, "tunepoint"] = current_tunepoint
            timings.at[current_idx, "board"] = board
            timings.at[current_idx, "c_files_path"] = c_files_path
            
            if skip_to_next_point_due_to_error:
                print("WARNING: skipped some lines due to missing files errors! (Current line: " + str(line_i) + ")")
            skip_to_next_point_due_to_error = False
            continue

        if skip_to_next_point_due_to_error:
            continue

        # Frequency setting
        match_set_freq = p_set_freq.match(line)
        if match_set_freq:
            if args.verbose: print("match_set_freq  on: " + line)
            if current_idx is None:
                if fastforward_due_to_errors(line):
                    continue
                print("Wrong format of the log -- found data before layer and tunepoint")
                print("Line " + str(line_i) + ": " + line)
                quit()

            timings.at[current_idx, "set_freq"] = int(match_set_freq.group("set_freq"))
            continue

        # Frequency report
        match_report_freq = p_report_freq.match(line)
        if match_report_freq:
            if args.verbose: print("match_report_freq  on: " + line)
            if current_idx is not None:
                line = next(logfile, None)
                line_i += 1

                match_report_freq = p_number.match(line)
                if match_report_freq is None:
                    if fastforward_due_to_errors(line):
                        continue

                    print("Wrong format of the log -- expected frequency value")
                    print("Line " + str(line_i) + ": " + line)
                    quit()
                else:
                    if args.verbose: print("match_report_freq  on: " + line)
                    timings.at[current_idx, "report_freq"] = int(match_report_freq.group("number"))

            continue


        # Temperature
        match_temp_before = p_temp_before.match(line)
        if match_temp_before:
            if args.verbose: print("match_temp_before  on: " + line)
            if current_idx is None:
                if fastforward_due_to_errors(line):
                    continue
                print("Wrong format of the log -- found data before layer and tunepoint")
                print("Line " + str(line_i) + ": " + line)

            line = next(logfile, None)
            line_i += 1

            # match_temp = p_number.match(line)
            match_temp, line_i, logfile, skip_to_next_point_due_to_error = \
                fastforward_until_match(line, p_number, line_i, logfile, skip_to_next_point_due_to_error)

            if match_temp is None:
                if fastforward_due_to_errors(line):
                    continue
                print("Wrong format of the log -- expected temperature value")
                print("Line " + str(line_i) + ": " + line)
                quit()
            else:
                if args.verbose: print("match_temp  on: " + line)
                timings.at[current_idx, "temp_before"] = int(match_temp.group("number"))

            continue
            

        # A timing
        match_timings = p_timings.match(line)
        if match_timings:
            if args.verbose: print("match_timings  on: " + line)
            # print("current_idx " + str(current_idx))
            # print("timings.at[current_idx, \"rt0\"] " + str(timings.at[current_idx, "rt0"]))
            # print("timings.at[current_idx, \"rt1\"] " + str(timings.at[current_idx, "rt1"]))
            # print("timings.at[current_idx, \"rt2\"] " + str(timings.at[current_idx, "rt2"]))
            # print("timings.at[current_idx, \"rt3\"] " + str(timings.at[current_idx, "rt3"]))
            
            if current_idx is None:
                if fastforward_due_to_errors(line):
                    continue
                print("Wrong format of the log -- found data before layer and tunepoint")
                quit()

            # Init
            if pd.isnull(timings.at[current_idx, "total_runtime"]):
                timings.at[current_idx, "total_runtime"] = 0
            rt = float(match_timings.group(2))

            timings.at[current_idx, "total_runtime"] += rt

            if pd.isnull(timings.at[current_idx, "rt0"]):
                timings.at[current_idx, "rt0"] = rt
            else:
                if pd.isnull(timings.at[current_idx, "rt1"]):
                    timings.at[current_idx, "rt1"] = rt
                else:
                    if pd.isnull(timings.at[current_idx, "rt2"]):
                        timings.at[current_idx, "rt2"] = rt
                    else:
                        if pd.isnull(timings.at[current_idx, "rt3"]):
                            timings.at[current_idx, "rt3"] = rt

            # print("timings.at[current_idx, \"rt0\"] " + str(timings.at[current_idx, "rt0"]))
            # print("timings.at[current_idx, \"rt1\"] " + str(timings.at[current_idx, "rt1"]))
            # print("timings.at[current_idx, \"rt2\"] " + str(timings.at[current_idx, "rt2"]))
            # print("timings.at[current_idx, \"rt3\"] " + str(timings.at[current_idx, "rt3"]))
            continue

        # Success status
        match_success = p_success.match(line)
        if match_success:
            if args.verbose: print("match_success  on: " + line)
            if current_idx is None:
                if fastforward_due_to_errors(line):
                    continue
                print("Wrong format of the log -- found data before layer and tunepoint")
                quit()

            timings.at[current_idx, "success_status"] = 1

            continue

        match_fail = p_fail.match(line)
        if match_fail:
            if args.verbose: print("match_fail  on: " + line)
            if current_idx is None:
                if fastforward_due_to_errors(line):
                    continue
                print("Wrong format of the log -- found data before layer and tunepoint")
                quit()

            timings.at[current_idx, "fail_reported"] = 1

            continue

        # Clock
        match_clock = p_clock.match(line)
        if match_clock:
            if args.verbose: print("match_clock  on: " + line)
            if current_idx is None:
                if fastforward_due_to_errors(line):
                    continue
                print("Wrong format of the log -- found data before layer and tunepoint")
                quit()

            datetime_object = datetime.strptime(match_clock.group(0), "%Y-%m-%d %H:%M:%S:%f")

            if pd.isnull(timings.at[current_idx, "clock_start"]):
                timings.at[current_idx, "clock_start"] = datetime_object
            else:
                timings.at[current_idx, "clock_stop"] = datetime_object

            continue

        
        # Temperature
        match_temp_after = p_temp_after.match(line)
        if match_temp_after:
            if args.verbose: print("match_temp_after  on: " + line)
            if current_idx is None:
                if fastforward_due_to_errors(line):
                    continue
                print("Wrong format of the log -- found data before layer and tunepoint")
                print("Line " + str(line_i) + ": " + line)

            line = next(logfile, None)
            line_i += 1

            match_temp, line_i, logfile, skip_to_next_point_due_to_error = \
                fastforward_until_match(line, p_number, line_i, logfile, skip_to_next_point_due_to_error)
            if match_temp is None:
                if args.verbose: print("match_temp  on: " + line)
                if fastforward_due_to_errors(line):
                    continue
                print("Wrong format of the log -- expected temperature value")
                print("Line " + str(line_i) + ": " + line)
                quit()
            else:
                timings.at[current_idx, "temp_after"] = int(match_temp.group("number"))

            continue

        # Killing for reaching the runtime limit
        match_killed = p_killed.match(line)
        if match_killed:
            if args.verbose: print("match_killed  on: " + line)
            if current_idx is None:
                if fastforward_due_to_errors(line):
                    continue
                print("Wrong format of the log -- found data before layer and tunepoint")
                quit()

            timings.at[current_idx, "reached_runtime_limit"] = int(match_killed.group("limit"))

            continue

    # Save rows as csv
    if current_idx is not None:
        filename = join(logdir, "csv", logname + "." + logchecksum + ".csv")
        # print("Generating a CSV logfile with timings: " + filename)

        if not os.path.exists(join(logdir, "csv")):
            os.makedirs(join(logdir, "csv"))

        # start = timer_start()
        # Add net and label info
        timings["net_name"] = args.netname
        timings["net_label"] = args.netlabel if \
            not args.netlabel.startswith("kernel_sharing_coalesce_fix_exp") \
                else "kernel_sharing_coalesce_fix"
        # timer_end(start, "broadcast")

        # print(timings)
        # start = timer_start()
        timings[0:current_idx + 1].to_csv(path_or_buf=filename, index=False)
        # timer_end(start, "to_csv")
    else:
        # print("No CSV produced for this log")
        print("0", end = '', flush=True)

# cProfile.run('main()')

pool = multiprocessing.Pool(int(args.n_threads))
# logdirs = ["/mnt/c/Users/Naums/hipeac20experiments/board_logs/vgg/repr_kernel_sharing_coalesce_fix_all_layers"]
for (search_space_idx, logdir) in enumerate(logdirs):

    if not os.path.exists(join(logdir, "csv")):
        os.makedirs(join(logdir, "csv"))

    start = timer_start()
    meta_dirs = [join(logdir, meta_d) for meta_d in os.listdir(logdir) if isdir(join(logdir, meta_d)) and meta_d.startswith("generated_files")]
    logpaths = [join(logdir, l) for l in os.listdir(logdir) if l.endswith(".log")] +\
                [join(logdir, meta_d, l) for meta_d in meta_dirs for l in os.listdir(meta_d) if l.endswith(".log")]
    old_cvs = os.listdir(join(logdir, "csv"))
    timer_end(start, "meta_dirs and logpaths")
    
    chunksize = max(1, int(len(logpaths) / args.n_threads))
    print("Parsing logs IN PARALLEL (%d threads, %d chunksize each, %d logpaths). Make sure the subprocesses don't share the state" % (args.n_threads, chunksize, len(logpaths)))
    result = pool.map(func = parse_log, iterable = map(lambda logpath: (logpath, old_cvs, logdir, search_space_idx), logpaths), chunksize = chunksize)

    for r in result:
        print(r)