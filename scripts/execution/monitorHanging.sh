#!/usr/bin/env bash
# set -x

booting_duration_limit=10
running_duration_limit=2
START_SECONDS=$SECONDS

echo "Waiting for a.out to start..."
#while ! a=$(ps aux | grep [a].out)
while ! PID=$(pidof a.out)
do
  if [[ $(( $SECONDS - $START_SECONDS )) -ge $booting_duration_limit ]]; then
    echo "Waited for a.out to start for more than $booting_duration_limit seconds; not starting the hanging monitor"
    exit
  fi
done

echo "Starting the hanging monitor"
while pidof a.out>/dev/null
do
  PID=$(ps -eo command,pid,etimes | awk '/^.\/a\.out/ {if ($4 > '$running_duration_limit') { print $3}}')
  if [ ! -z "$PID" ]
  then
    (sleep 1; kill -9 $PID) &
    tail --pid=$PID -f /dev/null
    echo "Killed the process for running for longer than ${running_duration_limit} seconds."
    exit
  fi
#   if [[ $(kill -9 $PID 2>&1; echo $?) == 0 ]];
#   then
#     echo "Killed the process for running for longer than $(running_duration_limit) seconds."
#     break
#   fi
  sleep 0.005
done

echo "Finished the hanging monitor"
