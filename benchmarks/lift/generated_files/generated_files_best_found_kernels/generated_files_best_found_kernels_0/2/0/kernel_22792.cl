// Layer 2, tune point 0
// inputChannels -> 64, inputWidth -> 112, inputHeight -> 112,
// padFuncX -> 1, padFuncY -> 1,
// kernelWidth -> 3, kernelHeight -> 3,
// kernelStrideX -> 1, kernelStrideY -> 1,
// numKernels -> 128
// Vectorization
// "mapVectorize.213." -> 1,
// "mapVectorize.246." -> 1
// Unsliding
// 
// Parallelization and fusion
// "mapTransform.28." -> (10*Code.mapWrg() + Code.parDims(0)()),
// "mapTransform.29." -> (10*Code.mapWrg() + Code.parDims(1)()),
// "mapTransform.30." -> (Code.replaceInnerMapWithOuter()),
// "mapTransform.51." -> (10*Code.mapWrg() + Code.parDims(2)()),
// "mapTransform.52." -> (10*Code.mapLcl() + Code.parDims(2)()),
// "mapTransform.68." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.70." -> (10*Code.mapLcl() + Code.parDims(0)()),
// "mapTransform.109." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.110." -> (10*Code.mapLcl() + Code.parDims(0)())
// padOptRight -> 4, padOptBottom -> 16,
// tileWidth -> 4, tileHeight -> 16, tileDepth -> 32,
// seqWindowsPerThreadX -> 4, seqWindowsPerThreadY -> 1,
// seqKernelsPerThread -> 4,
// kernelGroupSize -> 4,
// windowTileWidth -> 1, windowTileHeight -> 1,
// inputCacheSizeX -> 4, inputCacheSizeY -> 1,
// inputCacheDepth -> 4,
// kernelCacheSize -> 4,
// localSizes(0) -> 8,
// localSizes(1) -> 3,
// localSizes(2) -> 16,
// nWrgs(0) -> 32,
// nWrgs(1) -> 232,
// nWrgs(2) -> 1
// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

float id(float x){
    {
        { return x; }; 
    }
}
float4 id4(float4 x){
    {
        { return x; }; 
    }
}
float add(float x, float y){
    {
        { return x+y; }; 
    }
}
float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
kernel __attribute((reqd_work_group_size(8,3,16)))
void KERNEL(const global float* restrict v__11465, const global float* restrict v__11466, global float* v__11493){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        local float v__11487[6144];
        // Typed Value memory
        float v__10611; 
        float v__10620; 
        float v__10632; 
        // Private Memory
        float v__11469_0;
        float v__11469_1;
        float v__11469_2;
        float v__11469_3;
        float v__11469_4;
        float v__11469_5;
        float v__11469_6;
        float v__11469_7;
        float v__11469_8;
        float v__11469_9;
        float v__11469_10;
        float v__11469_11;
        float v__11469_12;
        float v__11469_13;
        float v__11469_14;
        float v__11469_15;
        float4 v__11472_0;
        float v__11473_0;
        float v__11473_1;
        float v__11473_2;
        float v__11473_3;
        float v__11473_4;
        float v__11473_5;
        float v__11473_6;
        float v__11473_7;
        float v__11473_8;
        float v__11473_9;
        float v__11473_10;
        float v__11473_11;
        float v__11473_12;
        float v__11473_13;
        float v__11473_14;
        float v__11473_15;
        float4 v__11475_0;
        float v__11476_0;
        float v__11476_1;
        float v__11476_2;
        float v__11476_3;
        float v__11476_4;
        float v__11476_5;
        float v__11476_6;
        float v__11476_7;
        float v__11476_8;
        float v__11476_9;
        float v__11476_10;
        float v__11476_11;
        float v__11476_12;
        float v__11476_13;
        float v__11476_14;
        float v__11476_15;
        float v__11477_0;
        float v__11477_1;
        float v__11477_2;
        float v__11477_3;
        float v__11477_4;
        float v__11477_5;
        float v__11477_6;
        float v__11477_7;
        float v__11477_8;
        float v__11477_9;
        float v__11477_10;
        float v__11477_11;
        float v__11477_12;
        float v__11477_13;
        float v__11477_14;
        float v__11477_15;
        float v__11481_0;
        float v__11481_1;
        float v__11481_2;
        float v__11481_3;
        float v__11481_4;
        float v__11481_5;
        float v__11481_6;
        float v__11481_7;
        float v__11481_8;
        float v__11481_9;
        float v__11481_10;
        float v__11481_11;
        float v__11481_12;
        float v__11481_13;
        float v__11481_14;
        float v__11481_15;
        float v__11488_0;
        // iteration count is exactly 1, no loop emitted
        {
            int v_wg_id_11400 = get_group_id(0); 
            // iteration count is exactly 1, no loop emitted
            {
                int v_wg_id_11401 = get_group_id(1); 
                // iteration count is exactly 1, no loop emitted
                {
                    int v_wg_id_11402 = get_group_id(2); 
                    // iteration count is exactly 1, no loop emitted
                    {
                        int v_l_id_11403 = get_local_id(2); 
                        // map_seq
                        // iteration count is exactly 1, no loop emitted
                        {
                            int v_i_11404 = 0; 
                            // iteration count is exactly 1, no loop emitted
                            {
                                int v_l_id_11405 = get_local_id(1); 
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_l_id_11406 = get_local_id(0); 
                                    float v_tmp_11495 = 0.0f; 
                                    v__10611 = v_tmp_11495; 
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11469_0 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11469_1 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11469_2 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11469_3 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11469_4 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11469_5 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11469_6 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11469_7 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11469_8 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11469_9 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11469_10 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11469_11 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11469_12 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11469_13 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11469_14 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11469_15 = id(v__10611); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // reduce_seq
                                    for (int v_i_11411 = 0; (v_i_11411 < 6); v_i_11411 = (1 + v_i_11411)){
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11472_0 = id4(vload4((v_l_id_11406 + (8 * (v_i_11411 / 3)) + (16 * (v_i_11411 % 3)) + (48 * v_l_id_11405) + (576 * v_wg_id_11400) + (576 * v_wg_id_11402)),v__11466 + 0)); 
                                        v__11473_0 = v__11472_0.s0; 
                                        v__11473_1 = v__11472_0.s1; 
                                        v__11473_2 = v__11472_0.s2; 
                                        v__11473_3 = v__11472_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11472_0 = id4(vload4((144 + v_l_id_11406 + (8 * (v_i_11411 / 3)) + (16 * (v_i_11411 % 3)) + (48 * v_l_id_11405) + (576 * v_wg_id_11400) + (576 * v_wg_id_11402)),v__11466 + 0)); 
                                        v__11473_4 = v__11472_0.s0; 
                                        v__11473_5 = v__11472_0.s1; 
                                        v__11473_6 = v__11472_0.s2; 
                                        v__11473_7 = v__11472_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11472_0 = id4(vload4((288 + v_l_id_11406 + (8 * (v_i_11411 / 3)) + (16 * (v_i_11411 % 3)) + (48 * v_l_id_11405) + (576 * v_wg_id_11400) + (576 * v_wg_id_11402)),v__11466 + 0)); 
                                        v__11473_8 = v__11472_0.s0; 
                                        v__11473_9 = v__11472_0.s1; 
                                        v__11473_10 = v__11472_0.s2; 
                                        v__11473_11 = v__11472_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11472_0 = id4(vload4((432 + v_l_id_11406 + (8 * (v_i_11411 / 3)) + (16 * (v_i_11411 % 3)) + (48 * v_l_id_11405) + (576 * v_wg_id_11400) + (576 * v_wg_id_11402)),v__11466 + 0)); 
                                        v__11473_12 = v__11472_0.s0; 
                                        v__11473_13 = v__11472_0.s1; 
                                        v__11473_14 = v__11472_0.s2; 
                                        v__11473_15 = v__11472_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11475_0 = id4(vload4((v_l_id_11406 + (8 * (v_i_11411 / 3)) + (16 * (v_i_11411 % 3)) + (64 * (v_wg_id_11401 % 29)) + (1888 * v_l_id_11403) + (1888 * v_l_id_11405) + (30208 * (v_wg_id_11401 / 29))),v__11465 + 0)); 
                                        v__11476_0 = v__11475_0.s0; 
                                        v__11476_1 = v__11475_0.s1; 
                                        v__11476_2 = v__11475_0.s2; 
                                        v__11476_3 = v__11475_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11475_0 = id4(vload4((16 + v_l_id_11406 + (8 * (v_i_11411 / 3)) + (16 * (v_i_11411 % 3)) + (64 * (v_wg_id_11401 % 29)) + (1888 * v_l_id_11403) + (1888 * v_l_id_11405) + (30208 * (v_wg_id_11401 / 29))),v__11465 + 0)); 
                                        v__11476_4 = v__11475_0.s0; 
                                        v__11476_5 = v__11475_0.s1; 
                                        v__11476_6 = v__11475_0.s2; 
                                        v__11476_7 = v__11475_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11475_0 = id4(vload4((32 + v_l_id_11406 + (8 * (v_i_11411 / 3)) + (16 * (v_i_11411 % 3)) + (64 * (v_wg_id_11401 % 29)) + (1888 * v_l_id_11403) + (1888 * v_l_id_11405) + (30208 * (v_wg_id_11401 / 29))),v__11465 + 0)); 
                                        v__11476_8 = v__11475_0.s0; 
                                        v__11476_9 = v__11475_0.s1; 
                                        v__11476_10 = v__11475_0.s2; 
                                        v__11476_11 = v__11475_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11475_0 = id4(vload4((48 + v_l_id_11406 + (8 * (v_i_11411 / 3)) + (16 * (v_i_11411 % 3)) + (64 * (v_wg_id_11401 % 29)) + (1888 * v_l_id_11403) + (1888 * v_l_id_11405) + (30208 * (v_wg_id_11401 / 29))),v__11465 + 0)); 
                                        v__11476_12 = v__11475_0.s0; 
                                        v__11476_13 = v__11475_0.s1; 
                                        v__11476_14 = v__11475_0.s2; 
                                        v__11476_15 = v__11475_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11496 = 0.0f; 
                                        v__10620 = v_tmp_11496; 
                                        v__11477_0 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_0 = mult(v__11476_0, v__11473_0); 
                                        v__11477_0 = add(v__11477_0, v__11481_0); 
                                        v__11481_0 = mult(v__11476_1, v__11473_1); 
                                        v__11477_0 = add(v__11477_0, v__11481_0); 
                                        v__11481_0 = mult(v__11476_2, v__11473_2); 
                                        v__11477_0 = add(v__11477_0, v__11481_0); 
                                        v__11481_0 = mult(v__11476_3, v__11473_3); 
                                        v__11477_0 = add(v__11477_0, v__11481_0); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11497 = 0.0f; 
                                        v__10620 = v_tmp_11497; 
                                        v__11477_1 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_1 = mult(v__11476_4, v__11473_0); 
                                        v__11477_1 = add(v__11477_1, v__11481_1); 
                                        v__11481_1 = mult(v__11476_5, v__11473_1); 
                                        v__11477_1 = add(v__11477_1, v__11481_1); 
                                        v__11481_1 = mult(v__11476_6, v__11473_2); 
                                        v__11477_1 = add(v__11477_1, v__11481_1); 
                                        v__11481_1 = mult(v__11476_7, v__11473_3); 
                                        v__11477_1 = add(v__11477_1, v__11481_1); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11498 = 0.0f; 
                                        v__10620 = v_tmp_11498; 
                                        v__11477_2 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_2 = mult(v__11476_8, v__11473_0); 
                                        v__11477_2 = add(v__11477_2, v__11481_2); 
                                        v__11481_2 = mult(v__11476_9, v__11473_1); 
                                        v__11477_2 = add(v__11477_2, v__11481_2); 
                                        v__11481_2 = mult(v__11476_10, v__11473_2); 
                                        v__11477_2 = add(v__11477_2, v__11481_2); 
                                        v__11481_2 = mult(v__11476_11, v__11473_3); 
                                        v__11477_2 = add(v__11477_2, v__11481_2); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11499 = 0.0f; 
                                        v__10620 = v_tmp_11499; 
                                        v__11477_3 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_3 = mult(v__11476_12, v__11473_0); 
                                        v__11477_3 = add(v__11477_3, v__11481_3); 
                                        v__11481_3 = mult(v__11476_13, v__11473_1); 
                                        v__11477_3 = add(v__11477_3, v__11481_3); 
                                        v__11481_3 = mult(v__11476_14, v__11473_2); 
                                        v__11477_3 = add(v__11477_3, v__11481_3); 
                                        v__11481_3 = mult(v__11476_15, v__11473_3); 
                                        v__11477_3 = add(v__11477_3, v__11481_3); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11500 = 0.0f; 
                                        v__10620 = v_tmp_11500; 
                                        v__11477_4 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_4 = mult(v__11476_0, v__11473_4); 
                                        v__11477_4 = add(v__11477_4, v__11481_4); 
                                        v__11481_4 = mult(v__11476_1, v__11473_5); 
                                        v__11477_4 = add(v__11477_4, v__11481_4); 
                                        v__11481_4 = mult(v__11476_2, v__11473_6); 
                                        v__11477_4 = add(v__11477_4, v__11481_4); 
                                        v__11481_4 = mult(v__11476_3, v__11473_7); 
                                        v__11477_4 = add(v__11477_4, v__11481_4); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11501 = 0.0f; 
                                        v__10620 = v_tmp_11501; 
                                        v__11477_5 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_5 = mult(v__11476_4, v__11473_4); 
                                        v__11477_5 = add(v__11477_5, v__11481_5); 
                                        v__11481_5 = mult(v__11476_5, v__11473_5); 
                                        v__11477_5 = add(v__11477_5, v__11481_5); 
                                        v__11481_5 = mult(v__11476_6, v__11473_6); 
                                        v__11477_5 = add(v__11477_5, v__11481_5); 
                                        v__11481_5 = mult(v__11476_7, v__11473_7); 
                                        v__11477_5 = add(v__11477_5, v__11481_5); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11502 = 0.0f; 
                                        v__10620 = v_tmp_11502; 
                                        v__11477_6 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_6 = mult(v__11476_8, v__11473_4); 
                                        v__11477_6 = add(v__11477_6, v__11481_6); 
                                        v__11481_6 = mult(v__11476_9, v__11473_5); 
                                        v__11477_6 = add(v__11477_6, v__11481_6); 
                                        v__11481_6 = mult(v__11476_10, v__11473_6); 
                                        v__11477_6 = add(v__11477_6, v__11481_6); 
                                        v__11481_6 = mult(v__11476_11, v__11473_7); 
                                        v__11477_6 = add(v__11477_6, v__11481_6); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11503 = 0.0f; 
                                        v__10620 = v_tmp_11503; 
                                        v__11477_7 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_7 = mult(v__11476_12, v__11473_4); 
                                        v__11477_7 = add(v__11477_7, v__11481_7); 
                                        v__11481_7 = mult(v__11476_13, v__11473_5); 
                                        v__11477_7 = add(v__11477_7, v__11481_7); 
                                        v__11481_7 = mult(v__11476_14, v__11473_6); 
                                        v__11477_7 = add(v__11477_7, v__11481_7); 
                                        v__11481_7 = mult(v__11476_15, v__11473_7); 
                                        v__11477_7 = add(v__11477_7, v__11481_7); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11504 = 0.0f; 
                                        v__10620 = v_tmp_11504; 
                                        v__11477_8 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_8 = mult(v__11476_0, v__11473_8); 
                                        v__11477_8 = add(v__11477_8, v__11481_8); 
                                        v__11481_8 = mult(v__11476_1, v__11473_9); 
                                        v__11477_8 = add(v__11477_8, v__11481_8); 
                                        v__11481_8 = mult(v__11476_2, v__11473_10); 
                                        v__11477_8 = add(v__11477_8, v__11481_8); 
                                        v__11481_8 = mult(v__11476_3, v__11473_11); 
                                        v__11477_8 = add(v__11477_8, v__11481_8); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11505 = 0.0f; 
                                        v__10620 = v_tmp_11505; 
                                        v__11477_9 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_9 = mult(v__11476_4, v__11473_8); 
                                        v__11477_9 = add(v__11477_9, v__11481_9); 
                                        v__11481_9 = mult(v__11476_5, v__11473_9); 
                                        v__11477_9 = add(v__11477_9, v__11481_9); 
                                        v__11481_9 = mult(v__11476_6, v__11473_10); 
                                        v__11477_9 = add(v__11477_9, v__11481_9); 
                                        v__11481_9 = mult(v__11476_7, v__11473_11); 
                                        v__11477_9 = add(v__11477_9, v__11481_9); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11506 = 0.0f; 
                                        v__10620 = v_tmp_11506; 
                                        v__11477_10 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_10 = mult(v__11476_8, v__11473_8); 
                                        v__11477_10 = add(v__11477_10, v__11481_10); 
                                        v__11481_10 = mult(v__11476_9, v__11473_9); 
                                        v__11477_10 = add(v__11477_10, v__11481_10); 
                                        v__11481_10 = mult(v__11476_10, v__11473_10); 
                                        v__11477_10 = add(v__11477_10, v__11481_10); 
                                        v__11481_10 = mult(v__11476_11, v__11473_11); 
                                        v__11477_10 = add(v__11477_10, v__11481_10); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11507 = 0.0f; 
                                        v__10620 = v_tmp_11507; 
                                        v__11477_11 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_11 = mult(v__11476_12, v__11473_8); 
                                        v__11477_11 = add(v__11477_11, v__11481_11); 
                                        v__11481_11 = mult(v__11476_13, v__11473_9); 
                                        v__11477_11 = add(v__11477_11, v__11481_11); 
                                        v__11481_11 = mult(v__11476_14, v__11473_10); 
                                        v__11477_11 = add(v__11477_11, v__11481_11); 
                                        v__11481_11 = mult(v__11476_15, v__11473_11); 
                                        v__11477_11 = add(v__11477_11, v__11481_11); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11508 = 0.0f; 
                                        v__10620 = v_tmp_11508; 
                                        v__11477_12 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_12 = mult(v__11476_0, v__11473_12); 
                                        v__11477_12 = add(v__11477_12, v__11481_12); 
                                        v__11481_12 = mult(v__11476_1, v__11473_13); 
                                        v__11477_12 = add(v__11477_12, v__11481_12); 
                                        v__11481_12 = mult(v__11476_2, v__11473_14); 
                                        v__11477_12 = add(v__11477_12, v__11481_12); 
                                        v__11481_12 = mult(v__11476_3, v__11473_15); 
                                        v__11477_12 = add(v__11477_12, v__11481_12); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11509 = 0.0f; 
                                        v__10620 = v_tmp_11509; 
                                        v__11477_13 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_13 = mult(v__11476_4, v__11473_12); 
                                        v__11477_13 = add(v__11477_13, v__11481_13); 
                                        v__11481_13 = mult(v__11476_5, v__11473_13); 
                                        v__11477_13 = add(v__11477_13, v__11481_13); 
                                        v__11481_13 = mult(v__11476_6, v__11473_14); 
                                        v__11477_13 = add(v__11477_13, v__11481_13); 
                                        v__11481_13 = mult(v__11476_7, v__11473_15); 
                                        v__11477_13 = add(v__11477_13, v__11481_13); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11510 = 0.0f; 
                                        v__10620 = v_tmp_11510; 
                                        v__11477_14 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_14 = mult(v__11476_8, v__11473_12); 
                                        v__11477_14 = add(v__11477_14, v__11481_14); 
                                        v__11481_14 = mult(v__11476_9, v__11473_13); 
                                        v__11477_14 = add(v__11477_14, v__11481_14); 
                                        v__11481_14 = mult(v__11476_10, v__11473_14); 
                                        v__11477_14 = add(v__11477_14, v__11481_14); 
                                        v__11481_14 = mult(v__11476_11, v__11473_15); 
                                        v__11477_14 = add(v__11477_14, v__11481_14); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11511 = 0.0f; 
                                        v__10620 = v_tmp_11511; 
                                        v__11477_15 = id(v__10620); 
                                        // reduce_seq
                                        // unroll
                                        v__11481_15 = mult(v__11476_12, v__11473_12); 
                                        v__11477_15 = add(v__11477_15, v__11481_15); 
                                        v__11481_15 = mult(v__11476_13, v__11473_13); 
                                        v__11477_15 = add(v__11477_15, v__11481_15); 
                                        v__11481_15 = mult(v__11476_14, v__11473_14); 
                                        v__11477_15 = add(v__11477_15, v__11481_15); 
                                        v__11481_15 = mult(v__11476_15, v__11473_15); 
                                        v__11477_15 = add(v__11477_15, v__11481_15); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11469_0 = add(v__11469_0, v__11477_0); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11469_1 = add(v__11469_1, v__11477_1); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11469_2 = add(v__11469_2, v__11477_2); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11469_3 = add(v__11469_3, v__11477_3); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11469_4 = add(v__11469_4, v__11477_4); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11469_5 = add(v__11469_5, v__11477_5); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11469_6 = add(v__11469_6, v__11477_6); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11469_7 = add(v__11469_7, v__11477_7); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11469_8 = add(v__11469_8, v__11477_8); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11469_9 = add(v__11469_9, v__11477_9); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11469_10 = add(v__11469_10, v__11477_10); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11469_11 = add(v__11469_11, v__11477_11); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11469_12 = add(v__11469_12, v__11477_12); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11469_13 = add(v__11469_13, v__11477_13); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11469_14 = add(v__11469_14, v__11477_14); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11469_15 = add(v__11469_15, v__11477_15); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                    // end reduce_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11487[(16 * (v_l_id_11406 + (8 * v_l_id_11405) + (24 * v_l_id_11403)))] = id(v__11469_0); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11487[(1 + (16 * v_l_id_11406) + (128 * v_l_id_11405) + (384 * v_l_id_11403))] = id(v__11469_1); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11487[(2 * (1 + (8 * v_l_id_11406) + (64 * v_l_id_11405) + (192 * v_l_id_11403)))] = id(v__11469_2); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11487[(3 + (16 * v_l_id_11406) + (128 * v_l_id_11405) + (384 * v_l_id_11403))] = id(v__11469_3); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11487[(4 * (1 + (4 * v_l_id_11406) + (32 * v_l_id_11405) + (96 * v_l_id_11403)))] = id(v__11469_4); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11487[(5 + (16 * v_l_id_11406) + (128 * v_l_id_11405) + (384 * v_l_id_11403))] = id(v__11469_5); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11487[(2 * (3 + (8 * v_l_id_11406) + (64 * v_l_id_11405) + (192 * v_l_id_11403)))] = id(v__11469_6); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11487[(7 + (16 * v_l_id_11406) + (128 * v_l_id_11405) + (384 * v_l_id_11403))] = id(v__11469_7); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11487[(8 * (1 + (2 * v_l_id_11406) + (16 * v_l_id_11405) + (48 * v_l_id_11403)))] = id(v__11469_8); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11487[(9 + (16 * v_l_id_11406) + (128 * v_l_id_11405) + (384 * v_l_id_11403))] = id(v__11469_9); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11487[(2 * (5 + (8 * v_l_id_11406) + (64 * v_l_id_11405) + (192 * v_l_id_11403)))] = id(v__11469_10); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11487[(11 + (16 * v_l_id_11406) + (128 * v_l_id_11405) + (384 * v_l_id_11403))] = id(v__11469_11); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11487[(4 * (3 + (4 * v_l_id_11406) + (32 * v_l_id_11405) + (96 * v_l_id_11403)))] = id(v__11469_12); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11487[(13 + (16 * v_l_id_11406) + (128 * v_l_id_11405) + (384 * v_l_id_11403))] = id(v__11469_13); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11487[(2 * (7 + (8 * v_l_id_11406) + (64 * v_l_id_11405) + (192 * v_l_id_11403)))] = id(v__11469_14); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11487[(15 + (16 * v_l_id_11406) + (128 * v_l_id_11405) + (384 * v_l_id_11403))] = id(v__11469_15); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                }
                            }
                            barrier(CLK_LOCAL_MEM_FENCE);
                            for (int v_l_id_11457 = get_local_id(1); (v_l_id_11457 < 4); v_l_id_11457 = (3 + v_l_id_11457)){
                                // map_seq
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_i_11458 = 0; 
                                    for (int v_l_id_11459 = get_local_id(0); (v_l_id_11459 < 4); v_l_id_11459 = (8 + v_l_id_11459)){
                                        float v_tmp_11512 = 0.0f; 
                                        v__10632 = v_tmp_11512; 
                                        // map_seq
                                        // unroll
                                        v__11488_0 = id(v__10632); 
                                        // end unroll
                                        // end map_seq
                                        // reduce_seq
                                        for (int v_i_11461 = 0; (v_i_11461 < 24); v_i_11461 = (1 + v_i_11461)){
                                            // map_seq
                                            // unroll
                                            v__11488_0 = add(v__11488_0, v__11487[(v_l_id_11459 + (4 * v_l_id_11457) + (16 * v_i_11461) + (384 * v_l_id_11403))]); 
                                            // end unroll
                                            // end map_seq
                                        }
                                        // end reduce_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11493[(v_l_id_11457 + (4 * v_wg_id_11400) + (4 * v_wg_id_11402) + (128 * v_l_id_11459) + (512 * (v_wg_id_11401 % 29)) + (14848 * v_l_id_11403) + (237568 * (v_wg_id_11401 / 29)))] = id(v__11488_0); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                }
                                // end map_seq
                            }
                        }
                        // end map_seq
                    }
                    barrier(CLK_LOCAL_MEM_FENCE);
                }
            }
        }
    }
}