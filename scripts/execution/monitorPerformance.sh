#!/usr/bin/env bash
# set -x

expected_runtime=10 # 7.6 ms actually
sleep_for=10 # seconds

CWD=$PWD
cd $CWD/sample_kernel/0/2645
OUT=$(sudo ./sample.out 2>&1 | grep OclFunCall_14355)
sampled_runtime=$(echo $OUT | awk -F"[, .]" '{print $5}')

if (( $sampled_runtime > $expected_runtime ));
then
    echo "BOARD PERFORMANCE DROPPED!"
    echo "Sample kernel runtime was $sampled_runtime ms instead of $expected_runtime ms"
    echo "Rebooting the board, then sleeping for $sleep_for seconds"
    echo "@reboot echo \"\" | sudo crontab - && sudo screen -S lift_runner -dm bash -c \"cd $CWD && sleep $sleep_for && ./runAll.sh; exec bash\"" | sudo crontab -

    sudo reboot
else
    echo "Sample kernel runtime is $sampled_runtime ms, which is below the limit of $expected_runtime ms"
fi

exit

echo "@reboot echo \"\" | sudo crontab - && sudo screen -S lift_runner -dm bash -c \"cd /home/shunya/naums/hipeac2020/generated_files_outofbounds_fix_20.01.2021_18.13.34/ && ls > ls.txt; exec bash\"" | sudo crontab -