import argparse
from os.path import join, isfile, isdir
import math
import csv

parser = argparse.ArgumentParser()
parser.add_argument('--label', action="store", required=True)
parser.add_argument('--first_point', action="store", required=True, type=int)
parser.add_argument('--until_point', action="store", required=True, type=int)
parser.add_argument('--first_layer', action="store", required=True, type=int)
parser.add_argument('--last_layer', action="store", required=True, type=int)
parser.add_argument('--trials', action="store", required=True, type=int)
parser.add_argument('--files_path', action="store", required=True)
parser.add_argument('--overwrite', action="store_true")
parser.add_argument('--n_boards', action="store", required=True, type=int)
parser.add_argument('--meta', action="store_true")

args = parser.parse_args()

runConfigsDir = "metaRunConfigs" if args.meta else "runConfigs"
supportedLayers = [0,1,2,3,4,5,7,8,10]

n_points = args.until_point - args.first_point

nPointsPerBoard = int(math.floor(n_points / args.n_boards))

boardPoints = [nPointsPerBoard] * args.n_boards
boardPoints[-1] = n_points - nPointsPerBoard * (args.n_boards - 1)
print("Points per board: " + str(boardPoints))

for boardIdx in range(args.n_boards):
    firstPointOfConfig = args.first_point + sum(boardPoints[0 : boardIdx])
    lastPointOfConfig = firstPointOfConfig + boardPoints[boardIdx] - 1

    output_path = join(runConfigsDir, "_".join([
        args.label, 
        "l" + (str(args.first_layer) + "-" + str(args.last_layer)),
        "k" + (str(firstPointOfConfig) + "-" + str(lastPointOfConfig + 1))]
        ) + ".csv")

    if not args.overwrite and isfile(output_path):
        print("File already exists, skipping: " + output_path)
    else:
        with open(output_path, "w") as config_file:
            print("Creating " + output_path)
            config_writer = csv.writer(config_file)
            if args.meta:
                config_writer.writerow("NETWORK_NAME,NETWORK_LABEL,LAYER_FIRST,LAYER_LAST,TUNEPOINT_GLOBAL_FIRST,TUNEPOINT_GLOBAL_LAST,TRIALS,FREQ,C_FILES_PATH".\
                    split(","))
            else:
                config_writer.writerow("NETWORK_NAME,NETWORK_LABEL,LAYER_FIRST,LAYER_LAST,TUNEPOINT_FIRST,TUNEPOINT_LAST,TRIALS,FREQ,C_FILES_PATH".\
                    split(","))
                    
            row = ["vgg", args.label, args.first_layer, args.last_layer, firstPointOfConfig, lastPointOfConfig, args.trials, "767000000", args.files_path]
            config_writer.writerow(row)

            # config_writer.writerow("")
            print(row)

        output_summary_path = output_path.replace(".csv", "_summary.csv")
        with open(output_summary_path, "w") as config_file:
            print("Creating " + output_summary_path)
            config_writer = csv.writer(config_file)
            config_writer.writerow("NETWORK_NAME,NETWORK_LABEL,LAYER_MIN,LAYER_MAX,TUNEPOINT_MIN,TUNEPOINT_MAX".\
                split(","))
            row = ["vgg", args.label, args.first_layer, args.last_layer, firstPointOfConfig, lastPointOfConfig]
            config_writer.writerow(row)

            # config_writer.writerow("")
            # print(row)
