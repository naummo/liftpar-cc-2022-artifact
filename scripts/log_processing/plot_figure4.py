import numpy as np
import argparse
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rc
from os.path import join
import seaborn as sns
from scipy import stats
from timeit import default_timer

def timer_start():
	# pass
	return default_timer()

def timer_end(start, msg):
	pass
	end = default_timer()
	print("%s: %.7f sec" % (msg, end - start))

parser = argparse.ArgumentParser()
parser.add_argument('--results_and_params_csv', action="store", dest="results_and_params_csv")
parser.add_argument('--comparison_timings_csv', action="store", dest="comparison_timings_csv")
parser.add_argument('--comparison_memory_csv', action="store", dest="comparison_memory_csv")
parser.add_argument('--plot_path', action="store", dest="plot_path")


args = parser.parse_args()


##################### Settings ######################
scatter_marker_size = 2.5
sns.set() 

plt.rc('text', usetex=True)
rc('font',**{'family':'serif','serif':['Palatino']})

rot = 0
fontsize = 13

rc("legend", facecolor="white")

colors = ["#56ae6c",
"#8960b3",
"#b0923b",
"#ba495b"]

##################### Helpers #####################
true_id_to_continuous_collated = lambda true_id : \
    0 if true_id == 0 else \
    1 if true_id == 2 else \
    2 if true_id == 5 else \
    3 if true_id == 7 else \
    4 if true_id == 10 else \
    5 if true_id == 12 else \
    7 if true_id == 17 else \
    8 if true_id == 19 else \
    10 # if true_id == 24


def get_n_layer_duplicates(row):
    return (
    2 if row["layer"] == 5 else
    2 if row["layer"] == 8 else
    3 if row["layer"] == 10 else
    1)
        

##################### Load data #####################
def load_lift_data(results_and_params_csv: str):
    start = timer_start()
    if "temp" in results_and_params_csv:
        lift_data = pd.read_csv(results_and_params_csv, sep='\s*,\s*', engine='python', dtype = {
            "temp_before": 'Int32',
            "temp_after": 'Int32',
            "mapUnslide": "str",
            "mapTransform": "str",
            "mapVectorize": "str"
        })
    else:
        lift_data = pd.read_csv(results_and_params_csv, dtype = {
            "temp_before": 'Int32',
            "temp_after": 'Int32',
            "mapUnslide": "str",
            "mapTransform": "str",
            "mapVectorize": "str"
        })
    timer_end(start, "read_csv")
    lift_data.columns = lift_data.columns.str.strip()
    if "collated_layer_id" not in lift_data.columns:
        lift_data["collated_layer_id"] = lift_data["layer"]

    ##################### Limits #######################

    start = timer_start()

    failed_tunepoints = lift_data[
        (lift_data["fail_reported"] == 1) |
        ( 
            (lift_data["success_status"] == 0) &
            pd.isnull(lift_data["reached_runtime_limit"])
        )
        ]["tunepoint"].unique()
    print("Dropping " + str(len(failed_tunepoints)) + " failed experiments")
    failed_tunepoints_layer = lift_data[(lift_data["fail_reported"] != 0)]["tunepoint"].unique()
    print("failed unique points: " + str(len(failed_tunepoints_layer)))
    lift_data_successful = lift_data.drop( lift_data[lift_data["tunepoint"].isin(failed_tunepoints)].index, inplace=False)

    layers_to_retain = lift_data_successful["layer"].notna()
    jobs_to_retain = True

    lift_data_filtered_layer_jobs = lift_data_successful[layers_to_retain & jobs_to_retain]

    lift_data_filtered = lift_data_filtered_layer_jobs

    lift_data_clean = lift_data.drop( 
        lift_data.index.difference(lift_data_filtered.index))#, inplace=True)

    lift_data = lift_data_clean.reset_index()
    
    timer_end(start, "filtering")


    ##################### Formatting #######################
    lift_data["temp_before"] /= 1000
    lift_data["temp_after"] /= 1000

    lift_data["n_layer_duplicates"] = lift_data.apply(get_n_layer_duplicates, axis="columns")
    print(lift_data)
    return lift_data

primary_lift_data = load_lift_data(results_and_params_csv = args.results_and_params_csv)

comparison_timings = pd.read_csv(args.comparison_timings_csv)
comparison_timings.columns = comparison_timings.columns.str.strip()
cols_to_copy_from_lift = ["inputWidth", "inputHeight", "inputChannels", "padFuncX", "padFuncY", 
                        "kernelWidth", "kernelHeight", "kernelStrideX", "kernelStrideY", "numKernels"]

def copy_selected_columns(comparison_row):
    if comparison_row["layer"] in primary_lift_data["layer"].values:
        return primary_lift_data[primary_lift_data["layer"] == comparison_row["layer"]].iloc[0][cols_to_copy_from_lift]
    else:
        return [np.nan] * len(cols_to_copy_from_lift)

comparison_timings = comparison_timings.reindex(columns = comparison_timings.columns.tolist() + cols_to_copy_from_lift)
comparison_timings[cols_to_copy_from_lift] = comparison_timings.apply(copy_selected_columns, axis="columns", result_type='expand')

comparison_timings["n_layer_duplicates"] = comparison_timings.apply(get_n_layer_duplicates, axis="columns")

def get_throughput_vector(inputWidth, inputHeight, inputChannels, padFuncX, padFuncY, kernelWidth, kernelHeight, kernelStrideX, kernelStrideY, numKernels, total_runtime_median):
    n_windows_x = np.floor((inputWidth + 2*padFuncX - (kernelWidth - kernelStrideX) / kernelStrideX))
    n_windows_y = np.floor((inputHeight + 2*padFuncY - (kernelHeight - kernelStrideY) / kernelStrideY))

    macs_per_out_el = kernelWidth * kernelHeight
    macs_per_feat_map = macs_per_out_el * inputChannels * numKernels * n_windows_x * n_windows_y
    return ((1000 * macs_per_feat_map / total_runtime_median) / 1000000000)
    
comparison_timings["gflops_required"] = (get_throughput_vector(
    comparison_timings.inputWidth.to_numpy(), comparison_timings.inputHeight.to_numpy(), 
    comparison_timings.inputChannels.to_numpy(),
    comparison_timings.padFuncX.to_numpy(), comparison_timings.padFuncY.to_numpy(), 
    comparison_timings.kernelWidth.to_numpy(), comparison_timings.kernelHeight.to_numpy(), 
    comparison_timings.kernelStrideX.to_numpy(), comparison_timings.kernelStrideY.to_numpy(), 
    comparison_timings.numKernels.to_numpy(), 1) / 1000).round(decimals=2)
    
comparison_timings["throughputGflops"] = get_throughput_vector(
    comparison_timings.inputWidth.to_numpy(), comparison_timings.inputHeight.to_numpy(), 
    comparison_timings.inputChannels.to_numpy(),
    comparison_timings.padFuncX.to_numpy(), comparison_timings.padFuncY.to_numpy(), 
    comparison_timings.kernelWidth.to_numpy(), comparison_timings.kernelHeight.to_numpy(), 
    comparison_timings.kernelStrideX.to_numpy(), comparison_timings.kernelStrideY.to_numpy(), 
    comparison_timings.numKernels.to_numpy(), comparison_timings.time.to_numpy()).round(decimals=2)

print(comparison_timings)

def compute_throughput(lift_data):
    start = timer_start()
    lift_data["layer_Gflops_required"] = (get_throughput_vector(
        lift_data.inputWidth.to_numpy(), lift_data.inputHeight.to_numpy(), 
        lift_data.inputChannels.to_numpy(),
        lift_data.padFuncX.to_numpy(), lift_data.padFuncY.to_numpy(), 
        lift_data.kernelWidth.to_numpy(), lift_data.kernelHeight.to_numpy(), 
        lift_data.kernelStrideX.to_numpy(), lift_data.kernelStrideY.to_numpy(), 
        lift_data.numKernels.to_numpy(), 1) / 1000).round(decimals=2)

    lift_data["throughputGflops"] = get_throughput_vector(
        lift_data.inputWidth.to_numpy(), lift_data.inputHeight.to_numpy(), 
        lift_data.inputChannels.to_numpy(),
        lift_data.padFuncX.to_numpy(), lift_data.padFuncY.to_numpy(), 
        lift_data.kernelWidth.to_numpy(), lift_data.kernelHeight.to_numpy(), 
        lift_data.kernelStrideX.to_numpy(), lift_data.kernelStrideY.to_numpy(), 
        lift_data.numKernels.to_numpy(), lift_data.total_runtime_median.to_numpy()).round(decimals=2)
    timer_end(start, "get_throughput_vector")

comparison_memory = None
avg = None
proper_layer_names = {
    0: "L0",
    1: "L2",
    2: "L5",
    3: "L7",
    4: "L10",
    5: "L12, L14",
    7: "L17",
    8: "L19, L21",
    10: "L24, L26\nL28"}

def compute_mem_consumption(lift_data):
    global comparison_memory
    global avg

    lift_local_buffer_size_per_layer_manually_collected = {
        0: 768,
        1: 6144,
        2: 6144,
        3: 6144, 
        4: 6144,
        5: 6144,
        7: 6144,
        8: 6144,
        10: 1200
    }

    def padded_input_width(row):
        return (row.inputWidth + row.padFuncX*2 + row.padOptRight)

    def padded_input_height(row):
        return (row.inputHeight + row.padFuncY*2 + row.padOptBottom)

    def padded_input_size(row):
        return padded_input_width(row) * padded_input_height(row) * row.inputChannels

    def padded_output_size(row):
        return ((padded_input_width(row) - (row.kernelWidth - row.kernelStrideX) ) / row.kernelStrideX) * \
                ((padded_input_height(row) - (row.kernelHeight - row.kernelStrideY) ) / row.kernelStrideY) * \
                row.numKernels

    def output_size(row):
        return (((row.inputWidth + row.padFuncX*2) - (row.kernelWidth - row.kernelStrideX) ) / row.kernelStrideX) * \
                (((row.inputHeight + row.padFuncY*2) - (row.kernelHeight - row.kernelStrideY) ) / row.kernelStrideY) * \
                row.numKernels

    def memory_consumptionMB(row):
        return ((padded_input_size(row) + lift_local_buffer_size_per_layer_manually_collected[row.layer] + \
                padded_output_size(row)) * 4) / (1024*1024)

    lift_data["mem_consumptionMB"] = lift_data.apply(lambda row: memory_consumptionMB(row), axis="columns")


    total_vgg_gflops = (lift_data.layer_Gflops_required * lift_data.n_layer_duplicates).sum()
    avg = pd.DataFrame(data={
        "avgallconv": [
            1000 * total_vgg_gflops / ((lift_data.total_runtime_median * lift_data.n_layer_duplicates).sum()),
            1000 * total_vgg_gflops / 
                (comparison_timings[comparison_timings["framework"] == "armcl-direct"].time * 
                    comparison_timings[comparison_timings["framework"] == "armcl-direct"].n_layer_duplicates).sum(),
            1000 * total_vgg_gflops / 
                (comparison_timings[comparison_timings["framework"] == "armcl-gemm"].time * 
                    comparison_timings[comparison_timings["framework"] == "armcl-gemm"].n_layer_duplicates).sum(),
            1000 * total_vgg_gflops / 
                (comparison_timings[comparison_timings["framework"] == "tvm"].time * 
                    comparison_timings[comparison_timings["framework"] == "tvm"].n_layer_duplicates).sum()
        ],
        "avg": [
            lift_data.throughputGflops.mean(),
            comparison_timings[comparison_timings["framework"] == "armcl-direct"].throughputGflops.mean(),
            comparison_timings[comparison_timings["framework"] == "armcl-gemm"].throughputGflops.mean(),
            comparison_timings[comparison_timings["framework"] == "tvm"].throughputGflops.mean()
        ], 
        })
    print(avg)

    comparison_memory = pd.read_csv(args.comparison_memory_csv)
    comparison_memory.columns = comparison_memory.columns.str.strip()
    comparison_memory.rename(columns={"Layer": "layer", "Direct": "direct-inputMB", "GEMM": "gemm-inputMB", "tvm": "tvmMemConsumptionMB"}, inplace = True)

    def get_output_size(comparison_row):
        if comparison_row["layer"] in lift_data["layer"].values:
            return output_size(lift_data[lift_data.layer == comparison_row.layer].iloc[0])*4 / (1024*1024)
        else:
            return np.nan
            
    comparison_memory["output_sizeMB"] = comparison_memory.apply(get_output_size, axis="columns")
    comparison_memory["directMemConsumptionMB"] = comparison_memory["direct-inputMB"] + comparison_memory["output_sizeMB"]
    comparison_memory["gemmMemConsumptionMB"] = comparison_memory["gemm-inputMB"] + comparison_memory["output_sizeMB"]


compute_throughput(primary_lift_data)
compute_mem_consumption(primary_lift_data)

lift_data = primary_lift_data

lift_data_aggregated = pd.DataFrame(columns=["throughputGflops", "layer", "mem_consumptionMB"])
for layer in lift_data["layer"].unique():
    rows = lift_data[lift_data["layer"] == layer]
    row = lift_data.iloc[rows["throughputGflops"].idxmax()]

    lift_data_aggregated = lift_data_aggregated.append(pd.Series({
        "throughputGflops": row.throughputGflops, 
        "layer": row.layer, 
        "mem_consumptionMB": row.mem_consumptionMB}), ignore_index = True)

print(lift_data_aggregated)

plot1_df_timings =\
    lift_data_aggregated.rename(columns={
        "throughputGflops": "liftThroughputGflops"}
        )[["layer","liftThroughputGflops"]].merge(
    comparison_timings[comparison_timings["framework"] == "armcl-direct"].rename(columns={
        "throughputGflops": "armclDirectThroughputGflops"}
        )[["layer","armclDirectThroughputGflops"]], 
    how="inner", left_on=["layer"], right_on=["layer"]).merge(
    comparison_timings[comparison_timings["framework"] == "armcl-gemm"].rename(columns={
        "throughputGflops": "armclGemmThroughputGflops"}
        )[["layer","armclGemmThroughputGflops"]], 
    how="inner", left_on=["layer"], right_on=["layer"]).merge(
    comparison_timings[comparison_timings["framework"] == "tvm"].rename(columns={
        "throughputGflops": "tvmThroughputGflops"}
        )[["layer","tvmThroughputGflops"]], 
    how="inner", left_on=["layer"], right_on=["layer"])

plot1_df_mem =\
    lift_data_aggregated.rename(columns={
        "mem_consumptionMB": "liftMemConsumptionMB"}
        )[["layer","liftMemConsumptionMB"]].merge(
    comparison_memory[["layer","directMemConsumptionMB"]], 
    how="inner", left_on=["layer"], right_on=["layer"]).merge(
    comparison_memory[["layer","gemmMemConsumptionMB"]], 
    how="inner", left_on=["layer"], right_on=["layer"]).merge(
    comparison_memory[["layer","tvmMemConsumptionMB"]], 
    how="inner", left_on=["layer"], right_on=["layer"])

fig, axes = plt.subplots(2, 3, figsize = (13, 4), gridspec_kw={'width_ratios': [7, 1, 1]})

# Timings
plot1_df_timings.drop(columns=["layer"]).plot.bar(ax = axes[0,0], rot = rot, color=colors,
        edgecolor="black", width=0.6, align='center', legend=False)
        
# axes[0,0].title.set_text('Performance')
axes[0,0].legend(labels = ["Lift", "ARM-CL-Direct", "ARM-CL-GEMM", "TVM"], 
                prop={'size': 10}, 
                loc="upper right",
                ncol=len(plot1_df_timings.columns))
axes[0,0].set_xticklabels(plot1_df_timings["layer"].apply(lambda name : proper_layer_names[name]))
axes[0][0].tick_params(axis='x', which='major', pad=-3)
axes[0][0].grid(False,axis="x")
axes[0][0].grid(True,axis="y")

axes[0][0].set_ylabel(r'\begin{center}\textbf{Performance\\\lbrack GFLOPs per sec\rbrack}\end{center}', horizontalalignment="center", va="bottom")
# axes[0][0].set_yscale('log')
axes[0][0].set_ylim(bottom = axes[0][0].get_ylim()[0], top = axes[0][0].get_ylim()[1] * 1.05)

plt.subplots_adjust(wspace=0.05)

# avg = pd.DataFrame(data=[np.mean(values[0:9], axis=0)])
# print(avg)

avg["avg"].transpose().plot.bar(
    ax = axes[0][1], rot = rot, color=colors,
    edgecolor="black", width=1, align='center', legend=False)
axes[0][1].set_xticklabels([])
axes[0][1].set_xlabel(r'\begin{center}AVG\\\end{center}', horizontalalignment="center")

axes[0][1].get_yaxis().set_visible(True)
axes[0][1].get_yaxis().set_ticklabels([])
axes[0][1].grid(False,axis="x")
axes[0][1].grid(True,axis="y")
# axes[0][1].set_yscale('log')
axes[0][1].set_ylim(bottom = axes[0][1].get_ylim()[0], top = axes[0][1].get_ylim()[1])
axes[0][1].set_ylim(bottom = axes[0][0].get_ylim()[0], top = axes[0][0].get_ylim()[1])
axes[0][1].tick_params(axis='x', which='major', pad=-7)

custom_fontsize = 13

r = "horizontal"
x_offset = 0.4
y_offset_normal = 3.5
weight="black"
axes[0][1].text(x = 0 - x_offset, y = avg.loc[0, "avg"] + y_offset_normal, 
    s='{:.0f}'.format(avg.loc[0, "avg"]), size = custom_fontsize, fontweight=weight, rotation=r)
axes[0][1].text(x = 1 - x_offset+0.2, y = avg.loc[1, "avg"] + y_offset_normal, 
    s='{:.0f}'.format(avg.loc[1, "avg"]), size = custom_fontsize, fontweight=weight, rotation=r)
# axes[0][1].text(x = 2 - x_offset+0.05, y = avg.loc[2, "avg"] - 2.5*y_offset_normal, 
axes[0][1].text(x = 2 - x_offset+0.05, y = avg.loc[2, "avg"] + y_offset_normal, 
    s='{:.0f}'.format(avg.loc[2, "avg"]), size = custom_fontsize, fontweight=weight, rotation=r)#, color="#ffffff")
axes[0][1].text(x = 3 - x_offset+0.05, y = avg.loc[3, "avg"] + y_offset_normal, 
    s='{:.0f}'.format(avg.loc[3, "avg"]), size = custom_fontsize, fontweight=weight, rotation=r)#, color="#ffffff")


avg["avgallconv"].transpose().plot.bar(
    ax = axes[0][2], rot = rot, color=colors,
    edgecolor="black", width=1, align='center', legend=False)
axes[0][2].set_xticklabels([])
axes[0][2].set_xlabel(r'\begin{center}ALL CONV\end{center}', horizontalalignment="center")

axes[0][2].get_yaxis().set_visible(True)
axes[0][2].get_yaxis().set_ticklabels([])
axes[0][2].grid(False,axis="x")
axes[0][2].grid(True,axis="y")
# axes[0][2].set_yscale('log')
# axes[0][2].set_ylim(bottom = axes[0][2].get_ylim()[0], top = axes[0][2].get_ylim()[1])
axes[0][2].set_ylim(bottom = axes[0][0].get_ylim()[0], top = axes[0][0].get_ylim()[1])
axes[0][2].tick_params(axis='x', which='major', pad=-7)

r = "horizontal"
x_offset = 0.4
y_offset_normal = 3.5
weight="black"
axes[0][2].text(x = 0 - x_offset, y = avg.loc[0, "avgallconv"] + y_offset_normal, 
    s='{:.0f}'.format(avg.loc[0, "avgallconv"]), size = custom_fontsize, fontweight=weight, rotation=r)
axes[0][2].text(x = 1 - x_offset+0.2, y = avg.loc[1, "avgallconv"] + y_offset_normal, 
    s='{:.0f}'.format(avg.loc[1, "avgallconv"]), size = custom_fontsize, fontweight=weight, rotation=r)
axes[0][2].text(x = 2 - x_offset, y = avg.loc[2, "avgallconv"] + y_offset_normal, 
    s='{:.0f}'.format(avg.loc[2, "avgallconv"]), size = custom_fontsize, fontweight=weight, rotation=r)#, color="#ffffff")
axes[0][2].text(x = 3 - x_offset + 0.1, y = avg.loc[3, "avgallconv"] + y_offset_normal, 
    s='{:.0f}'.format(avg.loc[3, "avgallconv"]), size = custom_fontsize, fontweight=weight, rotation=r)#, color="#ffffff")


plt.subplots_adjust(hspace=0.4)

# Memory
plot1_df_mem.drop(columns=["layer"]).plot.bar(ax = axes[1,0], rot = rot, color=colors,
        edgecolor="black", width=0.6, align='center', legend=False)

# axes[1,0].title.set_text('Memory consumption, MB')
axes[1,0].legend(labels = ["Lift", "Direct", "GEMM", "TVM"], 
                prop={'size': 10}, 
                loc="upper right",
                ncol=len(plot1_df_timings.columns))
axes[1,0].set_ylabel(r'\begin{center}\textbf{Memory consumption\\\lbrack Mbyte\rbrack}\end{center}', labelpad=2)
axes[1,0].set_xlabel(r'\begin{center}\textbf{VGG layer}\end{center}', horizontalalignment="center", labelpad=-8)
axes[1,0].set_xticklabels(plot1_df_timings["layer"].apply(lambda name : proper_layer_names[name]))
axes[1,0].tick_params(axis='x', which='major', pad=-3)
axes[1,0].set_yscale('log')
from matplotlib.ticker import ScalarFormatter
axes[1,0].yaxis.set_major_formatter(ScalarFormatter())
axes[1][0].grid(False,axis="x")
axes[1][0].grid(True,axis="y")
    

avg_mem = pd.DataFrame(data={
    "avg": [
        lift_data_aggregated["mem_consumptionMB"].mean(),
        comparison_memory["directMemConsumptionMB"].mean(),
        comparison_memory["gemmMemConsumptionMB"].mean(),
        comparison_memory["tvmMemConsumptionMB"].mean(),
    ],
    "max": [
        lift_data_aggregated["mem_consumptionMB"].max(),
        comparison_memory["directMemConsumptionMB"].max(),
        comparison_memory["gemmMemConsumptionMB"].max(),
        comparison_memory["tvmMemConsumptionMB"].max(),
    ]})
print(avg_mem)
    
avg_mem["avg"].transpose().plot.bar(
    ax = axes[1][1], rot = rot, color=colors,
    edgecolor="black", width=1, align='center', legend=False)
axes[1][1].set_xticklabels([])
axes[1][1].set_xlabel(r'\begin{center}AVG\end{center}', horizontalalignment="center")

axes[1][1].set_yscale('log')
axes[1][1].set_ylim(bottom = axes[1][0].get_ylim()[0], top = axes[1][0].get_ylim()[1])
axes[1][1].get_yaxis().set_visible(True)
axes[1][1].get_yaxis().set_ticklabels([])
axes[1][1].grid(False,axis="x")
axes[1][1].grid(True,axis="y")
axes[1][1].tick_params(axis='x', which='major', pad=-7)

custom_fontsize = 13
r = "horizontal"
x_offset = 0.3
y_offset_normal = 1.5
weight="black"
axes[1][1].text(x = 0 - x_offset, y = avg_mem.loc[0, "avg"] * y_offset_normal, 
    s='{:.0f}'.format(avg_mem.loc[0, "avg"]), size = custom_fontsize, fontweight=weight, rotation=r)
axes[1][1].text(x = 1 - x_offset+0.1, y = avg_mem.loc[1, "avg"] * y_offset_normal, 
    s='{:.0f}'.format(avg_mem.loc[1, "avg"]), size = custom_fontsize, fontweight=weight, rotation=r)
axes[1][1].text(x = 2 - x_offset - 0.1, y = avg_mem.loc[2, "avg"] * y_offset_normal, 
    s='{:.0f}'.format(avg_mem.loc[2, "avg"]), size = custom_fontsize, fontweight=weight, rotation=r)#, color="#ffffff")
axes[1][1].text(x = 3 - x_offset, y = avg_mem.loc[3, "avg"] * y_offset_normal, 
    s='{:.0f}'.format(avg_mem.loc[3, "avg"]), size = custom_fontsize, fontweight=weight, rotation=r)#, color="#ffffff")

avg_mem["max"].transpose().plot.bar(
    ax = axes[1][2], rot = rot, color=colors,
    edgecolor="black", width=1, align='center', legend=False)
axes[1][2].set_xticklabels([])
axes[1][2].set_xlabel(r'\begin{center}MAX\\\end{center}', horizontalalignment="center")

axes[1][2].set_yscale('log')
axes[1][2].set_ylim(bottom = axes[1][0].get_ylim()[0], top = axes[1][0].get_ylim()[1])
axes[1][2].get_yaxis().set_visible(True)
axes[1][2].get_yaxis().set_ticklabels([])
axes[1][2].grid(False,axis="x")
axes[1][2].grid(True,axis="y")
axes[1][2].tick_params(axis='x', which='major', pad=-7)

r = "horizontal"
x_offset = 0.35
y_offset_normal = 1.4
weight="black"
axes[1][2].text(x = 0 - x_offset, y = avg_mem.loc[0, "max"] * y_offset_normal, 
    s='{:.0f}'.format(avg_mem.loc[0, "max"]), size = custom_fontsize, fontweight=weight, rotation=r)
axes[1][2].text(x = 1 - x_offset, y = avg_mem.loc[1, "max"] * y_offset_normal, 
    s='{:.0f}'.format(avg_mem.loc[1, "max"]), size = custom_fontsize, fontweight=weight, rotation=r)
axes[1][2].text(x = 2 - x_offset+0.1, y = avg_mem.loc[2, "max"] * (0.25 / y_offset_normal), 
    s='{:.0f}'.format(avg_mem.loc[2, "max"]), size = custom_fontsize, fontweight=weight, rotation="vertical", color="#ffffff")
axes[1][2].text(x = 3 - x_offset, y = avg_mem.loc[3, "max"] * (0.4 / y_offset_normal), 
    s='{:.0f}'.format(avg_mem.loc[3, "max"]), size = custom_fontsize, fontweight=weight, rotation=r, color="#ffffff")

plt.savefig(join(args.plot_path))
# plt.show()
plt.close()