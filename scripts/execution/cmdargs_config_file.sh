#!/bin/bash

BOARD_WD="/home/shunya/cc22-artifact"

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -cf|--config_file)
    CONFIG_FILE="$2"
    shift # past argument
    shift # past value
    ;;
    -mh|--monitor_hanging)
    MONITOR_HANGING="$2"
    shift # past argument
    shift # past value
    ;;
    -mm|--monitor_mem)
    MONITOR_MEM="$2"
    shift # past argument
    shift # past value
    ;;
    -mp|--monitor_power)
    MONITOR_POWER="$2"
    shift # past argument
    shift # past value
    ;;
    -mt|--monitor_top)
    MONITOR_TOP="$2"
    shift # past argument
    shift # past value
    ;;
    -mT|--monitor_temp)
    MONITOR_TEMP="$2"
    shift # past argument
    shift # past value
    ;;
    -|--monitor_perf_and_cool_down)
    MONITOR_PERF_AND_COOL_DOWN="$2"
    shift # past argument
    shift # past value
    ;;
    -|--skip_verification)
    SKIP_VERIFICATION="$2"
    shift # past argument
    shift # past value
    ;;
    -|--meta_run)
    META_RUN="$2"
    shift # past argument
    shift # past value
    ;;
    -n|--network_name)
    NETWORK_NAME="$2"
    shift # past argument
    shift # past value
    ;;
    -nl|--network_label)
    NETWORK_LABEL="$2"
    shift # past argument
    shift # past value
    ;;
    # -f|--c_files_path)
    # C_FILES_PATH="$2"
    # shift # past argument
    # shift # past value
    # ;;
    # -i|--ip)
    # BOARD_IP="$2"
    # shift # past argument
    # shift # past value
    # ;;
    -b|--board_number)
    BOARD_NUMBER="$2"
    shift # past argument
    shift # past value
    ;;
    -u|--bulk_upload)
    BULK_UPLOAD="$2"
    shift # past argument
    shift # past value
    ;;
    -od|--use_old_data_layout)
    USE_OLD_DATA_LAYOUT="$2"
    shift # past argument
    shift # past value
    ;;
    -ge|--use_gemm)
    USE_GEMM="$2"
    shift # past argument
    shift # past value
    ;;
    -su|--skip_upload)
    SKIP_UPLOAD="$2"
    shift # past argument
    shift # past value
    ;;
    -pc|--parameter_csv)
    PARAMETER_CSV="$2"
    shift # past argument
    shift # past value
    ;;
    -sp|--skip_prepad)
    SKIP_PREPAD="$2"
    shift # past argument
    shift # past value
    ;;
    # -ru|--run_four_kernels)
    # RUN_FOUR_KERNELS="$2"
    # shift # past argument
    # shift # past value
    # ;;
    # -lf|--layer_first)
    # LAYER_FIRST="$2"
    # shift # past argument
    # shift # past value
    # ;;
    # -ll|--layer_last)
    # LAYER_LAST="$2"
    # shift # past argument
    # shift # past value
    # ;;
    # -tf|--tunepoint_first)
    # TUNEPOINT_FIRST="$2"
    # shift # past argument
    # shift # past value
    # ;;
    # -tl|--tunepoint_last)
    # TUNEPOINT_LAST="$2"
    # shift # past argument
    # shift # past value
    # ;;
    # -l|--lib)
    # LIBPATH="$2"
    # shift # past argument
    # shift # past value
    # ;;
    # --default)
    # DEFAULT=YES
    # shift # past argument
    # ;;
    # *)    # unknown option
    # POSITIONAL+=("$1") # save it in an array for later
    # shift # past argument
    # ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# echo FILE C_FILES_POSTFIX  = "${C_FILES_POSTFIX}"
# echo SEARCH PATH     = "${SEARCHPATH}"
# echo LIBRARY PATH    = "${LIBPATH}"
# echo DEFAULT         = "${DEFAULT}"
# echo "Number files in SEARCH PATH with EXTENSION:" $(ls -1 "${SEARCHPATH}"/*."${EXTENSION}" | wc -l)
# if [[ -n $1 ]]; then
#     echo "Last line of file specified as non-opt/last argument:"
#     tail -1 "$1"
# fi

. "$ROOTDIR/scripts/execution/get_board_ip.sh"
