
#include <bits/stdc++.h>

using namespace std;

    

#include <iostream>
    
#include <CL/cl2.hpp>
    
#include <fstream>

std::string readFile(const char *filename){

  std::ifstream in(filename, std::ios::in);

  if (in.fail())
  {
  std::cerr << "Error reading file " << filename << std::endl;
  exit(1); }

  std::string contents;
  in.seekg(0, std::ios::end);
  contents.resize(in.tellg());
  in.seekg(0, std::ios::beg);
  in.read(&contents[0], contents.size());
  in.close();
  return contents;
  }

    

int platformId = 0;
int deviceId = 0;

 std::vector<cl::Platform> allPlatforms;
 cl::Platform platform;
 std::vector<cl::Device> allDevices;
 cl::Device device;
 cl::Context context;
 cl::CommandQueue lift_queue;

 size_t lift_global_0 = 1, lift_global_1 = 1, lift_global_2 =1;

      ; 
std::string kernel_string_21739;
cl::Program::Sources kernel_source_21739;
cl::Program kernel_program_21739;
cl::Kernel kernel_21739;
std::chrono::milliseconds cpu_clock_start_21739;
std::chrono::milliseconds cpu_clock_end_21739;
cl::Event event_21739;
; 
; 
; 
; 
; 
; 
; 
; 
; 
void lift_init(const std::string & pwd){
    
	cl::Platform::get(&allPlatforms);
 if (allPlatforms.size() == 0) {
 std::cerr << " No platforms found. Check OpenCL installation!" << std::endl;
 exit(1);
 }

 platform = allPlatforms[platformId];

 platform.getDevices(CL_DEVICE_TYPE_ALL, &allDevices);
 if (allDevices.size() == 0) {
 std::cerr << " No devices found. Check OpenCL installation!" << std::endl;
 exit(1);
 }

 device = allDevices[deviceId];

 std::cerr << "Using platform: " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl;
 std::cerr << "Using device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;

 // create context
 cl::Context tmp_context({ device });
 context = std::move(tmp_context);

 // create queue
 cl::CommandQueue tmp_queue(context, device, CL_QUEUE_PROFILING_ENABLE);
 lift_queue = std::move(tmp_queue);
      ; 
    kernel_string_21739 = readFile((pwd + "/kernel_21739.cl").c_str()); 
    kernel_source_21739 = cl::Program::Sources(1, {kernel_string_21739.c_str(), kernel_string_21739.length()}); 
    kernel_program_21739 = cl::Program(context, kernel_source_21739); 
    if ((kernel_program_21739.build({ device }) != CL_SUCCESS)){
        std::cerr << "kernel build error" << std::endl; 
        char* log; 
        size_t logsize; 
        assert((clGetProgramBuildInfo(kernel_program_21739.get(), device.get(), CL_PROGRAM_BUILD_LOG, 0, NULL, &logsize) == CL_SUCCESS)); 
        log = (char*)malloc(sizeof(char) * logsize); 
        assert((clGetProgramBuildInfo(kernel_program_21739.get(), device.get(), CL_PROGRAM_BUILD_LOG, logsize, log, NULL) == CL_SUCCESS)); 
        std::cerr << log << std::endl; 
        exit(1); 
    }
    kernel_21739 = cl::Kernel(kernel_program_21739, "KERNEL"); 
}

double cpu_time_in_ms( std::chrono::milliseconds start, std::chrono::milliseconds finish ){
 return (finish - start).count();
}

double gpu_time_in_ms( cl::Event event ){

  cl_ulong start;
  cl_ulong end;
  cl_int err;

  event.wait();

  err = clGetEventProfilingInfo(event(), CL_PROFILING_COMMAND_START,
                                sizeof(start), &start, NULL);
  assert(err == CL_SUCCESS);

  err = clGetEventProfilingInfo(event(), CL_PROFILING_COMMAND_END,
                                sizeof(end), &end, NULL);
  assert(err == CL_SUCCESS);

  return ((double)(end - start)) * 1.0e-6;
}

double diff_percent(double lhs, double rhs) {
  if(std::min(lhs,rhs)==0)
    return -1;
  else
    return std::abs(lhs-rhs)/std::min(lhs,rhs);
}

          ; 
void print_clock(){
    std::cerr<<"func_name, cpu_time_ms, gpu_time_ms, diff_percentage"<<std::endl; 
    {
        double cpu_time_ms = cpu_time_in_ms(cpu_clock_start_21739, cpu_clock_end_21739);
        double gpu_time_ms = gpu_time_in_ms(event_21739);
        double diff_pc = diff_percent(cpu_time_ms, gpu_time_ms);
        std::cerr<<"OclFunCall_21739"<<", "<<cpu_time_ms<<", "<<gpu_time_ms<<", "<<diff_pc<<std::endl; 
    }
}
void post_execute(){
    print_clock(); 
}

namespace lift {; 

void execute(float * v_initial_param_21730_11065, float * v_initial_param_21731_11066, float * & v_user_func_21740_11070){
    // Allocate memory for output pointers
    cl::Buffer v_user_func_21736_11067(context, CL_MEM_READ_WRITE, (591872 * sizeof(float)));
    cl::Buffer v_user_func_21738_11068(context, CL_MEM_READ_WRITE, (2359296 * sizeof(float)));
    cl::Buffer v_user_func_21739_11069(context, CL_MEM_READ_WRITE, (524288 * sizeof(float)));
    v_user_func_21740_11070 = reinterpret_cast<float *>(malloc((524288 * sizeof(float)))); 
    ; 
    lift_queue.enqueueWriteBuffer(v_user_func_21736_11067, CL_TRUE, 0, (591872 * sizeof(float)), v_initial_param_21730_11065, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    ; 
    lift_queue.enqueueWriteBuffer(v_user_func_21738_11068, CL_TRUE, 0, (2359296 * sizeof(float)), v_initial_param_21731_11066, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    kernel_21739.setArg(0, v_user_func_21736_11067); 
    kernel_21739.setArg(1, v_user_func_21738_11068); 
    kernel_21739.setArg(2, v_user_func_21739_11069); 
    cpu_clock_start_21739 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    cerr << "thread block config(local): cl::NDRange(8,3,16)" << endl << "thread block config(global): cl::NDRange(1024,48,16)" << endl;
    lift_queue.enqueueNDRangeKernel(kernel_21739, cl::NullRange, cl::NDRange(1024,48,16), cl::NDRange(8,3,16), NULL, (&event_21739)); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    cpu_clock_end_21739 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    ; 
    lift_queue.enqueueReadBuffer(v_user_func_21739_11069, CL_TRUE, 0, (524288 * sizeof(float)), v_user_func_21740_11070, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    post_execute(); 
}
}; 