FROM ubuntu:21.04

ARG DEBIAN_FRONTEND=noninteractive
ENV LANG=C.UTF-8

# Install basics
RUN \
    apt-get update && \
    apt-get install -y \
        vim \
        rsync \
        wget \
        curl \
        git && \
    curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash && \
    apt-get install -y git-lfs && \
    git lfs install

# Install LaTeX
RUN \
    apt-get update && \
    apt-get install -y \
        dvipng \
        texlive-latex-extra \
        texlive-fonts-recommended \
        cm-super

# Install Python and Packages
RUN \
    apt-get update && \
    apt-get install -y \
        python3.9 \
        python3-pip \
        python-is-python3 && \
    pip3 install \
        matplotlib \
        seaborn \
        scipy \
        pandas

# Install JDK
RUN \
    apt-get update && \
    apt-get install -y --no-install-recommends openjdk-8-jdk

# Set environment variables
ENV ROOTDIR=/home
ENV DATADIR=$ROOTDIR/input_data/

# adjust if installed in a different dir
ENV SBT=$ROOTDIR/tools/sbt
ENV LIFT=$ROOTDIR/tools/lift

# Defines the number of runs per kernel
# from which we report the median
ENV ITERATIONS=3

ENV PATH=$ROOTDIR/scripts:$PATH
ENV PATH=$SBT/bin:$PATH
ENV PATH=$LIFT/scripts/compiled_scripts:$PATH
ENV PATH=$ROOTDIR/tools/cmake-3.9.5-Linux-x86_64/bin:$PATH

# Copy scripts and prepare the tools dir
COPY /scripts $ROOTDIR/scripts
COPY /tools $ROOTDIR/tools

# Install SBT
RUN $ROOTDIR/scripts/installation/build_sbt.sh

# Install Lift
RUN $ROOTDIR/scripts/installation/build_lift.sh

# Copy the rest of the files
COPY /benchmarks $ROOTDIR/benchmarks
COPY /golden_values $ROOTDIR/golden_values
COPY /input_data $ROOTDIR/input_data
COPY /layer_configs $ROOTDIR/layer_configs
COPY /logs $ROOTDIR/logs
COPY /plots $ROOTDIR/plots
COPY /README.md $ROOTDIR/README.md
