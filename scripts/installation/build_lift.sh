#!/bin/bash
set -e
: "${ROOTDIR:?Please set artifact env-vars: $ source environment.env}"

pushd $ROOTDIR/tools > /dev/null
git clone https://github.com/lift-project/lift.git
pushd lift > /dev/null
git checkout cc22-artifact
./updateSubmodules.sh
sbt compile
pushd scripts > /dev/null
python buildRunScripts.py
