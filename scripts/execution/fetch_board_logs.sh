#!/usr/bin/env bash

. "$ROOTDIR/scripts/execution/cmdargs_config_file.sh"

logdir=logs/${NETWORK_NAME}/${NETWORK_LABEL}
toplogdir=toplogs/${NETWORK_NAME}/${NETWORK_LABEL}

mkdir -p $ROOTDIR/benchmarks/lift/board_$logdir
mkdir -p $ROOTDIR/benchmarks/lift/board_$toplogdir

rsync -e "ssh -p ${BOARD_PORT}" -avz ${BOARD_IP}:$BOARD_WD/${logdir}/* $ROOTDIR/benchmarks/lift/board_${logdir}/
rsync -e "ssh -p ${BOARD_PORT}" -avz ${BOARD_IP}:$BOARD_WD/${toplogdir}/* $ROOTDIR/benchmarks/lift/board_${toplogdir}/
