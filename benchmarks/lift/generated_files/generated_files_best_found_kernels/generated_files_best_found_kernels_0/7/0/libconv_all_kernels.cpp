
#include <bits/stdc++.h>

using namespace std;

    

#include <iostream>
    
#include <CL/cl2.hpp>
    
#include <fstream>

std::string readFile(const char *filename){

  std::ifstream in(filename, std::ios::in);

  if (in.fail())
  {
  std::cerr << "Error reading file " << filename << std::endl;
  exit(1); }

  std::string contents;
  in.seekg(0, std::ios::end);
  contents.resize(in.tellg());
  in.seekg(0, std::ios::beg);
  in.read(&contents[0], contents.size());
  in.close();
  return contents;
  }

    

int platformId = 0;
int deviceId = 0;

 std::vector<cl::Platform> allPlatforms;
 cl::Platform platform;
 std::vector<cl::Device> allDevices;
 cl::Device device;
 cl::Context context;
 cl::CommandQueue lift_queue;

 size_t lift_global_0 = 1, lift_global_1 = 1, lift_global_2 =1;

      ; 
std::string kernel_string_22789;
cl::Program::Sources kernel_source_22789;
cl::Program kernel_program_22789;
cl::Kernel kernel_22789;
std::string kernel_string_22792;
cl::Program::Sources kernel_source_22792;
cl::Program kernel_program_22792;
cl::Kernel kernel_22792;
std::string kernel_string_22793;
cl::Program::Sources kernel_source_22793;
cl::Program kernel_program_22793;
cl::Kernel kernel_22793;
std::chrono::milliseconds cpu_clock_start_22793;
std::chrono::milliseconds cpu_clock_end_22793;
cl::Event event_22793;
std::chrono::milliseconds cpu_clock_start_22792;
std::chrono::milliseconds cpu_clock_end_22792;
cl::Event event_22792;
std::chrono::milliseconds cpu_clock_start_22789;
std::chrono::milliseconds cpu_clock_end_22789;
cl::Event event_22789;
; 
; 
; 
; 
; 
; 
; 
; 
; 
void lift_init(const std::string & pwd){
    
	cl::Platform::get(&allPlatforms);
 if (allPlatforms.size() == 0) {
 std::cerr << " No platforms found. Check OpenCL installation!" << std::endl;
 exit(1);
 }

 platform = allPlatforms[platformId];

 platform.getDevices(CL_DEVICE_TYPE_ALL, &allDevices);
 if (allDevices.size() == 0) {
 std::cerr << " No devices found. Check OpenCL installation!" << std::endl;
 exit(1);
 }

 device = allDevices[deviceId];

 std::cerr << "Using platform: " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl;
 std::cerr << "Using device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;

 // create context
 cl::Context tmp_context({ device });
 context = std::move(tmp_context);

 // create queue
 cl::CommandQueue tmp_queue(context, device, CL_QUEUE_PROFILING_ENABLE);
 lift_queue = std::move(tmp_queue);
      ; 
    kernel_string_22789 = readFile((pwd + "/kernel_22789.cl").c_str()); 
    kernel_source_22789 = cl::Program::Sources(1, {kernel_string_22789.c_str(), kernel_string_22789.length()}); 
    kernel_program_22789 = cl::Program(context, kernel_source_22789); 
    if ((kernel_program_22789.build({ device }) != CL_SUCCESS)){
        std::cerr << "kernel build error" << std::endl; 
        char* log; 
        size_t logsize; 
        assert((clGetProgramBuildInfo(kernel_program_22789.get(), device.get(), CL_PROGRAM_BUILD_LOG, 0, NULL, &logsize) == CL_SUCCESS)); 
        log = (char*)malloc(sizeof(char) * logsize); 
        assert((clGetProgramBuildInfo(kernel_program_22789.get(), device.get(), CL_PROGRAM_BUILD_LOG, logsize, log, NULL) == CL_SUCCESS)); 
        std::cerr << log << std::endl; 
        exit(1); 
    }
    kernel_22789 = cl::Kernel(kernel_program_22789, "KERNEL"); 
    kernel_string_22792 = readFile((pwd + "/kernel_22792.cl").c_str()); 
    kernel_source_22792 = cl::Program::Sources(1, {kernel_string_22792.c_str(), kernel_string_22792.length()}); 
    kernel_program_22792 = cl::Program(context, kernel_source_22792); 
    if ((kernel_program_22792.build({ device }) != CL_SUCCESS)){
        std::cerr << "kernel build error" << std::endl; 
        char* log; 
        size_t logsize; 
        assert((clGetProgramBuildInfo(kernel_program_22792.get(), device.get(), CL_PROGRAM_BUILD_LOG, 0, NULL, &logsize) == CL_SUCCESS)); 
        log = (char*)malloc(sizeof(char) * logsize); 
        assert((clGetProgramBuildInfo(kernel_program_22792.get(), device.get(), CL_PROGRAM_BUILD_LOG, logsize, log, NULL) == CL_SUCCESS)); 
        std::cerr << log << std::endl; 
        exit(1); 
    }
    kernel_22792 = cl::Kernel(kernel_program_22792, "KERNEL"); 
    kernel_string_22793 = readFile((pwd + "/kernel_22793.cl").c_str()); 
    kernel_source_22793 = cl::Program::Sources(1, {kernel_string_22793.c_str(), kernel_string_22793.length()}); 
    kernel_program_22793 = cl::Program(context, kernel_source_22793); 
    if ((kernel_program_22793.build({ device }) != CL_SUCCESS)){
        std::cerr << "kernel build error" << std::endl; 
        char* log; 
        size_t logsize; 
        assert((clGetProgramBuildInfo(kernel_program_22793.get(), device.get(), CL_PROGRAM_BUILD_LOG, 0, NULL, &logsize) == CL_SUCCESS)); 
        log = (char*)malloc(sizeof(char) * logsize); 
        assert((clGetProgramBuildInfo(kernel_program_22793.get(), device.get(), CL_PROGRAM_BUILD_LOG, logsize, log, NULL) == CL_SUCCESS)); 
        std::cerr << log << std::endl; 
        exit(1); 
    }
    kernel_22793 = cl::Kernel(kernel_program_22793, "KERNEL"); 
}

double cpu_time_in_ms( std::chrono::milliseconds start, std::chrono::milliseconds finish ){
 return (finish - start).count();
}

double gpu_time_in_ms( cl::Event event ){

  cl_ulong start;
  cl_ulong end;
  cl_int err;

  event.wait();

  err = clGetEventProfilingInfo(event(), CL_PROFILING_COMMAND_START,
                                sizeof(start), &start, NULL);
  assert(err == CL_SUCCESS);

  err = clGetEventProfilingInfo(event(), CL_PROFILING_COMMAND_END,
                                sizeof(end), &end, NULL);
  assert(err == CL_SUCCESS);

  return ((double)(end - start)) * 1.0e-6;
}

double diff_percent(double lhs, double rhs) {
  if(std::min(lhs,rhs)==0)
    return -1;
  else
    return std::abs(lhs-rhs)/std::min(lhs,rhs);
}

          ; 
void print_clock(){
    std::cerr<<"func_name, cpu_time_ms, gpu_time_ms, diff_percentage"<<std::endl; 
    {
        double cpu_time_ms = cpu_time_in_ms(cpu_clock_start_22793, cpu_clock_end_22793);
        double gpu_time_ms = gpu_time_in_ms(event_22793);
        double diff_pc = diff_percent(cpu_time_ms, gpu_time_ms);
        std::cerr<<"OclFunCall_22793"<<", "<<cpu_time_ms<<", "<<gpu_time_ms<<", "<<diff_pc<<std::endl; 
    }
    {
        double cpu_time_ms = cpu_time_in_ms(cpu_clock_start_22792, cpu_clock_end_22792);
        double gpu_time_ms = gpu_time_in_ms(event_22792);
        double diff_pc = diff_percent(cpu_time_ms, gpu_time_ms);
        std::cerr<<"OclFunCall_22792"<<", "<<cpu_time_ms<<", "<<gpu_time_ms<<", "<<diff_pc<<std::endl; 
    }
    {
        double cpu_time_ms = cpu_time_in_ms(cpu_clock_start_22789, cpu_clock_end_22789);
        double gpu_time_ms = gpu_time_in_ms(event_22789);
        double diff_pc = diff_percent(cpu_time_ms, gpu_time_ms);
        std::cerr<<"OclFunCall_22789"<<", "<<cpu_time_ms<<", "<<gpu_time_ms<<", "<<diff_pc<<std::endl; 
    }
}
void post_execute(){
    print_clock(); 
}

namespace lift {; 

void execute(float * v_initial_param_22768_11527, float * v_initial_param_22769_11528, float * & v_user_func_22794_11534){
    // Allocate memory for output pointers
    cl::Buffer v_user_func_22788_11529(context, CL_MEM_READ_WRITE, (200704 * sizeof(float)));
    cl::Buffer v_user_func_22793_11533(context, CL_MEM_READ_WRITE, (401408 * sizeof(float)));
    v_user_func_22794_11534 = reinterpret_cast<float *>(malloc((401408 * sizeof(float)))); 
    cl::Buffer v_user_func_22789_11530(context, CL_MEM_READ_WRITE, (295936 * sizeof(float)));
    cl::Buffer v_user_func_22792_11532(context, CL_MEM_READ_WRITE, (524288 * sizeof(float)));
    cl::Buffer v_user_func_22791_11531(context, CL_MEM_READ_WRITE, (1179648 * sizeof(float)));
    ; 
    lift_queue.enqueueWriteBuffer(v_user_func_22788_11529, CL_TRUE, 0, (200704 * sizeof(float)), v_initial_param_22768_11527, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    kernel_22789.setArg(0, v_user_func_22788_11529); 
    kernel_22789.setArg(1, v_user_func_22789_11530); 
    cpu_clock_start_22789 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    cerr << "thread block config(local): cl::NDRange(1,1,1)" << endl << "thread block config(global): cl::NDRange(256,34,34)" << endl;
    lift_queue.enqueueNDRangeKernel(kernel_22789, cl::NullRange, cl::NDRange(256,34,34), cl::NDRange(1,1,1), NULL, (&event_22789)); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    cpu_clock_end_22789 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    ; 
    lift_queue.enqueueWriteBuffer(v_user_func_22791_11531, CL_TRUE, 0, (1179648 * sizeof(float)), v_initial_param_22769_11528, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    kernel_22792.setArg(0, v_user_func_22789_11530); 
    kernel_22792.setArg(1, v_user_func_22791_11531); 
    kernel_22792.setArg(2, v_user_func_22792_11532); 
    cpu_clock_start_22792 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    cerr << "thread block config(local): cl::NDRange(8,3,16)" << endl << "thread block config(global): cl::NDRange(1024,48,16)" << endl;
    lift_queue.enqueueNDRangeKernel(kernel_22792, cl::NullRange, cl::NDRange(1024,48,16), cl::NDRange(8,3,16), NULL, (&event_22792)); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    cpu_clock_end_22792 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    kernel_22793.setArg(0, v_user_func_22792_11532); 
    kernel_22793.setArg(1, v_user_func_22793_11533); 
    cpu_clock_start_22793 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    cerr << "thread block config(local): cl::NDRange(1,1,1)" << endl << "thread block config(global): cl::NDRange(512,24,24)" << endl;
    lift_queue.enqueueNDRangeKernel(kernel_22793, cl::NullRange, cl::NDRange(512,24,24), cl::NDRange(1,1,1), NULL, (&event_22793)); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    cpu_clock_end_22793 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    ; 
    lift_queue.enqueueReadBuffer(v_user_func_22793_11533, CL_TRUE, 0, (401408 * sizeof(float)), v_user_func_22794_11534, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    post_execute(); 
}
}; 