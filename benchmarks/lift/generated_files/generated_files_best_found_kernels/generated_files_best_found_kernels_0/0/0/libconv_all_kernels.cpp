
#include <bits/stdc++.h>

using namespace std;

    

#include <iostream>
    
#include <CL/cl2.hpp>
    
#include <fstream>

std::string readFile(const char *filename){

  std::ifstream in(filename, std::ios::in);

  if (in.fail())
  {
  std::cerr << "Error reading file " << filename << std::endl;
  exit(1); }

  std::string contents;
  in.seekg(0, std::ios::end);
  contents.resize(in.tellg());
  in.seekg(0, std::ios::beg);
  in.read(&contents[0], contents.size());
  in.close();
  return contents;
  }

    

int platformId = 0;
int deviceId = 0;

 std::vector<cl::Platform> allPlatforms;
 cl::Platform platform;
 std::vector<cl::Device> allDevices;
 cl::Device device;
 cl::Context context;
 cl::CommandQueue lift_queue;

 size_t lift_global_0 = 1, lift_global_1 = 1, lift_global_2 =1;

      ; 
std::string kernel_string_22781;
cl::Program::Sources kernel_source_22781;
cl::Program kernel_program_22781;
cl::Kernel kernel_22781;
std::string kernel_string_22784;
cl::Program::Sources kernel_source_22784;
cl::Program kernel_program_22784;
cl::Kernel kernel_22784;
std::string kernel_string_22785;
cl::Program::Sources kernel_source_22785;
cl::Program kernel_program_22785;
cl::Kernel kernel_22785;
std::chrono::milliseconds cpu_clock_start_22785;
std::chrono::milliseconds cpu_clock_end_22785;
cl::Event event_22785;
std::chrono::milliseconds cpu_clock_start_22784;
std::chrono::milliseconds cpu_clock_end_22784;
cl::Event event_22784;
std::chrono::milliseconds cpu_clock_start_22781;
std::chrono::milliseconds cpu_clock_end_22781;
cl::Event event_22781;
; 
; 
; 
; 
; 
; 
; 
; 
; 
void lift_init(const std::string & pwd){
    
	cl::Platform::get(&allPlatforms);
 if (allPlatforms.size() == 0) {
 std::cerr << " No platforms found. Check OpenCL installation!" << std::endl;
 exit(1);
 }

 platform = allPlatforms[platformId];

 platform.getDevices(CL_DEVICE_TYPE_ALL, &allDevices);
 if (allDevices.size() == 0) {
 std::cerr << " No devices found. Check OpenCL installation!" << std::endl;
 exit(1);
 }

 device = allDevices[deviceId];

 std::cerr << "Using platform: " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl;
 std::cerr << "Using device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;

 // create context
 cl::Context tmp_context({ device });
 context = std::move(tmp_context);

 // create queue
 cl::CommandQueue tmp_queue(context, device, CL_QUEUE_PROFILING_ENABLE);
 lift_queue = std::move(tmp_queue);
      ; 
    kernel_string_22781 = readFile((pwd + "/kernel_22781.cl").c_str()); 
    kernel_source_22781 = cl::Program::Sources(1, {kernel_string_22781.c_str(), kernel_string_22781.length()}); 
    kernel_program_22781 = cl::Program(context, kernel_source_22781); 
    if ((kernel_program_22781.build({ device }) != CL_SUCCESS)){
        std::cerr << "kernel build error" << std::endl; 
        char* log; 
        size_t logsize; 
        assert((clGetProgramBuildInfo(kernel_program_22781.get(), device.get(), CL_PROGRAM_BUILD_LOG, 0, NULL, &logsize) == CL_SUCCESS)); 
        log = (char*)malloc(sizeof(char) * logsize); 
        assert((clGetProgramBuildInfo(kernel_program_22781.get(), device.get(), CL_PROGRAM_BUILD_LOG, logsize, log, NULL) == CL_SUCCESS)); 
        std::cerr << log << std::endl; 
        exit(1); 
    }
    kernel_22781 = cl::Kernel(kernel_program_22781, "KERNEL"); 
    kernel_string_22784 = readFile((pwd + "/kernel_22784.cl").c_str()); 
    kernel_source_22784 = cl::Program::Sources(1, {kernel_string_22784.c_str(), kernel_string_22784.length()}); 
    kernel_program_22784 = cl::Program(context, kernel_source_22784); 
    if ((kernel_program_22784.build({ device }) != CL_SUCCESS)){
        std::cerr << "kernel build error" << std::endl; 
        char* log; 
        size_t logsize; 
        assert((clGetProgramBuildInfo(kernel_program_22784.get(), device.get(), CL_PROGRAM_BUILD_LOG, 0, NULL, &logsize) == CL_SUCCESS)); 
        log = (char*)malloc(sizeof(char) * logsize); 
        assert((clGetProgramBuildInfo(kernel_program_22784.get(), device.get(), CL_PROGRAM_BUILD_LOG, logsize, log, NULL) == CL_SUCCESS)); 
        std::cerr << log << std::endl; 
        exit(1); 
    }
    kernel_22784 = cl::Kernel(kernel_program_22784, "KERNEL"); 
    kernel_string_22785 = readFile((pwd + "/kernel_22785.cl").c_str()); 
    kernel_source_22785 = cl::Program::Sources(1, {kernel_string_22785.c_str(), kernel_string_22785.length()}); 
    kernel_program_22785 = cl::Program(context, kernel_source_22785); 
    if ((kernel_program_22785.build({ device }) != CL_SUCCESS)){
        std::cerr << "kernel build error" << std::endl; 
        char* log; 
        size_t logsize; 
        assert((clGetProgramBuildInfo(kernel_program_22785.get(), device.get(), CL_PROGRAM_BUILD_LOG, 0, NULL, &logsize) == CL_SUCCESS)); 
        log = (char*)malloc(sizeof(char) * logsize); 
        assert((clGetProgramBuildInfo(kernel_program_22785.get(), device.get(), CL_PROGRAM_BUILD_LOG, logsize, log, NULL) == CL_SUCCESS)); 
        std::cerr << log << std::endl; 
        exit(1); 
    }
    kernel_22785 = cl::Kernel(kernel_program_22785, "KERNEL"); 
}

double cpu_time_in_ms( std::chrono::milliseconds start, std::chrono::milliseconds finish ){
 return (finish - start).count();
}

double gpu_time_in_ms( cl::Event event ){

  cl_ulong start;
  cl_ulong end;
  cl_int err;

  event.wait();

  err = clGetEventProfilingInfo(event(), CL_PROFILING_COMMAND_START,
                                sizeof(start), &start, NULL);
  assert(err == CL_SUCCESS);

  err = clGetEventProfilingInfo(event(), CL_PROFILING_COMMAND_END,
                                sizeof(end), &end, NULL);
  assert(err == CL_SUCCESS);

  return ((double)(end - start)) * 1.0e-6;
}

double diff_percent(double lhs, double rhs) {
  if(std::min(lhs,rhs)==0)
    return -1;
  else
    return std::abs(lhs-rhs)/std::min(lhs,rhs);
}

          ; 
void print_clock(){
    std::cerr<<"func_name, cpu_time_ms, gpu_time_ms, diff_percentage"<<std::endl; 
    {
        double cpu_time_ms = cpu_time_in_ms(cpu_clock_start_22785, cpu_clock_end_22785);
        double gpu_time_ms = gpu_time_in_ms(event_22785);
        double diff_pc = diff_percent(cpu_time_ms, gpu_time_ms);
        std::cerr<<"OclFunCall_22785"<<", "<<cpu_time_ms<<", "<<gpu_time_ms<<", "<<diff_pc<<std::endl; 
    }
    {
        double cpu_time_ms = cpu_time_in_ms(cpu_clock_start_22784, cpu_clock_end_22784);
        double gpu_time_ms = gpu_time_in_ms(event_22784);
        double diff_pc = diff_percent(cpu_time_ms, gpu_time_ms);
        std::cerr<<"OclFunCall_22784"<<", "<<cpu_time_ms<<", "<<gpu_time_ms<<", "<<diff_pc<<std::endl; 
    }
    {
        double cpu_time_ms = cpu_time_in_ms(cpu_clock_start_22781, cpu_clock_end_22781);
        double gpu_time_ms = gpu_time_in_ms(event_22781);
        double diff_pc = diff_percent(cpu_time_ms, gpu_time_ms);
        std::cerr<<"OclFunCall_22781"<<", "<<cpu_time_ms<<", "<<gpu_time_ms<<", "<<diff_pc<<std::endl; 
    }
}
void post_execute(){
    print_clock(); 
}

namespace lift {; 

void execute(float * v_initial_param_22760_11510, float * v_initial_param_22761_11511, float * & v_user_func_22786_11517){
    // Allocate memory for output pointers
    cl::Buffer v_user_func_22781_11513(context, CL_MEM_READ_WRITE, (166980 * sizeof(float)));
    cl::Buffer v_user_func_22783_11514(context, CL_MEM_READ_WRITE, (1728 * sizeof(float)));
    v_user_func_22786_11517 = reinterpret_cast<float *>(malloc((3211264 * sizeof(float)))); 
    cl::Buffer v_user_func_22784_11515(context, CL_MEM_READ_WRITE, (3502080 * sizeof(float)));
    cl::Buffer v_user_func_22780_11512(context, CL_MEM_READ_WRITE, (150528 * sizeof(float)));
    cl::Buffer v_user_func_22785_11516(context, CL_MEM_READ_WRITE, (3211264 * sizeof(float)));
    ; 
    lift_queue.enqueueWriteBuffer(v_user_func_22780_11512, CL_TRUE, 0, (150528 * sizeof(float)), v_initial_param_22760_11510, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    kernel_22781.setArg(0, v_user_func_22780_11512); 
    kernel_22781.setArg(1, v_user_func_22781_11513); 
    cpu_clock_start_22781 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    cerr << "thread block config(local): cl::NDRange(1,1,1)" << endl << "thread block config(global): cl::NDRange(3,230,242)" << endl;
    lift_queue.enqueueNDRangeKernel(kernel_22781, cl::NullRange, cl::NDRange(3,230,242), cl::NDRange(1,1,1), NULL, (&event_22781)); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    cpu_clock_end_22781 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    ; 
    lift_queue.enqueueWriteBuffer(v_user_func_22783_11514, CL_TRUE, 0, (1728 * sizeof(float)), v_initial_param_22761_11511, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    kernel_22784.setArg(0, v_user_func_22781_11513); 
    kernel_22784.setArg(1, v_user_func_22783_11514); 
    kernel_22784.setArg(2, v_user_func_22784_11515); 
    cpu_clock_start_22784 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    cerr << "thread block config(local): cl::NDRange(1,3,16)" << endl << "thread block config(global): cl::NDRange(16,2565,16)" << endl;
    lift_queue.enqueueNDRangeKernel(kernel_22784, cl::NullRange, cl::NDRange(16,2565,16), cl::NDRange(1,3,16), NULL, (&event_22784)); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    cpu_clock_end_22784 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    kernel_22785.setArg(0, v_user_func_22784_11515); 
    kernel_22785.setArg(1, v_user_func_22785_11516); 
    cpu_clock_start_22785 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    cerr << "thread block config(local): cl::NDRange(1,1,1)" << endl << "thread block config(global): cl::NDRange(64,220,208)" << endl;
    lift_queue.enqueueNDRangeKernel(kernel_22785, cl::NullRange, cl::NDRange(64,220,208), cl::NDRange(1,1,1), NULL, (&event_22785)); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    cpu_clock_end_22785 = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()); 
    ; 
    lift_queue.enqueueReadBuffer(v_user_func_22785_11516, CL_TRUE, 0, (3211264 * sizeof(float)), v_user_func_22786_11517, NULL, NULL); 
    assert(lift_queue.finish() == CL_SUCCESS); 
    ; 
    post_execute(); 
}
}; 