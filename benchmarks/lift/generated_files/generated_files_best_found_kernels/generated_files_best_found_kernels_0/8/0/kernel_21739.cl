// Layer 8, tune point 0
// inputChannels -> 512, inputWidth -> 28, inputHeight -> 28,
// padFuncX -> 1, padFuncY -> 1,
// kernelWidth -> 3, kernelHeight -> 3,
// kernelStrideX -> 1, kernelStrideY -> 1,
// numKernels -> 512
// Vectorization
// "mapVectorize.213." -> 1,
// "mapVectorize.246." -> 1
// Unsliding
// 
// Parallelization and fusion
// "mapTransform.28." -> (10*Code.mapWrg() + Code.parDims(0)()),
// "mapTransform.29." -> (10*Code.mapWrg() + Code.parDims(1)()),
// "mapTransform.30." -> (Code.replaceInnerMapWithOuter()),
// "mapTransform.51." -> (10*Code.mapWrg() + Code.parDims(2)()),
// "mapTransform.52." -> (10*Code.mapLcl() + Code.parDims(2)()),
// "mapTransform.68." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.70." -> (10*Code.mapLcl() + Code.parDims(0)()),
// "mapTransform.109." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.110." -> (10*Code.mapLcl() + Code.parDims(0)())
// padOptRight -> 4, padOptBottom -> 4,
// tileWidth -> 4, tileHeight -> 16, tileDepth -> 32,
// seqWindowsPerThreadX -> 4, seqWindowsPerThreadY -> 1,
// seqKernelsPerThread -> 4,
// kernelGroupSize -> 4,
// windowTileWidth -> 1, windowTileHeight -> 1,
// inputCacheSizeX -> 4, inputCacheSizeY -> 1,
// inputCacheDepth -> 4,
// kernelCacheSize -> 4,
// localSizes(0) -> 8,
// localSizes(1) -> 3,
// localSizes(2) -> 16,
// nWrgs(0) -> 128,
// nWrgs(1) -> 16,
// nWrgs(2) -> 1
// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

float id(float x){
    {
        { return x; }; 
    }
}
float4 id4(float4 x){
    {
        { return x; }; 
    }
}
float add(float x, float y){
    {
        { return x+y; }; 
    }
}
float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
kernel __attribute((reqd_work_group_size(8,3,16)))
void KERNEL(const global float* restrict v__11016, const global float* restrict v__11017, global float* v__11044){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        local float v__11038[6144];
        // Typed Value memory
        float v__10614; 
        float v__10623; 
        float v__10635; 
        // Private Memory
        float v__11020_0;
        float v__11020_1;
        float v__11020_2;
        float v__11020_3;
        float v__11020_4;
        float v__11020_5;
        float v__11020_6;
        float v__11020_7;
        float v__11020_8;
        float v__11020_9;
        float v__11020_10;
        float v__11020_11;
        float v__11020_12;
        float v__11020_13;
        float v__11020_14;
        float v__11020_15;
        float4 v__11023_0;
        float v__11024_0;
        float v__11024_1;
        float v__11024_2;
        float v__11024_3;
        float v__11024_4;
        float v__11024_5;
        float v__11024_6;
        float v__11024_7;
        float v__11024_8;
        float v__11024_9;
        float v__11024_10;
        float v__11024_11;
        float v__11024_12;
        float v__11024_13;
        float v__11024_14;
        float v__11024_15;
        float4 v__11026_0;
        float v__11027_0;
        float v__11027_1;
        float v__11027_2;
        float v__11027_3;
        float v__11027_4;
        float v__11027_5;
        float v__11027_6;
        float v__11027_7;
        float v__11027_8;
        float v__11027_9;
        float v__11027_10;
        float v__11027_11;
        float v__11027_12;
        float v__11027_13;
        float v__11027_14;
        float v__11027_15;
        float v__11028_0;
        float v__11028_1;
        float v__11028_2;
        float v__11028_3;
        float v__11028_4;
        float v__11028_5;
        float v__11028_6;
        float v__11028_7;
        float v__11028_8;
        float v__11028_9;
        float v__11028_10;
        float v__11028_11;
        float v__11028_12;
        float v__11028_13;
        float v__11028_14;
        float v__11028_15;
        float v__11032_0;
        float v__11032_1;
        float v__11032_2;
        float v__11032_3;
        float v__11032_4;
        float v__11032_5;
        float v__11032_6;
        float v__11032_7;
        float v__11032_8;
        float v__11032_9;
        float v__11032_10;
        float v__11032_11;
        float v__11032_12;
        float v__11032_13;
        float v__11032_14;
        float v__11032_15;
        float v__11039_0;
        // iteration count is exactly 1, no loop emitted
        {
            int v_wg_id_10951 = get_group_id(0); 
            // iteration count is exactly 1, no loop emitted
            {
                int v_wg_id_10952 = get_group_id(1); 
                // iteration count is exactly 1, no loop emitted
                {
                    int v_wg_id_10953 = get_group_id(2); 
                    // iteration count is exactly 1, no loop emitted
                    {
                        int v_l_id_10954 = get_local_id(2); 
                        // map_seq
                        // iteration count is exactly 1, no loop emitted
                        {
                            int v_i_10955 = 0; 
                            // iteration count is exactly 1, no loop emitted
                            {
                                int v_l_id_10956 = get_local_id(1); 
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_l_id_10957 = get_local_id(0); 
                                    float v_tmp_11046 = 0.0f; 
                                    v__10614 = v_tmp_11046; 
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11020_0 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11020_1 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11020_2 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11020_3 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11020_4 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11020_5 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11020_6 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11020_7 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11020_8 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11020_9 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11020_10 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11020_11 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11020_12 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11020_13 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11020_14 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11020_15 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // reduce_seq
                                    for (int v_i_10962 = 0; (v_i_10962 < 48); v_i_10962 = (1 + v_i_10962)){
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11023_0 = id4(vload4((v_l_id_10957 + (8 * (v_i_10962 / 3)) + (128 * (v_i_10962 % 3)) + (384 * v_l_id_10956) + (4608 * v_wg_id_10951) + (4608 * v_wg_id_10953)),v__11017 + 0)); 
                                        v__11024_0 = v__11023_0.s0; 
                                        v__11024_1 = v__11023_0.s1; 
                                        v__11024_2 = v__11023_0.s2; 
                                        v__11024_3 = v__11023_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11023_0 = id4(vload4((1152 + v_l_id_10957 + (8 * (v_i_10962 / 3)) + (128 * (v_i_10962 % 3)) + (384 * v_l_id_10956) + (4608 * v_wg_id_10951) + (4608 * v_wg_id_10953)),v__11017 + 0)); 
                                        v__11024_4 = v__11023_0.s0; 
                                        v__11024_5 = v__11023_0.s1; 
                                        v__11024_6 = v__11023_0.s2; 
                                        v__11024_7 = v__11023_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11023_0 = id4(vload4((2304 + v_l_id_10957 + (8 * (v_i_10962 / 3)) + (128 * (v_i_10962 % 3)) + (384 * v_l_id_10956) + (4608 * v_wg_id_10951) + (4608 * v_wg_id_10953)),v__11017 + 0)); 
                                        v__11024_8 = v__11023_0.s0; 
                                        v__11024_9 = v__11023_0.s1; 
                                        v__11024_10 = v__11023_0.s2; 
                                        v__11024_11 = v__11023_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11023_0 = id4(vload4((3456 + v_l_id_10957 + (8 * (v_i_10962 / 3)) + (128 * (v_i_10962 % 3)) + (384 * v_l_id_10956) + (4608 * v_wg_id_10951) + (4608 * v_wg_id_10953)),v__11017 + 0)); 
                                        v__11024_12 = v__11023_0.s0; 
                                        v__11024_13 = v__11023_0.s1; 
                                        v__11024_14 = v__11023_0.s2; 
                                        v__11024_15 = v__11023_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11026_0 = id4(vload4((v_l_id_10957 + (8 * (v_i_10962 / 3)) + (128 * (v_i_10962 % 3)) + (512 * (v_wg_id_10952 % 8)) + (4352 * v_l_id_10954) + (4352 * v_l_id_10956) + (69632 * (v_wg_id_10952 / 8))),v__11016 + 0)); 
                                        v__11027_0 = v__11026_0.s0; 
                                        v__11027_1 = v__11026_0.s1; 
                                        v__11027_2 = v__11026_0.s2; 
                                        v__11027_3 = v__11026_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11026_0 = id4(vload4((128 + v_l_id_10957 + (8 * (v_i_10962 / 3)) + (128 * (v_i_10962 % 3)) + (512 * (v_wg_id_10952 % 8)) + (4352 * v_l_id_10954) + (4352 * v_l_id_10956) + (69632 * (v_wg_id_10952 / 8))),v__11016 + 0)); 
                                        v__11027_4 = v__11026_0.s0; 
                                        v__11027_5 = v__11026_0.s1; 
                                        v__11027_6 = v__11026_0.s2; 
                                        v__11027_7 = v__11026_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11026_0 = id4(vload4((256 + v_l_id_10957 + (8 * (v_i_10962 / 3)) + (128 * (v_i_10962 % 3)) + (512 * (v_wg_id_10952 % 8)) + (4352 * v_l_id_10954) + (4352 * v_l_id_10956) + (69632 * (v_wg_id_10952 / 8))),v__11016 + 0)); 
                                        v__11027_8 = v__11026_0.s0; 
                                        v__11027_9 = v__11026_0.s1; 
                                        v__11027_10 = v__11026_0.s2; 
                                        v__11027_11 = v__11026_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11026_0 = id4(vload4((384 + v_l_id_10957 + (8 * (v_i_10962 / 3)) + (128 * (v_i_10962 % 3)) + (512 * (v_wg_id_10952 % 8)) + (4352 * v_l_id_10954) + (4352 * v_l_id_10956) + (69632 * (v_wg_id_10952 / 8))),v__11016 + 0)); 
                                        v__11027_12 = v__11026_0.s0; 
                                        v__11027_13 = v__11026_0.s1; 
                                        v__11027_14 = v__11026_0.s2; 
                                        v__11027_15 = v__11026_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11048 = 0.0f; 
                                        v__10623 = v_tmp_11048; 
                                        v__11028_0 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_0 = mult(v__11027_0, v__11024_0); 
                                        v__11028_0 = add(v__11028_0, v__11032_0); 
                                        v__11032_0 = mult(v__11027_1, v__11024_1); 
                                        v__11028_0 = add(v__11028_0, v__11032_0); 
                                        v__11032_0 = mult(v__11027_2, v__11024_2); 
                                        v__11028_0 = add(v__11028_0, v__11032_0); 
                                        v__11032_0 = mult(v__11027_3, v__11024_3); 
                                        v__11028_0 = add(v__11028_0, v__11032_0); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11049 = 0.0f; 
                                        v__10623 = v_tmp_11049; 
                                        v__11028_1 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_1 = mult(v__11027_4, v__11024_0); 
                                        v__11028_1 = add(v__11028_1, v__11032_1); 
                                        v__11032_1 = mult(v__11027_5, v__11024_1); 
                                        v__11028_1 = add(v__11028_1, v__11032_1); 
                                        v__11032_1 = mult(v__11027_6, v__11024_2); 
                                        v__11028_1 = add(v__11028_1, v__11032_1); 
                                        v__11032_1 = mult(v__11027_7, v__11024_3); 
                                        v__11028_1 = add(v__11028_1, v__11032_1); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11050 = 0.0f; 
                                        v__10623 = v_tmp_11050; 
                                        v__11028_2 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_2 = mult(v__11027_8, v__11024_0); 
                                        v__11028_2 = add(v__11028_2, v__11032_2); 
                                        v__11032_2 = mult(v__11027_9, v__11024_1); 
                                        v__11028_2 = add(v__11028_2, v__11032_2); 
                                        v__11032_2 = mult(v__11027_10, v__11024_2); 
                                        v__11028_2 = add(v__11028_2, v__11032_2); 
                                        v__11032_2 = mult(v__11027_11, v__11024_3); 
                                        v__11028_2 = add(v__11028_2, v__11032_2); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11051 = 0.0f; 
                                        v__10623 = v_tmp_11051; 
                                        v__11028_3 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_3 = mult(v__11027_12, v__11024_0); 
                                        v__11028_3 = add(v__11028_3, v__11032_3); 
                                        v__11032_3 = mult(v__11027_13, v__11024_1); 
                                        v__11028_3 = add(v__11028_3, v__11032_3); 
                                        v__11032_3 = mult(v__11027_14, v__11024_2); 
                                        v__11028_3 = add(v__11028_3, v__11032_3); 
                                        v__11032_3 = mult(v__11027_15, v__11024_3); 
                                        v__11028_3 = add(v__11028_3, v__11032_3); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11052 = 0.0f; 
                                        v__10623 = v_tmp_11052; 
                                        v__11028_4 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_4 = mult(v__11027_0, v__11024_4); 
                                        v__11028_4 = add(v__11028_4, v__11032_4); 
                                        v__11032_4 = mult(v__11027_1, v__11024_5); 
                                        v__11028_4 = add(v__11028_4, v__11032_4); 
                                        v__11032_4 = mult(v__11027_2, v__11024_6); 
                                        v__11028_4 = add(v__11028_4, v__11032_4); 
                                        v__11032_4 = mult(v__11027_3, v__11024_7); 
                                        v__11028_4 = add(v__11028_4, v__11032_4); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11053 = 0.0f; 
                                        v__10623 = v_tmp_11053; 
                                        v__11028_5 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_5 = mult(v__11027_4, v__11024_4); 
                                        v__11028_5 = add(v__11028_5, v__11032_5); 
                                        v__11032_5 = mult(v__11027_5, v__11024_5); 
                                        v__11028_5 = add(v__11028_5, v__11032_5); 
                                        v__11032_5 = mult(v__11027_6, v__11024_6); 
                                        v__11028_5 = add(v__11028_5, v__11032_5); 
                                        v__11032_5 = mult(v__11027_7, v__11024_7); 
                                        v__11028_5 = add(v__11028_5, v__11032_5); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11054 = 0.0f; 
                                        v__10623 = v_tmp_11054; 
                                        v__11028_6 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_6 = mult(v__11027_8, v__11024_4); 
                                        v__11028_6 = add(v__11028_6, v__11032_6); 
                                        v__11032_6 = mult(v__11027_9, v__11024_5); 
                                        v__11028_6 = add(v__11028_6, v__11032_6); 
                                        v__11032_6 = mult(v__11027_10, v__11024_6); 
                                        v__11028_6 = add(v__11028_6, v__11032_6); 
                                        v__11032_6 = mult(v__11027_11, v__11024_7); 
                                        v__11028_6 = add(v__11028_6, v__11032_6); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11055 = 0.0f; 
                                        v__10623 = v_tmp_11055; 
                                        v__11028_7 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_7 = mult(v__11027_12, v__11024_4); 
                                        v__11028_7 = add(v__11028_7, v__11032_7); 
                                        v__11032_7 = mult(v__11027_13, v__11024_5); 
                                        v__11028_7 = add(v__11028_7, v__11032_7); 
                                        v__11032_7 = mult(v__11027_14, v__11024_6); 
                                        v__11028_7 = add(v__11028_7, v__11032_7); 
                                        v__11032_7 = mult(v__11027_15, v__11024_7); 
                                        v__11028_7 = add(v__11028_7, v__11032_7); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11056 = 0.0f; 
                                        v__10623 = v_tmp_11056; 
                                        v__11028_8 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_8 = mult(v__11027_0, v__11024_8); 
                                        v__11028_8 = add(v__11028_8, v__11032_8); 
                                        v__11032_8 = mult(v__11027_1, v__11024_9); 
                                        v__11028_8 = add(v__11028_8, v__11032_8); 
                                        v__11032_8 = mult(v__11027_2, v__11024_10); 
                                        v__11028_8 = add(v__11028_8, v__11032_8); 
                                        v__11032_8 = mult(v__11027_3, v__11024_11); 
                                        v__11028_8 = add(v__11028_8, v__11032_8); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11057 = 0.0f; 
                                        v__10623 = v_tmp_11057; 
                                        v__11028_9 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_9 = mult(v__11027_4, v__11024_8); 
                                        v__11028_9 = add(v__11028_9, v__11032_9); 
                                        v__11032_9 = mult(v__11027_5, v__11024_9); 
                                        v__11028_9 = add(v__11028_9, v__11032_9); 
                                        v__11032_9 = mult(v__11027_6, v__11024_10); 
                                        v__11028_9 = add(v__11028_9, v__11032_9); 
                                        v__11032_9 = mult(v__11027_7, v__11024_11); 
                                        v__11028_9 = add(v__11028_9, v__11032_9); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11058 = 0.0f; 
                                        v__10623 = v_tmp_11058; 
                                        v__11028_10 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_10 = mult(v__11027_8, v__11024_8); 
                                        v__11028_10 = add(v__11028_10, v__11032_10); 
                                        v__11032_10 = mult(v__11027_9, v__11024_9); 
                                        v__11028_10 = add(v__11028_10, v__11032_10); 
                                        v__11032_10 = mult(v__11027_10, v__11024_10); 
                                        v__11028_10 = add(v__11028_10, v__11032_10); 
                                        v__11032_10 = mult(v__11027_11, v__11024_11); 
                                        v__11028_10 = add(v__11028_10, v__11032_10); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11059 = 0.0f; 
                                        v__10623 = v_tmp_11059; 
                                        v__11028_11 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_11 = mult(v__11027_12, v__11024_8); 
                                        v__11028_11 = add(v__11028_11, v__11032_11); 
                                        v__11032_11 = mult(v__11027_13, v__11024_9); 
                                        v__11028_11 = add(v__11028_11, v__11032_11); 
                                        v__11032_11 = mult(v__11027_14, v__11024_10); 
                                        v__11028_11 = add(v__11028_11, v__11032_11); 
                                        v__11032_11 = mult(v__11027_15, v__11024_11); 
                                        v__11028_11 = add(v__11028_11, v__11032_11); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11060 = 0.0f; 
                                        v__10623 = v_tmp_11060; 
                                        v__11028_12 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_12 = mult(v__11027_0, v__11024_12); 
                                        v__11028_12 = add(v__11028_12, v__11032_12); 
                                        v__11032_12 = mult(v__11027_1, v__11024_13); 
                                        v__11028_12 = add(v__11028_12, v__11032_12); 
                                        v__11032_12 = mult(v__11027_2, v__11024_14); 
                                        v__11028_12 = add(v__11028_12, v__11032_12); 
                                        v__11032_12 = mult(v__11027_3, v__11024_15); 
                                        v__11028_12 = add(v__11028_12, v__11032_12); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11061 = 0.0f; 
                                        v__10623 = v_tmp_11061; 
                                        v__11028_13 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_13 = mult(v__11027_4, v__11024_12); 
                                        v__11028_13 = add(v__11028_13, v__11032_13); 
                                        v__11032_13 = mult(v__11027_5, v__11024_13); 
                                        v__11028_13 = add(v__11028_13, v__11032_13); 
                                        v__11032_13 = mult(v__11027_6, v__11024_14); 
                                        v__11028_13 = add(v__11028_13, v__11032_13); 
                                        v__11032_13 = mult(v__11027_7, v__11024_15); 
                                        v__11028_13 = add(v__11028_13, v__11032_13); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11062 = 0.0f; 
                                        v__10623 = v_tmp_11062; 
                                        v__11028_14 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_14 = mult(v__11027_8, v__11024_12); 
                                        v__11028_14 = add(v__11028_14, v__11032_14); 
                                        v__11032_14 = mult(v__11027_9, v__11024_13); 
                                        v__11028_14 = add(v__11028_14, v__11032_14); 
                                        v__11032_14 = mult(v__11027_10, v__11024_14); 
                                        v__11028_14 = add(v__11028_14, v__11032_14); 
                                        v__11032_14 = mult(v__11027_11, v__11024_15); 
                                        v__11028_14 = add(v__11028_14, v__11032_14); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11063 = 0.0f; 
                                        v__10623 = v_tmp_11063; 
                                        v__11028_15 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11032_15 = mult(v__11027_12, v__11024_12); 
                                        v__11028_15 = add(v__11028_15, v__11032_15); 
                                        v__11032_15 = mult(v__11027_13, v__11024_13); 
                                        v__11028_15 = add(v__11028_15, v__11032_15); 
                                        v__11032_15 = mult(v__11027_14, v__11024_14); 
                                        v__11028_15 = add(v__11028_15, v__11032_15); 
                                        v__11032_15 = mult(v__11027_15, v__11024_15); 
                                        v__11028_15 = add(v__11028_15, v__11032_15); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11020_0 = add(v__11020_0, v__11028_0); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11020_1 = add(v__11020_1, v__11028_1); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11020_2 = add(v__11020_2, v__11028_2); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11020_3 = add(v__11020_3, v__11028_3); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11020_4 = add(v__11020_4, v__11028_4); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11020_5 = add(v__11020_5, v__11028_5); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11020_6 = add(v__11020_6, v__11028_6); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11020_7 = add(v__11020_7, v__11028_7); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11020_8 = add(v__11020_8, v__11028_8); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11020_9 = add(v__11020_9, v__11028_9); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11020_10 = add(v__11020_10, v__11028_10); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11020_11 = add(v__11020_11, v__11028_11); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11020_12 = add(v__11020_12, v__11028_12); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11020_13 = add(v__11020_13, v__11028_13); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11020_14 = add(v__11020_14, v__11028_14); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11020_15 = add(v__11020_15, v__11028_15); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                    // end reduce_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11038[(16 * (v_l_id_10957 + (8 * v_l_id_10956) + (24 * v_l_id_10954)))] = id(v__11020_0); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11038[(1 + (16 * v_l_id_10957) + (128 * v_l_id_10956) + (384 * v_l_id_10954))] = id(v__11020_1); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11038[(2 * (1 + (8 * v_l_id_10957) + (64 * v_l_id_10956) + (192 * v_l_id_10954)))] = id(v__11020_2); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11038[(3 + (16 * v_l_id_10957) + (128 * v_l_id_10956) + (384 * v_l_id_10954))] = id(v__11020_3); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11038[(4 * (1 + (4 * v_l_id_10957) + (32 * v_l_id_10956) + (96 * v_l_id_10954)))] = id(v__11020_4); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11038[(5 + (16 * v_l_id_10957) + (128 * v_l_id_10956) + (384 * v_l_id_10954))] = id(v__11020_5); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11038[(2 * (3 + (8 * v_l_id_10957) + (64 * v_l_id_10956) + (192 * v_l_id_10954)))] = id(v__11020_6); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11038[(7 + (16 * v_l_id_10957) + (128 * v_l_id_10956) + (384 * v_l_id_10954))] = id(v__11020_7); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11038[(8 * (1 + (2 * v_l_id_10957) + (16 * v_l_id_10956) + (48 * v_l_id_10954)))] = id(v__11020_8); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11038[(9 + (16 * v_l_id_10957) + (128 * v_l_id_10956) + (384 * v_l_id_10954))] = id(v__11020_9); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11038[(2 * (5 + (8 * v_l_id_10957) + (64 * v_l_id_10956) + (192 * v_l_id_10954)))] = id(v__11020_10); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11038[(11 + (16 * v_l_id_10957) + (128 * v_l_id_10956) + (384 * v_l_id_10954))] = id(v__11020_11); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11038[(4 * (3 + (4 * v_l_id_10957) + (32 * v_l_id_10956) + (96 * v_l_id_10954)))] = id(v__11020_12); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11038[(13 + (16 * v_l_id_10957) + (128 * v_l_id_10956) + (384 * v_l_id_10954))] = id(v__11020_13); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11038[(2 * (7 + (8 * v_l_id_10957) + (64 * v_l_id_10956) + (192 * v_l_id_10954)))] = id(v__11020_14); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11038[(15 + (16 * v_l_id_10957) + (128 * v_l_id_10956) + (384 * v_l_id_10954))] = id(v__11020_15); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                }
                            }
                            barrier(CLK_LOCAL_MEM_FENCE);
                            for (int v_l_id_11008 = get_local_id(1); (v_l_id_11008 < 4); v_l_id_11008 = (3 + v_l_id_11008)){
                                // map_seq
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_i_11009 = 0; 
                                    for (int v_l_id_11010 = get_local_id(0); (v_l_id_11010 < 4); v_l_id_11010 = (8 + v_l_id_11010)){
                                        float v_tmp_11064 = 0.0f; 
                                        v__10635 = v_tmp_11064; 
                                        // map_seq
                                        // unroll
                                        v__11039_0 = id(v__10635); 
                                        // end unroll
                                        // end map_seq
                                        // reduce_seq
                                        for (int v_i_11012 = 0; (v_i_11012 < 24); v_i_11012 = (1 + v_i_11012)){
                                            // map_seq
                                            // unroll
                                            v__11039_0 = add(v__11039_0, v__11038[(v_l_id_11010 + (4 * v_l_id_11008) + (16 * v_i_11012) + (384 * v_l_id_10954))]); 
                                            // end unroll
                                            // end map_seq
                                        }
                                        // end reduce_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11044[(v_l_id_11008 + (4 * v_wg_id_10951) + (4 * v_wg_id_10953) + (512 * v_l_id_11010) + (2048 * (v_wg_id_10952 % 8)) + (16384 * v_l_id_10954) + (262144 * (v_wg_id_10952 / 8)))] = id(v__11039_0); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                }
                                // end map_seq
                            }
                        }
                        // end map_seq
                    }
                    barrier(CLK_LOCAL_MEM_FENCE);
                }
            }
        }
    }
}