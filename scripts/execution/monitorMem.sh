#!/usr/bin/env bash

############## README ###################
# How to use this script:
# 1. Have another bash script that starts your mem-intensive process detached ("<process> &") and then calls 
#    this script with memory log destination directory path, e.g.:
#    ./runBenchmark.sh:
#       ./a.out &
#       ../../monitorMem.sh ../../memlogs/$layer/$tunepoint
# 2. Make sure your process logs the time with milliseconds before and after the memory-intensive computations take place 
# 3. In the code below, update the name of your executable.
# 4. When computation is done, inspect mem.log: using the timings from your executable, find the memory consumption before 
#    computations started and find the maximum consumption until computations where finished. The difference is your 
#    memory consumption.
# 5. Profit immensely.
#
# Naums

#while ! a=$(ps aux | grep [a].out)
while ! PID=$(pidof a.out)
do
  :
done

echo "" > $1
while pidof a.out>/dev/null
do
  date +"%Y-%m-%d %H:%M:%S,%3N" >> $1
  cat /proc/$PID/status | grep VmSize >> $1
  sleep 0.005
done

echo "Saved memory monitoring log to $1"
