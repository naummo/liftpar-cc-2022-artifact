#!/usr/bin/env bash 

# set -e
# set -o pipefail

# Activate debug
# set -x

trim() {
	echo -n "$(echo -e "${1}" | tr -d '[:space:]')"
}


COMMON_C_FILES_PATH=""
MIN_FREQ=103750000
CWD="$PWD"

# if [ -z "USE_GEMM" ]
# then
# 	TEST_HARNESS_CPP="test_harness.cpp"
# else
# 	TEST_HARNESS_CPP="test_harness_gemm_conv.cpp"
# fi
TEST_HARNESS_CPP="test_harness.cpp"

if test -f "$CWD/last_run_point.txt"; then
	last_run_layer_id=$(gawk 'match($0, /\/\* Layer ID \*\/ ([0-9]+)/, a) {print a[1]}' $CWD/last_run_point.txt)
	last_run_tuning_id=$(gawk 'match($0, /\/\* Tuning point ID \*\/ ([0-9]+)/, a) {print a[1]}' $CWD/last_run_point.txt)
	last_run_batch_id=$(gawk 'match($0, /\/\* Batch ID \*\/ ([0-9]+)/, a) {print a[1]}' $CWD/last_run_point.txt)
	skip_batches=true
	skip_points=true
	echo "last_run_layer_id=$last_run_layer_id last_run_tuning_id=$last_run_tuning_id last_run_batch_id=$last_run_batch_id"
else
	skip_batches=false
	skip_points=false
fi

header_traversed="false"
while IFS=',' read -r NETWORK_NAME NETWORK_LABEL LAYER_MIN LAYER_MAX TUNEPOINT_MIN TUNEPOINT_MAX;
do
	if [ $header_traversed == "false" ]
	then
		header_traversed="true"
		continue
	fi
	NETWORK_NAME=$(trim "$NETWORK_NAME")
	NETWORK_LABEL=$(trim "$NETWORK_LABEL")
	LAYER_MIN=$(trim "$LAYER_MIN")
	LAYER_MAX=$(trim "$LAYER_MAX")
	TUNEPOINT_MIN=$(trim "$TUNEPOINT_MIN")
	TUNEPOINT_MAX=$(trim "$TUNEPOINT_MAX")
	break
done < CONFIG_SUMMARY_FILE

date_start="$(date +'%Y-%m-%d-%H-%M-%S')"

header_traversed="false"
while IFS=',' read -r NETWORK_NAME NETWORK_LABEL LAYER_FIRST LAYER_LAST TUNEPOINT_FIRST TUNEPOINT_LAST TRIALS FREQ C_FILES_PATH;
do
	if [ $header_traversed == "false" ]
	then
		header_traversed="true"
		continue
	fi
	# Trim whitespace
	NETWORK_NAME=$(trim "$NETWORK_NAME")
	NETWORK_LABEL=$(trim "$NETWORK_LABEL")
	LAYER_FIRST=$(trim "$LAYER_FIRST")
	LAYER_LAST=$(trim "$LAYER_LAST")
	TUNEPOINT_FIRST=$(trim "$TUNEPOINT_FIRST")
	TUNEPOINT_LAST=$(trim "$TUNEPOINT_LAST")
	TRIALS=$(trim "$TRIALS")
	FREQ=$(trim "$FREQ")
	# if [ "$COMMON_C_FILES_PATH" == "" ]
	# then
	# 	COMMON_C_FILES_PATH=$(trim "$C_FILES_PATH")
	# fi	
	if [ ! -z "$C_FILES_PATH" ]
	then
		COMMON_C_FILES_PATH=$(trim "$C_FILES_PATH")
	fi
	if [ ! -z "META_RUN" ]
	then
		COMMON_C_FILES_ROOT_PATH=$(echo ${COMMON_C_FILES_PATH} | grep -oP '.*?(?=/)')
		logpath="BOARD_WD/logs/${NETWORK_NAME}/${NETWORK_LABEL}/${COMMON_C_FILES_ROOT_PATH}/logof_layers_${LAYER_MIN}_to_${LAYER_MAX}_kernels_${TUNEPOINT_MIN}_to_${TUNEPOINT_MAX}_board_BOARD_NUMBER_${date_start}.log"
	else
		if [ ! -z $(echo ${COMMON_C_FILES_PATH} | grep -oP '.*?(?=/)') ]
		then
			COMMON_C_FILES_ROOT_PATH=$(echo ${COMMON_C_FILES_PATH} | grep -oP '.*?(?=/)')
		else
			COMMON_C_FILES_ROOT_PATH=${COMMON_C_FILES_PATH}
		fi
		logpath="BOARD_WD/logs/${NETWORK_NAME}/${NETWORK_LABEL}/logof_layers_${LAYER_MIN}_to_${LAYER_MAX}_kernels_${TUNEPOINT_MIN}_to_${TUNEPOINT_MAX}_board_BOARD_NUMBER_${date_start}.log"
	fi

	mkdir -p BOARD_WD/logs/$NETWORK_NAME/$NETWORK_LABEL
	if [ ! -z "META_RUN" ]
	then
		mkdir -p BOARD_WD/logs/$NETWORK_NAME/$NETWORK_LABEL/$COMMON_C_FILES_ROOT_PATH
	fi
	if [ "MONITOR_MEM" = "true" ]
	then
		mkdir -p BOARD_WD/memlogs/$NETWORK_NAME/$NETWORK_LABEL
		if [ ! -z "META_RUN" ]
		then
			mkdir -p BOARD_WD/memlogs/$NETWORK_NAME/$NETWORK_LABEL/$COMMON_C_FILES_ROOT_PATH
		fi
	fi
	if [ "MONITOR_TOP" = "true" ]
	then
		mkdir -p BOARD_WD/toplogs/$NETWORK_NAME/$NETWORK_LABEL
		if [ ! -z "META_RUN" ]
		then
			mkdir -p BOARD_WD/toplogs/$NETWORK_NAME/$NETWORK_LABEL/$COMMON_C_FILES_ROOT_PATH
		fi
	fi
	break
done < CONFIG_FILE

echo "Logging to ${logpath}"
{
batch_id=0
header_traversed="false"
while IFS=',' read -r NETWORK_NAME NETWORK_LABEL LAYER_FIRST LAYER_LAST TUNEPOINT_FIRST TUNEPOINT_LAST TRIALS FREQ C_FILES_PATH;
do
	if [ $header_traversed == "false" ]
	then
		header_traversed="true"
		continue
	fi

	echo "batch_id=$batch_id skip_batches=$skip_batches skip_points=$skip_points"

	if $skip_batches ; then
		if [ $batch_id == $last_run_batch_id ]; then
			skip_batches=false
		else
			batch_id=$(( $batch_id + 1 ))
			continue
		fi
	fi

	# Trim whitespace
	NETWORK_NAME=$(trim "$NETWORK_NAME")
	NETWORK_LABEL=$(trim "$NETWORK_LABEL")
	LAYER_FIRST=$(trim "$LAYER_FIRST")
	LAYER_LAST=$(trim "$LAYER_LAST")
	TUNEPOINT_FIRST=$(trim "$TUNEPOINT_FIRST")
	TUNEPOINT_LAST=$(trim "$TUNEPOINT_LAST")
	TRIALS=$(trim "$TRIALS")
	FREQ=$(trim "$FREQ")
	if [ ! -z "$C_FILES_PATH" ]
	then
		COMMON_C_FILES_PATH=$(trim "$C_FILES_PATH")
	fi
	# Expects COMMON_C_FILES_ROOT_PATH to always be the same
	# if [ ! -z "META_RUN" ]
	# then
	# 	COMMON_C_FILES_ROOT_PATH=$(echo ${COMMON_C_FILES_PATH} | grep -oP '.*?(?=/)')
	# else
	# 	if [ ! -z $(echo ${COMMON_C_FILES_PATH} | grep -oP '.*?(?=/)') ]
	# 	then
	# 		COMMON_C_FILES_ROOT_PATH=$(echo ${COMMON_C_FILES_PATH} | grep -oP '.*?(?=/)')
	# 	else
	# 		COMMON_C_FILES_ROOT_PATH=${COMMON_C_FILES_PATH}
	# 	fi
	# fi

	echo "Running the following config:"
	echo "  network = ${NETWORK_NAME}_${NETWORK_LABEL}"
	echo "  layers = ${LAYER_FIRST}..${LAYER_LAST}"
	echo "  kernels = ${TUNEPOINT_FIRST}..${TUNEPOINT_LAST}"
	echo "  frequency = ${FREQ}"
	echo "  c_files_path = ${COMMON_C_FILES_PATH}"
	echo "  use_old_data_layout = USE_OLD_DATA_LAYOUT"
	echo "  meta_run = META_RUN"

	for ((layer=10#${LAYER_FIRST}; layer <= 10#${LAYER_LAST}; layer++)); do 
		for ((tunepoint=10#${TUNEPOINT_FIRST}; tunepoint <= 10#${TUNEPOINT_LAST}; tunepoint++)); do 
			echo "layer=$layer tunepoint=$tunepoint skip_points=$skip_points"

			if $skip_points ; then
				if [ "$layer" -ge "$last_run_layer_id" ]; then
					if [ "$tunepoint" -gt "$last_run_tuning_id" ]; then
						skip_points=false # Local version of skip_points
					fi
				fi
			fi
			if $skip_points ; then
				continue
			fi

			for ((trial=0; trial < ${TRIALS}; trial++)); do 
				echo "----------------------------------------------------------------"
				echo "Benchmarking layer config $layer with tuning combination $tunepoint: trial $trial..."
				echo

				echo "Setting frequency ${FREQ}..."
				sudo $CWD/set_gpu_freq.sh $FREQ
				echo "Now frequency is"
				$CWD/gpu_freq.sh


				if [ "MONITOR_PERF_AND_COOL_DOWN" = "true" ];
				then
					cd $CWD
					sudo $CWD/monitorPerformance.sh
				fi

				# cd $CWD/$layer/$tunepoint
				cd BOARD_WD/$COMMON_C_FILES_PATH/$layer/$tunepoint
				echo $PWD
				chmod +x ./runOne.sh

				echo "Logging the frequency transition table before running ..."
				echo
				cat /sys/class/misc/mali0/device/devfreq/e82c0000.mali/trans_stat 
				echo

				# rsync -a $CWD/runOne.sh ./
				rsync -a $CWD/${TEST_HARNESS_CPP} ./
				sed -i "s~LAYER_ID~${layer}~g" ./${TEST_HARNESS_CPP}

				PAD_OPT_RIGHT=$(grep ./runOne.sh -e "padOpt" | grep -o "[0-9]*" | head -1)
				PAD_OPT_BOTTOM=$(grep ./runOne.sh -e "padOpt" | grep -o "[0-9]*" | tail -n1)
				DEPTHWISE_TILING_PART_REDUCE_ONLY=$(grep ./runOne.sh -e "depthWiseTilingPartReduceOnly" | grep -o "true\|false")

				if [ -z "$PAD_OPT_RIGHT" ]; then
					PAD_OPT_RIGHT=0
				fi
				if [ -z "$PAD_OPT_BOTTOM" ]; then
					PAD_OPT_BOTTOM=0
				fi
				if [ -z "$DEPTHWISE_TILING_PART_REDUCE_ONLY" ]; then
					DEPTHWISE_TILING_PART_REDUCE_ONLY="false"
				fi

				echo "PAD_OPT_RIGHT = ${PAD_OPT_RIGHT}; PAD_OPT_BOTTOM = ${PAD_OPT_BOTTOM}"
				echo "DEPTHWISE_TILING_PART_REDUCE_ONLY = ${DEPTHWISE_TILING_PART_REDUCE_ONLY}"
				sed -i "s~PAD_OPT_RIGHT~${PAD_OPT_RIGHT}~g" ./${TEST_HARNESS_CPP}
				sed -i "s~PAD_OPT_BOTTOM~${PAD_OPT_BOTTOM}~g" ./${TEST_HARNESS_CPP}
				
				if [ "${DEPTHWISE_TILING_PART_REDUCE_ONLY}" = "true" ] && [ "SKIP_VERIFICATION" != "true" ]; then
					sed -i "s~PARTCONV~1~g" ./${TEST_HARNESS_CPP}
				else
					sed -i "s~PARTCONV~0~g" ./${TEST_HARNESS_CPP}
				fi

				if [ "MONITOR_TOP" = "true" ];
				then
					$CWD/logTop.sh $layer $tunepoint before BOARD_WD/toplogs/${NETWORK_NAME}/${NETWORK_LABEL}/${COMMON_C_FILES_PATH}_toplogof_layers_${LAYER_FIRST}_to_${LAYER_LAST}_kernels_${TUNEPOINT_FIRST}_to_${TUNEPOINT_LAST}_board_BOARD_NUMBER_${date_start}.log
				fi

				if [ "MONITOR_TEMP" = "true" ];
				then
					echo "Temperature before execution:"
					cat /sys/class/thermal/thermal_zone0/temp
				fi
				
				echo "Benchmarking layer config $layer with tuning combination $tunepoint: trial $trial..."
				if [ "MONITOR_HANGING" = "true" ] || [ "MONITOR_MEM" = "true" ] || [ "MONITOR_POWER" = "true" ];
				then
					./runOne.sh &
				else
					./runOne.sh
				fi

				if [ "MONITOR_HANGING" = "true" ];
				then
					sudo $CWD/monitorHanging.sh
				else

					if [ "MONITOR_MEM" = "true" ];
					then
						echo "Starting monitorMem"
						$CWD/monitorMem.sh BOARD_WD/memlogs/${NETWORK_NAME}/${NETWORK_LABEL}/${COMMON_C_FILES_PATH}_memlogof_layer_${layer}_kernel_${tunepoint}_trial_${trial}_board_BOARD_NUMBER.log
					fi

					if [ "MONITOR_POWER" = "true" ];
					then
						powerlogpath="/home/s1569687/powerlogs/${NETWORK_NAME}/${NETWORK_LABEL}/${COMMON_C_FILES_PATH}_powerlogof_layer_${layer}_kernel_${tunepoint}_trial_${trial}_board_BOARD_NUMBER.log"

						ssh s1569687@michel -X "screen -S powerMonitor -X kill > /dev/null"
						ssh s1569687@michel -X "mkdir -p /home/s1569687/powerlogs/${NETWORK_NAME}/${NETWORK_LABEL}"
						# TODO: add the clock module to Arduino and use its time here
						date="$(date +'%Y-%m-%d %H:%M:%S,%3N')"
						ssh s1569687@michel -X "echo \"$date\" > $powerlogpath"

						echo "Starting the power monitor"

						ssh s1569687@michel -X "screen -dmS powerMonitor bash -c '/home/s1569687/arduino-1.8.5/listen_to_arduino.sh >> $powerlogpath'"
						while ! PID=$(pidof a.out)
						do
							:
						done
						echo "Writing the power readings to michel:$powerlogpath"
						# Run while the computational process is live
						while pidof a.out>/dev/null
						do
							sleep 0.005
						done
						ssh s1569687@michel -X "screen -S powerMonitor -X kill"
						echo "Power monitor has been killed"
					fi
				fi

				if [ "MONITOR_TOP" = "true" ];
				then
					$CWD/logTop.sh $layer $tunepoint after BOARD_WD/toplogs/${NETWORK_NAME}/${NETWORK_LABEL}/${COMMON_C_FILES_PATH}_toplogof_layers_${LAYER_FIRST}_to_${LAYER_LAST}_kernels_${TUNEPOINT_FIRST}_to_${TUNEPOINT_LAST}_board_BOARD_NUMBER_${date_start}.log
				fi

				if [ "MONITOR_TEMP" = "true" ];
				then
					echo "Temperature after execution:"
					cat /sys/class/thermal/thermal_zone0/temp
				fi

				echo
				echo "Logging the frequency transition table after running ..."
				echo
				cat /sys/class/misc/mali0/device/devfreq/e82c0000.mali/trans_stat 
				echo
				cd $CWD
				echo "----------------------------------------------------------------"
			done

			echo "(/* Layer ID */ $layer, /* Tuning point ID */ $tunepoint, /* Batch ID */ $batch_id)" > $CWD/last_run_point.txt
		done
	done	

	skip_points=false
	batch_id=$(( $batch_id + 1 ))
done < CONFIG_FILE
} 2>&1 | tee ${logpath}
