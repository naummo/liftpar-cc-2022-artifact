#!/usr/bin/env bash 

. "$ROOTDIR/scripts/execution/cmdargs_config_file.sh"

set -e
set -o pipefail

# Activate debug
# set -x


trim() {
	echo -n "$(echo -e "${1}" | tr -d '[:space:]')"
}

header_traversed="false"
COMMON_C_FILES_PATH=""
COMMON_C_FILES_PATH_INITIALISED="false"
NETWORK_NAME_GLOBAL=""
LAYER_MIN=1000
LAYER_MAX=0
if [ ! -z "$USE_OLD_DATA_LAYOUT" ]
then
	DATA_LAYOUT_POSTFIX="_old_data_layout"
else
	DATA_LAYOUT_POSTFIX="_new_data_layout"
fi

CONFIG_SUMMARY_FILE=$(echo $CONFIG_FILE | sed 's/\.csv/_summary\.csv/')
echo "Config summary file:"
echo "$CONFIG_SUMMARY_FILE"

if [ -z "$SKIP_PREPAD" ]
then
	if [ -z "$USE_GEMM" ]
	then
		USE_GEMM_ARG="--im2col_too"
	fi
	# if [ ! -z "$USE_OLD_DATA_LAYOUT" ]
	# then
	# 	USE_OLD_DATA_LAYOUT_ARG="--outputs_in_old_data_layout"
	# fi
	PARAMETER_CSV_ARG="--parameter_csv_for_padopt $PARAMETER_CSV"

	# python golden_value_generation/2.create_conv_and_transform_to_lift.py $USE_OLD_DATA_LAYOUT_ARG $PARAMETER_CSV_ARG $USE_GEMM_ARG --update_derivatives --binary
	python golden_value_generation/2.create_conv_and_transform_to_lift.py $PARAMETER_CSV_ARG $USE_GEMM_ARG --prepad_inputs --read_format bin --write_format bin
fi

# if [ -z "$USE_GEMM" ]
# then
# 	TEST_HARNESS_CPP="test_harness.cpp"
# else
# 	TEST_HARNESS_CPP="test_harness_gemm_conv.cpp"
# fi
TEST_HARNESS_CPP="test_harness.cpp"

while IFS=',' read -r NETWORK_NAME NETWORK_LABEL LAYER_FIRST LAYER_LAST TUNEPOINT_FIRST TUNEPOINT_LAST TRIALS FREQ C_FILES_PATH;
do
	if [ $header_traversed == "false" ]
	then
		header_traversed="true"
		continue
	fi

	# Trim whitespace
	NETWORK_NAME=$(trim "$NETWORK_NAME")
	NETWORK_LABEL=$(trim "$NETWORK_LABEL")
	LAYER_FIRST=$(trim "$LAYER_FIRST")
	LAYER_LAST=$(trim "$LAYER_LAST")
	TUNEPOINT_FIRST=$(trim "$TUNEPOINT_FIRST")
	TUNEPOINT_LAST=$(trim "$TUNEPOINT_LAST")
	TRIALS=$(trim "$TRIALS")
	FREQ=$(trim "$FREQ")
	if [ $COMMON_C_FILES_PATH_INITIALISED == "false" ]
	then
		COMMON_C_FILES_PATH=$(trim "$C_FILES_PATH")
	else
		if [ ! -z "$C_FILES_PATH" ]
		then
		 	# COMMON_C_FILES_PATH_INITIALISED="false"
			COMMON_C_FILES_PATH=$(trim "$C_FILES_PATH")
		fi
	fi

	if [ "$LAYER_FIRST" -lt "$LAYER_MIN" ]; then
		LAYER_MIN=$LAYER_FIRST
	fi
	if [ "$LAYER_LAST" -gt "$LAYER_MAX" ]; then
		LAYER_MAX=$LAYER_LAST
	fi
	
	if [ ! -z "$META_RUN" ]
	then
		COMMON_C_FILES_ROOT_PATH=$(echo ${COMMON_C_FILES_PATH} | grep -oP '.*?(?=/)')
	else
		COMMON_C_FILES_ROOT_PATH=${COMMON_C_FILES_PATH}
	fi

	echo "Setting up the following config:"
	echo "  network = ${NETWORK_NAME}_${NETWORK_LABEL}"
	echo "  layers = ${LAYER_FIRST}..${LAYER_LAST}"
	echo "  kernels = ${TUNEPOINT_FIRST}..${TUNEPOINT_LAST}"
	echo "  freq = ${FREQ}"
	echo "  root_c_files_path = ${COMMON_C_FILES_ROOT_PATH}"
	echo "  c_files_path = ${COMMON_C_FILES_PATH}"
	if [ ! -z "$USE_OLD_DATA_LAYOUT" ]
	then
		echo "  use_old_data_layout = true"
	else
		echo "  use_old_data_layout = false"
	fi
 
	if [ $COMMON_C_FILES_PATH_INITIALISED == "false" ]
	then
		NETWORK_NAME_GLOBAL=${NETWORK_NAME}
		cp $ROOTDIR/scripts/execution/runAll.sh ./generated_files/${COMMON_C_FILES_ROOT_PATH}/
		# cp ./runOne.sh ./generated_files/${COMMON_C_FILES_ROOT_PATH}/
		cp $ROOTDIR/scripts/execution/gpu_freq.sh ./generated_files/${COMMON_C_FILES_ROOT_PATH}/
		cp $ROOTDIR/scripts/execution/logTop.sh ./generated_files/${COMMON_C_FILES_ROOT_PATH}/
		cp $ROOTDIR/scripts/execution/set_gpu_freq.sh ./generated_files/${COMMON_C_FILES_ROOT_PATH}/
		cp $ROOTDIR/scripts/execution/monitorMem.sh ./generated_files/${COMMON_C_FILES_ROOT_PATH}/
		cp $ROOTDIR/scripts/execution/monitorHanging.sh ./generated_files/${COMMON_C_FILES_ROOT_PATH}/
		cp $ROOTDIR/scripts/execution/monitorPerformance.sh ./generated_files/${COMMON_C_FILES_ROOT_PATH}/
		cp $ROOTDIR/scripts/execution/${TEST_HARNESS_CPP} ./generated_files/${COMMON_C_FILES_ROOT_PATH}/
		cp -r $ROOTDIR/scripts/execution/sample_kernel ./generated_files/${COMMON_C_FILES_ROOT_PATH}/
		mkdir -p ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runConfigs
		cp $CONFIG_FILE ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runConfigs/
		cp $CONFIG_SUMMARY_FILE ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runConfigs/		


		sed -i "s~NETWORK_NAME~${NETWORK_NAME}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/${TEST_HARNESS_CPP}
		if [ -z "$SKIP_VERIFICATION" ]
		then
			sed -i "s~SKIP_VERIFICATION_VAL~false~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/${TEST_HARNESS_CPP}
		else 
			sed -i "s~SKIP_VERIFICATION_VAL~true~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/${TEST_HARNESS_CPP}
		fi
		if [ ! -z "$USE_OLD_DATA_LAYOUT" ]
		then
			sed -i "s~USE_OLD_DATA_LAYOUT_VAL~1~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/${TEST_HARNESS_CPP}
		else
			sed -i "s~USE_OLD_DATA_LAYOUT_VAL~0~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/${TEST_HARNESS_CPP}
		fi
		if [ -z "$USE_GEMM" ]
		then
			sed -i "s~IM2COL_VAL~false~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/${TEST_HARNESS_CPP}
		else
			sed -i "s~IM2COL_VAL~true~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/${TEST_HARNESS_CPP}
		fi
		sed -i "s~CONFIG_FILE~${CONFIG_FILE}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runAll.sh
		sed -i "s~CONFIG_SUMMARY_FILE~${CONFIG_SUMMARY_FILE}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runAll.sh
		sed -i "s~BOARD_NUMBER~${BOARD_NUMBER}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runAll.sh
		sed -i "s~MONITOR_TOP~${MONITOR_TOP}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runAll.sh
		sed -i "s~MONITOR_HANGING~${MONITOR_HANGING}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runAll.sh
		sed -i "s~MONITOR_MEM~${MONITOR_MEM}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runAll.sh
		sed -i "s~MONITOR_POWER~${MONITOR_POWER}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runAll.sh
		sed -i "s~MONITOR_TEMP~${MONITOR_TEMP}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runAll.sh
		sed -i "s~BOARD_WD~${BOARD_WD}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runAll.sh
		sed -i "s~BOARD_WD~${BOARD_WD}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/${TEST_HARNESS_CPP}
		sed -i "s~BOARD_WD~${BOARD_WD}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/monitorPerformance.sh
		sed -i "s~BOARD_WD~${BOARD_WD}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/sample_kernel/0/2645/sample_host.cpp
		sed -i "s~MONITOR_PERF_AND_COOL_DOWN~${MONITOR_PERF_AND_COOL_DOWN}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runAll.sh
		sed -i "s~META_RUN~${META_RUN}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runAll.sh
		sed -i "s~USE_OLD_DATA_LAYOUT~${USE_OLD_DATA_LAYOUT}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runAll.sh
		# sed -i "s~USE_GEMM~${USE_GEMM}~g" ./generated_files/${COMMON_C_FILES_ROOT_PATH}/runAll.sh

		if [ -z "$BULK_UPLOAD" ]
		then
			echo "Transfering auxiliary files to the board separately..."
			rsync -e "ssh -p ${BOARD_PORT}" -avz -f"- */" -f"+ *" ./generated_files/${COMMON_C_FILES_PATH}/ ${BOARD_IP}:$BOARD_WD/${COMMON_C_FILES_PATH}/
		fi
		if [ ! -z "$SKIP_UPLOAD" ]
		then
			break
		fi
	fi

	if [ -z "$BULK_UPLOAD" ]
	then

		for ((layer=10#${LAYER_FIRST}; layer <= 10#${LAYER_LAST}; layer++)); do 
			# ln -s /home/shunya/naums/hipeac2020/generated_files_mapglb_off4_26.07.2020_20.42.56/0/test_data.txt generated_files_mapglb_off4_100_26.07.2020_22.46.08/0/data.txt
			# rsync -av ./golden_values/${NETWORK_NAME}/$layer/data.txt ./generated_files/${COMMON_C_FILES_ROOT_PATH}/$layer/
			# rsync -av ./layer_config_gen/${NETWORK_NAME}/$layer/layerconfig.cpp ./generated_files/${COMMON_C_FILES_ROOT_PATH}/$layer/

				# echo "Transferring layer configs and golden data of layer $layer..."
				# rsync -e "ssh -p ${BOARD_PORT}" -a --relative ./generated_files/${COMMON_C_FILES_PATH}/$layer/layerconfig.cpp ${BOARD_IP}:$BOARD_WD/
				# rsync -e "ssh -p ${BOARD_PORT}" -az --info=progress2 --relative ./generated_files/${COMMON_C_FILES_PATH}/$layer/data.txt ${BOARD_IP}:$BOARD_WD/

				echo "Transferring kernels for each point of layer $layer separately..."
				cd generated_files
				for ((tunepoint=10#${TUNEPOINT_FIRST}; tunepoint <= 10#${TUNEPOINT_LAST}; tunepoint++)); do 
					# cp ./data.txt ./generated_c_files/$layer/
					# rm ./generated_c_files/$layer/data.txt
					rsync -e "ssh -p ${BOARD_PORT}" -az --relative ./${COMMON_C_FILES_PATH}/$layer/$tunepoint ${BOARD_IP}:$BOARD_WD/
				done
				cd ..
		done
	fi

	if [ $COMMON_C_FILES_PATH_INITIALISED == "false" ]
	then
		COMMON_C_FILES_PATH_INITIALISED="true"
	fi
	
	if [ ! -z "$BULK_UPLOAD" ]
	then
		echo "Transferring generated files to the board in bulk..."
		cd generated_files
		rsync -e "ssh -p ${BOARD_PORT}" -az --relative --info=progress2 ./${COMMON_C_FILES_PATH}/ ${BOARD_IP}:$BOARD_WD/
		cd ..
	fi
done < $CONFIG_FILE

# echo "Transferring run configs..."
# if [ -z "$META_RUN" ]
# then
# 	rsync -e "ssh -p ${BOARD_PORT}" -av --rsync-path="mkdir -p $BOARD_WD/${COMMON_C_FILES_PATH} && rsync" ./generated_files/${COMMON_C_FILES_PATH}/runConfigs ${BOARD_IP}:$BOARD_WD/${COMMON_C_FILES_PATH}/
# else
# 	rsync -e "ssh -p ${BOARD_PORT}" -av --rsync-path="mkdir -p $BOARD_WD/${COMMON_C_FILES_META_PATH} && rsync" ./generated_files/${COMMON_C_FILES_PATH}/runConfigs ${BOARD_IP}:$BOARD_WD/${COMMON_C_FILES_META_PATH}/
# fi

echo "Transferring scripts, runConfigs and sample_kernel..."
cd ./generated_files/${COMMON_C_FILES_ROOT_PATH}/
rsync -e "ssh -p ${BOARD_PORT}" -az --info=progress2 *.sh ${BOARD_IP}:$BOARD_WD/${COMMON_C_FILES_ROOT_PATH}/
rsync -e "ssh -p ${BOARD_PORT}" -az --info=progress2 *.cpp ${BOARD_IP}:$BOARD_WD/${COMMON_C_FILES_ROOT_PATH}/
rsync -e "ssh -p ${BOARD_PORT}" -az --info=progress2 runConfigs ${BOARD_IP}:$BOARD_WD/${COMMON_C_FILES_ROOT_PATH}/
rsync -e "ssh -p ${BOARD_PORT}" -az --info=progress2 sample_kernel ${BOARD_IP}:$BOARD_WD/${COMMON_C_FILES_ROOT_PATH}/

cd ../../

if [ -z "$SKIP_UPLOAD" ]
then
	echo "Transfering sample input data to the board..."
	rsync -e "ssh -p ${BOARD_PORT}" -avz --info=progress2 --exclude '*.txt' --exclude '*.npy' $DATADIR/randomeX ${BOARD_IP}:$BOARD_WD/

	echo "Transfering sample weights to the board..."
	rsync -e "ssh -p ${BOARD_PORT}" -avz --info=progress2 --exclude '*.txt' --exclude '*.npy' $DATADIR/randomeW ${BOARD_IP}:$BOARD_WD/

	echo "Transferring layer configs and golden data..."
	for ((layer=10#${LAYER_MIN}; layer <= 10#${LAYER_MAX}; layer++)); do 
		if [ "$layer" == '6' ] || [ "$layer" == '9' ] || [ "$layer" == '11' ] || [ "$layer" == '12' ]; then
			continue
		fi
		ssh ${BOARD_IP} -p ${BOARD_PORT} "mkdir -p ${BOARD_WD}/layer_configs/${NETWORK_NAME_GLOBAL}/$layer"
		ssh ${BOARD_IP} -p ${BOARD_PORT} "mkdir -p ${BOARD_WD}/golden_data/${NETWORK_NAME_GLOBAL}${DATA_LAYOUT_POSTFIX}/$layer"
		rsync -e "ssh -p ${BOARD_PORT}" -a $ROOTDIR/layer_configs/${NETWORK_NAME_GLOBAL}/$layer/layerconfig.cpp ${BOARD_IP}:$BOARD_WD/layer_configs/${NETWORK_NAME_GLOBAL}/$layer/
		rsync -e "ssh -p ${BOARD_PORT}" -az --info=progress2 $ROOTDIR/golden_values/${NETWORK_NAME_GLOBAL}${DATA_LAYOUT_POSTFIX}/$layer/data.* ${BOARD_IP}:$BOARD_WD/golden_data/${NETWORK_NAME_GLOBAL}${DATA_LAYOUT_POSTFIX}/$layer/
	done
fi