#!/bin/bash
: "${ROOTDIR:?Please set artifact env-vars: $ source environment.env}"

pushd $ROOTDIR/tools > /dev/null
wget https://cmake.org/files/v3.9/cmake-3.9.5-Linux-x86_64.tar.gz
tar -xvf cmake-3.9.5-Linux-x86_64.tar.gz
rm cmake-3.9.5-Linux-x86_64.tar.gz
popd > /dev/null
