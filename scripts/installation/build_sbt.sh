#!/bin/bash
: "${ROOTDIR:?Please set artifact env-vars: $ source environment.env}"

pushd $ROOTDIR/tools > /dev/null
wget https://downloads.typesafe.com/sbt/0.13.15/sbt-0.13.15.zip
#tar -xvf sbt-0.13.15.tgz
unzip sbt-0.13.15.zip
rm sbt-0.13.15.zip
popd > /dev/null
