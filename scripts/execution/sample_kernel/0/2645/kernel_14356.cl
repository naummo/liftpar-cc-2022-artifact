float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__3900, global float* v__3901){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        // iteration count is exactly 1, no loop emitted
        {
            int v_gl_id_3897 = get_global_id(2); 
            // iteration count is exactly 1, no loop emitted
            {
                int v_gl_id_3898 = get_global_id(1); 
                // iteration count is exactly 1, no loop emitted
                {
                    int v_gl_id_3899 = get_global_id(0); 
                    v__3901[(v_gl_id_3899 + (224 * v_gl_id_3898) + (3211264 * v_gl_id_3897))] = id((((v_gl_id_3899 < 0) || (v_gl_id_3899 >= 230)) ? 0 : ((((v_gl_id_3898 % 224) < 0) || ((v_gl_id_3898 % 224) >= 230)) ? 0 : v__3900[(v_gl_id_3899 + (230 * (v_gl_id_3898 % 224)) + (52900 * (v_gl_id_3898 / 224)) + (3385600 * v_gl_id_3897))]))); 
                }
            }
        }
    }
}