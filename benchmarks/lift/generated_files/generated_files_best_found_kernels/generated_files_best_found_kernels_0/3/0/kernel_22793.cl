// Layer 3, tune point 0
// inputChannels -> 128, inputWidth -> 112, inputHeight -> 112,
// padFuncX -> 1, padFuncY -> 1,
// kernelWidth -> 3, kernelHeight -> 3,
// kernelStrideX -> 1, kernelStrideY -> 1,
// numKernels -> 128
// Vectorization
// "mapVectorize.213." -> 1,
// "mapVectorize.246." -> 1
// Unsliding
// 
// Parallelization and fusion
// "mapTransform.28." -> (10*Code.mapWrg() + Code.parDims(0)()),
// "mapTransform.29." -> (10*Code.mapWrg() + Code.parDims(1)()),
// "mapTransform.30." -> (Code.replaceInnerMapWithOuter()),
// "mapTransform.51." -> (10*Code.mapWrg() + Code.parDims(2)()),
// "mapTransform.52." -> (10*Code.mapLcl() + Code.parDims(2)()),
// "mapTransform.68." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.70." -> (10*Code.mapLcl() + Code.parDims(0)()),
// "mapTransform.109." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.110." -> (10*Code.mapLcl() + Code.parDims(0)())
// padOptRight -> 4, padOptBottom -> 16,
// tileWidth -> 4, tileHeight -> 16, tileDepth -> 32,
// seqWindowsPerThreadX -> 4, seqWindowsPerThreadY -> 1,
// seqKernelsPerThread -> 4,
// kernelGroupSize -> 4,
// windowTileWidth -> 1, windowTileHeight -> 1,
// inputCacheSizeX -> 4, inputCacheSizeY -> 1,
// inputCacheDepth -> 4,
// kernelCacheSize -> 4,
// localSizes(0) -> 8,
// localSizes(1) -> 3,
// localSizes(2) -> 16,
// nWrgs(0) -> 32,
// nWrgs(1) -> 232,
// nWrgs(2) -> 1
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__11088, global float* v__11089){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        for (int v_gl_id_11085 = get_global_id(2); (v_gl_id_11085 < 112); v_gl_id_11085 = (96 + v_gl_id_11085)){
            for (int v_gl_id_11086 = get_global_id(1); (v_gl_id_11086 < 112); v_gl_id_11086 = (108 + v_gl_id_11086)){
                // iteration count is exactly 1, no loop emitted
                {
                    int v_gl_id_11087 = get_global_id(0); 
                    v__11089[(v_gl_id_11087 + (128 * v_gl_id_11086) + (14336 * v_gl_id_11085))] = id((((v_gl_id_11086 < 0) || (v_gl_id_11086 >= 116)) ? 0 : (((v_gl_id_11085 < 0) || (v_gl_id_11085 >= 128)) ? 0 : v__11088[(v_gl_id_11087 + (128 * v_gl_id_11086) + (14848 * v_gl_id_11085))]))); 
                }
            }
        }
    }
}