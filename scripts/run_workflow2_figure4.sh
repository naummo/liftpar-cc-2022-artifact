#!/bin/bash
set -e
set -x

if [ $# -eq 1 ]
then 
	EXPERIMENT_LABEL=$1

	python $ROOTDIR/scripts/log_processing/logsToCsv.py \
		--netname vgg \
		--netlabel $EXPERIMENT_LABEL \
		--n_threads 1 \
		--logdir $ROOTDIR/benchmarks/lift/board_logs

	python $ROOTDIR/scripts/log_processing/timeCsvCollator.py \
		--netname vgg \
		--netlabel $EXPERIMENT_LABEL \
		--logdir $ROOTDIR/benchmarks/lift/board_logs \
		--timingsdir $ROOTDIR/benchmarks/lift/parsed_timings

	python $ROOTDIR/scripts/log_processing/aggregatorOfTimingsAndParameters.py \
		--netname vgg \
		--netlabel $EXPERIMENT_LABEL \
		--parameter_csv $ROOTDIR/benchmarks/lift/parsed_parameters/$EXPERIMENT_LABEL/ \
		--timings_csv $ROOTDIR/benchmarks/lift/parsed_timings/vgg_${EXPERIMENT_LABEL}_timings.csv \
		--aggregated_out_csv $ROOTDIR/benchmarks/lift/parsed_timings/vgg_${EXPERIMENT_LABEL}_params_and_results.csv
	
	python $ROOTDIR/scripts/log_processing/plot_figure4.py \
		--results_and_params_csv $ROOTDIR/benchmarks/lift/parsed_timings/vgg_${EXPERIMENT_LABEL}_params_and_results_medians.csv \
		--comparison_timings_csv $ROOTDIR/benchmarks/arm_compute_tvm/vgg_armcompute_tvm_timings.csv \
		--comparison_memory_csv $ROOTDIR/benchmarks/arm_compute_tvm/vgg_armcompute_tvm_memory.csv \
		--plot_path $ROOTDIR/plots/workflow2_figure4.pdf

else
	echo "Wrong arguments supplied"
		echo "Usage: scripts/run_workflow2_figure4.sh [experiment_label]"
		echo ""
		echo "What is the experiment label?"
	exit -1
fi
