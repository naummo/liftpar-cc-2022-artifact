// Layer 8, tune point 0
// inputChannels -> 512, inputWidth -> 28, inputHeight -> 28,
// padFuncX -> 1, padFuncY -> 1,
// kernelWidth -> 3, kernelHeight -> 3,
// kernelStrideX -> 1, kernelStrideY -> 1,
// numKernels -> 512
// Vectorization
// "mapVectorize.213." -> 1,
// "mapVectorize.246." -> 1
// Unsliding
// 
// Parallelization and fusion
// "mapTransform.28." -> (10*Code.mapWrg() + Code.parDims(0)()),
// "mapTransform.29." -> (10*Code.mapWrg() + Code.parDims(1)()),
// "mapTransform.30." -> (Code.replaceInnerMapWithOuter()),
// "mapTransform.51." -> (10*Code.mapWrg() + Code.parDims(2)()),
// "mapTransform.52." -> (10*Code.mapLcl() + Code.parDims(2)()),
// "mapTransform.68." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.70." -> (10*Code.mapLcl() + Code.parDims(0)()),
// "mapTransform.109." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.110." -> (10*Code.mapLcl() + Code.parDims(0)())
// padOptRight -> 4, padOptBottom -> 4,
// tileWidth -> 4, tileHeight -> 16, tileDepth -> 32,
// seqWindowsPerThreadX -> 4, seqWindowsPerThreadY -> 1,
// seqKernelsPerThread -> 4,
// kernelGroupSize -> 4,
// windowTileWidth -> 1, windowTileHeight -> 1,
// inputCacheSizeX -> 4, inputCacheSizeY -> 1,
// inputCacheDepth -> 4,
// kernelCacheSize -> 4,
// localSizes(0) -> 8,
// localSizes(1) -> 3,
// localSizes(2) -> 16,
// nWrgs(0) -> 128,
// nWrgs(1) -> 16,
// nWrgs(2) -> 1
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__11528, global float* v__11529){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        // iteration count is exactly 1, no loop emitted
        {
            int v_gl_id_11525 = get_global_id(2); 
            // iteration count is exactly 1, no loop emitted
            {
                int v_gl_id_11526 = get_global_id(1); 
                // iteration count is exactly 1, no loop emitted
                {
                    int v_gl_id_11527 = get_global_id(0); 
                    v__11529[(v_gl_id_11527 + (512 * v_gl_id_11526) + (17408 * v_gl_id_11525))] = id(((((-1 + v_gl_id_11526) < 0) || ((-1 + v_gl_id_11526) >= 28)) ? 0 : ((((-1 + v_gl_id_11525) < 0) || ((-1 + v_gl_id_11525) >= 28)) ? 0 : v__11528[(-14848 + v_gl_id_11527 + (512 * v_gl_id_11526) + (14336 * v_gl_id_11525))]))); 
                }
            }
        }
    }
}