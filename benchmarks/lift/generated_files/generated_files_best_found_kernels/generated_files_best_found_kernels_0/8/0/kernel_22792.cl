// Layer 8, tune point 0  (current best at 58.14 ms)
// inputChannels -> 512, inputWidth -> 28, inputHeight -> 28,
// padFuncX -> 1, padFuncY -> 1,
// kernelWidth -> 3, kernelHeight -> 3,
// kernelStrideX -> 1, kernelStrideY -> 1,
// numKernels -> 512
// Vectorization
// "mapVectorize.213." -> 1,
// "mapVectorize.246." -> 1
// Unsliding
// 
// Parallelization and fusion
// "mapTransform.28." -> (10*Code.mapWrg() + Code.parDims(0)()),
// "mapTransform.29." -> (10*Code.mapWrg() + Code.parDims(1)()),
// "mapTransform.30." -> (Code.replaceInnerMapWithOuter()),
// "mapTransform.51." -> (10*Code.mapWrg() + Code.parDims(2)()),
// "mapTransform.52." -> (10*Code.mapLcl() + Code.parDims(2)()),
// "mapTransform.68." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.70." -> (10*Code.mapLcl() + Code.parDims(0)()),
// "mapTransform.109." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.110." -> (10*Code.mapLcl() + Code.parDims(0)())
// padOptRight -> 4, padOptBottom -> 4,
// tileWidth -> 4, tileHeight -> 16, tileDepth -> 32,
// seqWindowsPerThreadX -> 4, seqWindowsPerThreadY -> 1,
// seqKernelsPerThread -> 4,
// kernelGroupSize -> 4,
// windowTileWidth -> 1, windowTileHeight -> 1,
// inputCacheSizeX -> 4, inputCacheSizeY -> 1,
// inputCacheDepth -> 4,
// kernelCacheSize -> 4,
// localSizes(0) -> 8,
// localSizes(1) -> 3,
// localSizes(2) -> 16,
// nWrgs(0) -> 128,
// nWrgs(1) -> 16,
// nWrgs(2) -> 1
// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

float id(float x){
    {
        { return x; }; 
    }
}
float4 id4(float4 x){
    {
        { return x; }; 
    }
}
float add(float x, float y){
    {
        { return x+y; }; 
    }
}
float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
kernel __attribute((reqd_work_group_size(8,3,16)))
void KERNEL(const global float* restrict v__11468, const global float* restrict v__11469, global float* v__11496){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        local float v__11490[6144];
        // Typed Value memory
        float v__10614; 
        float v__10623; 
        float v__10635; 
        // Private Memory
        float v__11472_0;
        float v__11472_1;
        float v__11472_2;
        float v__11472_3;
        float v__11472_4;
        float v__11472_5;
        float v__11472_6;
        float v__11472_7;
        float v__11472_8;
        float v__11472_9;
        float v__11472_10;
        float v__11472_11;
        float v__11472_12;
        float v__11472_13;
        float v__11472_14;
        float v__11472_15;
        float4 v__11475_0;
        float v__11476_0;
        float v__11476_1;
        float v__11476_2;
        float v__11476_3;
        float v__11476_4;
        float v__11476_5;
        float v__11476_6;
        float v__11476_7;
        float v__11476_8;
        float v__11476_9;
        float v__11476_10;
        float v__11476_11;
        float v__11476_12;
        float v__11476_13;
        float v__11476_14;
        float v__11476_15;
        float4 v__11478_0;
        float v__11479_0;
        float v__11479_1;
        float v__11479_2;
        float v__11479_3;
        float v__11479_4;
        float v__11479_5;
        float v__11479_6;
        float v__11479_7;
        float v__11479_8;
        float v__11479_9;
        float v__11479_10;
        float v__11479_11;
        float v__11479_12;
        float v__11479_13;
        float v__11479_14;
        float v__11479_15;
        float v__11480_0;
        float v__11480_1;
        float v__11480_2;
        float v__11480_3;
        float v__11480_4;
        float v__11480_5;
        float v__11480_6;
        float v__11480_7;
        float v__11480_8;
        float v__11480_9;
        float v__11480_10;
        float v__11480_11;
        float v__11480_12;
        float v__11480_13;
        float v__11480_14;
        float v__11480_15;
        float v__11484_0;
        float v__11484_1;
        float v__11484_2;
        float v__11484_3;
        float v__11484_4;
        float v__11484_5;
        float v__11484_6;
        float v__11484_7;
        float v__11484_8;
        float v__11484_9;
        float v__11484_10;
        float v__11484_11;
        float v__11484_12;
        float v__11484_13;
        float v__11484_14;
        float v__11484_15;
        float v__11491_0;
        // iteration count is exactly 1, no loop emitted
        {
            int v_wg_id_11403 = get_group_id(0); 
            // iteration count is exactly 1, no loop emitted
            {
                int v_wg_id_11404 = get_group_id(1); 
                // iteration count is exactly 1, no loop emitted
                {
                    int v_wg_id_11405 = get_group_id(2); 
                    // iteration count is exactly 1, no loop emitted
                    {
                        int v_l_id_11406 = get_local_id(2); 
                        // map_seq
                        // iteration count is exactly 1, no loop emitted
                        {
                            int v_i_11407 = 0; 
                            // iteration count is exactly 1, no loop emitted
                            {
                                int v_l_id_11408 = get_local_id(1); 
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_l_id_11409 = get_local_id(0); 
                                    float v_tmp_11498 = 0.0f; 
                                    v__10614 = v_tmp_11498; 
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11472_0 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11472_1 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11472_2 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11472_3 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11472_4 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11472_5 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11472_6 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11472_7 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11472_8 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11472_9 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11472_10 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11472_11 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11472_12 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11472_13 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11472_14 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11472_15 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // reduce_seq
                                    for (int v_i_11414 = 0; (v_i_11414 < 48); v_i_11414 = (1 + v_i_11414)){
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11475_0 = id4(vload4((v_l_id_11409 + (8 * (v_i_11414 / 3)) + (128 * (v_i_11414 % 3)) + (384 * v_l_id_11408) + (4608 * v_wg_id_11403) + (4608 * v_wg_id_11405)),v__11469 + 0)); 
                                        v__11476_0 = v__11475_0.s0; 
                                        v__11476_1 = v__11475_0.s1; 
                                        v__11476_2 = v__11475_0.s2; 
                                        v__11476_3 = v__11475_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11475_0 = id4(vload4((1152 + v_l_id_11409 + (8 * (v_i_11414 / 3)) + (128 * (v_i_11414 % 3)) + (384 * v_l_id_11408) + (4608 * v_wg_id_11403) + (4608 * v_wg_id_11405)),v__11469 + 0)); 
                                        v__11476_4 = v__11475_0.s0; 
                                        v__11476_5 = v__11475_0.s1; 
                                        v__11476_6 = v__11475_0.s2; 
                                        v__11476_7 = v__11475_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11475_0 = id4(vload4((2304 + v_l_id_11409 + (8 * (v_i_11414 / 3)) + (128 * (v_i_11414 % 3)) + (384 * v_l_id_11408) + (4608 * v_wg_id_11403) + (4608 * v_wg_id_11405)),v__11469 + 0)); 
                                        v__11476_8 = v__11475_0.s0; 
                                        v__11476_9 = v__11475_0.s1; 
                                        v__11476_10 = v__11475_0.s2; 
                                        v__11476_11 = v__11475_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11475_0 = id4(vload4((3456 + v_l_id_11409 + (8 * (v_i_11414 / 3)) + (128 * (v_i_11414 % 3)) + (384 * v_l_id_11408) + (4608 * v_wg_id_11403) + (4608 * v_wg_id_11405)),v__11469 + 0)); 
                                        v__11476_12 = v__11475_0.s0; 
                                        v__11476_13 = v__11475_0.s1; 
                                        v__11476_14 = v__11475_0.s2; 
                                        v__11476_15 = v__11475_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11478_0 = id4(vload4((v_l_id_11409 + (8 * (v_i_11414 / 3)) + (128 * (v_i_11414 % 3)) + (512 * (v_wg_id_11404 % 8)) + (4352 * v_l_id_11406) + (4352 * v_l_id_11408) + (69632 * (v_wg_id_11404 / 8))),v__11468 + 0)); 
                                        v__11479_0 = v__11478_0.s0; 
                                        v__11479_1 = v__11478_0.s1; 
                                        v__11479_2 = v__11478_0.s2; 
                                        v__11479_3 = v__11478_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11478_0 = id4(vload4((128 + v_l_id_11409 + (8 * (v_i_11414 / 3)) + (128 * (v_i_11414 % 3)) + (512 * (v_wg_id_11404 % 8)) + (4352 * v_l_id_11406) + (4352 * v_l_id_11408) + (69632 * (v_wg_id_11404 / 8))),v__11468 + 0)); 
                                        v__11479_4 = v__11478_0.s0; 
                                        v__11479_5 = v__11478_0.s1; 
                                        v__11479_6 = v__11478_0.s2; 
                                        v__11479_7 = v__11478_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11478_0 = id4(vload4((256 + v_l_id_11409 + (8 * (v_i_11414 / 3)) + (128 * (v_i_11414 % 3)) + (512 * (v_wg_id_11404 % 8)) + (4352 * v_l_id_11406) + (4352 * v_l_id_11408) + (69632 * (v_wg_id_11404 / 8))),v__11468 + 0)); 
                                        v__11479_8 = v__11478_0.s0; 
                                        v__11479_9 = v__11478_0.s1; 
                                        v__11479_10 = v__11478_0.s2; 
                                        v__11479_11 = v__11478_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11478_0 = id4(vload4((384 + v_l_id_11409 + (8 * (v_i_11414 / 3)) + (128 * (v_i_11414 % 3)) + (512 * (v_wg_id_11404 % 8)) + (4352 * v_l_id_11406) + (4352 * v_l_id_11408) + (69632 * (v_wg_id_11404 / 8))),v__11468 + 0)); 
                                        v__11479_12 = v__11478_0.s0; 
                                        v__11479_13 = v__11478_0.s1; 
                                        v__11479_14 = v__11478_0.s2; 
                                        v__11479_15 = v__11478_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11499 = 0.0f; 
                                        v__10623 = v_tmp_11499; 
                                        v__11480_0 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_0 = mult(v__11479_0, v__11476_0); 
                                        v__11480_0 = add(v__11480_0, v__11484_0); 
                                        v__11484_0 = mult(v__11479_1, v__11476_1); 
                                        v__11480_0 = add(v__11480_0, v__11484_0); 
                                        v__11484_0 = mult(v__11479_2, v__11476_2); 
                                        v__11480_0 = add(v__11480_0, v__11484_0); 
                                        v__11484_0 = mult(v__11479_3, v__11476_3); 
                                        v__11480_0 = add(v__11480_0, v__11484_0); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11500 = 0.0f; 
                                        v__10623 = v_tmp_11500; 
                                        v__11480_1 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_1 = mult(v__11479_4, v__11476_0); 
                                        v__11480_1 = add(v__11480_1, v__11484_1); 
                                        v__11484_1 = mult(v__11479_5, v__11476_1); 
                                        v__11480_1 = add(v__11480_1, v__11484_1); 
                                        v__11484_1 = mult(v__11479_6, v__11476_2); 
                                        v__11480_1 = add(v__11480_1, v__11484_1); 
                                        v__11484_1 = mult(v__11479_7, v__11476_3); 
                                        v__11480_1 = add(v__11480_1, v__11484_1); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11501 = 0.0f; 
                                        v__10623 = v_tmp_11501; 
                                        v__11480_2 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_2 = mult(v__11479_8, v__11476_0); 
                                        v__11480_2 = add(v__11480_2, v__11484_2); 
                                        v__11484_2 = mult(v__11479_9, v__11476_1); 
                                        v__11480_2 = add(v__11480_2, v__11484_2); 
                                        v__11484_2 = mult(v__11479_10, v__11476_2); 
                                        v__11480_2 = add(v__11480_2, v__11484_2); 
                                        v__11484_2 = mult(v__11479_11, v__11476_3); 
                                        v__11480_2 = add(v__11480_2, v__11484_2); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11502 = 0.0f; 
                                        v__10623 = v_tmp_11502; 
                                        v__11480_3 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_3 = mult(v__11479_12, v__11476_0); 
                                        v__11480_3 = add(v__11480_3, v__11484_3); 
                                        v__11484_3 = mult(v__11479_13, v__11476_1); 
                                        v__11480_3 = add(v__11480_3, v__11484_3); 
                                        v__11484_3 = mult(v__11479_14, v__11476_2); 
                                        v__11480_3 = add(v__11480_3, v__11484_3); 
                                        v__11484_3 = mult(v__11479_15, v__11476_3); 
                                        v__11480_3 = add(v__11480_3, v__11484_3); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11503 = 0.0f; 
                                        v__10623 = v_tmp_11503; 
                                        v__11480_4 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_4 = mult(v__11479_0, v__11476_4); 
                                        v__11480_4 = add(v__11480_4, v__11484_4); 
                                        v__11484_4 = mult(v__11479_1, v__11476_5); 
                                        v__11480_4 = add(v__11480_4, v__11484_4); 
                                        v__11484_4 = mult(v__11479_2, v__11476_6); 
                                        v__11480_4 = add(v__11480_4, v__11484_4); 
                                        v__11484_4 = mult(v__11479_3, v__11476_7); 
                                        v__11480_4 = add(v__11480_4, v__11484_4); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11504 = 0.0f; 
                                        v__10623 = v_tmp_11504; 
                                        v__11480_5 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_5 = mult(v__11479_4, v__11476_4); 
                                        v__11480_5 = add(v__11480_5, v__11484_5); 
                                        v__11484_5 = mult(v__11479_5, v__11476_5); 
                                        v__11480_5 = add(v__11480_5, v__11484_5); 
                                        v__11484_5 = mult(v__11479_6, v__11476_6); 
                                        v__11480_5 = add(v__11480_5, v__11484_5); 
                                        v__11484_5 = mult(v__11479_7, v__11476_7); 
                                        v__11480_5 = add(v__11480_5, v__11484_5); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11505 = 0.0f; 
                                        v__10623 = v_tmp_11505; 
                                        v__11480_6 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_6 = mult(v__11479_8, v__11476_4); 
                                        v__11480_6 = add(v__11480_6, v__11484_6); 
                                        v__11484_6 = mult(v__11479_9, v__11476_5); 
                                        v__11480_6 = add(v__11480_6, v__11484_6); 
                                        v__11484_6 = mult(v__11479_10, v__11476_6); 
                                        v__11480_6 = add(v__11480_6, v__11484_6); 
                                        v__11484_6 = mult(v__11479_11, v__11476_7); 
                                        v__11480_6 = add(v__11480_6, v__11484_6); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11506 = 0.0f; 
                                        v__10623 = v_tmp_11506; 
                                        v__11480_7 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_7 = mult(v__11479_12, v__11476_4); 
                                        v__11480_7 = add(v__11480_7, v__11484_7); 
                                        v__11484_7 = mult(v__11479_13, v__11476_5); 
                                        v__11480_7 = add(v__11480_7, v__11484_7); 
                                        v__11484_7 = mult(v__11479_14, v__11476_6); 
                                        v__11480_7 = add(v__11480_7, v__11484_7); 
                                        v__11484_7 = mult(v__11479_15, v__11476_7); 
                                        v__11480_7 = add(v__11480_7, v__11484_7); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11507 = 0.0f; 
                                        v__10623 = v_tmp_11507; 
                                        v__11480_8 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_8 = mult(v__11479_0, v__11476_8); 
                                        v__11480_8 = add(v__11480_8, v__11484_8); 
                                        v__11484_8 = mult(v__11479_1, v__11476_9); 
                                        v__11480_8 = add(v__11480_8, v__11484_8); 
                                        v__11484_8 = mult(v__11479_2, v__11476_10); 
                                        v__11480_8 = add(v__11480_8, v__11484_8); 
                                        v__11484_8 = mult(v__11479_3, v__11476_11); 
                                        v__11480_8 = add(v__11480_8, v__11484_8); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11508 = 0.0f; 
                                        v__10623 = v_tmp_11508; 
                                        v__11480_9 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_9 = mult(v__11479_4, v__11476_8); 
                                        v__11480_9 = add(v__11480_9, v__11484_9); 
                                        v__11484_9 = mult(v__11479_5, v__11476_9); 
                                        v__11480_9 = add(v__11480_9, v__11484_9); 
                                        v__11484_9 = mult(v__11479_6, v__11476_10); 
                                        v__11480_9 = add(v__11480_9, v__11484_9); 
                                        v__11484_9 = mult(v__11479_7, v__11476_11); 
                                        v__11480_9 = add(v__11480_9, v__11484_9); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11509 = 0.0f; 
                                        v__10623 = v_tmp_11509; 
                                        v__11480_10 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_10 = mult(v__11479_8, v__11476_8); 
                                        v__11480_10 = add(v__11480_10, v__11484_10); 
                                        v__11484_10 = mult(v__11479_9, v__11476_9); 
                                        v__11480_10 = add(v__11480_10, v__11484_10); 
                                        v__11484_10 = mult(v__11479_10, v__11476_10); 
                                        v__11480_10 = add(v__11480_10, v__11484_10); 
                                        v__11484_10 = mult(v__11479_11, v__11476_11); 
                                        v__11480_10 = add(v__11480_10, v__11484_10); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11510 = 0.0f; 
                                        v__10623 = v_tmp_11510; 
                                        v__11480_11 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_11 = mult(v__11479_12, v__11476_8); 
                                        v__11480_11 = add(v__11480_11, v__11484_11); 
                                        v__11484_11 = mult(v__11479_13, v__11476_9); 
                                        v__11480_11 = add(v__11480_11, v__11484_11); 
                                        v__11484_11 = mult(v__11479_14, v__11476_10); 
                                        v__11480_11 = add(v__11480_11, v__11484_11); 
                                        v__11484_11 = mult(v__11479_15, v__11476_11); 
                                        v__11480_11 = add(v__11480_11, v__11484_11); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11511 = 0.0f; 
                                        v__10623 = v_tmp_11511; 
                                        v__11480_12 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_12 = mult(v__11479_0, v__11476_12); 
                                        v__11480_12 = add(v__11480_12, v__11484_12); 
                                        v__11484_12 = mult(v__11479_1, v__11476_13); 
                                        v__11480_12 = add(v__11480_12, v__11484_12); 
                                        v__11484_12 = mult(v__11479_2, v__11476_14); 
                                        v__11480_12 = add(v__11480_12, v__11484_12); 
                                        v__11484_12 = mult(v__11479_3, v__11476_15); 
                                        v__11480_12 = add(v__11480_12, v__11484_12); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11512 = 0.0f; 
                                        v__10623 = v_tmp_11512; 
                                        v__11480_13 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_13 = mult(v__11479_4, v__11476_12); 
                                        v__11480_13 = add(v__11480_13, v__11484_13); 
                                        v__11484_13 = mult(v__11479_5, v__11476_13); 
                                        v__11480_13 = add(v__11480_13, v__11484_13); 
                                        v__11484_13 = mult(v__11479_6, v__11476_14); 
                                        v__11480_13 = add(v__11480_13, v__11484_13); 
                                        v__11484_13 = mult(v__11479_7, v__11476_15); 
                                        v__11480_13 = add(v__11480_13, v__11484_13); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11513 = 0.0f; 
                                        v__10623 = v_tmp_11513; 
                                        v__11480_14 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_14 = mult(v__11479_8, v__11476_12); 
                                        v__11480_14 = add(v__11480_14, v__11484_14); 
                                        v__11484_14 = mult(v__11479_9, v__11476_13); 
                                        v__11480_14 = add(v__11480_14, v__11484_14); 
                                        v__11484_14 = mult(v__11479_10, v__11476_14); 
                                        v__11480_14 = add(v__11480_14, v__11484_14); 
                                        v__11484_14 = mult(v__11479_11, v__11476_15); 
                                        v__11480_14 = add(v__11480_14, v__11484_14); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11514 = 0.0f; 
                                        v__10623 = v_tmp_11514; 
                                        v__11480_15 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11484_15 = mult(v__11479_12, v__11476_12); 
                                        v__11480_15 = add(v__11480_15, v__11484_15); 
                                        v__11484_15 = mult(v__11479_13, v__11476_13); 
                                        v__11480_15 = add(v__11480_15, v__11484_15); 
                                        v__11484_15 = mult(v__11479_14, v__11476_14); 
                                        v__11480_15 = add(v__11480_15, v__11484_15); 
                                        v__11484_15 = mult(v__11479_15, v__11476_15); 
                                        v__11480_15 = add(v__11480_15, v__11484_15); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11472_0 = add(v__11472_0, v__11480_0); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11472_1 = add(v__11472_1, v__11480_1); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11472_2 = add(v__11472_2, v__11480_2); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11472_3 = add(v__11472_3, v__11480_3); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11472_4 = add(v__11472_4, v__11480_4); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11472_5 = add(v__11472_5, v__11480_5); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11472_6 = add(v__11472_6, v__11480_6); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11472_7 = add(v__11472_7, v__11480_7); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11472_8 = add(v__11472_8, v__11480_8); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11472_9 = add(v__11472_9, v__11480_9); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11472_10 = add(v__11472_10, v__11480_10); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11472_11 = add(v__11472_11, v__11480_11); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11472_12 = add(v__11472_12, v__11480_12); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11472_13 = add(v__11472_13, v__11480_13); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11472_14 = add(v__11472_14, v__11480_14); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11472_15 = add(v__11472_15, v__11480_15); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                    // end reduce_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11490[(16 * (v_l_id_11409 + (8 * v_l_id_11408) + (24 * v_l_id_11406)))] = id(v__11472_0); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11490[(1 + (16 * v_l_id_11409) + (128 * v_l_id_11408) + (384 * v_l_id_11406))] = id(v__11472_1); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11490[(2 * (1 + (8 * v_l_id_11409) + (64 * v_l_id_11408) + (192 * v_l_id_11406)))] = id(v__11472_2); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11490[(3 + (16 * v_l_id_11409) + (128 * v_l_id_11408) + (384 * v_l_id_11406))] = id(v__11472_3); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11490[(4 * (1 + (4 * v_l_id_11409) + (32 * v_l_id_11408) + (96 * v_l_id_11406)))] = id(v__11472_4); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11490[(5 + (16 * v_l_id_11409) + (128 * v_l_id_11408) + (384 * v_l_id_11406))] = id(v__11472_5); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11490[(2 * (3 + (8 * v_l_id_11409) + (64 * v_l_id_11408) + (192 * v_l_id_11406)))] = id(v__11472_6); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11490[(7 + (16 * v_l_id_11409) + (128 * v_l_id_11408) + (384 * v_l_id_11406))] = id(v__11472_7); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11490[(8 * (1 + (2 * v_l_id_11409) + (16 * v_l_id_11408) + (48 * v_l_id_11406)))] = id(v__11472_8); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11490[(9 + (16 * v_l_id_11409) + (128 * v_l_id_11408) + (384 * v_l_id_11406))] = id(v__11472_9); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11490[(2 * (5 + (8 * v_l_id_11409) + (64 * v_l_id_11408) + (192 * v_l_id_11406)))] = id(v__11472_10); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11490[(11 + (16 * v_l_id_11409) + (128 * v_l_id_11408) + (384 * v_l_id_11406))] = id(v__11472_11); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11490[(4 * (3 + (4 * v_l_id_11409) + (32 * v_l_id_11408) + (96 * v_l_id_11406)))] = id(v__11472_12); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11490[(13 + (16 * v_l_id_11409) + (128 * v_l_id_11408) + (384 * v_l_id_11406))] = id(v__11472_13); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11490[(2 * (7 + (8 * v_l_id_11409) + (64 * v_l_id_11408) + (192 * v_l_id_11406)))] = id(v__11472_14); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11490[(15 + (16 * v_l_id_11409) + (128 * v_l_id_11408) + (384 * v_l_id_11406))] = id(v__11472_15); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                }
                            }
                            barrier(CLK_LOCAL_MEM_FENCE);
                            for (int v_l_id_11460 = get_local_id(1); (v_l_id_11460 < 4); v_l_id_11460 = (3 + v_l_id_11460)){
                                // map_seq
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_i_11461 = 0; 
                                    for (int v_l_id_11462 = get_local_id(0); (v_l_id_11462 < 4); v_l_id_11462 = (8 + v_l_id_11462)){
                                        float v_tmp_11515 = 0.0f; 
                                        v__10635 = v_tmp_11515; 
                                        // map_seq
                                        // unroll
                                        v__11491_0 = id(v__10635); 
                                        // end unroll
                                        // end map_seq
                                        // reduce_seq
                                        for (int v_i_11464 = 0; (v_i_11464 < 24); v_i_11464 = (1 + v_i_11464)){
                                            // map_seq
                                            // unroll
                                            v__11491_0 = add(v__11491_0, v__11490[(v_l_id_11462 + (4 * v_l_id_11460) + (16 * v_i_11464) + (384 * v_l_id_11406))]); 
                                            // end unroll
                                            // end map_seq
                                        }
                                        // end reduce_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11496[(v_l_id_11460 + (4 * v_wg_id_11403) + (4 * v_wg_id_11405) + (512 * v_l_id_11462) + (2048 * (v_wg_id_11404 % 8)) + (16384 * v_l_id_11406) + (262144 * (v_wg_id_11404 / 8)))] = id(v__11491_0); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                }
                                // end map_seq
                            }
                        }
                        // end map_seq
                    }
                    barrier(CLK_LOCAL_MEM_FENCE);
                }
            }
        }
    }
}