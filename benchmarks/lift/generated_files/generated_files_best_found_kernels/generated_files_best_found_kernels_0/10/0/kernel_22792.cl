// Layer 10, tune point 0
// inputChannels -> 512, inputWidth -> 14, inputHeight -> 14,
// padFuncX -> 1, padFuncY -> 1,
// kernelWidth -> 3, kernelHeight -> 3,
// kernelStrideX -> 1, kernelStrideY -> 1,
// numKernels -> 512
// Vectorization
// "mapVectorize.213." -> 1,
// "mapVectorize.246." -> 1
// Unsliding
// 
// Parallelization and fusion
// "mapTransform.28." -> (10*Code.mapWrg() + Code.parDims(0)()),
// "mapTransform.29." -> (10*Code.mapWrg() + Code.parDims(1)()),
// "mapTransform.30." -> (Code.replaceInnerMapWithOuter()),
// "mapTransform.51." -> (10*Code.mapWrg() + Code.parDims(2)()),
// "mapTransform.52." -> (10*Code.mapLcl() + Code.parDims(2)()),
// "mapTransform.68." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.70." -> (10*Code.mapLcl() + Code.parDims(0)()),
// "mapTransform.109." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.110." -> (10*Code.mapLcl() + Code.parDims(0)())
// padOptRight -> 1, padOptBottom -> 1,
// tileWidth -> 5, tileHeight -> 5, tileDepth -> 16,
// seqWindowsPerThreadX -> 5, seqWindowsPerThreadY -> 1,
// seqKernelsPerThread -> 4,
// kernelGroupSize -> 4,
// windowTileWidth -> 1, windowTileHeight -> 1,
// inputCacheSizeX -> 5, inputCacheSizeY -> 1,
// inputCacheDepth -> 4,
// kernelCacheSize -> 4,
// localSizes(0) -> 4,
// localSizes(1) -> 3,
// localSizes(2) -> 5,
// nWrgs(0) -> 128,
// nWrgs(1) -> 9,
// nWrgs(2) -> 1
// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

float id(float x){
    {
        { return x; }; 
    }
}
float4 id4(float4 x){
    {
        { return x; }; 
    }
}
float add(float x, float y){
    {
        { return x+y; }; 
    }
}
float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
kernel __attribute((reqd_work_group_size(4,3,5)))
void KERNEL(const global float* restrict v__11474, const global float* restrict v__11475, global float* v__11502){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        local float v__11496[1200];
        // Typed Value memory
        float v__10614; 
        float v__10623; 
        float v__10635; 
        // Private Memory
        float v__11478_0;
        float v__11478_1;
        float v__11478_2;
        float v__11478_3;
        float v__11478_4;
        float v__11478_5;
        float v__11478_6;
        float v__11478_7;
        float v__11478_8;
        float v__11478_9;
        float v__11478_10;
        float v__11478_11;
        float v__11478_12;
        float v__11478_13;
        float v__11478_14;
        float v__11478_15;
        float v__11478_16;
        float v__11478_17;
        float v__11478_18;
        float v__11478_19;
        float4 v__11481_0;
        float v__11482_0;
        float v__11482_1;
        float v__11482_2;
        float v__11482_3;
        float v__11482_4;
        float v__11482_5;
        float v__11482_6;
        float v__11482_7;
        float v__11482_8;
        float v__11482_9;
        float v__11482_10;
        float v__11482_11;
        float v__11482_12;
        float v__11482_13;
        float v__11482_14;
        float v__11482_15;
        float4 v__11484_0;
        float v__11485_0;
        float v__11485_1;
        float v__11485_2;
        float v__11485_3;
        float v__11485_4;
        float v__11485_5;
        float v__11485_6;
        float v__11485_7;
        float v__11485_8;
        float v__11485_9;
        float v__11485_10;
        float v__11485_11;
        float v__11485_12;
        float v__11485_13;
        float v__11485_14;
        float v__11485_15;
        float v__11485_16;
        float v__11485_17;
        float v__11485_18;
        float v__11485_19;
        float v__11486_0;
        float v__11486_1;
        float v__11486_2;
        float v__11486_3;
        float v__11486_4;
        float v__11486_5;
        float v__11486_6;
        float v__11486_7;
        float v__11486_8;
        float v__11486_9;
        float v__11486_10;
        float v__11486_11;
        float v__11486_12;
        float v__11486_13;
        float v__11486_14;
        float v__11486_15;
        float v__11486_16;
        float v__11486_17;
        float v__11486_18;
        float v__11486_19;
        float v__11490_0;
        float v__11490_1;
        float v__11490_2;
        float v__11490_3;
        float v__11490_4;
        float v__11490_5;
        float v__11490_6;
        float v__11490_7;
        float v__11490_8;
        float v__11490_9;
        float v__11490_10;
        float v__11490_11;
        float v__11490_12;
        float v__11490_13;
        float v__11490_14;
        float v__11490_15;
        float v__11490_16;
        float v__11490_17;
        float v__11490_18;
        float v__11490_19;
        float v__11497_0;
        // iteration count is exactly 1, no loop emitted
        {
            int v_wg_id_11409 = get_group_id(0); 
            // iteration count is exactly 1, no loop emitted
            {
                int v_wg_id_11410 = get_group_id(1); 
                // iteration count is exactly 1, no loop emitted
                {
                    int v_wg_id_11411 = get_group_id(2); 
                    // iteration count is exactly 1, no loop emitted
                    {
                        int v_l_id_11412 = get_local_id(2); 
                        // map_seq
                        // iteration count is exactly 1, no loop emitted
                        {
                            int v_i_11413 = 0; 
                            // iteration count is exactly 1, no loop emitted
                            {
                                int v_l_id_11414 = get_local_id(1); 
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_l_id_11415 = get_local_id(0); 
                                    float v_tmp_11503 = 0.0f; 
                                    v__10614 = v_tmp_11503; 
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11478_0 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_1 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_2 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_3 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_4 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11478_5 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_6 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_7 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_8 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_9 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11478_10 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_11 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_12 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_13 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_14 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11478_15 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_16 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_17 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_18 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11478_19 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // reduce_seq
                                    for (int v_i_11420 = 0; (v_i_11420 < 96); v_i_11420 = (1 + v_i_11420)){
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11481_0 = id4(vload4((v_l_id_11415 + (4 * (v_i_11420 / 3)) + (128 * (v_i_11420 % 3)) + (384 * v_l_id_11414) + (4608 * v_wg_id_11409) + (4608 * v_wg_id_11411)),v__11475 + 0)); 
                                        v__11482_0 = v__11481_0.s0; 
                                        v__11482_1 = v__11481_0.s1; 
                                        v__11482_2 = v__11481_0.s2; 
                                        v__11482_3 = v__11481_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11481_0 = id4(vload4((1152 + v_l_id_11415 + (4 * (v_i_11420 / 3)) + (128 * (v_i_11420 % 3)) + (384 * v_l_id_11414) + (4608 * v_wg_id_11409) + (4608 * v_wg_id_11411)),v__11475 + 0)); 
                                        v__11482_4 = v__11481_0.s0; 
                                        v__11482_5 = v__11481_0.s1; 
                                        v__11482_6 = v__11481_0.s2; 
                                        v__11482_7 = v__11481_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11481_0 = id4(vload4((2304 + v_l_id_11415 + (4 * (v_i_11420 / 3)) + (128 * (v_i_11420 % 3)) + (384 * v_l_id_11414) + (4608 * v_wg_id_11409) + (4608 * v_wg_id_11411)),v__11475 + 0)); 
                                        v__11482_8 = v__11481_0.s0; 
                                        v__11482_9 = v__11481_0.s1; 
                                        v__11482_10 = v__11481_0.s2; 
                                        v__11482_11 = v__11481_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11481_0 = id4(vload4((3456 + v_l_id_11415 + (4 * (v_i_11420 / 3)) + (128 * (v_i_11420 % 3)) + (384 * v_l_id_11414) + (4608 * v_wg_id_11409) + (4608 * v_wg_id_11411)),v__11475 + 0)); 
                                        v__11482_12 = v__11481_0.s0; 
                                        v__11482_13 = v__11481_0.s1; 
                                        v__11482_14 = v__11481_0.s2; 
                                        v__11482_15 = v__11481_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11484_0 = id4(vload4((v_l_id_11415 + (4 * (v_i_11420 / 3)) + (128 * (v_i_11420 % 3)) + (640 * (v_wg_id_11410 % 3)) + (2176 * v_l_id_11412) + (2176 * v_l_id_11414) + (10880 * (v_wg_id_11410 / 3))),v__11474 + 0)); 
                                        v__11485_0 = v__11484_0.s0; 
                                        v__11485_1 = v__11484_0.s1; 
                                        v__11485_2 = v__11484_0.s2; 
                                        v__11485_3 = v__11484_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11484_0 = id4(vload4((128 + v_l_id_11415 + (4 * (v_i_11420 / 3)) + (128 * (v_i_11420 % 3)) + (640 * (v_wg_id_11410 % 3)) + (2176 * v_l_id_11412) + (2176 * v_l_id_11414) + (10880 * (v_wg_id_11410 / 3))),v__11474 + 0)); 
                                        v__11485_4 = v__11484_0.s0; 
                                        v__11485_5 = v__11484_0.s1; 
                                        v__11485_6 = v__11484_0.s2; 
                                        v__11485_7 = v__11484_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11484_0 = id4(vload4((256 + v_l_id_11415 + (4 * (v_i_11420 / 3)) + (128 * (v_i_11420 % 3)) + (640 * (v_wg_id_11410 % 3)) + (2176 * v_l_id_11412) + (2176 * v_l_id_11414) + (10880 * (v_wg_id_11410 / 3))),v__11474 + 0)); 
                                        v__11485_8 = v__11484_0.s0; 
                                        v__11485_9 = v__11484_0.s1; 
                                        v__11485_10 = v__11484_0.s2; 
                                        v__11485_11 = v__11484_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11484_0 = id4(vload4((384 + v_l_id_11415 + (4 * (v_i_11420 / 3)) + (128 * (v_i_11420 % 3)) + (640 * (v_wg_id_11410 % 3)) + (2176 * v_l_id_11412) + (2176 * v_l_id_11414) + (10880 * (v_wg_id_11410 / 3))),v__11474 + 0)); 
                                        v__11485_12 = v__11484_0.s0; 
                                        v__11485_13 = v__11484_0.s1; 
                                        v__11485_14 = v__11484_0.s2; 
                                        v__11485_15 = v__11484_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11484_0 = id4(vload4((512 + v_l_id_11415 + (4 * (v_i_11420 / 3)) + (128 * (v_i_11420 % 3)) + (640 * (v_wg_id_11410 % 3)) + (2176 * v_l_id_11412) + (2176 * v_l_id_11414) + (10880 * (v_wg_id_11410 / 3))),v__11474 + 0)); 
                                        v__11485_16 = v__11484_0.s0; 
                                        v__11485_17 = v__11484_0.s1; 
                                        v__11485_18 = v__11484_0.s2; 
                                        v__11485_19 = v__11484_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11504 = 0.0f; 
                                        v__10623 = v_tmp_11504; 
                                        v__11486_0 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_0 = mult(v__11485_0, v__11482_0); 
                                        v__11486_0 = add(v__11486_0, v__11490_0); 
                                        v__11490_0 = mult(v__11485_1, v__11482_1); 
                                        v__11486_0 = add(v__11486_0, v__11490_0); 
                                        v__11490_0 = mult(v__11485_2, v__11482_2); 
                                        v__11486_0 = add(v__11486_0, v__11490_0); 
                                        v__11490_0 = mult(v__11485_3, v__11482_3); 
                                        v__11486_0 = add(v__11486_0, v__11490_0); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11505 = 0.0f; 
                                        v__10623 = v_tmp_11505; 
                                        v__11486_1 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_1 = mult(v__11485_4, v__11482_0); 
                                        v__11486_1 = add(v__11486_1, v__11490_1); 
                                        v__11490_1 = mult(v__11485_5, v__11482_1); 
                                        v__11486_1 = add(v__11486_1, v__11490_1); 
                                        v__11490_1 = mult(v__11485_6, v__11482_2); 
                                        v__11486_1 = add(v__11486_1, v__11490_1); 
                                        v__11490_1 = mult(v__11485_7, v__11482_3); 
                                        v__11486_1 = add(v__11486_1, v__11490_1); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11506 = 0.0f; 
                                        v__10623 = v_tmp_11506; 
                                        v__11486_2 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_2 = mult(v__11485_8, v__11482_0); 
                                        v__11486_2 = add(v__11486_2, v__11490_2); 
                                        v__11490_2 = mult(v__11485_9, v__11482_1); 
                                        v__11486_2 = add(v__11486_2, v__11490_2); 
                                        v__11490_2 = mult(v__11485_10, v__11482_2); 
                                        v__11486_2 = add(v__11486_2, v__11490_2); 
                                        v__11490_2 = mult(v__11485_11, v__11482_3); 
                                        v__11486_2 = add(v__11486_2, v__11490_2); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11507 = 0.0f; 
                                        v__10623 = v_tmp_11507; 
                                        v__11486_3 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_3 = mult(v__11485_12, v__11482_0); 
                                        v__11486_3 = add(v__11486_3, v__11490_3); 
                                        v__11490_3 = mult(v__11485_13, v__11482_1); 
                                        v__11486_3 = add(v__11486_3, v__11490_3); 
                                        v__11490_3 = mult(v__11485_14, v__11482_2); 
                                        v__11486_3 = add(v__11486_3, v__11490_3); 
                                        v__11490_3 = mult(v__11485_15, v__11482_3); 
                                        v__11486_3 = add(v__11486_3, v__11490_3); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11508 = 0.0f; 
                                        v__10623 = v_tmp_11508; 
                                        v__11486_4 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_4 = mult(v__11485_16, v__11482_0); 
                                        v__11486_4 = add(v__11486_4, v__11490_4); 
                                        v__11490_4 = mult(v__11485_17, v__11482_1); 
                                        v__11486_4 = add(v__11486_4, v__11490_4); 
                                        v__11490_4 = mult(v__11485_18, v__11482_2); 
                                        v__11486_4 = add(v__11486_4, v__11490_4); 
                                        v__11490_4 = mult(v__11485_19, v__11482_3); 
                                        v__11486_4 = add(v__11486_4, v__11490_4); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11509 = 0.0f; 
                                        v__10623 = v_tmp_11509; 
                                        v__11486_5 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_5 = mult(v__11485_0, v__11482_4); 
                                        v__11486_5 = add(v__11486_5, v__11490_5); 
                                        v__11490_5 = mult(v__11485_1, v__11482_5); 
                                        v__11486_5 = add(v__11486_5, v__11490_5); 
                                        v__11490_5 = mult(v__11485_2, v__11482_6); 
                                        v__11486_5 = add(v__11486_5, v__11490_5); 
                                        v__11490_5 = mult(v__11485_3, v__11482_7); 
                                        v__11486_5 = add(v__11486_5, v__11490_5); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11510 = 0.0f; 
                                        v__10623 = v_tmp_11510; 
                                        v__11486_6 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_6 = mult(v__11485_4, v__11482_4); 
                                        v__11486_6 = add(v__11486_6, v__11490_6); 
                                        v__11490_6 = mult(v__11485_5, v__11482_5); 
                                        v__11486_6 = add(v__11486_6, v__11490_6); 
                                        v__11490_6 = mult(v__11485_6, v__11482_6); 
                                        v__11486_6 = add(v__11486_6, v__11490_6); 
                                        v__11490_6 = mult(v__11485_7, v__11482_7); 
                                        v__11486_6 = add(v__11486_6, v__11490_6); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11511 = 0.0f; 
                                        v__10623 = v_tmp_11511; 
                                        v__11486_7 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_7 = mult(v__11485_8, v__11482_4); 
                                        v__11486_7 = add(v__11486_7, v__11490_7); 
                                        v__11490_7 = mult(v__11485_9, v__11482_5); 
                                        v__11486_7 = add(v__11486_7, v__11490_7); 
                                        v__11490_7 = mult(v__11485_10, v__11482_6); 
                                        v__11486_7 = add(v__11486_7, v__11490_7); 
                                        v__11490_7 = mult(v__11485_11, v__11482_7); 
                                        v__11486_7 = add(v__11486_7, v__11490_7); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11512 = 0.0f; 
                                        v__10623 = v_tmp_11512; 
                                        v__11486_8 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_8 = mult(v__11485_12, v__11482_4); 
                                        v__11486_8 = add(v__11486_8, v__11490_8); 
                                        v__11490_8 = mult(v__11485_13, v__11482_5); 
                                        v__11486_8 = add(v__11486_8, v__11490_8); 
                                        v__11490_8 = mult(v__11485_14, v__11482_6); 
                                        v__11486_8 = add(v__11486_8, v__11490_8); 
                                        v__11490_8 = mult(v__11485_15, v__11482_7); 
                                        v__11486_8 = add(v__11486_8, v__11490_8); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11513 = 0.0f; 
                                        v__10623 = v_tmp_11513; 
                                        v__11486_9 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_9 = mult(v__11485_16, v__11482_4); 
                                        v__11486_9 = add(v__11486_9, v__11490_9); 
                                        v__11490_9 = mult(v__11485_17, v__11482_5); 
                                        v__11486_9 = add(v__11486_9, v__11490_9); 
                                        v__11490_9 = mult(v__11485_18, v__11482_6); 
                                        v__11486_9 = add(v__11486_9, v__11490_9); 
                                        v__11490_9 = mult(v__11485_19, v__11482_7); 
                                        v__11486_9 = add(v__11486_9, v__11490_9); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11514 = 0.0f; 
                                        v__10623 = v_tmp_11514; 
                                        v__11486_10 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_10 = mult(v__11485_0, v__11482_8); 
                                        v__11486_10 = add(v__11486_10, v__11490_10); 
                                        v__11490_10 = mult(v__11485_1, v__11482_9); 
                                        v__11486_10 = add(v__11486_10, v__11490_10); 
                                        v__11490_10 = mult(v__11485_2, v__11482_10); 
                                        v__11486_10 = add(v__11486_10, v__11490_10); 
                                        v__11490_10 = mult(v__11485_3, v__11482_11); 
                                        v__11486_10 = add(v__11486_10, v__11490_10); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11515 = 0.0f; 
                                        v__10623 = v_tmp_11515; 
                                        v__11486_11 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_11 = mult(v__11485_4, v__11482_8); 
                                        v__11486_11 = add(v__11486_11, v__11490_11); 
                                        v__11490_11 = mult(v__11485_5, v__11482_9); 
                                        v__11486_11 = add(v__11486_11, v__11490_11); 
                                        v__11490_11 = mult(v__11485_6, v__11482_10); 
                                        v__11486_11 = add(v__11486_11, v__11490_11); 
                                        v__11490_11 = mult(v__11485_7, v__11482_11); 
                                        v__11486_11 = add(v__11486_11, v__11490_11); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11516 = 0.0f; 
                                        v__10623 = v_tmp_11516; 
                                        v__11486_12 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_12 = mult(v__11485_8, v__11482_8); 
                                        v__11486_12 = add(v__11486_12, v__11490_12); 
                                        v__11490_12 = mult(v__11485_9, v__11482_9); 
                                        v__11486_12 = add(v__11486_12, v__11490_12); 
                                        v__11490_12 = mult(v__11485_10, v__11482_10); 
                                        v__11486_12 = add(v__11486_12, v__11490_12); 
                                        v__11490_12 = mult(v__11485_11, v__11482_11); 
                                        v__11486_12 = add(v__11486_12, v__11490_12); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11517 = 0.0f; 
                                        v__10623 = v_tmp_11517; 
                                        v__11486_13 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_13 = mult(v__11485_12, v__11482_8); 
                                        v__11486_13 = add(v__11486_13, v__11490_13); 
                                        v__11490_13 = mult(v__11485_13, v__11482_9); 
                                        v__11486_13 = add(v__11486_13, v__11490_13); 
                                        v__11490_13 = mult(v__11485_14, v__11482_10); 
                                        v__11486_13 = add(v__11486_13, v__11490_13); 
                                        v__11490_13 = mult(v__11485_15, v__11482_11); 
                                        v__11486_13 = add(v__11486_13, v__11490_13); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11518 = 0.0f; 
                                        v__10623 = v_tmp_11518; 
                                        v__11486_14 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_14 = mult(v__11485_16, v__11482_8); 
                                        v__11486_14 = add(v__11486_14, v__11490_14); 
                                        v__11490_14 = mult(v__11485_17, v__11482_9); 
                                        v__11486_14 = add(v__11486_14, v__11490_14); 
                                        v__11490_14 = mult(v__11485_18, v__11482_10); 
                                        v__11486_14 = add(v__11486_14, v__11490_14); 
                                        v__11490_14 = mult(v__11485_19, v__11482_11); 
                                        v__11486_14 = add(v__11486_14, v__11490_14); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11519 = 0.0f; 
                                        v__10623 = v_tmp_11519; 
                                        v__11486_15 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_15 = mult(v__11485_0, v__11482_12); 
                                        v__11486_15 = add(v__11486_15, v__11490_15); 
                                        v__11490_15 = mult(v__11485_1, v__11482_13); 
                                        v__11486_15 = add(v__11486_15, v__11490_15); 
                                        v__11490_15 = mult(v__11485_2, v__11482_14); 
                                        v__11486_15 = add(v__11486_15, v__11490_15); 
                                        v__11490_15 = mult(v__11485_3, v__11482_15); 
                                        v__11486_15 = add(v__11486_15, v__11490_15); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11520 = 0.0f; 
                                        v__10623 = v_tmp_11520; 
                                        v__11486_16 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_16 = mult(v__11485_4, v__11482_12); 
                                        v__11486_16 = add(v__11486_16, v__11490_16); 
                                        v__11490_16 = mult(v__11485_5, v__11482_13); 
                                        v__11486_16 = add(v__11486_16, v__11490_16); 
                                        v__11490_16 = mult(v__11485_6, v__11482_14); 
                                        v__11486_16 = add(v__11486_16, v__11490_16); 
                                        v__11490_16 = mult(v__11485_7, v__11482_15); 
                                        v__11486_16 = add(v__11486_16, v__11490_16); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11521 = 0.0f; 
                                        v__10623 = v_tmp_11521; 
                                        v__11486_17 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_17 = mult(v__11485_8, v__11482_12); 
                                        v__11486_17 = add(v__11486_17, v__11490_17); 
                                        v__11490_17 = mult(v__11485_9, v__11482_13); 
                                        v__11486_17 = add(v__11486_17, v__11490_17); 
                                        v__11490_17 = mult(v__11485_10, v__11482_14); 
                                        v__11486_17 = add(v__11486_17, v__11490_17); 
                                        v__11490_17 = mult(v__11485_11, v__11482_15); 
                                        v__11486_17 = add(v__11486_17, v__11490_17); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11522 = 0.0f; 
                                        v__10623 = v_tmp_11522; 
                                        v__11486_18 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_18 = mult(v__11485_12, v__11482_12); 
                                        v__11486_18 = add(v__11486_18, v__11490_18); 
                                        v__11490_18 = mult(v__11485_13, v__11482_13); 
                                        v__11486_18 = add(v__11486_18, v__11490_18); 
                                        v__11490_18 = mult(v__11485_14, v__11482_14); 
                                        v__11486_18 = add(v__11486_18, v__11490_18); 
                                        v__11490_18 = mult(v__11485_15, v__11482_15); 
                                        v__11486_18 = add(v__11486_18, v__11490_18); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11523 = 0.0f; 
                                        v__10623 = v_tmp_11523; 
                                        v__11486_19 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11490_19 = mult(v__11485_16, v__11482_12); 
                                        v__11486_19 = add(v__11486_19, v__11490_19); 
                                        v__11490_19 = mult(v__11485_17, v__11482_13); 
                                        v__11486_19 = add(v__11486_19, v__11490_19); 
                                        v__11490_19 = mult(v__11485_18, v__11482_14); 
                                        v__11486_19 = add(v__11486_19, v__11490_19); 
                                        v__11490_19 = mult(v__11485_19, v__11482_15); 
                                        v__11486_19 = add(v__11486_19, v__11490_19); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11478_0 = add(v__11478_0, v__11486_0); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_1 = add(v__11478_1, v__11486_1); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_2 = add(v__11478_2, v__11486_2); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_3 = add(v__11478_3, v__11486_3); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_4 = add(v__11478_4, v__11486_4); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11478_5 = add(v__11478_5, v__11486_5); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_6 = add(v__11478_6, v__11486_6); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_7 = add(v__11478_7, v__11486_7); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_8 = add(v__11478_8, v__11486_8); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_9 = add(v__11478_9, v__11486_9); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11478_10 = add(v__11478_10, v__11486_10); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_11 = add(v__11478_11, v__11486_11); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_12 = add(v__11478_12, v__11486_12); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_13 = add(v__11478_13, v__11486_13); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_14 = add(v__11478_14, v__11486_14); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11478_15 = add(v__11478_15, v__11486_15); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_16 = add(v__11478_16, v__11486_16); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_17 = add(v__11478_17, v__11486_17); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_18 = add(v__11478_18, v__11486_18); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11478_19 = add(v__11478_19, v__11486_19); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                    // end reduce_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11496[(20 * (v_l_id_11415 + (4 * v_l_id_11414) + (12 * v_l_id_11412)))] = id(v__11478_0); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(1 + (20 * v_l_id_11415) + (80 * v_l_id_11414) + (240 * v_l_id_11412))] = id(v__11478_1); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(2 * (1 + (10 * v_l_id_11415) + (40 * v_l_id_11414) + (120 * v_l_id_11412)))] = id(v__11478_2); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(3 + (20 * v_l_id_11415) + (80 * v_l_id_11414) + (240 * v_l_id_11412))] = id(v__11478_3); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(4 * (1 + (5 * v_l_id_11415) + (20 * v_l_id_11414) + (60 * v_l_id_11412)))] = id(v__11478_4); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11496[(5 * (1 + (4 * v_l_id_11415) + (16 * v_l_id_11414) + (48 * v_l_id_11412)))] = id(v__11478_5); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(2 * (3 + (10 * v_l_id_11415) + (40 * v_l_id_11414) + (120 * v_l_id_11412)))] = id(v__11478_6); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(7 + (20 * v_l_id_11415) + (80 * v_l_id_11414) + (240 * v_l_id_11412))] = id(v__11478_7); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(4 * (2 + (5 * v_l_id_11415) + (20 * v_l_id_11414) + (60 * v_l_id_11412)))] = id(v__11478_8); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(9 + (20 * v_l_id_11415) + (80 * v_l_id_11414) + (240 * v_l_id_11412))] = id(v__11478_9); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11496[(10 * (1 + (2 * v_l_id_11415) + (8 * v_l_id_11414) + (24 * v_l_id_11412)))] = id(v__11478_10); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(11 + (20 * v_l_id_11415) + (80 * v_l_id_11414) + (240 * v_l_id_11412))] = id(v__11478_11); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(4 * (3 + (5 * v_l_id_11415) + (20 * v_l_id_11414) + (60 * v_l_id_11412)))] = id(v__11478_12); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(13 + (20 * v_l_id_11415) + (80 * v_l_id_11414) + (240 * v_l_id_11412))] = id(v__11478_13); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(2 * (7 + (10 * v_l_id_11415) + (40 * v_l_id_11414) + (120 * v_l_id_11412)))] = id(v__11478_14); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11496[(5 * (3 + (4 * v_l_id_11415) + (16 * v_l_id_11414) + (48 * v_l_id_11412)))] = id(v__11478_15); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(4 * (4 + (5 * v_l_id_11415) + (20 * v_l_id_11414) + (60 * v_l_id_11412)))] = id(v__11478_16); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(17 + (20 * v_l_id_11415) + (80 * v_l_id_11414) + (240 * v_l_id_11412))] = id(v__11478_17); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(2 * (9 + (10 * v_l_id_11415) + (40 * v_l_id_11414) + (120 * v_l_id_11412)))] = id(v__11478_18); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11496[(19 + (20 * v_l_id_11415) + (80 * v_l_id_11414) + (240 * v_l_id_11412))] = id(v__11478_19); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                }
                            }
                            barrier(CLK_LOCAL_MEM_FENCE);
                            for (int v_l_id_11466 = get_local_id(1); (v_l_id_11466 < 4); v_l_id_11466 = (3 + v_l_id_11466)){
                                // map_seq
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_i_11467 = 0; 
                                    for (int v_l_id_11468 = get_local_id(0); (v_l_id_11468 < 5); v_l_id_11468 = (4 + v_l_id_11468)){
                                        float v_tmp_11524 = 0.0f; 
                                        v__10635 = v_tmp_11524; 
                                        // map_seq
                                        // unroll
                                        v__11497_0 = id(v__10635); 
                                        // end unroll
                                        // end map_seq
                                        // reduce_seq
                                        for (int v_i_11470 = 0; (v_i_11470 < 12); v_i_11470 = (1 + v_i_11470)){
                                            // map_seq
                                            // unroll
                                            v__11497_0 = add(v__11497_0, v__11496[(v_l_id_11468 + (5 * v_l_id_11466) + (20 * v_i_11470) + (240 * v_l_id_11412))]); 
                                            // end unroll
                                            // end map_seq
                                        }
                                        // end reduce_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11502[(v_l_id_11466 + (4 * v_wg_id_11409) + (4 * v_wg_id_11411) + (512 * v_l_id_11468) + (2560 * (v_wg_id_11410 % 3)) + (7680 * v_l_id_11412) + (38400 * (v_wg_id_11410 / 3)))] = id(v__11497_0); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                }
                                // end map_seq
                            }
                        }
                        // end map_seq
                    }
                    barrier(CLK_LOCAL_MEM_FENCE);
                }
            }
        }
    }
}