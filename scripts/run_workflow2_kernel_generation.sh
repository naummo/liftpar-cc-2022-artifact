#!/bin/bash
set -e
set -x

if [ $# -eq 2 ]
then 

	N_POINTS_TO_EXPLORE_PER_LAYER=$1
	EXPERIMENT_LABEL=$2

	BUILD_DIR=$ROOTDIR/benchmarks/lift
	SCALA_LOG_PATH=$BUILD_DIR/scala_logs
	GENERATED_FILES_DIR=generated_files_$EXPERIMENT_LABEL

	FIRST_LAYER=0
	LAST_LAYER=10

	FIRST_POINT=0
	UNTIL_POINT=$N_POINTS_TO_EXPLORE_PER_LAYER

	RUNCONFIG_NAME=${EXPERIMENT_LABEL}_l${FIRST_LAYER}-${LAST_LAYER}_k${FIRST_POINT}-${N_POINTS_TO_EXPLORE_PER_LAYER}.csv

	mkdir -p $BUILD_DIR

	rm -rf $BUILD_DIR/scala_logs/${EXPERIMENT_LABEL}

	cd $BUILD_DIR

	# Generate Lift kernels
	$ROOTDIR/scripts/generation/generate_lift_kernels_in_par_search_space.sh ${EXPERIMENT_LABEL} ${FIRST_LAYER} ${LAST_LAYER} ${N_POINTS_TO_EXPLORE_PER_LAYER} $BUILD_DIR $SCALA_LOG_PATH
else
	echo "Wrong arguments supplied"
		echo "Usage: scripts/run_workflow2_figure4.sh [n_points_to_generate_per_layer] [experiment_label] [n_threads]"
		echo ""
		echo "How many points to generate per layer?"
		echo "What is the experiment label?"
		echo "How many threads to use for log parsing? 1-8 are good values."
	exit -1
fi
