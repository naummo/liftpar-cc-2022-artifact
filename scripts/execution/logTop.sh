#!/usr/bin/env bash

LAYER=$1
TUNEPOINT=$2
WHEN=$3
TOPLOG=$4

printf "\n" >> $TOPLOG
echo "TOP output $WHEN running layer $LAYER and tunepoint $2" >> $TOPLOG
top -b -n 1 >> $TOPLOG

echo "Saved TOP output $WHEN running the tunepoint"