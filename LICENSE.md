This artifact consists of many software components each with their own licences.

This file list the software components and their LICENSE files.

 * Lift in the folder ./tools/lift with the licence file at ./tools/lift/LICENSE

The following files are licensed under the MIT license (shown below):
 * ./README.md
 * ./environment.env
 * all files in the folder ./scripts
 * all files in the folder ./benchmarks/lift
 
For usage of any other files please contact the authors of this artifact:
 - Naums Mogers, University of Edinburgh (naums.mogers@ed.ac.uk)
 - Lu Li, University of Edinburgh (lu.li@ed.ac.uk)
 - Valentin Radu, University of Sheffield (v.radu@sheffield.ac.uk)
 - Christophe Dubach, McGill University (christophe.dubach@mcgill.ca)


 
================================================================================
MIT License

Copyright (c) 2022 Naums Mogers, Lu Li, Valentin Radu, Christoph Dubach

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
