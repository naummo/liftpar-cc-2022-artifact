from datetime import datetime
import numpy as np
import argparse
import pandas as pd
import re
import os
from os.path import join
import subprocess
from timeit import default_timer

def timer_start():
	# pass
	return default_timer()

def timer_end(start, msg):
	pass
	end = default_timer()
	print("%s: %.7f sec" % (msg, end - start))


parser = argparse.ArgumentParser()
parser.add_argument('--netname', action="store", dest="netname", required=True)
parser.add_argument('--netlabel', action="store", dest="netlabel", required=True)
parser.add_argument('--logdir', action="store", dest="logdir", required=True)
parser.add_argument('--timingsdir', action="store", dest="timingsdir", required=True)
args = parser.parse_args()


if not os.path.exists(args.timingsdir):
    os.makedirs(args.timingsdir)

collatedCsvFilename = join(args.timingsdir, args.netname + "_" + args.netlabel + "_timings.csv")
collatedCsvFilenameAggregated = join(args.timingsdir, args.netname + "_" + args.netlabel + "_timings_aggregated.csv")

column_dtypes = {
    "layer" : pd.UInt32Dtype(),
    "tunepoint" : pd.UInt32Dtype(),
    "total_runtime" : np.float32, 
    "rt0" : np.float32,
    "rt1" : np.float32,
    "rt2" : np.float32,
    "rt3" : np.float32,
    "success_status" : pd.UInt8Dtype(),
    "fail_reported" : pd.UInt8Dtype(),
    "reached_runtime_limit" : pd.UInt32Dtype(),
    "temp_before" : pd.UInt32Dtype(),
    "temp_after" : pd.UInt32Dtype(),
    # "clock_start" : "<M8[ns]", #np.dtype('datetime64[ns]'),
    # "clock_stop" : "<M8[ns]", #np.dtype('datetime64[ns]'),
    "board" : pd.UInt32Dtype(),
    "set_freq" : pd.UInt64Dtype(),
    "report_freq": pd.UInt64Dtype()
}
parsed_csvs = []
old_timings_csv = []
print("Reading " + collatedCsvFilename)
if os.path.isfile(collatedCsvFilename):
    start = timer_start()
    old_timings_csv = pd.read_csv(collatedCsvFilename, dtype=column_dtypes, parse_dates=["clock_start", "clock_stop"])
    timer_end(start, "read_csv old collated timings")
    parsed_csvs = old_timings_csv.csvpath.unique().tolist()


csvdirs = [join(args.logdir, args.netname, args.netlabel, "csv")]
p_c_files_path = re.compile(r".*generated_files_" + args.netlabel + r"_(?P<job_idx>\d+)_.*")
p_fname_csvpath_nochecksum = re.compile(r"(?P<logname>.+?\.log)\..*")

start_readcsvs = timer_start()
timings_list = []
for csvdir in csvdirs:
    for csvname in os.listdir(csvdir):
        if csvname.endswith(".csv"):
            csvpath = os.path.join(csvdir, csvname)

            if csvpath in parsed_csvs:
                continue

            m_csvpath_nochecksum = p_fname_csvpath_nochecksum.match(csvpath)
            assert m_csvpath_nochecksum is not None

            old_instances_of_this_csvpath = [path for path in parsed_csvs 
                if path.startswith(m_csvpath_nochecksum.group("logname"))]

            if len(old_instances_of_this_csvpath) > 0:
                print("csvpath: " + csvpath)
                print("Found old instances of this csvpath:\n" + str(old_instances_of_this_csvpath))
                start = timer_start()
                old_timings_csv = old_timings_csv[ ~ old_timings_csv.csvpath.isin(old_instances_of_this_csvpath) ]
                timer_end(start, "filter old timings")
             
            # print("Appending " + csvpath)

            # start = timer_start()
            new_timings = pd.read_csv(csvpath)
            # timer_end(start, "read")
            new_timings["csvpath"] = csvpath

            m_fname_c_files_path = p_c_files_path.match(csvname)
            if m_fname_c_files_path is not None:
                job_idx = int(m_fname_c_files_path.group("job_idx"))
                new_timings["job_idx"] = job_idx
                new_timings["files_dir"] = csvpath[csvpath.index("generated_files") : csvpath.index("_logof")]
            else:
                assert "c_files_path" in new_timings.columns
                # start = timer_start()
                new_timings["job_idx"] = new_timings.apply(lambda row: int(
                    p_c_files_path.match(row["c_files_path"]).group("job_idx") if p_c_files_path.match(row["c_files_path"]) is not None else 0
                    ), axis="columns")
                # timer_end(start, "parsing job_idx from c_files_path")
                # pd.set_option('display.max_colwidth', 300)
                # print(new_timings)
                # print(new_timings["c_files_path"])
                # print(new_timings["job_idx"])
                # print(new_timings)
                # new_timings.drop("c_files_path", axis="columns", inplace=True)
                new_timings.rename(columns={"c_files_path": "files_dir"}, inplace=True)
                # quit()

            # start = timer_start()
            # timer_end(start, "assign")
            # start = timer_start()
            # timer_end(start, "assign 2")
            timings_list.append(new_timings)
timer_end(start_readcsvs, "read_csvs")

print("New timings files: " + str(len(timings_list)))
print("New timings in total: " + str(sum([len(t.index) for t in timings_list])))

if len(timings_list) != 0:
    start = timer_start()
    timings = pd.concat(timings_list, ignore_index=True)
    timer_end(start, "concat")
else:
    timings = pd.DataFrame()

if len(old_timings_csv) != 0:
    start = timer_start()
    timings = pd.concat([old_timings_csv, timings], ignore_index=True)
    timer_end(start, "concat 2")
# start = timer_start()
# for col in column_dtypes:
#     timings.astype(column_dtypes[col])
# timer_end(start, "cast")

# if isinstance(timings["clock_stop"].iloc[0], str):
timings["clock_stop"] = pd.to_datetime(timings["clock_stop"])
timings["clock_start"] = pd.to_datetime(timings["clock_start"])


timings["exec_duration_sec"] = (timings["clock_stop"] - timings["clock_start"]).dt.total_seconds()

print("Collating the CSV file with all the timings: " + collatedCsvFilename)
timings.to_csv(path_or_buf=collatedCsvFilename, index=False)

print("Stats: ")

start = timer_start()

t = timings[timings["success_status"] == 1]
min_medians = t.groupby(by=["layer", "tunepoint", "job_idx"]).total_runtime.agg("median").groupby(by=["layer"]).idxmin()

best_lift_measurements = pd.DataFrame({ col : pd.Series([], dtype=dt) for (col, dt) in t.dtypes.to_dict().items()})
for row in min_medians:
    all_trials_of_best_point = t[(t.layer == row[0]) & (t.tunepoint == row[1]) & (t.job_idx == row[2])]
    # If there is an even number of trials, the median would return an average of the two middle runtimes.
    # Here we ensure that the higher of those two middle runtimes is chosen.
    if len(all_trials_of_best_point) % 2 == 0:
        all_trials_of_best_point = all_trials_of_best_point[all_trials_of_best_point.total_runtime != all_trials_of_best_point.total_runtime.min()]
    median = all_trials_of_best_point.total_runtime.median()
    best_lift_measurements = best_lift_measurements.append(all_trials_of_best_point[all_trials_of_best_point.total_runtime == median].iloc[0], ignore_index=True)

timer_end(start, "best_lift_measurements")

if ("reached_runtime_limit" in timings.columns):
    # best_lift_measurements["lr_fails"] = best_lift_measurements.apply(\
    #     lambda best_row_per_layer : \
    #         len(timings[
    #             (timings["layer"] == best_row_per_layer["layer"]) & 
    #             ((timings["success_status"] == 0) & (pd.isna(timings["reached_runtime_limit"])))]), 
    #             axis = "columns")
    best_lift_measurements["lr_fails"] = best_lift_measurements.apply(\
        lambda best_row_per_layer : \
            len(timings[
                ((~timings["layer"].isna()) & (timings["layer"] == best_row_per_layer["layer"])) &
                    (~(timings["fail_reported"].isna()) & (timings["fail_reported"] == 1))]), 
                axis = "columns")
    best_lift_measurements["lr_fail_nans"] = best_lift_measurements.apply(\
        lambda best_row_per_layer : \
            len(timings[
                ((~timings["layer"].isna()) & (timings["layer"] == best_row_per_layer["layer"])) &
                    (timings["fail_reported"].isna())]), 
                axis = "columns") 
    best_lift_measurements["lr_slow_krnl_kills"] = best_lift_measurements.apply(lambda best_row_per_layer : \
        len(timings[
            (timings["layer"] == best_row_per_layer["layer"]) & 
            pd.notna(timings["reached_runtime_limit"])].
                drop_duplicates(subset=["tunepoint", "job_idx"]).index), axis = "columns")
            
    # print(timings[
    #         (timings["layer"] == 8) & 
    #         pd.notna(timings["reached_runtime_limit"])][["layer", "tunepoint", "total_runtime", "success_status", "reached_runtime_limit"]])
    # best_lift_measurements["lr_slow_k_kills_div3"] = best_lift_measurements["lr_slow_krnl_kills"] / 3
    # best_lift_measurements.apply(lambda best_row_per_layer : \
    #     len(timings[
    #         (timings["layer"] == best_row_per_layer["layer"]) & 
    #         pd.notna(timings["reached_runtime_limit"])]) / 3, axis = "columns")

    # print(timings[(timings["success_status"] == 0) & (pd.isna(timings["reached_runtime_limit"]))].drop(columns=["csvpath"]))
    # print()
    # print(len(timings[(timings["success_status"] == 0) & (pd.isna(timings["reached_runtime_limit"]))].drop(columns=["csvpath"])))
    # quit()
else:
    best_lift_measurements["lr_fails"] = best_lift_measurements.apply(\
        lambda best_row_per_layer : \
            len(timings[
                (timings["layer"] == best_row_per_layer["layer"]) & (timings["success_status"] == 0)]), 
                axis = "columns")

    best_lift_measurements["lr_slow_krnl_kills"] = best_lift_measurements.apply(lambda best_row_per_layer : \
        len(timings[
            (timings["layer"] == best_row_per_layer["layer"])]), axis = "columns")

best_lift_measurements["layer_total_runs"] = best_lift_measurements.apply(lambda best_row_per_layer : \
    len(timings[timings["layer"] == best_row_per_layer["layer"]]), axis = "columns")

best_lift_measurements["lr_unique_points"] = best_lift_measurements.apply(lambda best_row_per_layer : \
    (timings[timings["layer"] == best_row_per_layer["layer"]].drop_duplicates(subset=["tunepoint", "job_idx"])).shape[0], axis = "columns")

# print(timings[timings["layer"] == 0]["tunepoint"].append(
#         timings[timings["layer"] == 0]["csvpath"]
#     ))
# quit()

# best_lift_measurements["extrRuns3trls"] = best_lift_measurements.apply(lambda best_row_per_layer : \
#     ((len(timings[timings["layer"] == best_row_per_layer["layer"]]) / 3) - 
#         timings[timings["layer"] == best_row_per_layer["layer"]]["tunepoint"].nunique() ) * 3, axis = "columns")

# best_lift_measurements["lr_fails"] = best_lift_measurements["layer_total_tunepoints"] - best_lift_measurements["layer_successes"]

best_lift_measurements["tvmApril20"] = best_lift_measurements.apply(lambda best_row_per_layer : \
    11.49 if best_row_per_layer["layer"] == 0 else
    61.59 if best_row_per_layer["layer"] == 1 else
    24.10 if best_row_per_layer["layer"] == 2 else
    84.21 if best_row_per_layer["layer"] == 3 else
    22.55 if best_row_per_layer["layer"] == 4 else
    76.55 if best_row_per_layer["layer"] in [5,6] else
    56.89 if best_row_per_layer["layer"] == 7 else
    40.43 if best_row_per_layer["layer"] in [8,9] else
    50.65 if best_row_per_layer["layer"] in [10,11,12] else -1, axis = "columns")
        
pd.set_option('display.max_columns', 500)
# pd.set_option('display.width', 1000)
def f(fmt): return lambda x : fmt.format(x)

print(best_lift_measurements[[
    "layer", "tunepoint", "total_runtime", "tvmApril20", "lr_fails", "lr_fail_nans", "lr_slow_krnl_kills",
    "lr_unique_points", "layer_total_runs", "net_label", "job_idx"]].to_string(
        col_space=15, index=False, justify="right", 
        formatters=[f(fmt) for fmt in ['{:4d}', '{:4d}', '{:0.2f}', '{:0.2f}', '{:4d}', '{:4d}', '{:0.1f}', '{:5d}', '{:5d}', '{}', '{:4d}']]))

print("MEDIAN (Lift / tvm) across layer configs:")
print("%.2f" % ((best_lift_measurements["total_runtime"] / best_lift_measurements["tvmApril20"]).median()))

# print(timings[(timings["layer"] == 1) & (timings["success_status"] == 0)].iloc[0])
# print(timings[(timings["layer"] == 1) & (timings["success_status"] == 0)].iloc[0]["csvpath"])
# print(timings[(timings["layer"] == 1) & (timings["success_status"] == 0) & timings[""]])