import os
import csv
import numpy as np
import re
import argparse
from enum import Enum
import subprocess
from os.path import join, isfile, isdir
from collections import deque
import itertools
from timeit import default_timer
import ntpath
import sys
import traceback
import pandas as pd
from typing import Tuple
import hashlib
import multiprocessing
from datetime import datetime, date, time

parser = argparse.ArgumentParser()
parser.add_argument('--netname', action="store", dest="netname", required=True)
parser.add_argument('--netlabel', action="store", dest="netlabel", required=True)
parser.add_argument('--n_threads', action="store", dest="n_threads", required=True, type=int)
parser.add_argument('--n_par_mappings_to_extract', action="store", dest="n_par_mappings_to_extract", type=int)
parser.add_argument('--verbose', action="store_true", dest="verbose")

parser_logs1 = parser.add_argument_group()
parser_logs1.add_argument('--parameter_logs_searchspace_1', action="append", \
    dest="parameter_logs_searchspace_1", required=True)
parser_logs1.add_argument('--format1', action="store", dest="format1", type=int, required=True)

parser_logs2 = parser.add_argument_group()
parser_logs2.add_argument('--parameter_logs_searchspace_2', action="append", \
    dest="parameter_logs_searchspace_2", required=False)
parser_logs2.add_argument('--format2', action="store", dest="format2", type=int, required=False)

# parser.add_argument('--parameter_csv_out', action="store", dest="parameter_csv_out", required=True)
parser.add_argument('--timingsdir', action="store", dest="timingsdir", required=True)
parser.add_argument('--parametersdir', action="store", dest="parametersdir", required=True)

parser.add_argument('--parmappings_with_search_times_csv', action="store", dest="parmappings_with_search_times_csv", required=False)

args = parser.parse_args()

assert args.format1 == 14 or args.parmappings_with_search_times_csv is None

known_formats = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

def timer_start():
    pass
    # return default_timer()

def timer_end(start, msg):
    pass
    # end = default_timer()
    # print("%s: %.7f sec" % (msg, end - start))

def get_time_diff_sec(start_time: datetime, finish_time: datetime):
    if finish_time.hour < start_time.hour:
        # Handle midnight
        print("WARNING: handling finish_time.hour < start_time.hour with the assumption that the two dates fall on two neighbouring dates: %s and %s" % (start_time, finish_time))
        started_at = datetime.combine(date(2022, month=1, day=1), time = start_time.time())
        finished_at = datetime.combine(date(2022, month=1, day=2), time = finish_time.time())
        diff = (finished_at - started_at).total_seconds()
    else:
        diff = (finish_time - start_time).total_seconds()
    return diff


if not int(args.format1) in known_formats:
    print("Unknown log format = " + str(args.format1) + "; known formats = " + ", ".join(str(f) for f in known_formats))
    quit()
if args.format2 is not None and not int(args.format2) in known_formats:
    print("Unknown log format = " + str(args.format2) + "; known formats = " + ", ".join(str(f) for f in known_formats))
    quit()

rewrite_encoding = {
    "Some(Map(f) => MapSeq(f))": "00",
    "Some(AbstractMap1(AbstractMap2(f)) => Split(I) o AbstractMap1(f) o Join())": "01",
    "Some(Map(f) => MapLcl(0)(f))": "20", # NB: not the same encoding as in Lift!
    "Some(Map(f) => MapLcl(1)(f))": "21",
    "Some(Map(f) => MapLcl(2)(f))": "22",
    "Some(Map(f) => MapWrg(0)(f))": "30",
    "Some(Map(f) => MapWrg(1)(f))": "31",
    "Some(Map(f) => MapWrg(2)(f))": "32",
    "Some(Map(f) => MapGlb(0)(f))": "40",
    "Some(Map(f) => MapGlb(1)(f))": "41",
    "Some(Map(f) => MapGlb(2)(f))": "42",
    "None": "00",
    "Some(MapSeq(uf) $ (arg: contiguous data) => MapSeqVector(uf.vectorize, uf, vectorLen) $ arg)": "01",
    "Some(Map(Map(f)) $ <slided 2d array> => Slide() o Map(f) o Unslide() $ <slided 2d array>)": "01",

    "(0)": "00",
    "(Code.replaceInnerMapWithOuter())": "01",
    "(10*Code.mapWrg() + Code.parDims(0)())": "30", # NB: not the same encoding as in Lift!
    "(10*Code.mapWrg() + Code.parDims(1)())": "31",
    "(10*Code.mapWrg() + Code.parDims(2)())": "32",
    "(10*Code.mapLcl() + Code.parDims(0)())": "20",
    "(10*Code.mapLcl() + Code.parDims(1)())": "21",
    "(10*Code.mapLcl() + Code.parDims(2)())": "22",
    "(10*Code.mapGlb() + Code.parDims(0)())": "40",
    "(10*Code.mapGlb() + Code.parDims(1)())": "41",
    "(10*Code.mapGlb() + Code.parDims(2)())": "42"
}

class Tag(Enum):
    INIT = 1
    RUN_CONFIG = 2
    EXPLORATION_TITLE = 3
    COMPILING = 4
    COMPILATION_SUCCESS = 5
    COMPILATION_FAIL = 6
    LAYER_CONFIG = 7
    DEPRECATED_STATE_3 = 8
    REWRITE_PASS_PARAMS = 9
    TUNE_PARAMS = 10
    DEPRECATED_STATE_0 = 11
    REWRITE_PASS_INIT = 12
    PICKING_PARFUSE_MAPPING_FROM_CSV = 13
    REWRITE_PASS_PARAMS_CANDIDATE = 14
    TUNE_PARAMS_CANDIDATE = 15
    DEPRECATED_STATE_1 = 16
    DEPRECATED_STATE_2 = 17

n_par_mappings_to_extract = args.n_par_mappings_to_extract if args.n_par_mappings_to_extract is not None else 0
csvParFile = None
par_writer = None
current_par_header = None
out_par_filename = None
par_header_initialised = False
current_par_mapping = None
previous_par_mappings = None

class State:
    writer = None
    current_header = None

    inputCsvParMaps_with_search_times = None
    current_par_search_start_time = None
    current_par_hash = None
    current_compile_start_time = None

    def __init__(self, 
        out_filename, 
        generator_idx, 
        search_space_idx, 
        tag = Tag.INIT, 
        value_dict = {}, 
        header_initialised = False, 
        run_config_recorded_in_cur_csv = False):

        self.out_filename = out_filename
        self.generator_idx = generator_idx
        self.search_space_idx = search_space_idx
        self.header_initialised = header_initialised
        self.tag = tag
        self.value_dict = value_dict
        self.value_dict["mapUnslide"] = ""
        self.value_dict["mapTransform"] = ""
        self.value_dict["mapVectorize"] = ""
        if "mapTransforms" in self.value_dict:
            self.value_dict["mapTransforms"] = ""
        self.value_dict["parMappingCsvUsedInGen"] = ""

        self.run_config_recorded_in_cur_csv = run_config_recorded_in_cur_csv

    def getKeys(self):
        unsorted_keys = list(self.value_dict.keys())

        idxs = ["layer_idx", "tune_point_idx"]
        for idx in idxs:
            assert idx in unsorted_keys
            
        keys = idxs + [key for key in sorted([k for k in unsorted_keys if k not in idxs])]
        return keys

    def getValues(self):
        vals = [self.value_dict[key] for key in self.getKeys()]
        return vals


# [] = terminal state, no write to file
# [.., None, ..] = optionally terminal state, write to file
# [None] = terminal state, write to file
    allowedNextStates_format_6 = {
        Tag.INIT: 								[Tag.EXPLORATION_TITLE],
        Tag.EXPLORATION_TITLE: 					[Tag.COMPILATION_SUCCESS, Tag.DEPRECATED_STATE_1],
        Tag.DEPRECATED_STATE_1:		[Tag.DEPRECATED_STATE_1, Tag.REWRITE_PASS_PARAMS_CANDIDATE],
        Tag.REWRITE_PASS_PARAMS_CANDIDATE:		[Tag.REWRITE_PASS_PARAMS_CANDIDATE, Tag.DEPRECATED_STATE_1, Tag.TUNE_PARAMS_CANDIDATE],
        Tag.TUNE_PARAMS_CANDIDATE:				[Tag.DEPRECATED_STATE_1, Tag.REWRITE_PASS_PARAMS_CANDIDATE, Tag.COMPILATION_SUCCESS],
        Tag.COMPILATION_SUCCESS:				[Tag.LAYER_CONFIG],
        Tag.LAYER_CONFIG: 	    				[Tag.DEPRECATED_STATE_2],
        Tag.DEPRECATED_STATE_2: 				[Tag.REWRITE_PASS_PARAMS],
        Tag.REWRITE_PASS_PARAMS: 				[Tag.TUNE_PARAMS],
        Tag.TUNE_PARAMS: 						[None]
    },
    allowedNextStates_format_7 = {
        Tag.INIT: 								[Tag.EXPLORATION_TITLE],
        Tag.EXPLORATION_TITLE: 					[Tag.COMPILATION_SUCCESS, Tag.DEPRECATED_STATE_1],
        Tag.DEPRECATED_STATE_1:		[Tag.DEPRECATED_STATE_1, Tag.TUNE_PARAMS_CANDIDATE, Tag.COMPILATION_SUCCESS],
        # Tag.REWRITE_PASS_PARAMS_CANDIDATE:		[Tag.REWRITE_PASS_PARAMS_CANDIDATE, Tag.DEPRECATED_STATE_1, Tag.TUNE_PARAMS_CANDIDATE],
        Tag.TUNE_PARAMS_CANDIDATE:				[Tag.DEPRECATED_STATE_1, Tag.COMPILATION_SUCCESS],
        Tag.COMPILATION_SUCCESS:				[Tag.LAYER_CONFIG],
        Tag.LAYER_CONFIG: 	    				[Tag.DEPRECATED_STATE_2],
        Tag.DEPRECATED_STATE_2: 				[Tag.REWRITE_PASS_PARAMS],
        Tag.REWRITE_PASS_PARAMS: 				[Tag.REWRITE_PASS_PARAMS, Tag.TUNE_PARAMS],
        Tag.TUNE_PARAMS: 						[None]
    },
    allowedNextStates_format_8 = {
        Tag.INIT: 								[Tag.EXPLORATION_TITLE],
        Tag.EXPLORATION_TITLE: 					[Tag.COMPILATION_SUCCESS, Tag.DEPRECATED_STATE_1],
        Tag.DEPRECATED_STATE_1:		[Tag.DEPRECATED_STATE_1, Tag.TUNE_PARAMS_CANDIDATE, Tag.COMPILATION_SUCCESS],
        # Tag.REWRITE_PASS_PARAMS_CANDIDATE:		[Tag.REWRITE_PASS_PARAMS_CANDIDATE, Tag.DEPRECATED_STATE_1, Tag.TUNE_PARAMS_CANDIDATE],
        Tag.TUNE_PARAMS_CANDIDATE:				[Tag.DEPRECATED_STATE_1, Tag.COMPILATION_SUCCESS],
        Tag.COMPILATION_SUCCESS:				[Tag.LAYER_CONFIG],
        Tag.LAYER_CONFIG: 	    				[Tag.DEPRECATED_STATE_2],
        Tag.DEPRECATED_STATE_2: 				[Tag.REWRITE_PASS_PARAMS],
        Tag.REWRITE_PASS_PARAMS: 				[Tag.REWRITE_PASS_PARAMS, Tag.TUNE_PARAMS],
        Tag.TUNE_PARAMS: 						[None]
    }

    allowedNextStates_format_910 = {
        Tag.INIT: 								[Tag.EXPLORATION_TITLE],
        Tag.EXPLORATION_TITLE: 					[Tag.COMPILATION_SUCCESS, Tag.DEPRECATED_STATE_0],
        Tag.DEPRECATED_STATE_0:		[Tag.DEPRECATED_STATE_1],
        Tag.DEPRECATED_STATE_1:		[Tag.DEPRECATED_STATE_0, Tag.TUNE_PARAMS_CANDIDATE],
        # Tag.REWRITE_PASS_PARAMS_CANDIDATE:		[Tag.DEPRECATED_STATE_3, Tag.REWRITE_PASS_PARAMS_CANDIDATE, Tag.TUNE_PARAMS_CANDIDATE],
        Tag.TUNE_PARAMS_CANDIDATE:				[Tag.DEPRECATED_STATE_0, Tag.COMPILATION_SUCCESS],
        Tag.COMPILATION_SUCCESS:				[Tag.LAYER_CONFIG],
        Tag.LAYER_CONFIG: 	    				[Tag.DEPRECATED_STATE_3],
        Tag.DEPRECATED_STATE_3:					[Tag.DEPRECATED_STATE_2],
        Tag.DEPRECATED_STATE_2: 				[Tag.REWRITE_PASS_PARAMS],
        Tag.REWRITE_PASS_PARAMS: 				[Tag.REWRITE_PASS_PARAMS, Tag.TUNE_PARAMS],
        Tag.TUNE_PARAMS: 						[None]
    }

    allowedNextStates_format_1112 = {
        Tag.INIT: 								[Tag.EXPLORATION_TITLE],
        Tag.EXPLORATION_TITLE: 					[Tag.COMPILATION_SUCCESS, Tag.DEPRECATED_STATE_0],
        Tag.DEPRECATED_STATE_0:		[Tag.DEPRECATED_STATE_1],
        Tag.DEPRECATED_STATE_1:		[Tag.DEPRECATED_STATE_1, Tag.DEPRECATED_STATE_0, Tag.TUNE_PARAMS_CANDIDATE],
        # Tag.REWRITE_PASS_PARAMS_CANDIDATE:		[Tag.DEPRECATED_STATE_3, Tag.REWRITE_PASS_PARAMS_CANDIDATE, Tag.TUNE_PARAMS_CANDIDATE],
        Tag.TUNE_PARAMS_CANDIDATE:				[Tag.DEPRECATED_STATE_0, Tag.TUNE_PARAMS_CANDIDATE, Tag.COMPILATION_SUCCESS],
        Tag.COMPILATION_SUCCESS:				[Tag.LAYER_CONFIG],
        Tag.LAYER_CONFIG: 	    				[Tag.DEPRECATED_STATE_3],
        Tag.DEPRECATED_STATE_3:					[Tag.DEPRECATED_STATE_2],
        Tag.DEPRECATED_STATE_2: 				[Tag.REWRITE_PASS_PARAMS],
        Tag.REWRITE_PASS_PARAMS: 				[Tag.REWRITE_PASS_PARAMS, Tag.TUNE_PARAMS],
        Tag.TUNE_PARAMS: 						[None]
    }

    allowedNextStates_format_13 = {
        Tag.INIT:							    [Tag.RUN_CONFIG, Tag.EXPLORATION_TITLE],
        Tag.RUN_CONFIG:							[Tag.EXPLORATION_TITLE],
        Tag.EXPLORATION_TITLE: 					[Tag.COMPILATION_SUCCESS, Tag.DEPRECATED_STATE_0],
        Tag.DEPRECATED_STATE_0:		[Tag.DEPRECATED_STATE_1],
        Tag.DEPRECATED_STATE_1:		[Tag.DEPRECATED_STATE_1, Tag.DEPRECATED_STATE_0, Tag.TUNE_PARAMS_CANDIDATE],
        # Tag.REWRITE_PASS_PARAMS_CANDIDATE:		[Tag.DEPRECATED_STATE_3, Tag.REWRITE_PASS_PARAMS_CANDIDATE, Tag.TUNE_PARAMS_CANDIDATE],
        Tag.TUNE_PARAMS_CANDIDATE:				[Tag.DEPRECATED_STATE_0, Tag.TUNE_PARAMS_CANDIDATE, Tag.COMPILATION_SUCCESS],
        Tag.COMPILATION_SUCCESS:				[Tag.LAYER_CONFIG],
        Tag.LAYER_CONFIG: 	    				[Tag.DEPRECATED_STATE_3],
        Tag.DEPRECATED_STATE_3:					[Tag.DEPRECATED_STATE_2],
        Tag.DEPRECATED_STATE_2: 				[Tag.REWRITE_PASS_PARAMS],
        Tag.REWRITE_PASS_PARAMS: 				[Tag.REWRITE_PASS_PARAMS, Tag.TUNE_PARAMS],
        Tag.TUNE_PARAMS: 						[None]
    }

    allowedNextStates_format_14 = {
        Tag.INIT:							    [Tag.RUN_CONFIG, Tag.EXPLORATION_TITLE],
        Tag.RUN_CONFIG:							[Tag.EXPLORATION_TITLE],
        Tag.EXPLORATION_TITLE: 					[Tag.COMPILING, Tag.DEPRECATED_STATE_0],
        Tag.DEPRECATED_STATE_0:		[Tag.DEPRECATED_STATE_1],
        Tag.DEPRECATED_STATE_1:		[Tag.DEPRECATED_STATE_1, Tag.DEPRECATED_STATE_0, Tag.REWRITE_PASS_INIT],
        Tag.REWRITE_PASS_INIT:					[Tag.REWRITE_PASS_PARAMS_CANDIDATE, Tag.PICKING_PARFUSE_MAPPING_FROM_CSV],
        Tag.PICKING_PARFUSE_MAPPING_FROM_CSV:   [Tag.REWRITE_PASS_PARAMS_CANDIDATE],
        Tag.REWRITE_PASS_PARAMS_CANDIDATE:		[Tag.DEPRECATED_STATE_3, Tag.DEPRECATED_STATE_1, Tag.REWRITE_PASS_INIT, 
                                                    Tag.REWRITE_PASS_PARAMS_CANDIDATE, Tag.PICKING_PARFUSE_MAPPING_FROM_CSV, Tag.TUNE_PARAMS_CANDIDATE, Tag.COMPILING],
        Tag.TUNE_PARAMS_CANDIDATE:				[Tag.DEPRECATED_STATE_0, Tag.TUNE_PARAMS_CANDIDATE, Tag.REWRITE_PASS_INIT, Tag.COMPILING],
        Tag.COMPILING:          				[Tag.COMPILATION_SUCCESS, Tag.COMPILATION_FAIL],
        Tag.COMPILATION_SUCCESS:				[Tag.LAYER_CONFIG],
        Tag.COMPILATION_FAIL:   				[Tag.LAYER_CONFIG, Tag.PICKING_PARFUSE_MAPPING_FROM_CSV, Tag.REWRITE_PASS_PARAMS_CANDIDATE],
        Tag.LAYER_CONFIG: 	    				[Tag.DEPRECATED_STATE_3],
        Tag.DEPRECATED_STATE_3:					[Tag.DEPRECATED_STATE_2],
        Tag.DEPRECATED_STATE_2: 				[Tag.REWRITE_PASS_PARAMS],
        Tag.REWRITE_PASS_PARAMS: 				[Tag.REWRITE_PASS_PARAMS, Tag.TUNE_PARAMS],
        Tag.TUNE_PARAMS: 						[None]
    }

    allowedNextStates_format_15 = {
        Tag.INIT:							    [Tag.RUN_CONFIG, Tag.EXPLORATION_TITLE],
        Tag.RUN_CONFIG:							[Tag.EXPLORATION_TITLE],
        Tag.EXPLORATION_TITLE: 					[Tag.COMPILING, Tag.REWRITE_PASS_INIT],
        Tag.REWRITE_PASS_INIT:					[Tag.REWRITE_PASS_PARAMS_CANDIDATE, Tag.PICKING_PARFUSE_MAPPING_FROM_CSV],
        Tag.PICKING_PARFUSE_MAPPING_FROM_CSV:   [Tag.REWRITE_PASS_PARAMS_CANDIDATE],
        Tag.REWRITE_PASS_PARAMS_CANDIDATE:		[Tag.REWRITE_PASS_INIT, Tag.REWRITE_PASS_PARAMS_CANDIDATE, Tag.PICKING_PARFUSE_MAPPING_FROM_CSV, Tag.TUNE_PARAMS_CANDIDATE, Tag.COMPILING],
        Tag.TUNE_PARAMS_CANDIDATE:				[Tag.TUNE_PARAMS_CANDIDATE, Tag.REWRITE_PASS_INIT, Tag.COMPILING],
        Tag.COMPILING:          				[Tag.COMPILATION_SUCCESS, Tag.COMPILATION_FAIL],
        Tag.COMPILATION_SUCCESS:				[Tag.LAYER_CONFIG],
        Tag.COMPILATION_FAIL:   				[Tag.LAYER_CONFIG, Tag.PICKING_PARFUSE_MAPPING_FROM_CSV, Tag.REWRITE_PASS_PARAMS_CANDIDATE],
        Tag.LAYER_CONFIG: 	    				[Tag.REWRITE_PASS_PARAMS],
        Tag.REWRITE_PASS_PARAMS: 				[Tag.REWRITE_PASS_PARAMS, Tag.TUNE_PARAMS],
        Tag.TUNE_PARAMS: 						[None]
    }


    
    allowedNextStates = { 
        6: allowedNextStates_format_6,
        7: allowedNextStates_format_7,
        8: allowedNextStates_format_8,
        9: allowedNextStates_format_910,
        10: allowedNextStates_format_910,
        11: allowedNextStates_format_1112,
        12: allowedNextStates_format_1112,
        13: allowedNextStates_format_13,
        14: allowedNextStates_format_14,
        15: allowedNextStates_format_15}

    # A nested one-element list at the end of the list indicates variable zero or more lines in the pattern
    max_lines_in_var_len_pattern = 100

    format6_regexes = {
        Tag.INIT: None,
        Tag.EXPLORATION_TITLE:
            [r".*Exploring layer (?P<layer_idx>\d+) batch \d+ tuning point (?P<tune_point_idx>\d+)(\r)*\n"],
        Tag.COMPILATION_SUCCESS:
            [r".*Compilation successful(\r)*\n"],
        Tag.LAYER_CONFIG: 	
            [r".*Layer config:(\r)*\n",
               r"inputChannels -> (?P<inputChannels>\d+), inputWidth -> (?P<inputWidth>\d+), inputHeight -> (?P<inputHeight>\d+),(\r)*\n",
             r"padFuncX -> (?P<padFuncX>\d+), padFuncY -> (?P<padFuncY>\d+),(\r)*\n",
             r"kernelWidth -> (?P<kernelWidth>\d+), kernelHeight -> (?P<kernelHeight>\d+),(\r)*\n",
             r"kernelStrideX -> (?P<kernelStrideX>\d+), kernelStrideY -> (?P<kernelStrideY>\d+),(\r)*\n",
             r"numKernels -> (?P<numKernels>\d+)(\r)*\n"],
        Tag.REWRITE_PASS_PARAMS: [
            r".*Rewrite pass params:(\r)*\n", 
            [r"mapTransform\.(?P<mapParam>\d+\.p\d+) -> (?P<appliedRule>[^\r\n]+?)(\r)*\n"] 
            ],
        Tag.TUNE_PARAMS: [
            r".*Tune params:(\r)*\n",
            r"tileWidth -> (?P<tileWidth>\d+), tileHeight -> (?P<tileHeight>\d+), tileDepth -> (?P<tileDepth>\d+),(\r)*\n",
            r"kernelGroupSize -> (?P<kernelGroupSize>\d+),(\r)*\n",
            r"inputCacheSizeX -> (?P<inputCacheSizeX>\d+), inputCacheSizeY -> (?P<inputCacheSizeY>\d+),(\r)*\n",
            r"kernelCacheSize -> (?P<kernelCacheSize>\d+)(\r)*\n",
            r"localSize0 -> (?P<l0>\d+)(\r)*\n",
            r"localSize1 -> (?P<l1>\d+)(\r)*\n",
            r"localSize2 -> (?P<l2>\d+)(\r)*\n",
            r"nWrgs0 -> (?P<w0>\d+)(\r)*\n",
            r"nWrgs1 -> (?P<w1>\d+)(\r)*\n",
            r"nWrgs2 -> (?P<w2>\d+)"]
    }
    format6_regexes[Tag.REWRITE_PASS_PARAMS_CANDIDATE] = format6_regexes[Tag.REWRITE_PASS_PARAMS]
    format6_regexes[Tag.TUNE_PARAMS_CANDIDATE] = format6_regexes[Tag.TUNE_PARAMS]

    format7_regexes = {
        Tag.INIT: None,
        Tag.REWRITE_PASS_PARAMS: [
            r".*: (Unsliding|Parallelization and fusion|Vectorization) rewrite pass params:(\r)*\n", 
            [r"map(?P<transformName>[^\.]+)\.(?P<mapId>\d+)\.p(?P<mapParam>\d+) -> (?P<appliedRule>[^\r\n]+?)(\r)*\n"] 
            ],
        Tag.TUNE_PARAMS: [
            r".*Tune params:(\r)*\n",
            r"tileWidth -> (?P<tileWidth>\d+), tileHeight -> (?P<tileHeight>\d+), tileDepth -> (?P<tileDepth>\d+),(\r)*\n",
            r"kernelGroupSize -> (?P<kernelGroupSize>\d+),(\r)*\n",
            r"inputCacheSizeX -> (?P<inputCacheSizeX>\d+), inputCacheSizeY -> (?P<inputCacheSizeY>\d+),(\r)*\n",
            r"kernelCacheSize -> (?P<kernelCacheSize>\d+),(\r)*\n",
            r"localSizes\(0\) -> (?P<l0>\d+),(\r)*\n",
            r"localSizes\(1\) -> (?P<l1>\d+),(\r)*\n",
            r"localSizes\(2\) -> (?P<l2>\d+),(\r)*\n",
            r"nWrgs\(0\) -> (?P<w0>\d+),(\r)*\n",
            r"nWrgs\(1\) -> (?P<w1>\d+),(\r)*\n",
            r"nWrgs\(2\) -> (?P<w2>\d+)"
            ]
    }

    format7_regexes[Tag.EXPLORATION_TITLE] = format6_regexes[Tag.EXPLORATION_TITLE]
    format7_regexes[Tag.COMPILATION_SUCCESS] = format6_regexes[Tag.COMPILATION_SUCCESS]
    format7_regexes[Tag.LAYER_CONFIG] = format6_regexes[Tag.LAYER_CONFIG]
    # format7_regexes[Tag.REWRITE_PASS_PARAMS_CANDIDATE] = format7_regexes[Tag.REWRITE_PASS_PARAMS]
    format7_regexes[Tag.TUNE_PARAMS_CANDIDATE] = format7_regexes[Tag.TUNE_PARAMS]

    format8_regexes = {
        Tag.INIT: None,
        Tag.REWRITE_PASS_PARAMS: [
            r".*: (Unsliding|Parallelization and fusion|Vectorization) rewrite pass params:(\r)*\n", 
            [r"\"map(?P<transformName>[^\.]+)\.(?P<mapId>\d+)\.\" -> (?P<appliedRule>[^\r\n]+?)(,)*(\r)*\n"] 
            ],
        Tag.TUNE_PARAMS: [
            r".*Tune params:(\r)*\n",
            r"tileWidth -> (?P<tileWidth>\d+), tileHeight -> (?P<tileHeight>\d+), tileDepth -> (?P<tileDepth>\d+),(\r)*\n",
            r"seqWindowsPerThreadX -> (?P<seqWindowsPerThreadX>\d+), seqWindowsPerThreadY -> (?P<seqWindowsPerThreadY>\d+),(\r)*\n",
            r"seqKernelsPerThread -> (?P<seqKernelsPerThread>\d+),(\r)*\n",
            r"kernelGroupSize -> (?P<kernelGroupSize>\d+),(\r)*\n",
            r"windowTileWidth -> (?P<windowTileWidth>\d+), windowTileHeight -> (?P<windowTileHeight>\d+),(\r)*\n",
            r"inputCacheSizeX -> (?P<inputCacheSizeX>\d+), inputCacheSizeY -> (?P<inputCacheSizeY>\d+),(\r)*\n",
            r"inputCacheDepth -> (?P<inputCacheDepth>\d+),(\r)*\n",
            r"kernelCacheSize -> (?P<kernelCacheSize>\d+),(\r)*\n",
            r"localSizes\(0\) -> (?P<l0>\d+),(\r)*\n",
            r"localSizes\(1\) -> (?P<l1>\d+),(\r)*\n",
            r"localSizes\(2\) -> (?P<l2>\d+),(\r)*\n",
            r"nWrgs\(0\) -> (?P<w0>\d+),(\r)*\n",
            r"nWrgs\(1\) -> (?P<w1>\d+),(\r)*\n",
            r"nWrgs\(2\) -> (?P<w2>\d+)"
            ]
    }

    format8_regexes[Tag.EXPLORATION_TITLE] = format7_regexes[Tag.EXPLORATION_TITLE]
    format8_regexes[Tag.COMPILATION_SUCCESS] = format7_regexes[Tag.COMPILATION_SUCCESS]
    format8_regexes[Tag.LAYER_CONFIG] = format7_regexes[Tag.LAYER_CONFIG]
    format8_regexes[Tag.TUNE_PARAMS_CANDIDATE] = format8_regexes[Tag.TUNE_PARAMS]

    format9_regexes = {
        Tag.INIT: None,
        Tag.TUNE_PARAMS: [
            r".*Tune params:(\r)*\n",
            r"padOptRight -> (?P<padOptRight>\d+), padOptBottom -> (?P<padOptBottom>\d+),(\r)*\n",
            r"tileWidth -> (?P<tileWidth>\d+), tileHeight -> (?P<tileHeight>\d+), tileDepth -> (?P<tileDepth>\d+),(\r)*\n",
            r"seqWindowsPerThreadX -> (?P<seqWindowsPerThreadX>\d+), seqWindowsPerThreadY -> (?P<seqWindowsPerThreadY>\d+),(\r)*\n",
            r"seqKernelsPerThread -> (?P<seqKernelsPerThread>\d+),(\r)*\n",
            r"kernelGroupSize -> (?P<kernelGroupSize>\d+),(\r)*\n",
            r"windowTileWidth -> (?P<windowTileWidth>\d+), windowTileHeight -> (?P<windowTileHeight>\d+),(\r)*\n",
            r"inputCacheSizeX -> (?P<inputCacheSizeX>\d+), inputCacheSizeY -> (?P<inputCacheSizeY>\d+),(\r)*\n",
            r"inputCacheDepth -> (?P<inputCacheDepth>\d+),(\r)*\n",
            r"kernelCacheSize -> (?P<kernelCacheSize>\d+),(\r)*\n",
            r"localSizes\(0\) -> (?P<l0>\d+),(\r)*\n",
            r"localSizes\(1\) -> (?P<l1>\d+),(\r)*\n",
            r"localSizes\(2\) -> (?P<l2>\d+),(\r)*\n",
            r"nWrgs\(0\) -> (?P<w0>\d+),(\r)*\n",
            r"nWrgs\(1\) -> (?P<w1>\d+),(\r)*\n",
            r"nWrgs\(2\) -> (?P<w2>\d+)"
            ]
    }

    format9_regexes[Tag.EXPLORATION_TITLE] = format8_regexes[Tag.EXPLORATION_TITLE]
    format9_regexes[Tag.COMPILATION_SUCCESS] = format8_regexes[Tag.COMPILATION_SUCCESS]
    format9_regexes[Tag.LAYER_CONFIG] = format8_regexes[Tag.LAYER_CONFIG]
    format9_regexes[Tag.REWRITE_PASS_PARAMS] = format8_regexes[Tag.REWRITE_PASS_PARAMS]

    format9_regexes[Tag.TUNE_PARAMS_CANDIDATE] = format9_regexes[Tag.TUNE_PARAMS]

    format10_regexes = format9_regexes

    format11_regexes = format10_regexes
    

    format12_regexes = format11_regexes

    format13_regexes = format12_regexes
    format13_regexes[Tag.RUN_CONFIG] = [
        r"^(parMappingsCsv|parPresetsCsv) = (?P<parMappingsCsv>None|Some\(.+?\))"]

    format1415_regexes = format13_regexes
    format1415_regexes[Tag.REWRITE_PASS_INIT] = [
            r"(?P<logTime>\d\d:\d\d:\d\d\.\d\d\d).*Initializing (?P<passName>.+) search space\.(\r)*\n"]
    format1415_regexes[Tag.PICKING_PARFUSE_MAPPING_FROM_CSV] = [
            r".*Picking parFuse mapping \#\d+ \(MD5: (?P<parMappingHash>.+)\) from the CSV set(\r)*\n"]
    format1415_regexes[Tag.REWRITE_PASS_PARAMS_CANDIDATE] = [
             r"(?P<logTime>\d\d:\d\d:\d\d\.\d\d\d) \[INFO\] \[benchmarks\.conv\.LayerSearch\$(?P<passClassName>.+)\]: \d+\. Rewrite pass params:(\r)*\n", 
            [r"map(?P<transformName>[^\.]+)\.(?P<mapId>\d+)\.p(?P<mapParam>\d+) -> (?P<appliedRule>[^\r\n]+?)(\r)*\n"] 
            ]
    format1415_regexes[Tag.COMPILING] = [
            r"(?P<logTime>\d\d:\d\d:\d\d\.\d\d\d).*Compiling the lambda\.\.\.(\r)*\n"]
    format1415_regexes[Tag.COMPILATION_SUCCESS] = [
            r"(?P<logTime>\d\d:\d\d:\d\d\.\d\d\d).*Compilation (?P<status>successful)(\r)*\n"]
    format1415_regexes[Tag.COMPILATION_FAIL] = [
            r"(?P<logTime>\d\d:\d\d:\d\d\.\d\d\d).*Compilation (?P<status>failed).*(\r)*\n"]

    regexes = {
        6: format6_regexes,
        7: format7_regexes,
        8: format8_regexes,
        9: format9_regexes,
        10: format10_regexes,
        11: format11_regexes,
        12: format12_regexes,
        13: format13_regexes,
        14: format1415_regexes,
        15: format1415_regexes
        }

    compiled_regexes = {}
    for formatNo in regexes.keys():
        compiled_regexes[formatNo] = {}
        for tag, tag_regexes in [r for r in regexes[formatNo].items() if r[1] is not None]:

            var_len_pattern = isinstance(tag_regexes[-1], list)
            if var_len_pattern:					
                assert len(tag_regexes[-1]) == 1

            compiled_regexes[formatNo][tag] = [None, None]
            if var_len_pattern:
                # compiled_regexes[formatNo][tag][0] = re.compile(''.join(tag_regexes[:-1]) + tag_regexes[-1][0])
                compiled_regexes[formatNo][tag][0] = re.compile(''.join(tag_regexes[:-1]))
                compiled_regexes[formatNo][tag][1] = re.compile(tag_regexes[-1][0])
            else:
                compiled_regexes[formatNo][tag][0] = re.compile(''.join(tag_regexes))



    def generic_handler(self, newTag, match):
        p_list = re.compile(r".*(List|Vector)\((?P<items>[^\)]+)\)\)")
        self.tag = newTag
        if args.verbose: print("<" + self.tag.name + ">")
        for k in match.groupdict():
            if match.groupdict()[k] is not None:
                # if "Vector" in match.groupdict()[k]:
                # 	m = p_list.match(match.groupdict()[k])
                # 	print(match.groupdict()[k])
                # 	print(m)
                # 	print(m.group("items"))
                # 	print(p_list.sub("\g<items>", match.groupdict()[k]))
                # 	quit()

                self.value_dict[k] = p_list.sub("\g<items>", match.groupdict()[k]).\
                    replace("Some", "").replace("None", "N").\
                    replace("global", "gl").replace("local", "lc").replace("private", "pr").\
                    replace("GlobalMemory", "gl").replace("LocalMemory", "lc").replace("PrivateMemory", "pr").\
                    replace("(", "").replace(")", "").\
                    replace(" ", "").replace(",", "")
                    # replace("List(0, 1)", "L01").replace("List(1, 0)", "L10").\
                    # replace("List(0, 2, 3, 1)", "L0231").replace("List(0, 1, 2, 3)", "L0123").\

        # self.value_dict = dict(self.value_dict, **match.groupdict())

    def dont_save_candidate(self, newTag, match):
        self.tag = newTag
        if args.verbose: print("<" + self.tag.name + ">")

    def tune_point_idx_handler(self, newTag, match):
        # After generic handler
        self.value_dict["tune_point_idx"] = str(self.generator_idx) + "_" + str(int(self.value_dict["tune_point_idx"]) + 
                                                1000 * self.search_space_idx)

    def rewrite_pass_init_handler(self, newTag, match):
        
        # Without generic handler
        self.tag = newTag
        if args.verbose: print("<" + self.tag.name + ">")

        if match.group("passName") == "parFuse" and args.parmappings_with_search_times_csv is None:
            if self.current_par_search_start_time is None:
                self.current_par_search_start_time = datetime.strptime(match.group("logTime"), '%H:%M:%S.%f')

    def picking_parfuse_from_csv_handler(self, newTag, match):
        
        # Without generic handler
        self.tag = newTag
        if args.verbose: print("<" + self.tag.name + ">")

        assert self.current_par_hash is None
        self.current_par_hash = match.group("parMappingHash")


    def rewrite_pass_params_handler(self, newTag, match):
        global current_par_mapping
        
        # Without generic handler
        self.tag = newTag
        if args.verbose: print("<" + self.tag.name + ">")
        # print("match.groupdict():")
        # print(match.groupdict().keys())
        if "transformName" in match.groupdict().keys():
            field_name = "map" + match.group("transformName")
            if not field_name in self.value_dict:
                self.value_dict[field_name] = ""
            # self.value_dict["mapTransforms"] += "|" + match.group("mapParam") + ":" + \
            # 	match.group("appliedRule").replace(" ", "").\
            # 		replace("Some", "S").replace("Map", "M").\
            # 		replace("Abstract", "A").\
            # 		replace("(","").replace(")","").replace("=>","")
            # assert match.group("appliedRule") in rewrite_encoding
            # self.value_dict["mapTransforms"] += "|" + rewrite_encoding[match.group("appliedRule")]
            self.value_dict[field_name] += \
                rewrite_encoding[match.group("appliedRule")] if match.group("appliedRule") in rewrite_encoding \
                    else match.group("appliedRule")

            # if n_par_mappings_to_extract > 0 and field_name == "mapTransform":
            if field_name == "mapTransform":
                par_mapping_id = (args.netname, args.netlabel, self.value_dict["layer_idx"], self.value_dict["tune_point_idx"])

                if current_par_mapping is None or current_par_mapping[0] != par_mapping_id:
                    current_par_mapping = [par_mapping_id, ""]
                
                current_par_mapping[1] += \
                    match.group("mapId") + ":" + (
                        rewrite_encoding[match.group("appliedRule")] if match.group("appliedRule") in rewrite_encoding \
                        else match.group("appliedRule")) + ";"

    def rewrite_pass_params_candidate_handler(self, newTag, match):
        
        # Without generic handler
        self.tag = newTag
        if args.verbose: print("<" + self.tag.name + ">")
        # print("match.groupdict():")
        # print(match.groupdict().keys())
        if "passClassName" in match.groupdict().keys() and match.group("passClassName") == "ParFuseParamValuesObtaining":

            if args.parmappings_with_search_times_csv is None:

                assert self.current_par_search_start_time is not None

                finish_time = datetime.strptime(match.group("logTime"), '%H:%M:%S.%f')

                search_duration_sec = get_time_diff_sec(self.current_par_search_start_time, finish_time)

            else:
                assert self.current_par_hash is not None
                try:
                    assert self.inputCsvParMaps_with_search_times.parMappingHash.str.contains(self.current_par_hash).any()
                except AssertionError:
                    print(self.inputCsvParMaps_with_search_times.parMappingHash)
                    print(self.current_par_hash)
                    raise AssertionError

                search_duration_sec = self.inputCsvParMaps_with_search_times[
                    self.inputCsvParMaps_with_search_times.parMappingHash == self.current_par_hash].iloc[0].search_duration_sec

                self.current_par_hash = None

            # print(search_duration_sec)
            self.value_dict["parFuse_search_duration_sec"] = search_duration_sec

    def compiling_handler(self, newTag, match):
        
        # Without generic handler
        self.tag = newTag
        if args.verbose: print("<" + self.tag.name + ">")

        assert self.current_compile_start_time is None
        self.current_compile_start_time = datetime.strptime(match.group("logTime"), '%H:%M:%S.%f')

    def compilation_finish_handler(self, newTag, match):
        
        # Without generic handler
        self.tag = newTag
        if args.verbose: print("<" + self.tag.name + ">")
        # print("match.groupdict():")
        # print(match.groupdict().keys())
        if match.group("status") == "successful":

            if args.parmappings_with_search_times_csv is None:
                self.current_par_search_start_time = None

            assert self.current_compile_start_time is not None
            compile_finish_time = datetime.strptime(match.group("logTime"), '%H:%M:%S.%f')
            
            self.value_dict["compile_duration_sec"] = get_time_diff_sec(self.current_compile_start_time, compile_finish_time)
            # self.value_dict["compile_status"] = 1
            
            self.current_compile_start_time = None
        else:
            assert match.group("status") == "failed" # or match.group("status") == "unsuccessful"
            assert self.current_compile_start_time is not None

            # self.value_dict["compile_status"] = 0

            self.current_compile_start_time = None

    def run_config_handler(self, newTag, match):
        assert not self.run_config_recorded_in_cur_csv
        
        self.tag = newTag
        if args.verbose: 
            print("<" + self.tag.name + ">")
            print(''.join(self.line_buf_slice(0, 1)))

        self.value_dict["parMappingCsvUsedInGen"] = match.group("parMappingsCsv")

    def writeParsedValuesToFile(self):
        if not self.header_initialised:
            self.csvFile = open(self.out_filename, 'w', newline='\n', encoding='utf8')
            self.writer = csv.writer(self.csvFile)

            self.writer.writerow(["net_name", "net_label"] + self.getKeys() + ["parMappingHash"])
            self.current_header = set(["net_name", "net_label"] + self.getKeys() + ["parMappingHash"])
            # self.writer.writerow(str(self.getKeys()))
            self.header_initialised = True

        new_header = ["net_name", "net_label"] + self.getKeys() + ["parMappingHash"]
        header_diff = self.current_header.symmetric_difference(set(new_header))
        if len(header_diff) > 0:
            self.csvFile.close()

            old_file_df = pd.read_csv(self.out_filename)
            for col in header_diff:
                old_file_df[col] = np.nan
            old_file_df.to_csv(path_or_buf=self.out_filename, columns=new_header, index=False, header=True)
            self.current_header = set(new_header)

            self.csvFile = open(self.out_filename, 'a', newline='\n', encoding='utf8')
            self.writer = csv.writer(self.csvFile)


        par_mapping_id = (args.netname, args.netlabel, self.value_dict["layer_idx"], self.value_dict["tune_point_idx"])

        if current_par_mapping is None or current_par_mapping[0] != par_mapping_id:
            raise AssertionError 

        parMappingHash = get_string_hash(current_par_mapping[1])
            
        self.writer.writerow([args.netname, args.netlabel] + self.getValues() + [parMappingHash])

        if self.value_dict["parMappingCsvUsedInGen"] != "":
            self.run_config_recorded_in_cur_csv = True

        if n_par_mappings_to_extract > 0:
            self.writeParsedParMappingsToFile(parMappingHash)


    def writeParsedParMappingsToFile(self, parMappingHash: str):
        global n_par_mappings_to_extract
        global par_header_initialised
        global csvParFile
        global out_par_filename
        global par_writer

        # Performed above
        # par_mapping_id = (args.netname, args.netlabel, self.value_dict["layer_idx"], self.value_dict["tune_point_idx"])
        # if current_par_mapping is None or current_par_mapping[0] != par_mapping_id:
        # 	return 
            
        if current_par_mapping[1] in previous_par_mappings.mapTransformWithMapIds:
            print("Duplicate mapping encountered; skipping")
            return

        if csvParFile is None:
            csvParFile = open(out_par_filename, 'a', newline='\n', encoding='utf8')
            par_writer = csv.writer(csvParFile)

        if not par_header_initialised:
            par_writer.writerow(list(previous_par_mappings.columns))
            par_header_initialised = True

        vals = [args.netname, args.netlabel, self.value_dict["layer_idx"], self.value_dict["tune_point_idx"], parMappingHash, current_par_mapping[1]]
        par_writer.writerow(vals)
        previous_par_mappings.append(vals)
        
        n_par_mappings_to_extract -= 1

        csvParFile.close()
        csvParFile = None
        par_writer = None

    format_generic_handlers = {
        Tag.EXPLORATION_TITLE: 		[generic_handler, tune_point_idx_handler],
        Tag.REWRITE_PASS_PARAMS: 	[rewrite_pass_params_handler],
        Tag.DEPRECATED_STATE_0:	[dont_save_candidate],
        Tag.DEPRECATED_STATE_1:	[dont_save_candidate],
        Tag.REWRITE_PASS_PARAMS_CANDIDATE:	[dont_save_candidate],
        Tag.TUNE_PARAMS_CANDIDATE:	[dont_save_candidate]
    }
    
    format13_handlers = format_generic_handlers
    format13_handlers[Tag.RUN_CONFIG] = [run_config_handler]

    format1415_handlers = format13_handlers
    format1415_handlers[Tag.REWRITE_PASS_INIT] = [rewrite_pass_init_handler]
    format1415_handlers[Tag.PICKING_PARFUSE_MAPPING_FROM_CSV] = [picking_parfuse_from_csv_handler]    
    format1415_handlers[Tag.REWRITE_PASS_PARAMS_CANDIDATE] = [rewrite_pass_params_candidate_handler]
    format1415_handlers[Tag.COMPILING] = [compiling_handler]
    format1415_handlers[Tag.COMPILATION_SUCCESS] = [compilation_finish_handler]
    format1415_handlers[Tag.COMPILATION_FAIL] = [compilation_finish_handler]

    handlers = {
        6: format_generic_handlers,
        7: format_generic_handlers,
        8: format_generic_handlers,
        9: format_generic_handlers,
        10: format_generic_handlers,
        11: format_generic_handlers,
        12: format_generic_handlers,
        13: format13_handlers,
        14: format1415_handlers,
        15: format1415_handlers}

    def next_state(self, newTag, match, formatNo):
        start = timer_start()
        if newTag in self.allowedNextStates[formatNo][self.tag]:

            if newTag in self.handlers[formatNo]:
                for handler in self.handlers[formatNo][newTag]:
                    handler(self, newTag, match)
            else:
                self.generic_handler(newTag, match)
            timer_end(start, "next_state true")
            return True
        else:
            timer_end(start, "next_state false")
            return False

    points_parsed = 0

    def reset_state(self, withWrite: bool, tag_to_set: Tag):
        if withWrite:
            self.points_parsed += 1
            if self.points_parsed % 100 == 0: 
                print("Parsed " + str(self.points_parsed) + " points...")
            self.writeParsedValuesToFile()
        self.__init__(
            tag = tag_to_set, 
            generator_idx = self.generator_idx,
            search_space_idx = self.search_space_idx,
            out_filename = self.out_filename, 
            header_initialised = self.header_initialised,
            run_config_recorded_in_cur_csv = self.run_config_recorded_in_cur_csv)

    lines_to_parse_buffer = deque()
    lines_read = 0

    def read_lines_into_buffer(self, fp, n):
        lines_in_buffer = min(n, len(self.lines_to_parse_buffer))
        line = True
        # Only read if buffer doesn't have enough lines
        while line and lines_in_buffer < n:
            line = fp.readline()
            if line: 
                self.lines_to_parse_buffer.append(line)
                self.lines_read += 1
                lines_in_buffer += 1
        return lines_in_buffer == n

    def line_buf_slice(self, idx_from, idx_until):
        return list(itertools.islice(self.lines_to_parse_buffer, idx_from, idx_until))

    def parse_lines(self, fp):
        formatNo = args.format1 if self.search_space_idx == 0 else args.format2

        eof_across_all_regexes = False
        n_lines_read = 0
        last_line_parsed = None
        while not eof_across_all_regexes:
            eof_across_all_regexes = True
            lines_parsed = False
            last_matched_tag = None

            allowed_next_tags_to_match = self.allowedNextStates[formatNo][self.tag]

            for newTag, tag_regexes in [r for r in self.regexes[formatNo].items() if r[1] is not None]:

                var_len_pattern = isinstance(tag_regexes[-1], list)

                lines_in_pattern = len(tag_regexes) 
                if var_len_pattern:
                    lines_in_pattern -= 1

                eof = not self.read_lines_into_buffer(fp, lines_in_pattern)

                eof_across_all_regexes = eof_across_all_regexes and eof

                if not eof:
                    # if var_len_pattern:
                    # 	match = re.compile(''.join(tag_regexes[:-1]) + tag_regexes[-1][0]).match(''.join(self.line_buf_slice(0, lines_in_pattern)))
                    # else:
                    # 	match = re.compile(''.join(tag_regexes)).match(''.join(self.line_buf_slice(0, lines_in_pattern)))
                    start = timer_start()
                    match = self.compiled_regexes[formatNo][newTag][0].match(''.join(self.line_buf_slice(0, lines_in_pattern)))
                    timer_end(start, "match")

                    if match:
                        last_matched_tag = newTag
                        if self.next_state(newTag, match, formatNo):
                            lines_parsed = True

                            if var_len_pattern:
                                # Get the variable number of lines that match the last part of the pattern

                                reached_end_of_pattern = False
                                extra_lines_read_for_pattern = 0
                                while (not eof and not reached_end_of_pattern and
                                    extra_lines_read_for_pattern < self.max_lines_in_var_len_pattern):

                                    # Clear the lines we already consumed first
                                    # for _ in range(lines_in_pattern):
                                    # 	self.lines_to_parse_buffer.popleft()

                                    # lines_in_pattern = 1
                                    toread = lines_in_pattern + extra_lines_read_for_pattern + 1
                                    eof = not self.read_lines_into_buffer(fp, toread)
                                    eof_across_all_regexes = eof_across_all_regexes and eof
                                    extra_lines_read_for_pattern += 1
                                    if not eof:
                                        substr = self.line_buf_slice(
                                                lines_in_pattern + extra_lines_read_for_pattern - 1,
                                                lines_in_pattern + extra_lines_read_for_pattern)
                                        # match = re.compile(tag_regexes[-1][0]).match(''.join(
                                        match = self.compiled_regexes[formatNo][newTag][1].match(''.join(
                                            substr))
                                        # if newTag == Tag.REWRITE_PASS_PARAMS:
                                        # 	print("match:")
                                        # 	print(match)
                                        # 	print(substr)
                                        if match:
                                            if extra_lines_read_for_pattern == self.max_lines_in_var_len_pattern:
                                                raise ValueError("Matched more than the limit of " + str(self.max_lines_in_var_len_pattern) + 
                                                    " lines per pattern for the variable-length pattern of the currentTag = " + state.tag.name + 
                                                    "; newTag = " + newTag.name + "; lines:\n" + "\n".join(self.line_buf_slice(0, lines_in_pattern)))

                                            # next_state() without checking if the newTag is allowed since we are still parsing the same pattern
                                            if newTag in self.handlers[formatNo]:
                                                for handler in self.handlers[formatNo][newTag]:
                                                    handler(self, newTag, match)
                                            else:
                                                self.generic_handler(newTag, match)
                                        else:
                                            reached_end_of_pattern = True

                                # Update lines_in_pattern so that the buffer can be cleared correctly
                                lines_in_pattern += extra_lines_read_for_pattern - 1


                            if self.allowedNextStates[formatNo][newTag] == [None]:
                                self.reset_state(withWrite = True, tag_to_set = Tag.RUN_CONFIG)
                            if self.allowedNextStates[formatNo][newTag] == []:
                                self.reset_state(withWrite = False, tag_to_set = Tag.RUN_CONFIG)

                        else:
                            if newTag in allowed_next_tags_to_match:
                                allowed_next_tags_to_match.remove(newTag)
                            
                            if allowed_next_tags_to_match:
                                # Matched on a wrong tag, but there might be other tags that are allowed that we need to check
                                if args.verbose:
                                    print("---- Matched on a wrong tag ----")
                                    print(self.tag)
                                    print(newTag)
                                    print(allowed_next_tags_to_match)
                                    print(self.lines_to_parse_buffer[0][:100])
                                pass
                            else:
                                # Matched on a wrong tag, and there are no other allowed next tags to be attempted
                                
                                if None in self.allowedNextStates[formatNo][self.tag]:
                                    self.reset_state(withWrite = True, tag_to_set = Tag.RUN_CONFIG)
                                    lines_parsed = self.next_state(newTag, match, formatNo)
                                
                                if not lines_parsed:
                                    raise ValueError("\nUnexpected tag, currentTag = " + self.tag.name + "; newTag = " + newTag.name +
                                        "\nLast line parsed correctly: " + str(last_line_parsed) + 
                                        "\nFirst line being parsed: " + str(n_lines_read) + 
                                        "\nLines being parsed:\n" + "\n".join(self.line_buf_slice(0, lines_in_pattern)) +
                                        "\nFile name:\n" + fp.name)

                    if lines_parsed:
                        # Clear the lines that were successfuly matched
                        for _ in range(lines_in_pattern):
                            self.lines_to_parse_buffer.popleft()
                            n_lines_read += 1
                        last_line_parsed = n_lines_read
                        break

            if last_matched_tag is not None and not lines_parsed:
                raise ValueError("\nUnexpected tag, currentTag = " + self.tag.name + "; newTag = " + last_matched_tag.name +
                    "\nLast line parsed correctly: " + str(last_line_parsed) + 
                    "\nFirst line being parsed: " + str(n_lines_read) + 
                    "\nLines being parsed:\n" + "\n".join(self.line_buf_slice(0, lines_in_pattern)) +
                    "\nFile name:\n" + fp.name)

            if not lines_parsed and len(self.lines_to_parse_buffer) != 0:
                # Clear one line since nothing was matched
                self.lines_to_parse_buffer.popleft()
                n_lines_read += 1
                

p_checksum = re.compile(r"(?P<checksum>.+?)\s.*")


## Equivalent to:
## printf "%b" "<input>" | md5sum -
def get_string_hash(input):
    return hashlib.md5(input.encode('utf-8')).hexdigest()


def get_file_contents_hash(filepath):
    # print("md5sum", filepath)
    print(".", end="")

    # start = timer_start()
    md5sum_output = subprocess.run(["md5sum", filepath], stdout=subprocess.PIPE)
    # timer_end(start, "run")
    # start = timer_start()
    match_checksum = p_checksum.match(md5sum_output.stdout.decode('utf-8'))
    # timer_end(start, "match")
    if not match_checksum:
        print("Could not get checksum of the following filepath using 'md5sum': " + filepath)
        quit()
    return match_checksum.group("checksum")


def is_log_parsed(loghash):
    if not os.path.exists(join(args.timingsdir, "checksums")):
        try:
            os.makedirs(join(args.timingsdir, "checksums"))
        except FileExistsError:
            print(join(args.timingsdir, "checksums") + " already exists! Not creating.")
    
    for logname_tag in os.listdir(join(args.timingsdir, "checksums")):
        if logname_tag.startswith("parameters_parsed_") and logname_tag.endswith("_" + loghash + ".checksum"):
            return True
    return False


def mark_log_parsed(parameter_log, parameter_log_checksum):
    if not os.path.exists(join(args.timingsdir, "checksums")):
        try:
            os.makedirs(join(args.timingsdir, "checksums"))
        except FileExistsError:
            print(join(args.timingsdir, "checksums") + " already exists! Not creating.")

    for logname_tag in os.listdir(join(args.timingsdir, "checksums")):
        if logname_tag.startswith("parameters_parsed_" + os.path.basename(parameter_log)) and \
            logname_tag.endswith(".checksum"):
            os.remove(join(args.timingsdir, "checksums", logname_tag))

    logname_tag = "parameters_parsed_" + os.path.split(parameter_log)[1] + "_" + parameter_log_checksum + ".checksum"
    open(join(args.timingsdir, "checksums", logname_tag), "w", newline='', encoding='utf8').close()



if isfile(args.parameter_logs_searchspace_1[0]):
    parameter_log_lists = [[(args.parameter_logs_searchspace_1[0], 0)]]
else:
    p_logname = re.compile(r".*log_.+?_(?P<generator_idx>\d+).log")
    if isdir(args.parameter_logs_searchspace_1[0]):
        files = [join(args.parameter_logs_searchspace_1[0], l) for l in os.listdir(args.parameter_logs_searchspace_1[0]) if l.endswith(".log")]
        parameter_log_lists = []
        parameter_log_lists.append([])
        for filename in files:
            if not filename.endswith(".log"):
                continue
            m_logname = p_logname.match(filename)
            if m_logname is None:
                raise ValueError("Couldn't match the filename to the pattern: " + filename)
            parameter_log_lists[0].append((filename, int(m_logname.group("generator_idx"))))
    else:
        raise IOError("parameter_logs_searchspace_1 is not a dir and not a file") 

if args.parameter_logs_searchspace_2 is not None:
    parameter_log_lists.append((args.parameter_logs_searchspace_2, 0))


def parse_log(logInfo: Tuple[int, str, int]):
    global n_par_mappings_to_extract
    global out_par_filename
    global previous_par_mappings

    search_space_idx, parameter_log, generator_idx = logInfo

    parameter_log_checksum = get_file_contents_hash(parameter_log)

    if is_log_parsed(parameter_log_checksum):
        if args.verbose:
            print("Skipping " + parameter_log + " because it is already parsed")
        return False

    print(str(search_space_idx) + ". Parsing " + parameter_log)
    if not os.path.isdir(args.parametersdir):
        try:
            os.makedirs(args.parametersdir)
        except FileExistsError:
            print(args.parametersdir + " already exists! Not creating.")

    if isfile(args.parameter_logs_searchspace_1[0]):
        out_filename = join(args.parametersdir, ntpath.basename(parameter_log).replace(".log", ".csv"))
    else:
        dirpath = join(args.parametersdir, os.path.basename(os.path.normpath(args.parameter_logs_searchspace_1[0])))
        # print("dirpath")
        # print(dirpath)
        if not os.path.isdir(dirpath):
            try:
                os.makedirs(dirpath)			
            except FileExistsError:
                print(dirpath + " already exists! Not creating.")
        out_filename = join(dirpath, ntpath.basename(parameter_log).replace(".log", ".csv"))

    if n_par_mappings_to_extract > 0 and out_par_filename is None:
        if isfile(args.parameter_logs_searchspace_1[0]):
            out_par_filename = join(args.parametersdir, ntpath.basename(parameter_log).replace(".log", "_par_mappings.csv"))
        else:
            out_par_filename = join(dirpath, os.path.basename(os.path.normpath(args.parameter_logs_searchspace_1[0])) + "_par_mappings.csv")


        par_dtypes = {
                "net_name": str, 
                "net_label": str, 
                "layer_idx": 'Int32', 
                "tune_point_idx": str, 
                "parMappingHash": str,
                "mapTransformWithMapIds": str}

        assert previous_par_mappings is None
        if isfile(out_par_filename):
            try:
                previous_par_mappings = pd.read_csv(out_par_filename, dtype = par_dtypes)

                n_par_mappings_to_extract = max(0, n_par_mappings_to_extract - len(previous_par_mappings.index))

            except pd.errors.EmptyDataError:
                pass

        if n_par_mappings_to_extract > 0:
            if previous_par_mappings is None:
                previous_par_mappings = pd.DataFrame({c: pd.Series(dtype=t) for c, t in par_dtypes.items()})
            
                print("Saving into " + out_par_filename)

    print("Saving into " + out_filename)

    state = State(tag = Tag.INIT, generator_idx = generator_idx, search_space_idx = search_space_idx, out_filename = out_filename)

    if args.parmappings_with_search_times_csv is not None:
        with open(args.parmappings_with_search_times_csv, 'r', newline='', encoding='utf8') as fp:
            state.inputCsvParMaps_with_search_times = \
                pd.read_csv(args.parmappings_with_search_times_csv, dtype = {"parMappingHash": str, "search_duration_sec": float})


    with open(parameter_log, 'r', newline='', encoding='utf8') as fp:
        
        if n_par_mappings_to_extract:
            print("%d par mappings left to extract" % n_par_mappings_to_extract)

        report_n_par_mappings_to_extract = n_par_mappings_to_extract > 0

        state.parse_lines(fp)

        if report_n_par_mappings_to_extract:
            print("Now %d par mappings left to extract" % n_par_mappings_to_extract)
        
    try:
        state.csvFile.close()
    except:
        print(sys.exc_info()[1])
        print(traceback.print_tb(sys.exc_info()[2]))
        pass
    mark_log_parsed(parameter_log, parameter_log_checksum)

    return True

pool = multiprocessing.Pool(int(args.n_threads))
for (search_space_idx, parameter_logs) in enumerate(parameter_log_lists):
    # for (parameter_log, generator_idx) in parameter_logs:
    logs = [(search_space_idx, parameter_log, generator_idx) for parameter_log, generator_idx in parameter_logs]
    if n_par_mappings_to_extract > 0 or args.n_threads <= 1:
        print("Parsing logs sequentially")
        result = map(parse_log, logs)
    else:
        chunksize = int(len(logs) / args.n_threads)
        print("Parsing logs IN PARALLEL (%d threads, %d chunksize each, %d logs to parse). Make sure the subprocesses don't share the state" % (args.n_threads, chunksize, len(logs)))
        result = pool.map(func = parse_log, iterable = logs, chunksize = chunksize)

    for r in result:
        print(r)
    

print("Done.")