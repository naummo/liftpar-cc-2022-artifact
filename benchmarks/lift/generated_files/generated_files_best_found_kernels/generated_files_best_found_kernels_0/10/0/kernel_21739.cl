// Layer 10, tune point 0
// inputChannels -> 512, inputWidth -> 14, inputHeight -> 14,
// padFuncX -> 1, padFuncY -> 1,
// kernelWidth -> 3, kernelHeight -> 3,
// kernelStrideX -> 1, kernelStrideY -> 1,
// numKernels -> 512
// Vectorization
// "mapVectorize.213." -> 1,
// "mapVectorize.246." -> 1
// Unsliding
// 
// Parallelization and fusion
// "mapTransform.28." -> (10*Code.mapWrg() + Code.parDims(0)()),
// "mapTransform.29." -> (10*Code.mapWrg() + Code.parDims(1)()),
// "mapTransform.30." -> (Code.replaceInnerMapWithOuter()),
// "mapTransform.51." -> (10*Code.mapWrg() + Code.parDims(2)()),
// "mapTransform.52." -> (10*Code.mapLcl() + Code.parDims(2)()),
// "mapTransform.68." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.70." -> (10*Code.mapLcl() + Code.parDims(0)()),
// "mapTransform.109." -> (10*Code.mapLcl() + Code.parDims(1)()),
// "mapTransform.110." -> (10*Code.mapLcl() + Code.parDims(0)())
// padOptRight -> 1, padOptBottom -> 1,
// tileWidth -> 5, tileHeight -> 5, tileDepth -> 16,
// seqWindowsPerThreadX -> 5, seqWindowsPerThreadY -> 1,
// seqKernelsPerThread -> 4,
// kernelGroupSize -> 4,
// windowTileWidth -> 1, windowTileHeight -> 1,
// inputCacheSizeX -> 5, inputCacheSizeY -> 1,
// inputCacheDepth -> 4,
// kernelCacheSize -> 4,
// localSizes(0) -> 4,
// localSizes(1) -> 3,
// localSizes(2) -> 5,
// nWrgs(0) -> 128,
// nWrgs(1) -> 9,
// nWrgs(2) -> 1
// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

float id(float x){
    {
        { return x; }; 
    }
}
float4 id4(float4 x){
    {
        { return x; }; 
    }
}
float add(float x, float y){
    {
        { return x+y; }; 
    }
}
float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
kernel __attribute((reqd_work_group_size(4,3,5)))
void KERNEL(const global float* restrict v__11018, const global float* restrict v__11019, global float* v__11046){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        local float v__11040[1200];
        // Typed Value memory
        float v__10614; 
        float v__10623; 
        float v__10635; 
        // Private Memory
        float v__11022_0;
        float v__11022_1;
        float v__11022_2;
        float v__11022_3;
        float v__11022_4;
        float v__11022_5;
        float v__11022_6;
        float v__11022_7;
        float v__11022_8;
        float v__11022_9;
        float v__11022_10;
        float v__11022_11;
        float v__11022_12;
        float v__11022_13;
        float v__11022_14;
        float v__11022_15;
        float v__11022_16;
        float v__11022_17;
        float v__11022_18;
        float v__11022_19;
        float4 v__11025_0;
        float v__11026_0;
        float v__11026_1;
        float v__11026_2;
        float v__11026_3;
        float v__11026_4;
        float v__11026_5;
        float v__11026_6;
        float v__11026_7;
        float v__11026_8;
        float v__11026_9;
        float v__11026_10;
        float v__11026_11;
        float v__11026_12;
        float v__11026_13;
        float v__11026_14;
        float v__11026_15;
        float4 v__11028_0;
        float v__11029_0;
        float v__11029_1;
        float v__11029_2;
        float v__11029_3;
        float v__11029_4;
        float v__11029_5;
        float v__11029_6;
        float v__11029_7;
        float v__11029_8;
        float v__11029_9;
        float v__11029_10;
        float v__11029_11;
        float v__11029_12;
        float v__11029_13;
        float v__11029_14;
        float v__11029_15;
        float v__11029_16;
        float v__11029_17;
        float v__11029_18;
        float v__11029_19;
        float v__11030_0;
        float v__11030_1;
        float v__11030_2;
        float v__11030_3;
        float v__11030_4;
        float v__11030_5;
        float v__11030_6;
        float v__11030_7;
        float v__11030_8;
        float v__11030_9;
        float v__11030_10;
        float v__11030_11;
        float v__11030_12;
        float v__11030_13;
        float v__11030_14;
        float v__11030_15;
        float v__11030_16;
        float v__11030_17;
        float v__11030_18;
        float v__11030_19;
        float v__11034_0;
        float v__11034_1;
        float v__11034_2;
        float v__11034_3;
        float v__11034_4;
        float v__11034_5;
        float v__11034_6;
        float v__11034_7;
        float v__11034_8;
        float v__11034_9;
        float v__11034_10;
        float v__11034_11;
        float v__11034_12;
        float v__11034_13;
        float v__11034_14;
        float v__11034_15;
        float v__11034_16;
        float v__11034_17;
        float v__11034_18;
        float v__11034_19;
        float v__11041_0;
        // iteration count is exactly 1, no loop emitted
        {
            int v_wg_id_10953 = get_group_id(0); 
            // iteration count is exactly 1, no loop emitted
            {
                int v_wg_id_10954 = get_group_id(1); 
                // iteration count is exactly 1, no loop emitted
                {
                    int v_wg_id_10955 = get_group_id(2); 
                    // iteration count is exactly 1, no loop emitted
                    {
                        int v_l_id_10956 = get_local_id(2); 
                        // map_seq
                        // iteration count is exactly 1, no loop emitted
                        {
                            int v_i_10957 = 0; 
                            // iteration count is exactly 1, no loop emitted
                            {
                                int v_l_id_10958 = get_local_id(1); 
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_l_id_10959 = get_local_id(0); 
                                    float v_tmp_11047 = 0.0f; 
                                    v__10614 = v_tmp_11047; 
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11022_0 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_1 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_2 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_3 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_4 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11022_5 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_6 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_7 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_8 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_9 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11022_10 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_11 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_12 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_13 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_14 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11022_15 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_16 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_17 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_18 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11022_19 = id(v__10614); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // reduce_seq
                                    for (int v_i_10964 = 0; (v_i_10964 < 96); v_i_10964 = (1 + v_i_10964)){
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11025_0 = id4(vload4((v_l_id_10959 + (4 * (v_i_10964 / 3)) + (128 * (v_i_10964 % 3)) + (384 * v_l_id_10958) + (4608 * v_wg_id_10953) + (4608 * v_wg_id_10955)),v__11019 + 0)); 
                                        v__11026_0 = v__11025_0.s0; 
                                        v__11026_1 = v__11025_0.s1; 
                                        v__11026_2 = v__11025_0.s2; 
                                        v__11026_3 = v__11025_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11025_0 = id4(vload4((1152 + v_l_id_10959 + (4 * (v_i_10964 / 3)) + (128 * (v_i_10964 % 3)) + (384 * v_l_id_10958) + (4608 * v_wg_id_10953) + (4608 * v_wg_id_10955)),v__11019 + 0)); 
                                        v__11026_4 = v__11025_0.s0; 
                                        v__11026_5 = v__11025_0.s1; 
                                        v__11026_6 = v__11025_0.s2; 
                                        v__11026_7 = v__11025_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11025_0 = id4(vload4((2304 + v_l_id_10959 + (4 * (v_i_10964 / 3)) + (128 * (v_i_10964 % 3)) + (384 * v_l_id_10958) + (4608 * v_wg_id_10953) + (4608 * v_wg_id_10955)),v__11019 + 0)); 
                                        v__11026_8 = v__11025_0.s0; 
                                        v__11026_9 = v__11025_0.s1; 
                                        v__11026_10 = v__11025_0.s2; 
                                        v__11026_11 = v__11025_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11025_0 = id4(vload4((3456 + v_l_id_10959 + (4 * (v_i_10964 / 3)) + (128 * (v_i_10964 % 3)) + (384 * v_l_id_10958) + (4608 * v_wg_id_10953) + (4608 * v_wg_id_10955)),v__11019 + 0)); 
                                        v__11026_12 = v__11025_0.s0; 
                                        v__11026_13 = v__11025_0.s1; 
                                        v__11026_14 = v__11025_0.s2; 
                                        v__11026_15 = v__11025_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11028_0 = id4(vload4((v_l_id_10959 + (4 * (v_i_10964 / 3)) + (128 * (v_i_10964 % 3)) + (640 * (v_wg_id_10954 % 3)) + (2176 * v_l_id_10956) + (2176 * v_l_id_10958) + (10880 * (v_wg_id_10954 / 3))),v__11018 + 0)); 
                                        v__11029_0 = v__11028_0.s0; 
                                        v__11029_1 = v__11028_0.s1; 
                                        v__11029_2 = v__11028_0.s2; 
                                        v__11029_3 = v__11028_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11028_0 = id4(vload4((128 + v_l_id_10959 + (4 * (v_i_10964 / 3)) + (128 * (v_i_10964 % 3)) + (640 * (v_wg_id_10954 % 3)) + (2176 * v_l_id_10956) + (2176 * v_l_id_10958) + (10880 * (v_wg_id_10954 / 3))),v__11018 + 0)); 
                                        v__11029_4 = v__11028_0.s0; 
                                        v__11029_5 = v__11028_0.s1; 
                                        v__11029_6 = v__11028_0.s2; 
                                        v__11029_7 = v__11028_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11028_0 = id4(vload4((256 + v_l_id_10959 + (4 * (v_i_10964 / 3)) + (128 * (v_i_10964 % 3)) + (640 * (v_wg_id_10954 % 3)) + (2176 * v_l_id_10956) + (2176 * v_l_id_10958) + (10880 * (v_wg_id_10954 / 3))),v__11018 + 0)); 
                                        v__11029_8 = v__11028_0.s0; 
                                        v__11029_9 = v__11028_0.s1; 
                                        v__11029_10 = v__11028_0.s2; 
                                        v__11029_11 = v__11028_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11028_0 = id4(vload4((384 + v_l_id_10959 + (4 * (v_i_10964 / 3)) + (128 * (v_i_10964 % 3)) + (640 * (v_wg_id_10954 % 3)) + (2176 * v_l_id_10956) + (2176 * v_l_id_10958) + (10880 * (v_wg_id_10954 / 3))),v__11018 + 0)); 
                                        v__11029_12 = v__11028_0.s0; 
                                        v__11029_13 = v__11028_0.s1; 
                                        v__11029_14 = v__11028_0.s2; 
                                        v__11029_15 = v__11028_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // mapSeqVector vector loop
                                        // unroll
                                        v__11028_0 = id4(vload4((512 + v_l_id_10959 + (4 * (v_i_10964 / 3)) + (128 * (v_i_10964 % 3)) + (640 * (v_wg_id_10954 % 3)) + (2176 * v_l_id_10956) + (2176 * v_l_id_10958) + (10880 * (v_wg_id_10954 / 3))),v__11018 + 0)); 
                                        v__11029_16 = v__11028_0.s0; 
                                        v__11029_17 = v__11028_0.s1; 
                                        v__11029_18 = v__11028_0.s2; 
                                        v__11029_19 = v__11028_0.s3; 
                                        // end unroll
                                        // end mapSeqVector vector loop
                                        // iteration count is 0, no loop emitted
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11049 = 0.0f; 
                                        v__10623 = v_tmp_11049; 
                                        v__11030_0 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_0 = mult(v__11029_0, v__11026_0); 
                                        v__11030_0 = add(v__11030_0, v__11034_0); 
                                        v__11034_0 = mult(v__11029_1, v__11026_1); 
                                        v__11030_0 = add(v__11030_0, v__11034_0); 
                                        v__11034_0 = mult(v__11029_2, v__11026_2); 
                                        v__11030_0 = add(v__11030_0, v__11034_0); 
                                        v__11034_0 = mult(v__11029_3, v__11026_3); 
                                        v__11030_0 = add(v__11030_0, v__11034_0); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11050 = 0.0f; 
                                        v__10623 = v_tmp_11050; 
                                        v__11030_1 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_1 = mult(v__11029_4, v__11026_0); 
                                        v__11030_1 = add(v__11030_1, v__11034_1); 
                                        v__11034_1 = mult(v__11029_5, v__11026_1); 
                                        v__11030_1 = add(v__11030_1, v__11034_1); 
                                        v__11034_1 = mult(v__11029_6, v__11026_2); 
                                        v__11030_1 = add(v__11030_1, v__11034_1); 
                                        v__11034_1 = mult(v__11029_7, v__11026_3); 
                                        v__11030_1 = add(v__11030_1, v__11034_1); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11051 = 0.0f; 
                                        v__10623 = v_tmp_11051; 
                                        v__11030_2 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_2 = mult(v__11029_8, v__11026_0); 
                                        v__11030_2 = add(v__11030_2, v__11034_2); 
                                        v__11034_2 = mult(v__11029_9, v__11026_1); 
                                        v__11030_2 = add(v__11030_2, v__11034_2); 
                                        v__11034_2 = mult(v__11029_10, v__11026_2); 
                                        v__11030_2 = add(v__11030_2, v__11034_2); 
                                        v__11034_2 = mult(v__11029_11, v__11026_3); 
                                        v__11030_2 = add(v__11030_2, v__11034_2); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11052 = 0.0f; 
                                        v__10623 = v_tmp_11052; 
                                        v__11030_3 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_3 = mult(v__11029_12, v__11026_0); 
                                        v__11030_3 = add(v__11030_3, v__11034_3); 
                                        v__11034_3 = mult(v__11029_13, v__11026_1); 
                                        v__11030_3 = add(v__11030_3, v__11034_3); 
                                        v__11034_3 = mult(v__11029_14, v__11026_2); 
                                        v__11030_3 = add(v__11030_3, v__11034_3); 
                                        v__11034_3 = mult(v__11029_15, v__11026_3); 
                                        v__11030_3 = add(v__11030_3, v__11034_3); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11053 = 0.0f; 
                                        v__10623 = v_tmp_11053; 
                                        v__11030_4 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_4 = mult(v__11029_16, v__11026_0); 
                                        v__11030_4 = add(v__11030_4, v__11034_4); 
                                        v__11034_4 = mult(v__11029_17, v__11026_1); 
                                        v__11030_4 = add(v__11030_4, v__11034_4); 
                                        v__11034_4 = mult(v__11029_18, v__11026_2); 
                                        v__11030_4 = add(v__11030_4, v__11034_4); 
                                        v__11034_4 = mult(v__11029_19, v__11026_3); 
                                        v__11030_4 = add(v__11030_4, v__11034_4); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11054 = 0.0f; 
                                        v__10623 = v_tmp_11054; 
                                        v__11030_5 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_5 = mult(v__11029_0, v__11026_4); 
                                        v__11030_5 = add(v__11030_5, v__11034_5); 
                                        v__11034_5 = mult(v__11029_1, v__11026_5); 
                                        v__11030_5 = add(v__11030_5, v__11034_5); 
                                        v__11034_5 = mult(v__11029_2, v__11026_6); 
                                        v__11030_5 = add(v__11030_5, v__11034_5); 
                                        v__11034_5 = mult(v__11029_3, v__11026_7); 
                                        v__11030_5 = add(v__11030_5, v__11034_5); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11055 = 0.0f; 
                                        v__10623 = v_tmp_11055; 
                                        v__11030_6 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_6 = mult(v__11029_4, v__11026_4); 
                                        v__11030_6 = add(v__11030_6, v__11034_6); 
                                        v__11034_6 = mult(v__11029_5, v__11026_5); 
                                        v__11030_6 = add(v__11030_6, v__11034_6); 
                                        v__11034_6 = mult(v__11029_6, v__11026_6); 
                                        v__11030_6 = add(v__11030_6, v__11034_6); 
                                        v__11034_6 = mult(v__11029_7, v__11026_7); 
                                        v__11030_6 = add(v__11030_6, v__11034_6); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11056 = 0.0f; 
                                        v__10623 = v_tmp_11056; 
                                        v__11030_7 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_7 = mult(v__11029_8, v__11026_4); 
                                        v__11030_7 = add(v__11030_7, v__11034_7); 
                                        v__11034_7 = mult(v__11029_9, v__11026_5); 
                                        v__11030_7 = add(v__11030_7, v__11034_7); 
                                        v__11034_7 = mult(v__11029_10, v__11026_6); 
                                        v__11030_7 = add(v__11030_7, v__11034_7); 
                                        v__11034_7 = mult(v__11029_11, v__11026_7); 
                                        v__11030_7 = add(v__11030_7, v__11034_7); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11057 = 0.0f; 
                                        v__10623 = v_tmp_11057; 
                                        v__11030_8 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_8 = mult(v__11029_12, v__11026_4); 
                                        v__11030_8 = add(v__11030_8, v__11034_8); 
                                        v__11034_8 = mult(v__11029_13, v__11026_5); 
                                        v__11030_8 = add(v__11030_8, v__11034_8); 
                                        v__11034_8 = mult(v__11029_14, v__11026_6); 
                                        v__11030_8 = add(v__11030_8, v__11034_8); 
                                        v__11034_8 = mult(v__11029_15, v__11026_7); 
                                        v__11030_8 = add(v__11030_8, v__11034_8); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11058 = 0.0f; 
                                        v__10623 = v_tmp_11058; 
                                        v__11030_9 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_9 = mult(v__11029_16, v__11026_4); 
                                        v__11030_9 = add(v__11030_9, v__11034_9); 
                                        v__11034_9 = mult(v__11029_17, v__11026_5); 
                                        v__11030_9 = add(v__11030_9, v__11034_9); 
                                        v__11034_9 = mult(v__11029_18, v__11026_6); 
                                        v__11030_9 = add(v__11030_9, v__11034_9); 
                                        v__11034_9 = mult(v__11029_19, v__11026_7); 
                                        v__11030_9 = add(v__11030_9, v__11034_9); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11059 = 0.0f; 
                                        v__10623 = v_tmp_11059; 
                                        v__11030_10 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_10 = mult(v__11029_0, v__11026_8); 
                                        v__11030_10 = add(v__11030_10, v__11034_10); 
                                        v__11034_10 = mult(v__11029_1, v__11026_9); 
                                        v__11030_10 = add(v__11030_10, v__11034_10); 
                                        v__11034_10 = mult(v__11029_2, v__11026_10); 
                                        v__11030_10 = add(v__11030_10, v__11034_10); 
                                        v__11034_10 = mult(v__11029_3, v__11026_11); 
                                        v__11030_10 = add(v__11030_10, v__11034_10); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11060 = 0.0f; 
                                        v__10623 = v_tmp_11060; 
                                        v__11030_11 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_11 = mult(v__11029_4, v__11026_8); 
                                        v__11030_11 = add(v__11030_11, v__11034_11); 
                                        v__11034_11 = mult(v__11029_5, v__11026_9); 
                                        v__11030_11 = add(v__11030_11, v__11034_11); 
                                        v__11034_11 = mult(v__11029_6, v__11026_10); 
                                        v__11030_11 = add(v__11030_11, v__11034_11); 
                                        v__11034_11 = mult(v__11029_7, v__11026_11); 
                                        v__11030_11 = add(v__11030_11, v__11034_11); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11061 = 0.0f; 
                                        v__10623 = v_tmp_11061; 
                                        v__11030_12 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_12 = mult(v__11029_8, v__11026_8); 
                                        v__11030_12 = add(v__11030_12, v__11034_12); 
                                        v__11034_12 = mult(v__11029_9, v__11026_9); 
                                        v__11030_12 = add(v__11030_12, v__11034_12); 
                                        v__11034_12 = mult(v__11029_10, v__11026_10); 
                                        v__11030_12 = add(v__11030_12, v__11034_12); 
                                        v__11034_12 = mult(v__11029_11, v__11026_11); 
                                        v__11030_12 = add(v__11030_12, v__11034_12); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11062 = 0.0f; 
                                        v__10623 = v_tmp_11062; 
                                        v__11030_13 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_13 = mult(v__11029_12, v__11026_8); 
                                        v__11030_13 = add(v__11030_13, v__11034_13); 
                                        v__11034_13 = mult(v__11029_13, v__11026_9); 
                                        v__11030_13 = add(v__11030_13, v__11034_13); 
                                        v__11034_13 = mult(v__11029_14, v__11026_10); 
                                        v__11030_13 = add(v__11030_13, v__11034_13); 
                                        v__11034_13 = mult(v__11029_15, v__11026_11); 
                                        v__11030_13 = add(v__11030_13, v__11034_13); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11063 = 0.0f; 
                                        v__10623 = v_tmp_11063; 
                                        v__11030_14 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_14 = mult(v__11029_16, v__11026_8); 
                                        v__11030_14 = add(v__11030_14, v__11034_14); 
                                        v__11034_14 = mult(v__11029_17, v__11026_9); 
                                        v__11030_14 = add(v__11030_14, v__11034_14); 
                                        v__11034_14 = mult(v__11029_18, v__11026_10); 
                                        v__11030_14 = add(v__11030_14, v__11034_14); 
                                        v__11034_14 = mult(v__11029_19, v__11026_11); 
                                        v__11030_14 = add(v__11030_14, v__11034_14); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        float v_tmp_11064 = 0.0f; 
                                        v__10623 = v_tmp_11064; 
                                        v__11030_15 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_15 = mult(v__11029_0, v__11026_12); 
                                        v__11030_15 = add(v__11030_15, v__11034_15); 
                                        v__11034_15 = mult(v__11029_1, v__11026_13); 
                                        v__11030_15 = add(v__11030_15, v__11034_15); 
                                        v__11034_15 = mult(v__11029_2, v__11026_14); 
                                        v__11030_15 = add(v__11030_15, v__11034_15); 
                                        v__11034_15 = mult(v__11029_3, v__11026_15); 
                                        v__11030_15 = add(v__11030_15, v__11034_15); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11065 = 0.0f; 
                                        v__10623 = v_tmp_11065; 
                                        v__11030_16 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_16 = mult(v__11029_4, v__11026_12); 
                                        v__11030_16 = add(v__11030_16, v__11034_16); 
                                        v__11034_16 = mult(v__11029_5, v__11026_13); 
                                        v__11030_16 = add(v__11030_16, v__11034_16); 
                                        v__11034_16 = mult(v__11029_6, v__11026_14); 
                                        v__11030_16 = add(v__11030_16, v__11034_16); 
                                        v__11034_16 = mult(v__11029_7, v__11026_15); 
                                        v__11030_16 = add(v__11030_16, v__11034_16); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11066 = 0.0f; 
                                        v__10623 = v_tmp_11066; 
                                        v__11030_17 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_17 = mult(v__11029_8, v__11026_12); 
                                        v__11030_17 = add(v__11030_17, v__11034_17); 
                                        v__11034_17 = mult(v__11029_9, v__11026_13); 
                                        v__11030_17 = add(v__11030_17, v__11034_17); 
                                        v__11034_17 = mult(v__11029_10, v__11026_14); 
                                        v__11030_17 = add(v__11030_17, v__11034_17); 
                                        v__11034_17 = mult(v__11029_11, v__11026_15); 
                                        v__11030_17 = add(v__11030_17, v__11034_17); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11067 = 0.0f; 
                                        v__10623 = v_tmp_11067; 
                                        v__11030_18 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_18 = mult(v__11029_12, v__11026_12); 
                                        v__11030_18 = add(v__11030_18, v__11034_18); 
                                        v__11034_18 = mult(v__11029_13, v__11026_13); 
                                        v__11030_18 = add(v__11030_18, v__11034_18); 
                                        v__11034_18 = mult(v__11029_14, v__11026_14); 
                                        v__11030_18 = add(v__11030_18, v__11034_18); 
                                        v__11034_18 = mult(v__11029_15, v__11026_15); 
                                        v__11030_18 = add(v__11030_18, v__11034_18); 
                                        // end unroll
                                        // end reduce_seq
                                        float v_tmp_11068 = 0.0f; 
                                        v__10623 = v_tmp_11068; 
                                        v__11030_19 = id(v__10623); 
                                        // reduce_seq
                                        // unroll
                                        v__11034_19 = mult(v__11029_16, v__11026_12); 
                                        v__11030_19 = add(v__11030_19, v__11034_19); 
                                        v__11034_19 = mult(v__11029_17, v__11026_13); 
                                        v__11030_19 = add(v__11030_19, v__11034_19); 
                                        v__11034_19 = mult(v__11029_18, v__11026_14); 
                                        v__11030_19 = add(v__11030_19, v__11034_19); 
                                        v__11034_19 = mult(v__11029_19, v__11026_15); 
                                        v__11030_19 = add(v__11030_19, v__11034_19); 
                                        // end unroll
                                        // end reduce_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11022_0 = add(v__11022_0, v__11030_0); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_1 = add(v__11022_1, v__11030_1); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_2 = add(v__11022_2, v__11030_2); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_3 = add(v__11022_3, v__11030_3); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_4 = add(v__11022_4, v__11030_4); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11022_5 = add(v__11022_5, v__11030_5); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_6 = add(v__11022_6, v__11030_6); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_7 = add(v__11022_7, v__11030_7); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_8 = add(v__11022_8, v__11030_8); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_9 = add(v__11022_9, v__11030_9); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11022_10 = add(v__11022_10, v__11030_10); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_11 = add(v__11022_11, v__11030_11); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_12 = add(v__11022_12, v__11030_12); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_13 = add(v__11022_13, v__11030_13); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_14 = add(v__11022_14, v__11030_14); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11022_15 = add(v__11022_15, v__11030_15); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_16 = add(v__11022_16, v__11030_16); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_17 = add(v__11022_17, v__11030_17); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_18 = add(v__11022_18, v__11030_18); 
                                        // end unroll
                                        // end map_seq
                                        // map_seq
                                        // unroll
                                        v__11022_19 = add(v__11022_19, v__11030_19); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                    // end reduce_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11040[(20 * (v_l_id_10959 + (4 * v_l_id_10958) + (12 * v_l_id_10956)))] = id(v__11022_0); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(1 + (20 * v_l_id_10959) + (80 * v_l_id_10958) + (240 * v_l_id_10956))] = id(v__11022_1); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(2 * (1 + (10 * v_l_id_10959) + (40 * v_l_id_10958) + (120 * v_l_id_10956)))] = id(v__11022_2); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(3 + (20 * v_l_id_10959) + (80 * v_l_id_10958) + (240 * v_l_id_10956))] = id(v__11022_3); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(4 * (1 + (5 * v_l_id_10959) + (20 * v_l_id_10958) + (60 * v_l_id_10956)))] = id(v__11022_4); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11040[(5 * (1 + (4 * v_l_id_10959) + (16 * v_l_id_10958) + (48 * v_l_id_10956)))] = id(v__11022_5); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(2 * (3 + (10 * v_l_id_10959) + (40 * v_l_id_10958) + (120 * v_l_id_10956)))] = id(v__11022_6); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(7 + (20 * v_l_id_10959) + (80 * v_l_id_10958) + (240 * v_l_id_10956))] = id(v__11022_7); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(4 * (2 + (5 * v_l_id_10959) + (20 * v_l_id_10958) + (60 * v_l_id_10956)))] = id(v__11022_8); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(9 + (20 * v_l_id_10959) + (80 * v_l_id_10958) + (240 * v_l_id_10956))] = id(v__11022_9); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11040[(10 * (1 + (2 * v_l_id_10959) + (8 * v_l_id_10958) + (24 * v_l_id_10956)))] = id(v__11022_10); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(11 + (20 * v_l_id_10959) + (80 * v_l_id_10958) + (240 * v_l_id_10956))] = id(v__11022_11); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(4 * (3 + (5 * v_l_id_10959) + (20 * v_l_id_10958) + (60 * v_l_id_10956)))] = id(v__11022_12); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(13 + (20 * v_l_id_10959) + (80 * v_l_id_10958) + (240 * v_l_id_10956))] = id(v__11022_13); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(2 * (7 + (10 * v_l_id_10959) + (40 * v_l_id_10958) + (120 * v_l_id_10956)))] = id(v__11022_14); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    // map_seq
                                    // unroll
                                    v__11040[(5 * (3 + (4 * v_l_id_10959) + (16 * v_l_id_10958) + (48 * v_l_id_10956)))] = id(v__11022_15); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(4 * (4 + (5 * v_l_id_10959) + (20 * v_l_id_10958) + (60 * v_l_id_10956)))] = id(v__11022_16); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(17 + (20 * v_l_id_10959) + (80 * v_l_id_10958) + (240 * v_l_id_10956))] = id(v__11022_17); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(2 * (9 + (10 * v_l_id_10959) + (40 * v_l_id_10958) + (120 * v_l_id_10956)))] = id(v__11022_18); 
                                    // end unroll
                                    // end map_seq
                                    // map_seq
                                    // unroll
                                    v__11040[(19 + (20 * v_l_id_10959) + (80 * v_l_id_10958) + (240 * v_l_id_10956))] = id(v__11022_19); 
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                    // end unroll
                                    // end map_seq
                                }
                            }
                            barrier(CLK_LOCAL_MEM_FENCE);
                            for (int v_l_id_11010 = get_local_id(1); (v_l_id_11010 < 4); v_l_id_11010 = (3 + v_l_id_11010)){
                                // map_seq
                                // iteration count is exactly 1, no loop emitted
                                {
                                    int v_i_11011 = 0; 
                                    for (int v_l_id_11012 = get_local_id(0); (v_l_id_11012 < 5); v_l_id_11012 = (4 + v_l_id_11012)){
                                        float v_tmp_11069 = 0.0f; 
                                        v__10635 = v_tmp_11069; 
                                        // map_seq
                                        // unroll
                                        v__11041_0 = id(v__10635); 
                                        // end unroll
                                        // end map_seq
                                        // reduce_seq
                                        for (int v_i_11014 = 0; (v_i_11014 < 12); v_i_11014 = (1 + v_i_11014)){
                                            // map_seq
                                            // unroll
                                            v__11041_0 = add(v__11041_0, v__11040[(v_l_id_11012 + (5 * v_l_id_11010) + (20 * v_i_11014) + (240 * v_l_id_10956))]); 
                                            // end unroll
                                            // end map_seq
                                        }
                                        // end reduce_seq
                                        // map_seq
                                        // unroll
                                        // map_seq
                                        // unroll
                                        v__11046[(v_l_id_11010 + (4 * v_wg_id_10953) + (4 * v_wg_id_10955) + (512 * v_l_id_11012) + (2560 * (v_wg_id_10954 % 3)) + (7680 * v_l_id_10956) + (38400 * (v_wg_id_10954 / 3)))] = id(v__11041_0); 
                                        // end unroll
                                        // end map_seq
                                        // end unroll
                                        // end map_seq
                                    }
                                }
                                // end map_seq
                            }
                        }
                        // end map_seq
                    }
                    barrier(CLK_LOCAL_MEM_FENCE);
                }
            }
        }
    }
}