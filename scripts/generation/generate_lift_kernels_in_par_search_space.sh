#!/bin/bash
set -e
set -x

if [ $# -eq 6 ]
then 
    EXPERIMENT_LABEL=$1
    FROM_LAYER=$2
    TO_LAYER=$3
    N_POINTS_TO_EXPLORE_PER_LAYER=$4
    BUILD_DIR=$5
    SCALA_LOG_PATH=$6

    LOGBACK_TARGET=$BUILD_DIR/logback.xml

    mkdir -p $SCALA_LOG_PATH

    cp -v $LIFT/src/main/benchmarks/conv/resources/logback.xml ${LOGBACK_TARGET}

    sed -i "s~SCALA_LOG_PATH~${SCALA_LOG_PATH}~g" ${LOGBACK_TARGET}

    mkdir -p $BUILD_DIR
    cp -v $LIFT/src/main/benchmarks/conv/settings/envSettings.json $BUILD_DIR/envSettingsConcrete.json
    sed -i "s~BUILD_DIR~${BUILD_DIR}~g" $BUILD_DIR/envSettingsConcrete.json
    sed -i "s~LIFT_DIR~${LIFT}~g" $BUILD_DIR/envSettingsConcrete.json

    ulimit -v
    lscpu

    ConvExploration -Dlogback.configurationFile=${LOGBACK_TARGET} -Drun_label=${EXPERIMENT_LABEL} -Dexperiment_label=${EXPERIMENT_LABEL}_0 "--path_to_settings $BUILD_DIR/envSettingsConcrete.json --net_name vgg --compile_for_board --overwrite_compiled_files --enforce_solution_uniqueness_through_cstr --old_advanced_test_harness --compilation_timeout 10 --total_tune_points ${N_POINTS_TO_EXPLORE_PER_LAYER} --tune_points_batch_size 200 --from_layer $FROM_LAYER --to_layer $TO_LAYER --lambda_dir_subroot generated_files_${EXPERIMENT_LABEL} --choose_local_size_deterministically --job_id 0 --first_search_space_to_explore 1"

else
	echo "TODO Wrong arguments supplied"
		echo "Usage: scripts/generate_lift_kernels_in_par_search_space.sh [experiment_label] [from_layer] [to_layer] [n_points_to_generate_per_layer] [build_dir] [exploration_log_path]"
		echo ""
		echo "What is the experiment label?"
		echo "What is the first layer to generate?"
		echo "What is the last layer to generate?"
		echo "How many points to generate per layer?"
		echo "What is the build directory used for generated files?"
		echo "What is the log directory used for the exploration log?"
	exit -1
fi