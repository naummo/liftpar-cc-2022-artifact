import argparse
import pandas as pd
import numpy as np
import re
from os.path import join, isfile, isdir
import os
from timeit import default_timer

def timer_start():
	# pass
	return default_timer()

def timer_end(start, msg):
	pass
	end = default_timer()
	print("%s: %.7f sec" % (msg, end - start))

parser = argparse.ArgumentParser()
parser.add_argument('--netname', action="store", dest="netname", required=True)
parser.add_argument('--netlabel', action="store", dest="netlabel", required=True)
parser.add_argument('--parameter_netlabel', action="store", dest="parameter_netlabel")
parser.add_argument('--parameter_csv', action="store", dest="parameter_csv", required=True)
parser_timings = parser.add_mutually_exclusive_group(required=True)
parser_timings.add_argument('--timings_and_mem_csv', action="store", dest="timings_and_mem_csv")
parser_timings.add_argument('--timings_csv', action="store", dest="timings_csv")

parser.add_argument('--aggregated_out_csv', action="store", dest="aggregated_out_csv", required=True)

args = parser.parse_args()


##################### Helpers #####################
true_id_to_continuous_collated = lambda true_id : \
    0 if true_id == 0 else \
    1 if true_id == 2 else \
    2 if true_id == 5 else \
    3 if true_id == 7 else \
    4 if true_id == 10 else \
    5 if true_id == 12 else \
    7 if true_id == 17 else \
    8 if true_id == 19 else \
    10 # if true_id == 24

continuous_uncollated_to_collated_id = lambda uncollated_id : \
    uncollated_id if uncollated_id <= 5 else \
    5 if uncollated_id == 6 else \
    7 if uncollated_id == 7 else \
    8 if uncollated_id == 8 else \
    8 if uncollated_id == 9 else \
    10 if uncollated_id == 10 else \
    10 if uncollated_id == 11 else \
    10 # if uncollated_id == 12 else


##################### Load data #####################
if args.timings_and_mem_csv is not None:
    measurements = pd.read_csv(args.timings_and_mem_csv)
else:
    measurements = pd.read_csv(args.timings_csv)
measurements.columns = measurements.columns.str.strip()
if args.parameter_netlabel:
    measurements["net_label"] = args.parameter_netlabel
p_csvpath = re.compile(r".*csv/generated_files_[^\.]+?_(?P<generator_idx>\d+)_\d\d\.\d\d\.\d\d\d\d\_\d\d\.\d\d\.\d\d\_logof")
# pd.set_option('display.max_colwidth', 2000)
# measurements["tunepoint"] = measurements.apply(lambda row : p_csvpath.match(row["csvpath"]).group("generator_idx") + "_" + str(row["tunepoint"]),
#                                 axis = "columns")
measurements["tunepoint"] = measurements["job_idx"].astype(str) + "_" + measurements["tunepoint"].astype(str)

parameter_dtypes = {"mapUnslide": "str", "mapTransform": "str", "mapVectorize": "str"}
if isfile(args.parameter_csv):
    parameters = pd.read_csv(args.parameter_csv, dtype=parameter_dtypes)
else:
    assert isdir(args.parameter_csv)
    files = [join(args.parameter_csv, l) for l in os.listdir(args.parameter_csv) if l.endswith(".csv")]
    parameters = None
    for filename in files:
        new_parameters = pd.read_csv(filename, dtype=parameter_dtypes)
        if parameters is None:
            parameters = new_parameters
        else:
            parameters = pd.concat([parameters, new_parameters], ignore_index=True)
    

parameters.columns = parameters.columns.str.strip()
# parameters["reduceSeqUnroll"] = parameters["reduceSeqUnroll"].apply(lambda b: 1 if b == True else 0)
# parameters["vectorise4"] = parameters["vectorise4"].apply(lambda b: 1 if b == True else 0)

# print("measurements")
# print(measurements)
# print("parameters")
# print(parameters[parameters["tune_point_idx"].str.startswith("8_")])
lift_data = pd.merge(measurements, parameters, how="inner", \
    left_on=["layer", "tunepoint", "net_label", "net_name"], 
    right_on=["layer_idx", "tune_point_idx", "net_label", "net_name"])

lift_data.drop(columns=["layer_idx", "tune_point_idx"], inplace=True)

lift_data["layer"] = lift_data["layer"].astype('Int32')
# lift_data["tunepoint"] = lift_data["tunepoint"].astype('Int32')
lift_data["success_status"] = lift_data["success_status"].astype('Int8')
lift_data["board"] = lift_data["board"].astype('Int32')
lift_data["set_freq"] = lift_data["set_freq"].astype('Int64')
lift_data["report_freq"] = lift_data["report_freq"].astype('Int64')


##################### Collate layers #####################
# lift_data["uncollated_layer_id"] = lift_data["layer"]
print(lift_data)    
# lift_data["collated_layer_id"] = lift_data["layer"].apply(continuous_uncollated_to_collated_id)
# lift_data.drop(labels=["layer"], axis="columns", inplace=True)


##################### Add data #####################
if args.timings_and_mem_csv is not None:
    lift_data["liftMemConsumKB"] = (lift_data["max_mem_consum_during_comp"] - lift_data["mem_consum_before_comp_started"]).astype('Int64')
    lift_data["liftMemConsumMB"] = lift_data["liftMemConsumKB"].map(lambda mem : mem / 1024)
else:
    lift_data["liftMemConsumKB"] = np.NaN
lift_data["lTotal"] = lift_data["l0"] * lift_data["l1"] * lift_data["l2"]
lift_data["gTotal"] = lift_data["w0"] * lift_data["w1"] * lift_data["w2"]


##################### Save to CSV #####################
lift_data.to_csv(path_or_buf=args.aggregated_out_csv, index=False)
print("Saved aggregated timings and measurements to " + args.aggregated_out_csv)

collatedCsvFilenameAggregated = args.aggregated_out_csv.replace(".csv", "_medians.csv")
print("Collating the median-aggregated CSV file with all the timings (without trials): " + collatedCsvFilenameAggregated)

agg_funs = {}
def return_first(arg):
    # print(arg[0])
    return arg.iloc[0]
for c in ['total_runtime', #'rt0', 'rt1', 'rt2','rt3', 
        #'success_status', 'fail_reported', 'reached_runtime_limit',
       'temp_before', 'temp_after',
    #    'set_freq', 'report_freq', 
    #    'net_name', 'net_label', 'csvpath',
       ]:
    agg_funs[c] = "median"
for c in ["success_status", 'fail_reported', 'reached_runtime_limit',
         'set_freq', 'report_freq']:
    agg_funs[c] = "min"
agg_funs['exec_duration_sec'] = "sum"
# for c in ['net_name', 'net_label', 'csvpath', 'board',
#         'job_idx', 'files_dir', 'finalReduceResultMem', 'clock_start', 'clock_stop',
#        'inTileCacheFetchReorder', 'inTileCacheLoadVectorLen', 'inTileCacheMem',
#        'inTileCacheReorder', 'inTileFetchReorder', 'inTileLoadVectorLen',
#        'inTileMem', 'inTileReorder', 'inputCacheSizeX', 'inputCacheSizeY',
#        'inputChannels', 'inputHeight', 'inputWidth', 'kernelCacheFetchReorder',
#        'kernelCacheLoadVectorLen', 'kernelCacheMem', 'kernelCacheReorder',
#        'kernelCacheSize', 'kernelGroupFetchReorder',
#        'kernelGroupLoadVectorLen', 'kernelGroupMem', 'kernelGroupReorder',
#        'kernelGroupSize', 'kernelHeight', 'kernelStrideX', 'kernelStrideY',
#        'kernelWidth', 'l0', 'l1', 'l2', 'mapTransform', 'mapUnslide',
#        'mapVectorize', 'numKernels', 'padFuncX', 'padFuncY',
#        'partReduceResultMem', 'partReduceVectorLen', 'tileDepth', 'tileHeight',
#        'tileWidth', 'w0', 'w1', 'w2', #'collated_layer_id', 
#        'liftMemConsumKB',
#        'lTotal', 'gTotal']:
#     agg_funs[c] = return_first
for c in lift_data.columns:
    if c not in agg_funs.keys():
        agg_funs[c] = return_first
agg_funs.pop("layer", None)
agg_funs.pop("tunepoint", None)
# print(timings.groupby(["layer", "tunepoint"]))
start = timer_start()
# pd.set_option('display.max_rows', None)
# print(lift_data.dtypes)
lift_data_aggregated = lift_data.groupby(["layer", "tunepoint"]).agg(agg_funs)
timer_end(start, "aggregating")
lift_data_aggregated.reset_index(["layer", "tunepoint"], inplace=True)
# print(lift_data_aggregated)
# print(lift_data_aggregated.columns)
lift_data_aggregated.rename(columns={"total_runtime" : "total_runtime_median"}, inplace = True)
lift_data_aggregated.rename(columns={"exec_duration_sec" : "total_exec_duration_sec"}, inplace = True)
print("Reduced trials to " + str(len(lift_data_aggregated.index)) + " rows")
lift_data_aggregated.to_csv(path_or_buf=collatedCsvFilenameAggregated, index=False)

